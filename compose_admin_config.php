<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:8000/admin/');
define('HTTP_CATALOG', 'http://localhost:8000/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost:8000/admin/');
define('HTTPS_CATALOG', 'http://localhost:8000/');

// DIR
define('DIR_HOME_DIR', '/var/www/');
define('DIR_APPLICATION', DIR_HOME_DIR . 'html/admin/');
define('DIR_SYSTEM', DIR_HOME_DIR . 'html/system/');
define('DIR_IMAGE', DIR_HOME_DIR . 'html/image/');
define('DIR_STORAGE', DIR_HOME_DIR . 'storage/');
define('DIR_CATALOG', DIR_HOME_DIR . 'public_html/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'mysql-mariadb10-service');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'Killbill_Docker_Killbill');
define('DB_DATABASE', 'ventstroysnab');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
define('OPENCARTFORUM_SERVER', 'https://opencartforum.com/');
