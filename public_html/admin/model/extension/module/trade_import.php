<?php
class ModelExtensionModuleTradeImport extends Model {
    private function set_query(&$query, $data, $query_limit) {
        $key = $query['data_key'];
        $query['data'][$key] .= $data;
        if (strlen($query['data'][$key]) > $query_limit) {
            $query['data_key']++;
        }
    }

    public function set_tables() {
        $this->db->query('ALTER TABLE ' . DB_PREFIX . 'product ENGINE = InnoDB');
        $this->db->query('ALTER TABLE ' . DB_PREFIX . 'category ENGINE = InnoDB');
        $this->db->query('ALTER TABLE ' . DB_PREFIX . 'option ENGINE = InnoDB');
        $this->db->query('ALTER TABLE ' . DB_PREFIX . 'option_value ENGINE = InnoDB');
        $this->db->query('CREATE TABLE IF NOT EXISTS ' . DB_PREFIX . 'trade_import (operation_id INT(11) NOT NULL AUTO_INCREMENT, timestamp TIMESTAMP, json_timestamp TIMESTAMP, success BOOLEAN, PRIMARY KEY (operation_id))');
        $this->db->query('CREATE TABLE IF NOT EXISTS ' . DB_PREFIX . 'trade_orders (operation_id INT(11) NOT NULL AUTO_INCREMENT, order_id INT(11), order_data TEXT, response TEXT, PRIMARY KEY (operation_id))');
        $this->db->query('CREATE TABLE IF NOT EXISTS ' . DB_PREFIX . 'trade_import_product_codes (`product_id` INT(11) NOT NULL, `nomenclature_uuid` VARCHAR(100) NOT NULL, `group_uuid` VARCHAR(100) NOT NULL, PRIMARY KEY (nomenclature_uuid), FOREIGN KEY (product_id) REFERENCES ' . DB_PREFIX . 'product (product_id) ON DELETE CASCADE)');
        $this->db->query('CREATE TABLE IF NOT EXISTS ' . DB_PREFIX . 'trade_import_category_codes (`category_id` INT(11) NOT NULL, `group_uuid` VARCHAR(100) NOT NULL, PRIMARY KEY (group_uuid), FOREIGN KEY (category_id) REFERENCES ' . DB_PREFIX . 'category (category_id) ON DELETE CASCADE)');
        $this->db->query('CREATE TABLE IF NOT EXISTS ' . DB_PREFIX . 'trade_import_option_codes (`option_id` INT(11) NOT NULL, `nomenclature_uuid` VARCHAR(100) NOT NULL, PRIMARY KEY (nomenclature_uuid), FOREIGN KEY (option_id) REFERENCES ' . DB_PREFIX . 'option (option_id) ON DELETE CASCADE)');
        $this->db->query('CREATE TABLE IF NOT EXISTS ' . DB_PREFIX . 'trade_import_option_value_codes (`option_value_id` INT(11) NOT NULL, `characteristic_uuid` VARCHAR(100) NOT NULL, `nomenclature_uuid` VARCHAR(100) NOT NULL, PRIMARY KEY (characteristic_uuid), FOREIGN KEY (option_value_id) REFERENCES ' . DB_PREFIX . 'option_value (option_value_id) ON DELETE CASCADE)');
        $this->db->query('CREATE TABLE IF NOT EXISTS ' . DB_PREFIX . 'trade_import_complect (`complect_id` INT(11) NOT NULL AUTO_INCREMENT, `complect_uuid` VARCHAR(100) NOT NULL, `name` VARCHAR(255), `min_air` INT(10), `max_air` INT(10), `min_pressure` INT(10), `max_pressure` INT(10), PRIMARY KEY (`complect_id`))');
        $this->db->query('CREATE TABLE IF NOT EXISTS ' . DB_PREFIX . 'trade_import_complect_products (`complect_id` INT(11), `product_id` INT(11), `quantity` INT(11), `ignore_quantity` BOOLEAN, FOREIGN KEY (`complect_id`) REFERENCES ' . DB_PREFIX . 'trade_import_complect (`complect_id`) ON DELETE CASCADE)');
    }

    public function clean_tables() {
        $this->db->query('DROP TABLE ' . DB_PREFIX . 'trade_import');
        $this->db->query('DROP TABLE ' . DB_PREFIX . 'trade_orders');
        $this->db->query('DROP TABLE ' . DB_PREFIX . 'trade_import_product_codes');
        $this->db->query('DROP TABLE ' . DB_PREFIX . 'trade_import_category_codes');
        $this->db->query('DROP TABLE ' . DB_PREFIX . 'trade_import_option_codes');
        $this->db->query('DROP TABLE ' . DB_PREFIX . 'trade_import_option_value_codes');
        $this->db->query('DROP TABLE ' . DB_PREFIX . 'trade_import_complect_products');
        $this->db->query('DROP TABLE ' . DB_PREFIX . 'trade_import_complect');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'category');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'category_description');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'category_path');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'category_filter');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'category_to_store');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'category_to_layout');
        $this->db->query('ALTER TABLE ' . DB_PREFIX . 'category AUTO_INCREMENT = 50');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_attribute');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_description');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_discount');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_filter');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_image');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_option');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_option_value');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_recurring');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_related');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_related_article');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_related_mn');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_related_wb');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_reward');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_special');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_to_category');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_to_download');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_to_layout');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_to_store');
        $this->db->query('ALTER TABLE ' . DB_PREFIX . 'product AUTO_INCREMENT = 50');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'option');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'option_description');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'option_value');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'option_value_description');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'filter');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'filter_description');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'filter_group');
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'filter_group_description');
        $this->db->query('DELETE FROM ' . DB_PREFIX . "seo_url WHERE query LIKE '%product_id=%' OR query LIKE '%category_id=%'");
        $this->db->query('SET @var:=0');
        $this->db->query('UPDATE ' . DB_PREFIX . "seo_url SET seo_url_id=(@var:=@var+1)");
        $this->db->query('ALTER TABLE ' . DB_PREFIX . 'seo_url AUTO_INCREMENT = 1');
        $this->set_tables();
    }

    public function clean_orders() {
        $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'trade_orders');
    }

    public function get_store_name() {
        $result = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE `key` = 'config_name'");
        return $result->row['value'];
    }

    public function add_multiple_categories($arr, $debug = false) {
        $start_id = $this->db->query("SELECT MAX(category_id) FROM " . DB_PREFIX . "category");
        $start_id = $start_id->row['MAX(category_id)'] == NULL ? 50 : (int)($start_id->row['MAX(category_id)'] + 1);
        $arr_codes = array();
        $category_codes = $this->get_category_codes();
        $category_query = "INSERT INTO " . DB_PREFIX . "category(`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`, `noindex`) VALUES ";
        $delete_category_description_query = "DELETE FROM " . DB_PREFIX . "category_description WHERE category_id IN (";
        $category_description_query = "INSERT INTO " . DB_PREFIX . "category_description VALUES ";
        $delete_category_to_store_query = "DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id IN (";
        $category_to_store_query = "INSERT INTO " . DB_PREFIX . "category_to_store VALUES ";
        $delete_category_path_query = "DELETE FROM " . DB_PREFIX . "category_path WHERE category_id IN (";
        $category_path_query = "INSERT INTO " . DB_PREFIX . "category_path VALUES ";
        $delete_seo_url_query = "DELETE FROM " . DB_PREFIX . "seo_url WHERE query IN ('category_id=";
        $seo_url_query = "INSERT INTO " . DB_PREFIX . "seo_url(`store_id`, `language_id`, `query`, `keyword`) VALUES ";
        $delete_category_code_query = "DELETE FROM " . DB_PREFIX . "trade_import_category_codes WHERE category_id IN (";
        $category_code_query = "INSERT INTO " . DB_PREFIX . "trade_import_category_codes VALUES ";

        foreach ($arr as $data) {
            if (!isset($category_codes[$data['code']])) {
                if ($debug) {
                    echo 'Adding category ' . $data['category_description'][1]['name'] . "\n";
                }
                $category_codes[$data['code']] = $start_id;
                $start_id++;
            } else {
                if ($debug) {
                    echo $data['category_description'][1]['name'] . " category exists. Editing\n";
                }
            }
            $level = 0;
            $codes = explode("/", $data['path']);
            foreach ($codes as $key => $code) {
                if (!in_array($code, $data['parent_category_code'])) {
                    $category_path_query .= "('" . $category_codes[$data['code']] . "', '" . $category_codes[$code] . "', '" . $level . "'),";
                    $parent_code = isset($codes[$key-1]) ? $codes[$key-1] : $data['parent_category_id'];
                    $level++;
                }
            }
            $top = (int)!($level - 1);
            $arr_codes[$data['code']] = $start_id;

            $category_query .= "('" . $category_codes[$data['code']] . "', NULL, '" . (isset($category_codes[$parent_code]) ? $category_codes[$parent_code] : (int)$data['parent_category_id']) . "', '" . $top . "', '" . (int)$data['column'] . "', '" . (int)$data['sort_order'] . "', '" . (int)$data['status'] . "', NOW(), NOW(), '" . (int)$data['noindex'] . "'),";

            foreach ($data['category_description'] as $language_id => $value) {
                $category_description_query .= "('" . $category_codes[$data['code']] . "', '" . (int)$language_id . "', '" . $this->db->escape($value['name']) . "', '" . $this->db->escape($value['description']) . "', '" . $this->db->escape($value['meta_title']) . "', '" . $this->db->escape($value['meta_description']) . "', '" . $this->db->escape($value['meta_keyword']) . "', '" . $this->db->escape($value['meta_h1']) . "'),";
            }

            foreach ($data['category_store'] as $store_id) {
                $category_to_store_query .= "('" . $category_codes[$data['code']] . "', '" . (int)$store_id . "'),";
            }

            foreach ($data['category_seo_url'] as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (!empty($keyword)) {
                        $seo_url_query .= "('" . (int)$store_id . "', '" . (int)$language_id . "', 'category_id=" . $category_codes[$data['code']] . "', '" . $this->db->escape($keyword) . "'),";
                    }
                }
            }

            $category_code_query .= "('" . $category_codes[$data['code']] . "', '" . $this->db->escape($data['code']) . "'),";
        }

        $category_query = substr($category_query, 0, -1) . " ON DUPLICATE KEY UPDATE `image` = VALUES(`image`), `parent_id` = VALUES(`parent_id`), `top` = VALUES(`top`), `column` = VALUES(`column`), `sort_order` = VALUES(`sort_order`), `status` = VALUES(`status`), `date_added` = `date_added`, `date_modified` = NOW(), `noindex` = VALUES(`noindex`)";

        $update_codes = array_intersect_key($category_codes, $arr_codes);
        $update_codes_string = implode(',', $update_codes) . ")";
        $delete_category_description_query .= $update_codes_string;
        $delete_category_to_store_query .= $update_codes_string;
        $delete_category_path_query .= $update_codes_string;
        $delete_seo_url_query .= implode("','category_id=", $update_codes) . "')";
        $delete_category_code_query .= $update_codes_string;
        $category_description_query = substr($category_description_query, 0, -1);
        $category_to_store_query = substr($category_to_store_query, 0, -1);
        $category_path_query = substr($category_path_query, 0, -1);
        $seo_url_query = substr($seo_url_query, 0, -1);
        $category_code_query = substr($category_code_query, 0, -1);
        if ($debug) {
            echo $category_query . "\n";
            echo $delete_category_description_query . "\n";
            echo $delete_category_to_store_query . "\n";
            echo $delete_category_path_query . "\n";
            echo $delete_seo_url_query . "\n";
            echo $delete_category_code_query . "\n";
            echo $category_description_query . "\n";
            echo $category_to_store_query . "\n";
            echo $category_path_query . "\n";
            echo $seo_url_query . "\n";
            echo $category_code_query . "\n";
        }
        $this->db->query($category_query);
        if (!empty($update_codes)) {
            $this->db->query($delete_category_description_query);
            $this->db->query($delete_category_to_store_query);
            $this->db->query($delete_category_path_query);
            $this->db->query($delete_seo_url_query);
            $this->db->query($delete_category_code_query);
        }
        $this->db->query($category_description_query);
        $this->db->query($category_to_store_query);
        $this->db->query($category_path_query);
        $this->db->query($seo_url_query);
        $this->db->query($category_code_query);

        $this->cache->delete('category');
        
        if ($this->config->get('config_seo_pro')){      
            $this->cache->delete('seopro');
        }
    }

    public function delete_multiple_categories($arr_codes, $debug = false) {
        $category_codes = $this->get_category_codes();
        $delete_category_query = "DELETE FROM " . DB_PREFIX . "category WHERE category_id IN (";
        $delete_category_description_query = "DELETE FROM " . DB_PREFIX . "category_description WHERE category_id IN (";
        $delete_category_to_store_query = "DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id IN (";
        $delete_category_path_query = "DELETE FROM " . DB_PREFIX . "category_path WHERE category_id IN (";
        $delete_seo_url_query = "DELETE FROM " . DB_PREFIX . "seo_url WHERE query IN ('category_id=";
        $delete_category_code_query = "DELETE FROM " . DB_PREFIX . "trade_import_category_codes WHERE category_id IN (";
        
        $delete_codes = array_diff_key($category_codes, $arr_codes);
        $delete_codes_string = implode(',', $delete_codes) . ")";
        if (!empty($delete_codes)) {
            if ($debug) {
                echo "Categories (" . $delete_codes_string . "are not present in JSON. Removing\n";
            }
            $this->db->query($delete_category_query . $delete_codes_string);
            $this->db->query($delete_category_description_query . $delete_codes_string);
            $this->db->query($delete_category_to_store_query . $delete_codes_string);
            $this->db->query($delete_category_path_query . $delete_codes_string);
            $this->db->query($delete_category_code_query . $delete_codes_string);
            $this->db->query($delete_seo_url_query . implode("','category_id=", $delete_codes) . "')");
        }

        $this->cache->delete('category');
        
        if ($this->config->get('config_seo_pro')){      
            $this->cache->delete('seopro');
        }
    }

    public function add_multiple_products($arr, $separate = false, $delete = false, $debug = false) {
        $query_limit = 1000000;
        $product_start_id = $this->db->query("SELECT MAX(product_id) FROM " . DB_PREFIX . "product");
        $product_start_id = $product_start_id->row['MAX(product_id)'] == NULL ? 50 : (int)($product_start_id->row['MAX(product_id)'] + 1);
        $product_arr_codes = array();
        $product_codes = $this->get_product_codes();
        $option_start_id = $this->db->query("SELECT MAX(option_id) FROM " . DB_PREFIX . "option");
        $option_start_id = $option_start_id->row['MAX(option_id)'] == NULL ? 1 : (int)($option_start_id->row['MAX(option_id)'] + 1);
        $option_arr_codes = array();
        $option_codes = $this->get_option_codes();
        $option_value_start_id = $this->db->query("SELECT MAX(option_value_id) FROM " . DB_PREFIX . "option_value");
        $option_value_start_id = $option_value_start_id->row['MAX(option_value_id)'] == NULL ? 1 : (int)($option_value_start_id->row['MAX(option_value_id)'] + 1);
        $option_value_arr_codes = array();
        $option_value_codes = $this->get_option_value_codes();
        $product_option_start_id = $this->db->query("SELECT MAX(product_option_id) FROM " . DB_PREFIX . "product_option");
        $product_option_start_id = $product_option_start_id->row['MAX(product_option_id)'] == NULL ? 1 : (int)($product_option_start_id->row['MAX(product_option_id)'] + 1);
        $category_codes = $this->get_category_codes();
        $check_not_empty = array(
            'product_special' => false,
            'option' => false
        );
        $product_option_empty = false;
        $product_option_value_empty = false;
        $queries = array(
            'option' => array(
                'delete' => array(
                    'delete_option' => "DELETE FROM " . DB_PREFIX . "option WHERE option_id IN (",
                    'delete_option_value' => "DELETE FROM " . DB_PREFIX . "option_value WHERE option_id IN (",
                    'delete_option_description' => "DELETE FROM " . DB_PREFIX . "option_description WHERE option_id IN (",
                    'delete_option_value_description' => "DELETE FROM " . DB_PREFIX . "option_value_description WHERE option_id IN (",
                    'delete_option_codes' => "DELETE FROM " . DB_PREFIX . "trade_import_option_codes WHERE option_id IN (",
                    'delete_option_value_codes' => "DELETE FROM " . DB_PREFIX . "trade_import_option_value_codes WHERE nomenclature_uuid IN ('"
                ),
                'add' => array(
                    'option' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "option(`option_id`, `type`, `sort_order`) VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'option_description' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "option_description VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'option_value' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "option_value(`option_value_id`, `option_id`, `image`, `sort_order`) VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'option_value_description' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "option_value_description VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'option_codes' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "trade_import_option_codes VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'option_value_codes' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "trade_import_option_value_codes VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    )
                )
            ),
            'product' => array(
                'delete' => array(
                    'delete_product' => "DELETE FROM " . DB_PREFIX . "product WHERE product_id IN (",
                    'delete_product_description' => "DELETE FROM " . DB_PREFIX . "product_description WHERE product_id IN (",
                    'delete_product_option' => "DELETE FROM " . DB_PREFIX . "product_option WHERE product_id IN (",
                    'delete_product_option_value' => "DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id IN (",
                    'delete_product_special' => "DELETE FROM " . DB_PREFIX . "product_special WHERE product_id IN (",
                    'delete_product_to_category' => "DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id IN (",
                    'delete_product_to_store' => "DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id IN (",
                    'delete_product_codes' => "DELETE FROM " . DB_PREFIX . "trade_import_product_codes WHERE product_id IN (",
                    'delete_seo_url' => "DELETE FROM " . DB_PREFIX . "seo_url WHERE query IN ('product_id="
                ),
                'add' => array(
                    'product' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "product(`product_id`, `model`, `sku`, `ean`, `quantity`, `stock_status_id`, `image`, `shipping`, `price`, `date_available`, `subtract`, `minimum`, `sort_order`, `status`, `date_added`, `date_modified`, `noindex`) VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'product_description' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "product_description VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'product_option' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "product_option VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'product_option_value' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "product_option_value VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'product_special' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "product_special VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'product_to_category' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "product_to_category VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'product_to_store' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "product_to_store VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'product_codes' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "trade_import_product_codes VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    ),
                    'seo_url' => array(
                        'query' => "INSERT INTO " . DB_PREFIX . "seo_url(`store_id`, `language_id`, `query`, `keyword`) VALUES ",
                        'data' => array(),
                        'data_key' => 0
                    )
                )
            )
        );

        foreach ($arr as $data) {
            if (!isset($product_codes[$data['code']])) {
                if ($debug) {
                    echo 'Adding product ' . $data['product_description'][1]['name'] . "\n";
                }
                $product_codes[$data['code']] = $product_start_id;
                $product_start_id++;
            } else {
                if ($debug) {
                    echo $data['product_description'][1]['name'] . " product exists. Editing\n";
                }
            }
            $product_arr_codes[$data['code']] = $product_start_id;

            $this->set_query($queries['product']['add']['product'], "('" . $product_codes[$data['code']] . "', '" . $this->db->escape($data['model']) . "', '" . $this->db->escape($data['sku']) . "', '" . (isset($data['ean']) ? $this->db->escape($data['ean']) : "") . "', '" . (int)$data['quantity'] . "', '" . (int)$data['stock_status_id'] . "', " . (isset($data['image']) ? ("'" . $this->db->escape($data['image']) . "'") : "NULL") . ", '" . (int)$data['shipping'] . "', '" . (float)$data['price'] . "', NOW(), '" . (int)$data['subtract'] . "', '" . (int)$data['minimum'] . "', '" . (int)$data['sort_order'] . "', '" . (int)$data['status'] . "', NOW(), NOW(), '" . (int)$data['noindex'] . "'),", $query_limit);

            foreach ($data['product_description'] as $language_id => $value) {
                $this->set_query($queries['product']['add']['product_description'], "('" . $product_codes[$data['code']] . "', '" . (int)$language_id . "', '" . $this->db->escape($value['name']) . "', '" . $this->db->escape($value['description']) . "', '" . $this->db->escape($value['tag']) . "', '" . $this->db->escape($value['meta_title']) . "', '" . $this->db->escape($value['meta_description']) . "', '" . $this->db->escape($value['meta_keyword']) . "', '" . $this->db->escape($value['meta_h1']) . "'),", $query_limit);
            }

            if (isset($data['product_special'])) {
                $check_not_empty['product_special'] = true;
                foreach ($data['product_special'] as $product_special) {
                    $this->set_query($queries['product']['add']['product_special'], "(NULL, '" . $product_codes[$data['code']] . "', '" . (int)$product_special['customer_group_id'] . "', '" . (int)$product_special['priority'] . "', '" . (float)$product_special['price'] . "', '" . $this->db->escape($product_special['date_start']) . "', '" . $this->db->escape($product_special['date_end']) . "'),", $query_limit);
                }
            }

            foreach ($data['product_store'] as $store_id) {
                $this->set_query($queries['product']['add']['product_to_store'], "('" . $product_codes[$data['code']] . "', '" . (int)$store_id . "'),", $query_limit);
            }

            foreach (explode("/", $data['product_category']) as $category_key => $category_code) {
                if (!in_array($category_code, $data['parent_category_code'])) {
                    $this->set_query($queries['product']['add']['product_to_category'], "('" . $product_codes[$data['code']] . "', '" . $category_codes[$category_code] . "', '" . (int)!$category_key . "'),", $query_limit);
                }
            }

            if (!$separate) {
                if (!empty($data['option_data'])) {
                    $check_not_empty['option'] = true;
                    foreach ($data['option_data'] as $option_data) {
                        if (!isset($option_codes[$data['code']])) {
                            if ($debug) {
                                echo 'Adding options for product ' . $data['product_description'][1]['name'] . "\n";
                                $option_codes[$data['code']] = $option_start_id;
                                $option_start_id++;
                            }
                        } else {
                            if ($debug) {
                                echo 'Editing options for product ' . $data['product_description'][1]['name'] . "\n";
                            }
                        }
                        $option_arr_codes[$data['code']] = $option_start_id;

                        $this->set_query($queries['option']['add']['option'], "('" . $option_codes[$data['code']] . "', '" . $this->db->escape($option_data['type']) . "', '" . (int)$option_data['sort_order'] . "'),", $query_limit);

                        foreach ($option_data['option_description'] as $language_id => $value) {
                            $this->set_query($queries['option']['add']['option_description'], "('" . $option_codes[$data['code']] . "', '" . (int)$language_id . "', '" . $this->db->escape($value['name']) . "'),", $query_limit);
                        }

                        $this->set_query($queries['option']['add']['option_codes'], "('" . $option_codes[$data['code']] . "', '" . $data['code'] . "'),", $query_limit);

                        foreach ($option_data['option_value'] as $option_value) {
                            if (!isset($option_value_codes[$data['code']][$option_value['code']])) {
                                $option_value_codes[$data['code']][$option_value['code']] = $option_value_start_id;
                                $option_value_start_id++;
                            }

                            $this->set_query($queries['option']['add']['option_value'], "('" . $option_value_codes[$data['code']][$option_value['code']] . "', '" . $option_codes[$data['code']] . "', '" . (isset($option_value['image']) ? $this->db->escape($option_value['image']) : "") . "', '" . (int)$option_value['sort_order'] . "'),", $query_limit);

                            foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
                                $this->set_query($queries['option']['add']['option_value_description'], "('" . $option_value_codes[$data['code']][$option_value['code']] . "', '" . (int)$language_id . "', '" . $option_codes[$data['code']] . "', '" . $this->db->escape($option_value_description['name']) . "'),", $query_limit);
                            }

                            $this->set_query($queries['option']['add']['option_value_codes'], "('" . $option_value_codes[$data['code']][$option_value['code']] . "', '" . $option_value['code'] . "', '" . $data['code'] . "'),", $query_limit);
                        }

                        foreach ($data['product_option'] as $product_option) {
                            $this->set_query($queries['product']['add']['product_option'], "('" . $product_option_start_id . "', '" . $product_codes[$data['code']] . "', '" . $option_codes[$data['code']] . "', NULL, '" . $product_option['required'] . "'),", $query_limit);

                            foreach ($product_option['product_option_value'] as $product_option_value) {
                                $this->set_query($queries['product']['add']['product_option_value'], "(NULL, '" . $product_option_start_id . "', '" . $product_codes[$data['code']] . "', '" . $option_codes[$data['code']] . "', '" . $option_value_codes[$data['code']][$this->db->escape($product_option_value['code'])] . "', '" . (int)$product_option_value['quantity'] . "', '" . (int)$product_option_value['subtract'] . "', '" . (float)$product_option_value['price'] . "', '" . $this->db->escape($product_option_value['price_prefix']) . "', NULL, '" . $this->db->escape($product_option_value['points_prefix']) . "', NULL, '" . $this->db->escape($product_option_value['weight_prefix']) . "'),", $query_limit);
                            }
                            $product_option_start_id++;
                        }
                    }
                }
            } else {
                if (!empty($data['option_data'])) {
                    foreach ($data['option_data'][0]['option_value'] as $option_data) {
                        if (!isset($product_codes[$option_data['code']])) {
                            if ($debug) {
                                echo 'Adding product ' . $option_data['name'] . "\n";
                            }
                            $product_codes[$option_data['code']] = $product_start_id;
                            $product_start_id++;
                        } else {
                            if ($debug) {
                                echo $option_data['name'] . " product exists. Editing\n";
                            }
                        }
                        $product_arr_codes[$option_data['code']] = $product_start_id;

                        $this->set_query($queries['product']['add']['product'], "('" . $product_codes[$option_data['code']] . "', '" . $this->db->escape($data['model']) . "', '" . $this->db->escape($data['sku']) . "', '" . (isset($data['ean']) ? $this->db->escape($data['ean']) : "") . "', '" . (int)$option_data['quantity'] . "', '" . (int)$data['stock_status_id'] . "', " . (isset($data['image']) ? ("'" . $this->db->escape($data['image']) . "'") : "NULL") . ", '" . (int)$data['shipping'] . "', '" . (float)$option_data['price'] . "', NOW(), '" . (int)$data['subtract'] . "', '" . (int)$data['minimum'] . "', '" . (int)$data['sort_order'] . "', '" . (int)$option_data['status'] . "', NOW(), NOW(), '" . (int)$data['noindex'] . "'),", $query_limit);

                        foreach ($option_data['product_description'] as $language_id => $value) {
                           $this->set_query($queries['product']['add']['product_description'], "('" . $product_codes[$option_data['code']] . "', '" . (int)$language_id . "', '" . $this->db->escape($value['name']) . "', '" . $this->db->escape($value['description']) . "', '" . $this->db->escape($value['tag']) . "', '" . $this->db->escape($value['meta_title']) . "', '" . $this->db->escape($value['meta_description']) . "', '" . $this->db->escape($value['meta_keyword']) . "', '" . $this->db->escape($value['meta_h1']) . "'),", $query_limit);
                        }

                        if (isset($option_data['product_special'])) {
                            $check_not_empty['product_special'] = true;
                            foreach ($option_data['product_special'] as $product_special) {
                               $this->set_query($queries['product']['add']['product_special'], "(NULL, '" . $product_codes[$option_data['code']] . "', '" . (int)$product_special['customer_group_id'] . "', '" . (int)$product_special['priority'] . "', '" . (float)$product_special['price'] . "', '" . $this->db->escape($product_special['date_start']) . "', '" . $this->db->escape($product_special['date_end']) . "'),", $query_limit);
                            }
                        }

                        foreach ($data['product_store'] as $store_id) {
                            $this->set_query($queries['product']['add']['product_to_store'], "('" . $product_codes[$option_data['code']] . "', '" . (int)$store_id . "'),", $query_limit);
                        }

                        foreach (explode("/", $data['product_category']) as $category_key => $category_code) {
                            if (!in_array($category_code, $data['parent_category_code'])) {
                                $this->set_query($queries['product']['add']['product_to_category'], "('" . $product_codes[$option_data['code']] . "', '" . $category_codes[$category_code] . "', '" . (int)!$category_key . "'),", $query_limit);
                            }
                        }

                        foreach ($option_data['product_seo_url'] as $store_id => $language) {
                            foreach ($language as $language_id => $keyword) {
                                if (!empty($keyword)) {
                                    $this->set_query($queries['product']['add']['seo_url'], "('" . (int)$store_id . "', '" . (int)$language_id . "', 'product_id=" . $product_codes[$option_data['code']] . "', '" . $this->db->escape($keyword) . "'),", $query_limit);
                                }
                            }
                        }

                        $this->set_query($queries['product']['add']['product_codes'], "('" . $product_codes[$option_data['code']] . "', '" . $option_data['code'] . "', '" . $data['group_code'] . "'),", $query_limit);
                    }
                }
            }

            foreach ($data['product_seo_url'] as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (!empty($keyword)) {
                        $this->set_query($queries['product']['add']['seo_url'], "('" . (int)$store_id . "', '" . (int)$language_id . "', 'product_id=" . $product_codes[$data['code']] . "', '" . $this->db->escape($keyword) . "'),", $query_limit);
                    }
                }
            }

            $this->set_query($queries['product']['add']['product_codes'], "('" . $product_codes[$data['code']] . "', '" . $data['code'] . "', '" . $data['group_code'] . "'),", $query_limit);
        }

        foreach($check_not_empty as $key => $check) {
            if (!$check) {
                if ($key == 'option') {
                    unset($queries['product']['add']['product_option']);
                    unset($queries['product']['add']['product_option_value']);
                    unset($queries['option']);
                } else {
                    unset($queries['product']['add'][$key]);
                }
            }
        }

        foreach ($queries['product']['add'] as $key => $query) {
            foreach ($query['data'] as $k => $q) {
                $queries['product']['add'][$key]['query_ready'][$k] = substr($queries['product']['add'][$key]['query'] . $q, 0, -1);
            }
        }
        foreach ($queries['product']['add']['product']['query_ready'] as $key => $query) {
            $queries['product']['add']['product']['query_ready'][$key] .= " ON DUPLICATE KEY UPDATE `product_id` = VALUES(`product_id`), `model` = VALUES(`model`), `sku` = VALUES(`sku`), `ean` = VALUES(`ean`), `quantity` = VALUES(`quantity`), `stock_status_id` = VALUES(`stock_status_id`), `image` = IFNULL(VALUES(`image`), `image`), `shipping` = VALUES(`shipping`), `price` = VALUES(`price`), `date_available` = `date_available`, `subtract` = VALUES(`subtract`), `minimum` = VALUES(`minimum`), `sort_order` = VALUES(`sort_order`), `status` = VALUES(`status`), `date_added` = `date_added`, `date_modified` = NOW(), `noindex` = VALUES(`noindex`)";
        }

        if ($delete) {
            $delete_product_codes = array_diff_key($product_codes, $product_arr_codes);
            $delete_product_codes_string = implode(',', $delete_product_codes) . ")";
            $delete_product_codes_seo_string = implode("','product_id=", $delete_product_codes) . "')";
            if (!empty($delete_product_codes)) {
                if ($debug) {
                    echo "Products (" . $delete_product_codes_string . "are not present in JSON. Removing\n"; 
                }
                foreach ($queries['product']['delete'] as $key => $query) {
                    $query .= $key == 'delete_seo_url' ? $delete_product_codes_seo_string : $delete_product_codes_string;
                    $this->db->query($query);
                }

                if (isset($queries['option'])) {
                    $delete_option_codes = array_diff_key($option_codes, $option_arr_codes);
                    $delete_option_codes_string = implode(',', $delete_option_codes) . ")";
                    $delete_option_value_codes_string = implode("','", array_keys($delete_option_codes)) . "')";
                    if (!empty($delete_option_codes)) {
                        foreach ($queries['option']['delete'] as $key => $query) {
                            $query .= $key == 'delete_option_value_codes' ? $delete_option_value_codes_string : $delete_option_codes_string;
                            $this->db->query($query);
                        }
                    }
                }
            }
        }

        unset($queries['product']['delete']['delete_product']);
        unset($queries['option']['delete']['delete_option']);
        unset($queries['option']['delete']['delete_option_value']);

        $update_product_codes = array_intersect_key($product_codes, $product_arr_codes);
        $update_product_codes_string = implode(',', $update_product_codes) . ")";
        $update_product_codes_seo_string = implode("','product_id=", $update_product_codes) . "')";
        foreach ($queries['product']['delete'] as $key => $query) {
            $queries['product']['delete'][$key] .= $key == 'delete_seo_url' ? $update_product_codes_seo_string : $update_product_codes_string;
        }

        if (isset($queries['option'])) {
            foreach ($queries['option']['add'] as $key => $query) {
                foreach ($query['data'] as $k => $q) {
                    $queries['option']['add'][$key]['query_ready'][$k] = substr($queries['option']['add'][$key]['query'] . $q, 0, -1);
                }
            }
            foreach ($queries['option']['add']['option']['query_ready'] as $key => $query) {
                $queries['option']['add']['option']['query_ready'][$key] .= " ON DUPLICATE KEY UPDATE `option_id` = VALUES(`option_id`), `type` = VALUES(`type`), `sort_order` = VALUES(`sort_order`)";
            }
            foreach ($queries['option']['add']['option_value']['query_ready'] as $key => $query) {
                $queries['option']['add']['option_value']['query_ready'][$key] .= " ON DUPLICATE KEY UPDATE `option_value_id` = VALUES(`option_value_id`), `option_id` = VALUES(`option_id`), `image` = VALUES(`image`), `sort_order` = VALUES(`sort_order`)";
            }
            $update_option_codes = array_intersect_key($option_codes, $option_arr_codes);
            $update_option_codes_string = implode(',', $update_option_codes) . ")";
            $update_option_value_codes_string = implode("','", array_keys($update_option_codes)) . "')";
            foreach ($queries['option']['delete'] as $key => $query) {
                $queries['option']['delete'][$key] .= $key == 'delete_option_value_codes' ? $update_option_value_codes_string : $update_option_codes_string;
            }
        }

        foreach ($queries as $query) {
            foreach ($query as $k => $q) {
                if ($k == 'delete') {
                    foreach ($q as $a) {
                        if ($debug) {
                            echo $a . "\n";
                            echo "Size: " . strlen($a) . "\n";
                        }
                        $this->db->query($a);
                    }
                } else {
                    foreach ($q as $a) {
                        foreach ($a['query_ready'] as $b) {
                            if ($debug) {
                                echo $b . "\n";
                                echo "Size: " . strlen($b) . "\n";
                            }
                            $this->db->query($b);
                        }
                    }
                }
            }
        }

        $this->cache->delete('product');
    
        if($this->config->get('config_seo_pro')){       
            $this->cache->delete('seopro');
        }
    }

    public function add_multiple_complects($arr, $debug = false) {
        $query_limit = 1000000;
        $start_id = 1;
        $this->db->query("DELETE FROM " . DB_PREFIX . "trade_import_complect WHERE complect_id > 0");
        $this->db->query("ALTER TABLE " . DB_PREFIX . "trade_import_complect AUTO_INCREMENT = 1");
        $queries = array(
            'complect' => array(
                'query' => "INSERT INTO " . DB_PREFIX . "trade_import_complect(`complect_id`, `complect_uuid`, `name`, `min_air`, `max_air`, `min_pressure`, `max_pressure`) VALUES ",
                'data' => array(),
                'data_key' => 0
            ),
            'complect_products' => array(
                'query' => "INSERT INTO " . DB_PREFIX . "trade_import_complect_products VALUES ",
                'data' => array(),
                'data_key' => 0
            ),
        );

        foreach ($arr as $data) {
            if ($debug) {
                echo 'Adding complect ' . $data['name'] . "\n";
            }

            $this->set_query($queries['complect'], "('" . $start_id . "', '" . $this->db->escape($data['complect_uuid']) . "', '" . $this->db->escape($data['name']) . "', '" . (int)$data['min_air'] . "', '" . (int)$data['max_air'] . "', '" . (int)$data['min_pressure'] . "', '" . (int)$data['max_pressure'] . "'),", $query_limit);

            foreach ($data['nomenclatures'] as $nomenclature) {
                $this->set_query($queries['complect_products'], "('" . $start_id . "', '" . $nomenclature['product_id'] . "', '" . (int)$nomenclature['quantity'] . "', '" . (int)$nomenclature['ignore_quantity'] . "'),", $query_limit);
            }

            $start_id++;
        }

        foreach ($queries as $key => $query) {
            foreach ($query['data'] as $k => $q) {
                $queries[$key]['query_ready'][$k] = substr($queries[$key]['query'] . $q, 0, -1);
            }
        }

        foreach ($queries as $query) {
            foreach($query['query_ready'] as $q) {
                if ($debug) {
                    echo $q . "\n";
                    echo "Size : " . strlen($q) . "\n";
                }
                $this->db->query($q);
            }
        }       
    }

    public function addCategory($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', noindex = '" . (int)$data['noindex'] . "', date_modified = NOW(), date_added = NOW()");

        $category_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
        }

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
            }
        }
        
        if (isset($data['category_seo_url'])) {
            foreach ($data['category_seo_url'] as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (!empty($keyword)) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($keyword) . "'");
                    }
                }
            }
        }
        
        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related_wb SET category_id = '" . (int)$category_id . "', product_id = '" . (int)$related_id . "'");
            }
        }
    
        if (isset($data['article_related'])) {
            foreach ($data['article_related'] as $related_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "article_related_wb SET category_id = '" . (int)$category_id . "', article_id = '" . (int)$related_id . "'");
            }
        }
        
        // Set which layout to use with this category
        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->cache->delete('category');
        
        if($this->config->get('config_seo_pro')){       
        $this->cache->delete('seopro');
        }

        return $category_id;
    }

    public function editCategory($category_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', noindex = '" . (int)$data['noindex'] . "', date_modified = NOW() WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE path_id = '" . (int)$category_id . "' ORDER BY level ASC");

        if ($query->rows) {
            foreach ($query->rows as $category_path) {
                // Delete the path below the current one
                $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' AND level < '" . (int)$category_path['level'] . "'");

                $path = array();

                // Get the nodes new parents
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Get whats left of the nodes current path
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Combine the paths with a new level
                $level = 0;

                foreach ($path as $path_id) {
                    $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_path['category_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

                    $level++;
                }
            }
        } else {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_id . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', level = '" . (int)$level . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        // SEO URL
        $this->db->query("DELETE FROM `" . DB_PREFIX . "seo_url` WHERE query = 'category_id=" . (int)$category_id . "'");

        if (isset($data['category_seo_url'])) {
            foreach ($data['category_seo_url'] as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (!empty($keyword)) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($keyword) . "'");
                    }
                }
            }
        }
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related_wb WHERE category_id = '" . (int)$category_id . "'");
    
        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related_wb WHERE category_id = '" . (int)$category_id . "' AND product_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related_wb SET category_id = '" . (int)$category_id . "', product_id = '" . (int)$related_id . "'");
                
    
            }
        }
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "article_related_wb WHERE category_id = '" . (int)$category_id . "'");
    
        if (isset($data['article_related'])) {
            foreach ($data['article_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "article_related_wb WHERE category_id = '" . (int)$category_id . "' AND article_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "article_related_wb SET category_id = '" . (int)$category_id . "', article_id = '" . (int)$related_id . "'");
                
    
            }
        }
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->cache->delete('category');
        
        if($this->config->get('config_seo_pro')){       
        $this->cache->delete('seopro');
        }
    }

    public function deleteCategory($category_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_path WHERE category_id = '" . (int)$category_id . "'");

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_path WHERE path_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $this->deleteCategory($result['category_id']);
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'category_id=" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related_wb WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "article_related_wb WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "coupon_category WHERE category_id = '" . (int)$category_id . "'");

        $this->cache->delete('category');
        
        if($this->config->get('config_seo_pro')){       
        $this->cache->delete('seopro');
        }
    }

    public function getCategory($category_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.category_id) AS path FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (c.category_id = cd2.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");
        
        return $query->row;
    }

    public function getCategoryPath($category_id, $parent_id = 0) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.category_id ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.category_id) AS path FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (c.category_id = cd2.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");
        if (!empty($query->row)) {
            $path = explode("&nbsp;&nbsp;&gt;&nbsp;&nbsp;", $query->row['path']);
            $path[] = $category_id;
            return $path;
        } else {
            return array($parent_id);
        }
    }

    public function getCategories($data = array()) {
        $sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order, c1.noindex FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY cp.category_id";

        $sort_data = array(
            'name',
            'sort_order',
            'noindex'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function get_categoryid_by_code($code) {
        $result = $this->db->query('SELECT category_id FROM ' . DB_PREFIX . "trade_import_category_codes WHERE group_uuid = '" . $this->db->escape($code) . "'");
        if (isset($result->row['category_id'])) {
            return $result->row['category_id'];
        } else {
            return 0;
        }
    }

    public function get_category_by_code($code) {
        return $this->getCategory($this->get_categoryid_by_code($code));
    }

    public function get_category_codes() {
        $result = $this->db->query('SELECT category_id, group_uuid FROM ' . DB_PREFIX . 'trade_import_category_codes ORDER BY category_id ASC');
        $arr = array();
        foreach ($result->rows as $row) {
            $arr[$row['group_uuid']] = $row['category_id'];
        }
        return $arr;
    }

    public function add_category_code($id, $code) {
        $this->db->query('INSERT INTO ' . DB_PREFIX . 'trade_import_category_codes VALUES(' . (int)$id . ", '" . $this->db->escape($code) . "')");
    }

    public function edit_category_code($id, $code) {
        $this->db->query('UPDATE ' . DB_PREFIX . "trade_import_category_codes SET group_uuid = '" . $this->db->escape($code) . "' WHERE category_id = " . (int)$id);
    }

    public function delete_category_code($code) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "trade_import_category_codes WHERE group_uuid = '" . $this->db->escape($code) . "'");
    }
    
    public function hide_category($id) {
        $this->db->query('UPDATE ' . DB_PREFIX . 'category SET status = 0 WHERE category_id = ' . (int)$id);
    }

    public function show_category($id) {
        $this->db->query('UPDATE ' . DB_PREFIX . 'category SET status = 1 WHERE category_id = ' . (int)$id);
    }

    public function getCustomerGroups($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $sort_data = array(
            'cgd.name',
            'cg.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY cgd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function addProduct($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', noindex = '" . (int)$data['noindex'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW(), date_modified = NOW()");

        $product_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        if (isset($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['product_attribute'])) {
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    // Removes duplicates
                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "' AND language_id = '" . (int)$language_id . "'");

                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }
        }

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
                }
            }
        }

        if (isset($data['product_recurring'])) {
            foreach ($data['product_recurring'] as $recurring) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int)$product_id . ", customer_group_id = " . (int)$recurring['customer_group_id'] . ", `recurring_id` = " . (int)$recurring['recurring_id']);
            }
        }
        
        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
            }
        }

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
            }
        }

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
            }
        }

        if (isset($data['product_download'])) {
            foreach ($data['product_download'] as $download_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
            }
        }

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }
        
        if (isset($data['main_category_id']) && $data['main_category_id'] > 0) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "' AND category_id = '" . (int)$data['main_category_id'] . "'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$data['main_category_id'] . "', main_category = 1");
                } elseif (isset($data['product_category'][0])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product_to_category SET main_category = 1 WHERE product_id = '" . (int)$product_id . "' AND category_id = '" . (int)$data['product_category'][0] . "'");
        }

        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
            }
        }
        
        if (isset($data['product_related_article'])) {
            foreach ($data['product_related_article'] as $article_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related_article WHERE product_id = '" . (int)$product_id . "' AND article_id = '" . (int)$article_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related_article SET product_id = '" . (int)$product_id . "', article_id = '" . (int)$article_id . "'");
            }
        }

        if (isset($data['product_reward'])) {
            foreach ($data['product_reward'] as $customer_group_id => $product_reward) {
                if ((int)$product_reward['points'] > 0) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$product_reward['points'] . "'");
                }
            }
        }
        
        // SEO URL
        if (isset($data['product_seo_url'])) {
            foreach ($data['product_seo_url'] as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (!empty($keyword)) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($keyword) . "'");
                    }
                }
            }
        }
        
        if (isset($data['product_layout'])) {
            foreach ($data['product_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }


        $this->cache->delete('product');
        
        if($this->config->get('config_seo_pro')){       
        $this->cache->delete('seopro');
        }

        return $product_id;
    }

    public function editProduct($product_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', noindex = '" . (int)$data['noindex'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");

        if (!empty($data['product_attribute'])) {
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    // Removes duplicates
                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_value_id = '" . (int)$product_option_value['product_option_value_id'] . "', product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
                }
            }
        }

        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_recurring` WHERE product_id = " . (int)$product_id);

        if (isset($data['product_recurring'])) {
            foreach ($data['product_recurring'] as $product_recurring) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int)$product_id . ", customer_group_id = " . (int)$product_recurring['customer_group_id'] . ", `recurring_id` = " . (int)$product_recurring['recurring_id']);
            }
        }
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_download'])) {
            foreach ($data['product_download'] as $download_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['main_category_id']) && $data['main_category_id'] > 0) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "' AND category_id = '" . (int)$data['main_category_id'] . "'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$data['main_category_id'] . "', main_category = 1");
        } elseif (isset($data['product_category'][0])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product_to_category SET main_category = 1 WHERE product_id = '" . (int)$product_id . "' AND category_id = '" . (int)$data['product_category'][0] . "'");
        }
        
        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
            }
        }
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related_article WHERE product_id = '" . (int)$product_id . "'");
        
        if (isset($data['product_related_article'])) {
            foreach ($data['product_related_article'] as $article_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related_article WHERE product_id = '" . (int)$product_id . "' AND article_id = '" . (int)$article_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related_article SET product_id = '" . (int)$product_id . "', article_id = '" . (int)$article_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_reward'])) {
            foreach ($data['product_reward'] as $customer_group_id => $value) {
                if ((int)$value['points'] > 0) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$value['points'] . "'");
                }
            }
        }
        
        // SEO URL
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . (int)$product_id . "'");
        
        if (isset($data['product_seo_url'])) {
            foreach ($data['product_seo_url']as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (!empty($keyword)) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($keyword) . "'");
                    }
                }
            }
        }
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_layout'])) {
            foreach ($data['product_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->cache->delete('product');
        
        if($this->config->get('config_seo_pro')){       
        $this->cache->delete('seopro');
        }
    }

    public function deleteProduct($product_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related_article WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_recurring WHERE product_id = " . (int)$product_id);
        $this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "coupon_product WHERE product_id = '" . (int)$product_id . "'");

        $this->cache->delete('product');
        
        if($this->config->get('config_seo_pro')){       
        $this->cache->delete('seopro');
        }
    }

    public function getProduct($product_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getProducts($product_ids) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id IN (" . $this->db->escape(implode(",", $product_ids)) . ") AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY pd.name");

        return $query->rows;
    }

    public function getProductsByCategoryId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");

        return $query->rows;
    }

    public function getProductImages($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function addOption($data) {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "option` SET type = '" . $this->db->escape($data['type']) . "', sort_order = '" . (int)$data['sort_order'] . "'");

        $option_id = $this->db->getLastId();

        foreach ($data['option_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . (int)$option_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
        }

        if (isset($data['option_value'])) {
            foreach ($data['option_value'] as $option_value) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int)$option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$option_value['sort_order'] . "'");

                $option_value_id = $this->db->getLastId();

                foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$language_id . "', option_id = '" . (int)$option_id . "', name = '" . $this->db->escape($option_value_description['name']) . "'");
                }
            }
        }

        return $option_id;
    }

    public function editOption($option_id, $data) {
        $this->db->query("UPDATE `" . DB_PREFIX . "option` SET type = '" . $this->db->escape($data['type']) . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE option_id = '" . (int)$option_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "option_description WHERE option_id = '" . (int)$option_id . "'");

        foreach ($data['option_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . (int)$option_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "option_value WHERE option_id = '" . (int)$option_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "option_value_description WHERE option_id = '" . (int)$option_id . "'");

        if (isset($data['option_value'])) {
            foreach ($data['option_value'] as $option_value) {
                if ($option_value['option_value_id']) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_value_id = '" . (int)$option_value['option_value_id'] . "', option_id = '" . (int)$option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$option_value['sort_order'] . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int)$option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$option_value['sort_order'] . "'");
                }

                $option_value_id = $this->db->getLastId();

                foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$language_id . "', option_id = '" . (int)$option_id . "', name = '" . $this->db->escape($option_value_description['name']) . "'");
                }
            }

        }
    }

    public function deleteOption($option_id) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "option` WHERE option_id = '" . (int)$option_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "option_description WHERE option_id = '" . (int)$option_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "option_value WHERE option_id = '" . (int)$option_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "option_value_description WHERE option_id = '" . (int)$option_id . "'");
    }

    public function getOptionValues($option_id) {
        $option_value_data = array();

        $option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE ov.option_id = '" . (int)$option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ov.sort_order, ovd.name");

        foreach ($option_value_query->rows as $option_value) {
            $option_value_data[] = array(
                'option_value_id' => $option_value['option_value_id'],
                'name'            => $option_value['name'],
                'image'           => $option_value['image'],
                'sort_order'      => $option_value['sort_order']
            );
        }

        return $option_value_data;
    }

    public function get_productid_by_code($code) {
        $result = $this->db->query('SELECT product_id FROM ' . DB_PREFIX . "trade_import_product_codes WHERE nomenclature_uuid = '" . $this->db->escape($code) . "'");
        if (isset($result->row['product_id'])) {
            return $result->row['product_id'];
        } else {
            return NULL;
        }
    }

    public function get_product_by_code($code) {
        return $this->getProduct($this->get_productid_by_code($code));
    }

    public function get_product_codes() {
        $result = $this->db->query('SELECT product_id, nomenclature_uuid FROM ' . DB_PREFIX . 'trade_import_product_codes ORDER BY product_id ASC');
        $arr = array();
        foreach ($result->rows as $row) {
            $arr[$row['nomenclature_uuid']] = $row['product_id'];
        }
        return $arr;
    }

    public function get_product_code_by_id($id) {
        $result = $this->db->query('SELECT nomenclature_uuid FROM ' . DB_PREFIX . "trade_import_product_codes WHERE product_id = '" . (int)$id . "'");
        if (isset($result->row['nomenclature_uuid'])) {
            return $result->row['nomenclature_uuid'];
        } else {
            return NULL;
        }
    }

    public function get_product_group_code($code) {
        $result = $this->db->query('SELECT group_uuid FROM ' . DB_PREFIX . "trade_import_product_codes WHERE nomenclature_uuid = '" . $this->db->escape($code) . "'");
        if (isset($result->row['group_uuid'])) {
            return $result->row['group_uuid'];
        } else {
            return NULL;
        }
    }

    public function get_product_group_code_by_id($id) {
        $result = $this->db->query('SELECT group_uuid FROM ' . DB_PREFIX . "trade_import_product_codes WHERE product_id = '" . (int)$id . "'");
        if (isset($result->row['group_uuid'])) {
            return $result->row['group_uuid'];
        } else {
            return NULL;
        }
    }

    public function add_product_code($id, $code, $group_code) {
        $this->db->query('INSERT INTO ' . DB_PREFIX . 'trade_import_product_codes VALUES(' . (int)$id . ", '" . $this->db->escape($code) . "', '" . $this->db->escape($group_code) . "')");
    }

    public function edit_product_code($id, $code, $group_code) {
        $this->db->query('UPDATE ' . DB_PREFIX . "trade_import_product_codes SET nomenclature_uuid = '" . $this->db->escape($code) . "', group_uuid = '" . $this->db->escape($group_code) . "' WHERE product_id = " . (int)$id);
    }

    public function hide_products() {
        $this->db->query('UPDATE ' . DB_PREFIX . 'product SET status = 0 WHERE quantity < 1 OR price <= 0');
    }

    public function show_products() {
        $this->db->query('UPDATE ' . DB_PREFIX . 'product SET status = 1 WHERE 1');
    }

    public function get_optionid_by_code($code) {
        $result = $this->db->query('SELECT option_id FROM ' . DB_PREFIX . "trade_import_option_codes WHERE nomenclature_uuid = '" . $this->db->escape($code) . "'");
        if (isset($result->row['option_id'])) {
            return $result->row['option_id'];
        } else {
            return NULL;
        }
    }

    public function add_option_code($id, $code) {
        $this->db->query('INSERT INTO ' . DB_PREFIX . 'trade_import_option_codes VALUES(' . (int)$id . ", '" . $this->db->escape($code) . "')");
    }

    public function edit_option_code($id, $code) {
        $this->db->query('UPDATE ' . DB_PREFIX . "trade_import_option_codes SET nomenclature_uuid = '" . $this->db->escape($code) . "' WHERE option_id = " . (int)$id);
    }

    public function get_option_codes() {
        $result = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'trade_import_option_codes');
        $arr = array();
        foreach ($result->rows as $row) {
            $arr[$row['nomenclature_uuid']] = $row['option_id'];
        }
        return $arr;
    }

    public function get_option_value_codes() {
        $result = $this->db->query('SELECT * FROM ' . DB_PREFIX . 'trade_import_option_value_codes');
        $arr = array();
        foreach ($result->rows as $row) {
            $arr[$row['nomenclature_uuid']][$row['characteristic_uuid']] = $row['option_value_id'];
        }
        return $arr;
    }

    public function get_option_valueid_by_code($code) {
        $result = $this->db->query('SELECT option_value_id FROM ' . DB_PREFIX . "trade_import_option_value_codes WHERE characteristic_uuid = '" . $this->db->escape($code) . "'");
        if (isset($result->row['option_value_id'])) {
            return $result->row['option_value_id'];
        } else {
            return NULL;
        }
    }

    public function get_option_value_code_by_id($id) {
        $result = $this->db->query('SELECT characteristic_uuid FROM ' . DB_PREFIX . "trade_import_option_value_codes WHERE option_value_id = '" . (int)$id . "'");
        if (isset($result->row['characteristic_uuid'])) {
            return $result->row['characteristic_uuid'];
        } else {
            return NULL;
        }
    }

    public function add_option_value_code($id, $code, $nomenclature_uuid) {
        $this->db->query('INSERT INTO ' . DB_PREFIX . 'trade_import_option_value_codes VALUES(' . (int)$id . ", '" . $this->db->escape($code) . "', '" . $this->db->escape($nomenclature_uuid) . "')");
    }

    public function edit_option_value_code($id, $code, $nomenclature_uuid) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "trade_import_option_value_codes WHERE characteristic_uuid = '" . $this->db->escape($code) . "'");
        $this->add_option_value_code($id, $code, $nomenclature_uuid);
    }

    public function delete_product_code($code) {
        $this->deleteOption($this->get_optionid_by_code($code));
        $this->db->query("DELETE FROM " . DB_PREFIX . "trade_import_product_codes WHERE nomenclature_uuid = '" . $this->db->escape($code) . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "trade_import_option_codes WHERE nomenclature_uuid = '" . $this->db->escape($code) . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "trade_import_option_value_codes WHERE nomenclature_uuid = '" . $this->db->escape($code) . "'");
    }

    public function get_complect_codes() {
        $result = $this->db->query('SELECT complect_id, complect_uuid FROM ' . DB_PREFIX . 'trade_import_complect ORDER BY complect_id ASC');
        $arr = array();
        foreach ($result->rows as $row) {
            $arr[$row['complect_uuid']] = $row['complect_id'];
        }
        return $arr;
    }

    public function add_operation($json_timestamp, $success) {
        $this->db->query('INSERT INTO ' . DB_PREFIX . "trade_import VALUES (NULL, CURRENT_TIMESTAMP, '" . $this->db->escape($json_timestamp) . "', '" . (bool)$success . "')");
    }

    public function get_latest_operation() {
        $result = $this->db->query("SHOW TABLES LIKE '%" . DB_PREFIX . "trade_import%'");
        $key = array_keys($result->rows[0]);
        $key = $key[0];
        if (array_search(DB_PREFIX . 'trade_import', array_column($result->rows, $key)) !== null) {
            $result = $this->db->query('SELECT MAX(operation_id) FROM ' . DB_PREFIX . "trade_import");
            $id = $result->row['MAX(operation_id)']; 
            $result = $this->db->query('SELECT * FROM ' . DB_PREFIX . "trade_import WHERE operation_id = '" . (int)$id . "'");
            return $result->row;
        }
    }

    public function add_order($order_data, $order_id, $response) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "trade_orders VALUES (NULL, '" . (int)$order_id . "', '" . $this->db->escape($order_data) . "', '" . $this->db->escape($response) . "')");
    }

    public function get_orders() {
        $result = $this->db->query("SELECT * FROM " . DB_PREFIX . "trade_orders ORDER BY operation_id DESC");
        return $result->rows;
    }

    public function get_order_address($order_id) {
        $result = $this->db->query("SELECT payment_address_1 FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");
        if (isset($result->row['payment_address_1'])) {
            return $result->row['payment_address_1'];
        } else {
            return NULL;
        }
    }

    public function editSettingValue($code = '', $key = '', $value = '', $store_id = 0) {
        if (!is_array($value)) {
            $this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape($value) . "', serialized = '0'  WHERE `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "' AND store_id = '" . (int)$store_id . "'");
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape(json_encode($value)) . "', serialized = '1' WHERE `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "' AND store_id = '" . (int)$store_id . "'");
        }
    }
}