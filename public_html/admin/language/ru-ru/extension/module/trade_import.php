<?php
// Heading
$_['heading_title']    = 'Trade Import v2.5';

// Text
$_['text_extension']   = 'Расширения';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';
$_['text_additional_info_groups']	= 'Группы';
$_['text_additional_info_nomenclatures']	= 'Номенклатуры';
$_['text_2_min']	= '2 минуты';
$_['text_10_min']	= '10 минут';
$_['text_30_min']	= '30 минут';
$_['text_1_hour']	= '1 час';
$_['text_2_hour']	= '2 часа';
$_['text_3_hour']	= '3 часа';
$_['text_6_hour']	= '6 часов';
$_['text_12_hour']	= '12 часов';
$_['text_1_day']	= '1 день';
$_['text_2_days']	= '2 дня';
$_['text_3_days']	= '3 дня';
$_['text_1_week']	= '1 неделя';
$_['text_enable_sync']	= 'Синхронизация';
$_['text_latest_operation']	= 'Последняя синхронизация';
$_['text_latest_id'] = 'ID операции';
$_['text_latest_timestamp']	= 'Время операции';
$_['text_latest_json_timestamp']	= 'Время JSON';
$_['text_latest_success']	= 'Успех';
$_['text_next_schedule']	= 'Следующая синхронизация';
$_['text_loading']	= 'Загружаю...';

// Entry
$_['entry_server']	   = 'Адрес сервера';
$_['entry_code']       = 'Адрес авторизации';
$_['entry_nomenclature']	= 'Адрес номенклатур';
$_['entry_token']	   = 'Токен';
$_['entry_enable_old_api']	= 'Использовать старый API';
$_['entry_old_api_address']	= 'Адрес старого API';
$_['entry_old_api_token']	= 'Токен старого API';
$_['entry_price']	   = 'UUID цены';
$_['entry_price_map']  = 'Цены к группам покупателей';
$_['entry_parent_id']  = 'ID родительской категории';
$_['entry_top_category']	= 'UUID родительской категории';
$_['entry_ignore_category'] = 'Игнорировать категории';
$_['entry_enable_order']	= 'Включить Trade Заказ';
$_['entry_order_address']	= 'Адрес Trade заказа';
$_['entry_order_token']		= 'Токен Trade заказа';
$_['entry_status']     = 'Статус';
$_['entry_enable_sync']= 'Авто синхронизация';
$_['entry_sync_period']= 'Период синхронизации';
$_['entry_time_zone']  = 'Временная зона';
$_['entry_local_json'] = 'Локальный JSON';
$_['entry_save_json']  = 'Сохранять JSON';
$_['entry_add_category']	= 'Добавлять категории';
$_['entry_delete_category']	= 'Удалять категории';
$_['entry_hide_category']	= 'Скрывать категории';
$_['entry_sub_filters']	= 'Подкатегории как фильтры';
$_['entry_add_product']	= 'Добавлять товары';
$_['entry_delete_product']	= 'Удалять товары';
$_['entry_hide_product']	= 'Скрывать товары';
$_['entry_add_separate_products'] = 'Добавлять каждую опцию как отдельный товар';
$_['entry_add_one_product'] = '[Дебаг] UUID продукта';
$_['entry_complects_group'] = 'UUID группы с комплектами';
$_['entry_complects_ignore_quantity'] = 'Игнорировать количество товаров (UUID) в комплекте';

// Load
$_['load_text']		   = 'Дебаг';
$_['load_button']	   = 'Импорт';
$_['get_button']	   = 'Получить JSON';
$_['get_prices']	   = 'Вывести цены';
$_['get_groups']	   = 'Вывести группы';
$_['get_nomenclatures']= 'Вывести номенклатуры';
$_['add_one_product']  = 'Добавить продукт';
$_['add_one_separate_product'] = 'Добавить отдельные<br>продукты от опций';
$_['clean_orders'] = 'Очистить заказы';
$_['clean_tables'] = 'Очистить таблицы<br>Trade Import';
$_['get_customer_groups']	= 'Вывести группы покупателей';
$_['get_orders'] = 'Вывести заказы';
$_['confirm_clean'] = 'Вы действительно хотите очистить таблицы Trade Import?';

// Help
$_['help_code']			= 'Trade Import';
$_['help_entry_price']	= 'Получите UUID, нажав на кнопку Вывести цены';
$_['help_entry_price_map']	= 'Соответствие UUID дополнительных цен различным ID группам покупателей в формате: цена1:группа1,группа2;<br>цена2:группа3,группа4';
$_['help_entry_parent_id'] = '(Опционально) ID родительской категории, к которой будут импортироваться данные.';
$_['help_enable_sync']	= 'Включить автоматическую синхронизацию';
$_['help_entry_top_category'] = 'UUID родительской категории, которая будет импортирована из JSON. Несколько категорий разделяются запятыми';
$_['help_entry_ignore_category'] = 'UUID категории, которые будут пропущены. Несколько категорий разделяются запятыми';
$_['help_enable_order']	= 'Включить Trade заказ';
$_['help_entry_order_address']	= 'Адрес для заказа по Trade (https://4ait.4ait.ru/api/v1/orders)';
$_['help_entry_order_token']		= 'Токен для заказа по Trade';
$_['help_sync_period']	= 'Период синхронизации';
$_['help_time_zone']	= 'Выберите вашу временную зону для корректной синхронизации';
$_['help_local_json']	= 'Считывать records.json с сервера вместо скачивания по адресу. Для корректной работы положите records.json в папку admin опенкарта';
$_['help_save_json']	= 'Сохранять скачанный JSON на сервер';
$_['help_add_category']	= 'Добавлять категории из JSON';
$_['help_delete_category']	= 'Удалять категории, отсутствующие в JSON';
$_['help_hide_category']	= 'Скрывать пустые категории';
$_['help_add_product']	= 'Добавлять товары из JSON';
$_['help_delete_product']	= 'Удалять товары, отсутствующие в JSON';
$_['help_hide_product']	= 'Скрывать товары, отсутствующие на складе.';
$_['help_add_separate_products'] = 'Добавлять каждую опцию как отдельный товар';
$_['help_add_one_product']	= 'Добавить один продукт по UUID (для дебага)';
$_['help_sub_filters']	= 'Добавлять подгруппы как фильтры вместо подкатегорий';
$_['help_entry_complects_group'] = 'UUID группы с комплектами';
$_['help_entry_complects_ignore_quantity'] = 'Игнорировать количество товаров (UUID) в комплекте';

// Error
$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';
$_['error_code']       = 'Необходим адрес';

