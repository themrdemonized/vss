    class Size
    {
        constructor(width, height)
        {
            this.width = width;
            this.height = height;
        }
    }

    class ImageInfo
    {
        constructor(image, requireWidthPercent, canvas)
        {
            this.image = image;
            this.requireWidthPercent = requireWidthPercent;
            this.canvas = canvas;
            this.width = 0;
            this.height = 0;
        }

        updateSize()
        {
            this.width = this.requireWidthPercent*this.canvas.width;
            this.height = this.width/(this.image.width/this.image.height);
        }
    }

    function shuffle(a) 
    {
        var j, x, i;
        a = a.slice(0);
        for (i = a.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = a[i];
            a[i] = a[j];
            a[j] = x;
        }
        return a;
    }

    function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    class BoxInfo
    {
        constructor(image, offsetXRocketPercent, offsetYRocketPercent)
        {
            this.image = image;
            this.offsetXRocketPercent = offsetXRocketPercent;
            this.offsetYRocketPercent = offsetYRocketPercent;
        }
    }

    class BoxState
    {
        constructor(boxInfo, posXPercent, posYPercent)
        {
            this.boxInfo = boxInfo;
            this.posXPercent = posXPercent;
            this.posYPercent = posYPercent;
            this.speedYPercent = 0.5;
            this.onGround = false;
        }
    }

    class AnimationCanvas
    {
        constructor(querySelector)
        {
            this.animationRatio = 0.80952380952380952380952380952381;
            this.prevTime = 0;
            this.countDrawed = 0;

            this.canvas = document.querySelector(querySelector);
            this.context = this.canvas.getContext("2d");
            
            this.imageBeautyStore = new ImageInfo(
                this.initImage("catalog/view/javascript/Canvas/1/buildings/beauty-store.svg"), 0.3, this.canvas);
            this.imageFactory = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/buildings/factory.svg"), 0.3, this.canvas);
            this.imageFlatroom = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/buildings/flatroom.svg"), 0.3, this.canvas);
            this.imageGym = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/buildings/gym.svg"), 0.3, this.canvas);
            this.imageHome = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/buildings/home.svg"), 0.3, this.canvas);
            this.imageOffice = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/buildings/office.svg"), 0.3, this.canvas);
            this.imagePharma = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/buildings/pharma.svg"), 0.3, this.canvas);
            this.imagePool = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/buildings/pool.svg"), 0.3, this.canvas);
            this.imageRestaunt = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/buildings/restaunt.svg"), 0.3, this.canvas);
            this.imageShop = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/buildings/shop.svg"), 0.3, this.canvas);
            this.imageStorehouse = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/buildings/storehouse.svg"), 0.3, this.canvas);
            this.imageTree1 = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/buildings/tree1.svg"), 0.15, this.canvas);
            this.imageTree2 = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/buildings/tree2.svg"), 0.15, this.canvas);
            
            this.imageCloud1 = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/cloud-1.svg"), 0.30, this.canvas);
            this.imageCloud2 = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/cloud-2.svg"), 0.30, this.canvas);
            this.imageCloud3 = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/cloud-3.svg"), 0.30, this.canvas);

            this.imageBoxLarge = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/boxes/large.svg"), 0.08, this.canvas);
            this.imageBoxMedium = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/boxes/medium.svg"), 0.08, this.canvas);
            this.imageBoxSmall = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/boxes/small.svg"), 0.08, this.canvas);

            var rawImgBird = this.initImage("catalog/view/javascript/Canvas/1/bird.svg");
            this.imageBird1 = new ImageInfo(rawImgBird, 0.05, this.canvas);
            this.imageBird2 = new ImageInfo(rawImgBird, 0.05, this.canvas);

            this.imageRocket = new ImageInfo(this.initImage("catalog/view/javascript/Canvas/1/rocket.svg"), 0.5, this.canvas);

            this.buildings = [this.imageBeautyStore, 
                                this.imageFactory,
                                this.imageFlatroom,
                                this.imageGym,
                                this.imageHome,
                                this.imageOffice,
                                this.imagePharma,
                                this.imagePool,
                                this.imageRestaunt,
                                this.imageShop,
                                this.imageStorehouse,
                                this.imageTree1,
                                this.imageTree2];
            
            this.boxes = [
                new BoxInfo(this.imageBoxLarge, 0.2, 0.1), 
                new BoxInfo(this.imageBoxMedium, 0.2, 0.1), 
                new BoxInfo(this.imageBoxSmall, 0.2, 0.1)
                ];

            this.remainingBuildings = shuffle(this.buildings);
            this.activeBuildings = [];

            this.maxBuildingHeightPercent = 0;

            this.offsetBuildingsXPercent = 0;

            this.distanceBetweenBuildingsPercent = 0.05;

            this.speedShiftBuildingsPercentInSec = 0.4;
            
            // Cloud properties
            this.cloudSpawnPaddingPercent = 0.05;

            this.cloud1PosXPercent = -1000;
            this.cloud2PosXPercent = -1000;
            this.cloud3PosXPercent = -1000;

            this.cloud1PosYPercent = -1000;
            this.cloud2PosYPercent = -1000;
            this.cloud3PosYPercent = -1000;

            this.cloud1CurrentWaitSec = 0;
            this.cloud2CurrentWaitSec = 0;
            this.cloud3CurrentWaitSec = 0;

            this.cloud1SpeedPercent = 0.7;
            this.cloud2SpeedPercent = 0.5;
            this.cloud3SpeedPercent = 0.3;

            this.cloud1DelaySec = 1;
            this.cloud2DelaySec = 2;
            this.cloud3DelaySec = 3;

            // Bird properties
            this.bird1PosXPercent = -1000;
            this.bird2PosXPercent = -1000;

            this.bird1PosYPercent = -1000;
            this.bird2PosYPercent = -1000;

            this.bird1CurrentWaitSec = 0;
            this.bird2CurrentWaitSec = 0;

            this.bird1SpeedPercent = 0.8;
            this.bird2SpeedPercent = 0.4;

            this.bird1DelaySec = 1;
            this.bird2DelaySec = 2;

            // Rocket properties
            this.rocketPosYStartPercent = 0.15;
            this.rocketPosYEndPercent = 0.2;

            this.amplitudeRocket = 2.0;

            this.speedRocketPercent = 0.8;

            this.rocketPosXPercent = 0.26;
            this.rocketPosYPercent = this.rocketPosYStartPercent;
            this.rocketCurCoeff = 0;

            // Box properties
            this.accelerationPerSecBoxPercent = 1.9;
            this.delaySecCreateBox = 3;
            this.currentTimeBoxes = 0;
            this.activeBoxes = [];
        }

        isIntersectCloud(cloud1PosStart, cloud1PosEnd, cloud2PosStart, cloud2PosEnd)
        {
            return (cloud2PosEnd >= cloud1PosStart && cloud2PosStart <= cloud1PosStart) ||
                    (cloud2PosStart <= cloud1PosEnd && cloud1PosEnd <= cloud2PosEnd) ||
                    (cloud2PosStart >= cloud1PosStart && cloud2PosEnd <= cloud1PosEnd) ||
                     (cloud2PosStart <= cloud1PosStart && cloud2PosStart >= cloud1PosEnd);
        }

        initialize()
        {
            for(var i=0;i<this.buildings.length;i++)
            {
                var building = this.buildings[i];
                
                var buildingHeightPercent = building.requireWidthPercent / (building.image.width/building.image.height);
                if(buildingHeightPercent > this.maxBuildingHeightPercent)
                {
                    this.maxBuildingHeightPercent = buildingHeightPercent;
                }

            }
        }

        onDraw(curTime)
        {
            var dtTime = (curTime - this.prevTime) / 1000;
            this.prevTime = curTime;

            if(dtTime > 60/1000)
            {
                dtTime = 60/1000;
            }

            if(this.canvas.width != this.canvas.clientWidth)
            {
                this.canvas.width = this.canvas.clientWidth;
            }

            if(this.canvas.height != this.canvas.clientHeight)
            {
                this.canvas.height = this.canvas.clientHeight
            }
            
            // update box coords
            for(var i=0;i<this.activeBoxes.length;i++)
            {
                var box = this.activeBoxes[i];

                box.boxInfo.image.updateSize();

                box.posXPercent -= this.speedShiftBuildingsPercentInSec * dtTime;

                if(box.onGround == false)
                {
                    box.speedYPercent += this.accelerationPerSecBoxPercent * dtTime;
                    box.posYPercent += box.speedYPercent * dtTime;

                    var lowPosYPercent = 1 - (box.boxInfo.image.requireWidthPercent/(box.boxInfo.image.image.width/box.boxInfo.image.image.height)) 
                                        / (this.canvas.height/this.canvas.width);
            
                    if(box.posYPercent >= lowPosYPercent)
                    {
                        box.posYPercent = lowPosYPercent;
                        box.onGround = true;
                    }
                }

                if(box.posXPercent + box.boxInfo.image.requireWidthPercent < 0)
                {
                    this.activeBoxes.splice(i, 1);
                    i--;
                }
                
            }

            // update box periods
            this.currentTimeBoxes += dtTime;
            if(this.currentTimeBoxes >= this.delaySecCreateBox)
            {
                var boxInfo = this.boxes[getRandomInt(0, 2)];

                this.activeBoxes.push(new BoxState(boxInfo, 
                        this.rocketPosXPercent + boxInfo.offsetXRocketPercent,
                        this.rocketPosYPercent + boxInfo.offsetYRocketPercent));

                this.currentTimeBoxes -= this.delaySecCreateBox;
            }

            // Update positions and states
            this.offsetBuildingsXPercent -= this.speedShiftBuildingsPercentInSec * dtTime;

            // remove unused buildings
            while(this.activeBuildings.length != 0)
            {
                if(this.activeBuildings[0].requireWidthPercent + this.distanceBetweenBuildingsPercent > -this.offsetBuildingsXPercent)
                {
                    break;
                }
                else
                {
                    this.offsetBuildingsXPercent += this.activeBuildings[0].requireWidthPercent + this.distanceBetweenBuildingsPercent;
                    this.activeBuildings.shift();
                }
            }

            var usedSpacesWidthPercent = this.offsetBuildingsXPercent;
            for(var i=0;i<this.activeBuildings.length;i++)
            {
                usedSpacesWidthPercent += this.activeBuildings[i].requireWidthPercent + this.distanceBetweenBuildingsPercent;
            }

            while(usedSpacesWidthPercent <= 1)
            {
                var building =  this.remainingBuildings.pop();    
                if(this.remainingBuildings.length == 0)
                {
                    this.remainingBuildings = shuffle(this.buildings);
                }
                usedSpacesWidthPercent += building.requireWidthPercent + this.distanceBetweenBuildingsPercent;
                this.activeBuildings.push(building);
            }
            // end update building state

            // update cloud states
            if(this.cloud3PosXPercent > -1000)
            {
                this.cloud3PosXPercent -= this.cloud3SpeedPercent * dtTime;
                if(this.cloud3PosXPercent + this.imageCloud3.requireWidthPercent < 0)
                {
                    this.cloud3PosXPercent = -1000;
                    this.cloud3CurrentWaitSec = 0;
                }
            }
            else
            {
                this.cloud3CurrentWaitSec += dtTime;
                if(this.cloud3CurrentWaitSec >= this.cloud3DelaySec)
                {
                    this.cloud3PosXPercent = 1;
                    this.cloud3PosYPercent = getRandomArbitrary(this.cloudSpawnPaddingPercent, 1 - (this.maxBuildingHeightPercent + this.cloudSpawnPaddingPercent));
                }
            }

            // update cloud states
            if(this.cloud2PosXPercent > -1000)
            {
                this.cloud2PosXPercent -= this.cloud2SpeedPercent * dtTime;
                if(this.cloud2PosXPercent + this.imageCloud2.requireWidthPercent < 0)
                {
                    this.cloud2PosXPercent = -1000;
                    this.cloud2CurrentWaitSec = 0;
                }
            }
            else
            {
                this.cloud2CurrentWaitSec += dtTime;
                if(this.cloud2CurrentWaitSec >= this.cloud2DelaySec)
                {
                    this.cloud2PosXPercent = 1;
                    this.cloud2PosYPercent = getRandomArbitrary(this.cloudSpawnPaddingPercent, 1 - (this.maxBuildingHeightPercent + this.cloudSpawnPaddingPercent));
                }
            }

            // update cloud states
            if(this.cloud1PosXPercent > -1000)
            {
                this.cloud1PosXPercent -= this.cloud1SpeedPercent * dtTime;
                if(this.cloud1PosXPercent + this.imageCloud1.requireWidthPercent < 0)
                {
                    this.cloud1PosXPercent = -1000;
                    this.cloud1CurrentWaitSec = 0;
                }
            }
            else
            {
                this.cloud1CurrentWaitSec += dtTime;
                if(this.cloud1CurrentWaitSec >= this.cloud1DelaySec)
                {
                    this.cloud1PosXPercent = 1;
                    this.cloud1PosYPercent = getRandomArbitrary(this.cloudSpawnPaddingPercent, 1 - (this.maxBuildingHeightPercent + this.cloudSpawnPaddingPercent));
                }
            }
            
            var cloud1PosEnd = this.cloud1PosYPercent + this.imageCloud1.requireWidthPercent/(this.imageCloud1.image.width/this.imageCloud1.image.height);
            var cloud2PosEnd = this.cloud2PosYPercent + this.imageCloud2.requireWidthPercent/(this.imageCloud2.image.width/this.imageCloud2.image.height);
            var cloud3PosEnd = this.cloud3PosYPercent + this.imageCloud3.requireWidthPercent/(this.imageCloud3.image.width/this.imageCloud3.image.height);

            // check intersection
            if(this.cloud1PosXPercent == 1)
            {
                if( (this.cloud2PosXPercent > -1000 && this.isIntersectCloud(this.cloud1PosYPercent, cloud1PosEnd, this.cloud2PosYPercent, cloud2PosEnd)) || 
                    (this.cloud3PosXPercent > -1000 && this.isIntersectCloud(this.cloud1PosYPercent, cloud1PosEnd, this.cloud3PosYPercent, cloud3PosEnd)))
                {
                    this.cloud1PosXPercent = -1000;
                }
            }

            if(this.cloud2PosXPercent == 1)
            {
                if( (this.cloud1PosXPercent > -1000 && this.isIntersectCloud(this.cloud2PosYPercent, cloud2PosEnd, this.cloud1PosYPercent, cloud1PosEnd)) || 
                    (this.cloud3PosXPercent > -1000 && this.isIntersectCloud(this.cloud2PosYPercent, cloud2PosEnd, this.cloud3PosYPercent, cloud3PosEnd)))
                {
                    this.cloud2PosXPercent = -1000;
                }
            }

            if(this.cloud3PosXPercent == 1)
            {
                if( (this.cloud1PosXPercent > -1000 && this.isIntersectCloud(this.cloud3PosYPercent, cloud3PosEnd, this.cloud1PosYPercent, cloud1PosEnd)) || 
                    (this.cloud2PosXPercent > -1000 && this.isIntersectCloud(this.cloud3PosYPercent, cloud3PosEnd, this.cloud2PosYPercent, cloud2PosEnd)))
                {
                    this.cloud3PosXPercent = -1000;
                }
            }

            var bird1PosEnd = this.bird1PosYPercent + this.imageBird1.requireWidthPercent/(this.imageBird1.image.width/this.imageBird1.image.height);
            var bird2PosEnd = this.bird2PosYPercent + this.imageBird2.requireWidthPercent/(this.imageBird2.image.width/this.imageBird2.image.height);
            // Update bird 1 states
            if(this.bird1PosXPercent > -1000)
            {
                this.bird1PosXPercent -= this.bird1SpeedPercent * dtTime;
                if(this.bird1PosXPercent + this.imageBird1.requireWidthPercent < 0)
                {
                    this.bird1PosXPercent = -1000;
                    this.bird1CurrentWaitSec = 0;
                }
            }
            else
            {
                this.bird1CurrentWaitSec += dtTime;
                if(this.bird1CurrentWaitSec >= this.cloud1DelaySec)
                {
                    this.bird1PosXPercent = 1;
                    this.bird1PosYPercent = getRandomArbitrary(this.cloudSpawnPaddingPercent, 1 - (this.maxBuildingHeightPercent + this.cloudSpawnPaddingPercent));
                }
            }

            // Update bird 2 states
            if(this.bird2PosXPercent > -1000)
            {
                this.bird2PosXPercent -= this.bird2SpeedPercent * dtTime;
                if(this.bird2PosXPercent + this.imageBird2.requireWidthPercent < 0)
                {
                    this.bird2PosXPercent = -1000;
                    this.bird2CurrentWaitSec = 0;
                }
            }
            else
            {
                this.bird2CurrentWaitSec += dtTime;
                if(this.bird2CurrentWaitSec >= this.cloud2DelaySec)
                {
                    this.bird2PosXPercent = 1;
                    this.bird2PosYPercent = getRandomArbitrary(this.cloudSpawnPaddingPercent, 1 - (this.maxBuildingHeightPercent + this.cloudSpawnPaddingPercent));
                }
            }

            // check intersection
            if(this.bird1PosXPercent == 1)
            {
                if( (this.bird2PosXPercent > -1000 && this.isIntersectCloud(this.bird1PosYPercent, bird1PosEnd, this.bird2PosYPercent, bird2PosEnd)))
                {
                    this.bird1PosXPercent = -1000;
                }
            }

            if(this.bird2PosXPercent == 1)
            {
                if( (this.bird1PosXPercent > -1000 && this.isIntersectCloud(this.bird1PosYPercent, bird1PosEnd, this.bird2PosYPercent, bird2PosEnd)))
                {
                    this.bird2PosXPercent = -1000;
                }
            }

            // Update rocket state
            this.rocketCurCoeff += this.speedRocketPercent * dtTime;
            if(this.rocketCurCoeff > 1)
            {
                this.rocketPosYPercent = this.rocketPosYEndPercent;
                this.rocketCurCoeff = 0;
                var x;
                x = this.rocketPosYEndPercent;
                this.rocketPosYEndPercent = this.rocketPosYStartPercent;
                this.rocketPosYStartPercent = x;
            }
            else
            {
                var coeff = Math.pow(this.rocketCurCoeff, this.amplitudeRocket) / 
                    (Math.pow(this.rocketCurCoeff, this.amplitudeRocket) + Math.pow(1 - this.rocketCurCoeff, this.amplitudeRocket) );
                this.rocketPosYPercent = this.rocketPosYStartPercent + (this.rocketPosYEndPercent - this.rocketPosYStartPercent) * coeff;
            }

            this.context.fillStyle = "#f4f6f8";
            this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
            
            // draw buildings
            var currentOffsetWidth = this.canvas.width * this.offsetBuildingsXPercent;
            for(var i=0;i<this.activeBuildings.length;i++)
            {
                var building = this.activeBuildings[i];
                building.updateSize();
                this.context.drawImage(building.image, 
                    currentOffsetWidth, this.canvas.height - building.height,  
                    building.width, building.height);

                currentOffsetWidth += building.width + this.distanceBetweenBuildingsPercent * this.canvas.width;
            }

            // draw cloud 3
            if(this.cloud3PosXPercent > -1000)
            {
                this.imageCloud3.updateSize();

                this.context.drawImage(this.imageCloud3.image, 
                    this.cloud3PosXPercent * this.canvas.width, (this.cloud3PosYPercent) * this.canvas.height,  
                    this.imageCloud3.width, this.imageCloud3.height);
            }

            // draw cloud 2
            if(this.cloud2PosXPercent > -1000)
            {
                this.imageCloud2.updateSize();

                this.context.drawImage(this.imageCloud2.image, 
                    this.cloud2PosXPercent * this.canvas.width, (this.cloud2PosYPercent)* this.canvas.height,  
                    this.imageCloud2.width, this.imageCloud2.height);
            }

            // draw bird 1
            if(this.bird1PosXPercent > -1000)
            {
                this.imageBird1.updateSize();

                this.context.drawImage(this.imageBird1.image, 
                    this.bird1PosXPercent * this.canvas.width, (this.bird1PosYPercent) * this.canvas.height,  
                    this.imageBird1.width, this.imageBird1.height);
            }

            // draw boxes
            for(var i=0;i<this.activeBoxes.length;i++)
            {
                var box = this.activeBoxes[i];

                box.boxInfo.image.updateSize();

                this.context.drawImage(box.boxInfo.image.image, 
                    box.posXPercent * this.canvas.width, box.posYPercent * this.canvas.height,  
                    box.boxInfo.image.width, box.boxInfo.image.height);
            }

            // Draw rocket
            this.imageRocket.updateSize();
            this.context.drawImage(this.imageRocket.image, 
                this.rocketPosXPercent * this.canvas.width, this.rocketPosYPercent * this.canvas.height,  
                this.imageRocket.width, this.imageRocket.height);

            // draw bird 2
            if(this.bird2PosXPercent > -1000)
            {
                this.imageBird2.updateSize();

                this.context.drawImage(this.imageBird2.image, 
                    this.bird2PosXPercent * this.canvas.width, (this.bird2PosYPercent) * this.canvas.height,  
                    this.imageBird2.width, this.imageBird2.height);
            }

            // draw cloud 1
            if(this.cloud1PosXPercent > -1000)
            {
                this.imageCloud1.updateSize();

                this.context.drawImage(this.imageCloud1.image, 
                    this.cloud1PosXPercent * this.canvas.width, (this.cloud1PosYPercent) * this.canvas.height,  
                    this.imageCloud1.width, this.imageCloud1.height);
            }
            

            
            var _this = this;
            window.requestAnimationFrame(function(curTime){
                _this.onDraw(curTime);
            });
        }

        updateCloudState()
        {
            if(this.cloud3PosPercent > -1000)
            {
                this.cloud3PosPercent -= this.cloud3SpeedPercent * dtTime;
                if(this.cloud3PosPercent + this.imageCloud3.requireWidthPercent < 0)
                {
                    this.cloud3PosPercent = -1000;
                    this.cloud3CurrentWaitSec = 0;
                }
            }
            else
            {
                cloud3CurrentWaitSec += dtTime;
                if(cloud3CurrentWaitSec >= cloud3DelaySec)
                {
                    this.cloud3PosXPercent = 1;
                    this.cloud3PosYPercent = getRandomArbitrary(this.maxBuildingHeightPercent + this.cloudSpawnPaddingPercent + 
                        this.imageCloud3.requireWidthPercent/(this.imageCloud3.width/this.imageCloud3.height), 1 - this.cloudSpawnPaddingPercent);
                }
            }
        }

        initImage(svgPath)
        {
            var image = new Image();

            var _this = this;
            image.onload = function() 
            {
                _this.countDrawed++;
                if(_this.countDrawed == 20)
                {
                    _this.initialize();
                    _this.onDraw(0);
                }
            };
            image.src = svgPath;

            return image;
        }

    }

    

    var anim = new AnimationCanvas("canvas");
