$(document).ready(function() {

  function add_slider(min, max, name) {
    var slider = "<input class='calc-element calc-slider' type='range' name='"+name+"' min='"+min+"' max='"+max+"' value='"+min+"'/>";
    return slider;
  }

  function slider() {
    $('.calc-content').find('input.calc-slider').each(function () {
      var $handle;
      $(this).rangeslider({
        polyfill: false,
        rangeClass: 'rangeslider',
        disabledClass: 'rangeslider--disabled',
        horizontalClass: 'rangeslider--horizontal',
        verticalClass: 'rangeslider--vertical',
        fillClass: 'rangeslider__fill',
        handleClass: 'rangeslider__handle',
        onInit: function() {
          $handle = $('.rangeslider__handle', this.$range);
          updateHandle($handle[0], this.value);
        }
      }).on('input', function() {
        updateHandle($handle[0], this.value);
        $(this).attr('value', this.value);
        if ($(this).attr('name') == 'floor') {
          $('input.calc-slider[name="myfloor"]').attr('max', $(this).attr('value')).attr('value', Math.min($(this).attr('value'), $('input.calc-slider[name="myfloor"]').attr('value'))).change().rangeslider('update', true);
        }
      });
    });
  }

  function updateHandle(el, val) {
    el.textContent = val;
  }

  function add_number(min, max, name, m) {
    if (!m) {
      m = '';
    }
    var number = "<div class='calc-number-container'><input type='number' name='"+name+"' value='"+min+"' min='"+min+"' max='"+max+"' class='calc-element calc-number'/><span>"+m+"</span></div>";
    return number;
  }

  function add_text(name, type = 'text', placeholder = '') {
    var text = "<div class='calc-text-container'><input type='"+type+"' name='"+name+"' class='calc-text calc-element' placeholder='"+placeholder+"'></div>";
    return text;
  }

  var ts;
  var step = 1;
  var titles = [];
  var tooltips = [];
  var calc_next = $('.calc-next').html();
  titles[1] = "Назначение помещения";
  titles[2] = "Описание помещения";
  titles[3] = "Расход воздуха";
  tooltips[1] = "Выберите подходящий тип помещения. От этого параметра зависит тип вентиляции, которая требуется, чтобы вы смогли свободно дышать.";
  tooltips[2] = tooltips[3] = "Этаж и габариты помещения понадобятся нам для того, чтобы определить высоту и длину воздуховодов.";
  tooltips[100] = "Мы считаем, вам прекрасно подходит";
  titles[100] = "Успех!";
  titles[101] = "ВентПакет уже в пути!";
  titles[404] = "Ошибка выполнения";
  var blocks = [];
  blocks[20] = [];
  blocks[20][0] = "<div class='calc-content-container'><div class='calc-slider-input inline-container-center'><p>Количество этажей: </p>" + add_slider(1, 25, 'floor') + "</div>";
  blocks[20][0] += "<div class='calc-slider-input inline-container-center'><p>Мой этаж: </p>" + add_slider(1, 25, 'myfloor') + "</div>";
  blocks[20][0] += "<div class='calc-block'><h3>Габариты помещения</h3>";
  blocks[20][0] += "<div class='calc-dim-input inline-container-center'><div>" + add_number(1, 120, 'length', '') + "<p>Длина, м</p></div><span class='calc-dim-x'>x</span>";
  blocks[20][0] += "<div>" + add_number(1, 120, 'width', '') + "<p>Ширина, м</p></div><span class='calc-dim-x'>x</span>";
  blocks[20][0] += "<div>" + add_number(1, 14, 'height', '') + "<p>Высота, м</p></div></div></div>";
  blocks[20][3] = "<div class='calc-content-container'><div class='calc-slider-input inline-container-center'><p>Количество этажей: </p>" + add_slider(1, 10, 'floor') + "</div>";
  blocks[20][3] += "<div class='calc-slider-input inline-container-center'><p>Расположение установки: </p>" + add_slider(1, 10, 'myfloor') + "</div>";
  blocks[20][3] += "<div class='calc-block'><h3>Здание</h3>";
  blocks[20][3] += "<div class='calc-dim-input inline-container-center'><div>" + add_number(1, 120, 'length', '') + "<p>Длина здания, м</p></div><span class='calc-dim-x'>x</span>";
  blocks[20][3] += "<div>" + add_number(1, 120, 'width', '') + "<p>Ширина здания, м</p></div><span class='calc-dim-x'>x</span>";
  blocks[20][3] += "<div>" + add_number(1, 14, 'height', '') + "<p>Высота помещения, м</p></div></div></div>";
  blocks[30] = [];
  blocks[30][0] = "<div class='calc-content-container'>";
  blocks[30][0] += "<div class='calc-number-input inline-container-center'><p>Сумма киловатт электрической мощности оборудования кухни</p>" + add_number(1, 1000, 'power', 'кВт') + "</div></div>";
  blocks[30][1] = "<div class='calc-content-container'>";
  blocks[30][1] += "<div class='calc-number-input inline-container-center'><p>Количество рабочих</p>" + add_number(1, 200, 'workers', 'чел.') + "</div>";
  blocks[30][1] += "<div class='calc-number-input inline-container-center'><p>Количество гостей (посетителей)</p>" + add_number(0, 200, 'guests', 'чел.') + "</div></div>";
  blocks[30][2] = "<div class='calc-content-container'>";
  blocks[30][2] += "<div class='calc-number-input inline-container-center'><p>Количество человек в спортзале</p>" + add_number(1, 100, 'workers', 'чел.') + "</div></div>";
  blocks[30][3] = "<div class='calc-content-container'>";
  blocks[30][3] += "<div class='calc-number-input inline-container-center'><p>Количество людей</p>" + add_number(1, 200, 'workers', 'чел.') + "</div>";
  blocks[30][3] += "<div class='calc-number-input inline-container-center'><p>Количество комнат</p>" + add_number(1, 200, 'guests', 'шт.') + "</div></div>";
  blocks[30][4] = "<div class='calc-content-container'>";
  blocks[30][4] += "<div class='calc-number-input inline-container-center'><p>Количество рабочих</p>" + add_number(1, 200, 'workers', 'чел.') + "</div></div>";  
  blocks[30][5] = "<div class='calc-content-container'>";
  blocks[30][5] += "<div class='calc-number-input inline-container-center'><p>Количество рабочих</p>" + add_number(1, 200, 'workers', 'чел.') + "</div></div>";  
  blocks[30][6] = "<div class='calc-content-container'>";
  blocks[30][6] += "<div class='calc-number-input inline-container-center'><p>Количество рабочих</p>" + add_number(1, 200, 'workers', 'чел.') + "</div></div>";
  blocks[100] = "<div class='calc-content-container'><p class='calc-subtitle text-medium center'>Заполните форму для получения предложения на почту</p>";
  blocks[100] += "<div class='calc-packet'><div class='inline-container-center flex-justify mobile-block'><div><p class='complect text-high'></p><p class='h3 price text-high'></p></div><button id='get-complect' class='get-complect btn'>Подробнее</button></div><p class='complect-service caption text-medium' style='margin-top: 16px;'></p></div>";
  blocks[100] += "<div class='calc-block'>"
  blocks[100] += "<div class='calc-text-input inline-container-center'><p>ФИО: </p>" + add_text('FIO', 'text', 'Иванов Иван Иванович') + "</div>";
  blocks[100] += "<div class='calc-text-input inline-container-center'><p>E-mail: </p>" + add_text('email', 'email', 'mail@mail.ru')+ "</div>";
  blocks[100] += "<div class='calc-text-input inline-container-center'><p>Телефон: </p>" + add_text('phone', 'text', '89991234567')+ "</div>";
  blocks[100] += "<div class='calc-text-input inline-container-center'><p>ИНН: </p>" + add_text('inn', 'text', '')+ "</div>";
  blocks[100] += "<div class='calc-text-input inline-container-center'><p>Название объекта: </p>" + add_text('object_name', 'text', '')+ "</div>";
  blocks[100] += "<div class='calc-text-input inline-container-center'><p>Адрес объекта: </p>" + add_text('address', 'text', '')+ "</div></div></div>";
  blocks[101] = "<div class='calc-content-container'><p>Спасибо за уделенное время! В ближайшее время заказ будет обработан и с вами свяжется менеджер, чтобы мы могли начать наше плодородное сотрудничество.</p></div>";
  blocks[404] = "<div class='calc-content-container'><p>К сожалению, наша система не нашла подходящего пакета. Возможно, были введены некорректные данные. Повторите попытку</p></div>";
  var elements = [];
  var data = {};

  function add_elements(step) {
    $('input.calc-slider').rangeslider('destroy');
    elements[step] = $('.calc-content').html();
  }

  function min_value(obj) {
    obj.value=Math.max(obj.value, obj.min);
  }
 
  function restrict_value(obj) {
    if (obj.value != '') {
      obj.value=Math.min(obj.value, obj.max);
    }
  }

  function show_block(title, block, tooltip) {
    $('#calc-title').text(title);
    $('.calc-content').html(function() {
      return block;
    });
    $('.calc-warning').hide();
    $('.calc-next').html(calc_next).removeClass('calc-agree');
    $('.choose-tooltip p').text(tooltip);
  }

  function set_block(step) {
    show_block(titles[step], blocks[step], tooltips[step]);
    slider();
  }

  function set_step(step) {
    for (var i = 1; i <= step; i++) {
      if (i != step) {
        $('.calc-step:nth-child(' + i + ')').addClass('calc-step-complete');
      }
      $('.calc-step:nth-child(' + i + ')').addClass('calc-step-active');
    }
  }

  function set_data() {
    $('.calc-element').each(function() {
      if ($(this).hasClass('calc-choose')) {
        if ($(this).hasClass('active')) {
          data[$(this).attr('name')] = $(this).val();
          if ($(this).hasClass('calc-room-type')) {
            data['room_name'] = $(this).attr('data-room-name');
          }
        }
      } else {
        data[$(this).attr('name')] = $(this).val();
      }
    });
    //console.log(data);
  }

  function set_client_data() {
    $('.calc-content .calc-block .calc-text-input').each(function() {
      var text_field = $(this).find('.calc-text');
      data[text_field.attr('name')] = text_field.val() || "";
    });
    //console.log(data);
  }

  function validate_client_data() {
    var non_valid_fields = [];
    $('.calc-content .calc-block .calc-text-input').each(function() {
      var text_field = $(this).find('.calc-text');
      if (data[text_field.attr('name')] == "") {
        non_valid_fields.push(text_field)
      }
    });
    return non_valid_fields;
  }

  function format_price(price) {
    return new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'RUB' }).format(price).replace(/\D00(?=\D*$)/, '');
  }
         
  $('.calc-content').on("input", 'input.calc-number', function() {
    restrict_value(this);
    $(this).attr('value', this.value);
  }).on("change", 'input.calc-number', function() {
    min_value(this);
    $(this).attr('value', this.value);
  }).on('mousewheel', 'input.calc-number', function(event) {
    if (event.deltaY > 0) {
      $(this).val(function(i, oldval) {
        return ++oldval;
      });
      restrict_value(this);
    } else {
      $(this).val(function(i, oldval) {
        return --oldval;
      });
      min_value(this);
    }
    $(this).attr('value', this.value);
    event.preventDefault();
  }).on('touchstart', 'input.calc-number', function(event) {
    ts = event.originalEvent.touches[0].clientY;
  }).on('touchmove', 'input.calc-number', function(event) {
    var te = event.originalEvent.changedTouches[0].clientY;
    if (Math.abs(ts - te) > 10) {
      if (ts > te) {
        $(this).val(function(i, oldval) {
          return ++oldval;
        });
        restrict_value(this); 
      } else {
        $(this).val(function(i, oldval) {
          return --oldval;
        });
        min_value(this);
      }
      $(this).attr('value', this.value);
      ts = te;
    }
    event.preventDefault();
  });

  $('.calc-content').on("click", 'button.calc-choose', function() {
    $(this).addClass('active');
    $('button.calc-choose').not(this).removeClass('active');
    if ($(this).hasClass('calc-room-type')) {
      var room_name = $(this).attr('data-room-name');
      $('.calc-warning').hide();
      $('.choose-vis[data-room-name="'+room_name+'"]').addClass('active');
      $('.choose-vis:not([data-room-name="'+room_name+'"])').removeClass('active');
    }
    //console.log(data[$('button.calc-choose.active').attr('name')]);
  });

  $('.calc-content').on("focusout", 'input.calc-element', function() {
    $(this).attr('value', this.value);
  });

  $('.modal-close, .modal-window-bg-close').click(function() {
    $('.modal-window-wrapper').removeClass('active');
  });

  $('.calc-bottom').on("click", '.calc-next:not(.calc-agree)', function() {
    set_data();
    if (step == 1) {
      if (!data['room_type']) {
        return 0;
      }
      blocks[2] = blocks[20][data['room_type']] || blocks[20][0];
      blocks[3] = blocks[30][data['room_type']] || blocks[30][0];
    }
    if (step == 4) {
      $('#calc').find('.calc-step:nth-child(1)').trigger('click');
      return;
    }
    if (!blocks[step+1]) {
      set_data();
      add_elements(step);
      set_step(step + 1);
      step++;
      $.ajax({
        url: 'index.php?route=common/home/get_complect',
        type: 'POST',
        dataType: 'JSON',
        data: data,
      })
      .done(function(json) {
        $('.calc-container').animate({'opacity': 0}, 200, function() {
          set_block(100);
          $('.calc-container input[name*=phone]').on('input keyup change', function(e) {
            var val = $(this).val();
            // console.log(val);

            val = val.replace(/\D+/g, '');
            if (val.length == 0) {
              $(this).val(val);
              return
            }

            if (val.charAt(0) === '7') {
              val = val.replace(val.charAt(0), '8');
            }

            if (val.charAt(0) !== '8') {
              val = '8' + val;
            }

            val = val.substring(0, 11);
            $(this).val(val);
          });
          // $('.calc-container input[name*=phone]').mask('80000000000');
          $('.calc-container input[name*=inn]').mask('#');
          tippy('#get-complect', {
            allowHTML: true,
            interactive: true,
            arrow: true,
            duration: 200,
            maxWidth: 606,
            placement: 'right-start',
            trigger: 'click',
            content: 'Загрузка...',
            zIndex: 10001,
            onShow(instance) {
              $.ajax({
                url: 'index.php?route=common/home/get_complect_info',
                type: 'POST',
                dataType: 'JSON',
                data: data,
              }).done(function(json) {
                  var content = "<p class='get-complect-hide mobile'>x</p><table class='complect-info'>";
                  $.each(json['products'], function(index, value) {
                    content += "<tr><td>" + value['name'] + "</td><td class='right quantity'>" + value['quantity'] + " шт</td></tr>";
                  });
                  content += "</table>";
                  console.log(json);
                  instance.setContent(content);
                  $('#get-complect').addClass('active').text('Скрыть');
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                  console.log(jqXHR);
                  console.log(errorThrown);
                })
                .always(function() {
                  console.log("complete");
                });
            },
            onHide(instance) {
              $('#get-complect').removeClass('active').text('Подробнее');
            }
          });
          $('.calc-next').text('Подтвердить').addClass('calc-agree');
          $('.complect').text('ВентПакет ' + json['num']);
          $('.price').text(format_price(json['sum'] / 1.37));
          $('.complect-id').val(json['complect_id']);
          $('.complect-service').text('Пакет не включает в себя стоимость монтажа, примерная стоимость услуг монтажа – ' + (format_price(json['sum'] - (json['sum'] / 1.37))));
          $('.calc-container').animate({'opacity': 1}, 200);
        });
        data['SUMM'] = json['data']['SUMM'];
        data['Y'] = json['data']['Y'];
        data['name'] = json['name'];
        data['complect_id'] = json['complect_id'];
        console.log(json);
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        console.log(errorThrown);
        $('.calc-container').animate({'opacity': 0}, 200, function() {
          set_block(404);
          $('.calc-container').animate({'opacity': 1}, 200);
        });
        console.log("error");
      })
      .always(function() {
        console.log(data);
      });
      return 0;
    }
    set_step(step + 1);
    $('.calc-container').animate({'opacity': 0}, 200, function() {
      add_elements(step);
      step++;
      set_block(step);
      $('.calc-container').animate({'opacity': 1}, 200);
    }); 
  });

  $('body').on('click', '.get-complect-hide', function(event) {
    $('#get-complect').trigger('click');
  });

  $('.calc-bottom').on("click", 'button.calc-agree', function() {
    $('.calc-warning').hide();
    set_client_data();
    var non_valid_fields = validate_client_data();
    if (non_valid_fields.length > 0) {
      $('.calc-warning').text('Пожалуйста, заполните все поля').show();
      return 0;
    }
    $.ajax({
      url: 'index.php?route=common/home/get_complect_nomenclatures',
      type: 'POST',
      dataType: 'JSON',
      data: data,
      beforeSend: function(xhr) {
        $('button.calc-agree').text("Подождите, пожалуйста");
      }
    })
    .done(function(json) {
      $('.calc-container').animate({'opacity': 0}, 200, function() {
        tooltips[101] = "Рады сотрудничеству, " + data['FIO'] + "!";
        set_block(101);
        set_step(6);
        $('.calc-container').animate({'opacity': 1}, 200);
      });
      console.log(json);
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      console.log(jqXHR);
      console.log(errorThrown);
    })
    .always(function() {
      console.log("complete");
    });
    
  });

  $('#calc').find('.calc-step').each(function() {
    $(this).click(function() {
      if ($(this).attr('data-step') < step) {
        step = parseInt($(this).attr('data-step'));
        $(this).removeClass('calc-step-complete');
        $('.calc-step:nth-child(n+'+(step+1)+')').removeClass('calc-step-active calc-step-complete');
        $('.calc-container').animate({'opacity': 0}, 200, function() {
          $('.calc-next').show();
          show_block(titles[step], elements[step], tooltips[step]);
          slider();
          $('.calc-container').animate({'opacity': 1}, 200);
        });
      }
    });
  });

  $('.calc-prev').click(function() {
    $('.calc-step:nth-child('+(step-1)+')').trigger('click');
  });
});