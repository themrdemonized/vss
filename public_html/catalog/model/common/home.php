<?php

class ModelCommonHome extends Model {

	public function get_complect($data) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "trade_import_complect WHERE max_air >= " . (float) $data['V'] . " AND min_pressure >= " . (float) $data['P'] . " ORDER BY max_air ASC");
		$result = $query->rows;
		return $result;
	}

	public function get_complect_nomenclatures($complect_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "trade_import_complect_products WHERE complect_id = " . (int) $complect_id . " ORDER BY ignore_quantity DESC");
		return $query->rows;
	}

	public function get_complect_by_number($number) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "trade_import_complect WHERE name LIKE '%" . (int) $number . "' ORDER BY name ASC");
		return $query->rows;
	}

}