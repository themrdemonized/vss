<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$canonical = $this->url->link('common/home');
			if ($this->config->get('config_seo_pro') && !$this->config->get('config_seopro_addslash')) {
				$canonical = rtrim($canonical, '/');
			}
			$this->document->addLink($canonical, 'canonical');
		}

		$data = array();

		// $data['column_left'] = $this->load->controller('common/column_left');
		// $data['column_right'] = $this->load->controller('common/column_right');
		// $data['content_top'] = $this->load->controller('common/content_top');
		// $data['content_bottom'] = $this->load->controller('common/content_bottom');
		// $data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['year'] = date('Y', time());

		$this->response->setOutput($this->load->view('common/home', $data));
	}

	public function get_complect() {

		function round_up ($value, $places=0) {
		  if ($places < 0) { $places = 0; }
		  $mult = pow(10, $places);
		  return ceil($value * $mult) / $mult;
		}

		$data = $this->request->post;
		$this->load->model('common/home');
		$this->load->model('extension/module/trade_import');

		$computed = array();

		if ($data['room_type'] == 0) {
			$computed['S'] = $data['width'] * $data['length'];
			$computed['L1'] = ceil(sqrt($computed['S'] + $data['length'] - 1) * 2);
			$computed['L2'] = ceil(($data['floor'] - $data['myfloor']) * $data['height'] + 1);
			$computed['V'] = $data['power'] * 108;
			$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 275;
			$computed['Sv1'] = round_up(sqrt($computed['V'] / (5 * 3600)) * 4, 1);
			$computed['Sv2'] = round_up(sqrt($computed['V'] / (8 * 3600)) * 4, 1);
			$computed['Y'] = ceil($computed['L2'] * $computed['Sv2']);
			$computed['SUMM'] = ceil($computed['L1'] * $computed['Sv1'] + $computed['L2'] * $computed['Sv2']);
			
			$complect = $this->model_common_home->get_complect($computed);
			if (!$complect) {
				$complect = $this->model_common_home->get_complect_by_number(13);
			}
		}

		if ($data['room_type'] == 1) {
			$people = $data['guests'] + $data['workers'];
			$computed['S'] = $data['width'] * $data['length'];
			$computed['L1'] = $data['length'] * 2 - 1;
			$computed['L2'] = ceil(($data['floor'] - $data['myfloor']) * $data['height'] + 1);
			$computed['V'] = $data['workers'] * 40 + $data['guests'] * 20;
			$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 275;
			if ($people >= 151) {
				$complect = $this->model_common_home->get_complect_by_number(13);
			} else if ($people >= 126) {
				$complect = $this->model_common_home->get_complect_by_number(12);
			} else if ($people >= 94) {
				$complect = $this->model_common_home->get_complect_by_number(11);
			} else if ($people >= 71) {
				$complect = $this->model_common_home->get_complect_by_number(10);
			} else if ($people >= 47) {
				$complect = $this->model_common_home->get_complect_by_number(9);
			} else if ($people >= 35) {
				$complect = $this->model_common_home->get_complect_by_number(8);
			} else if ($people >= 28) {
				$complect = $this->model_common_home->get_complect_by_number(7);
			} else if ($people >= 22) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 150;
				$complect = $this->model_common_home->get_complect_by_number(6);
			} else if ($people >= 18) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 150;
				$complect = $this->model_common_home->get_complect_by_number(5);
			} else if ($people >= 12) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 120;
				$complect = $this->model_common_home->get_complect_by_number(4);
			} else if ($people >= 7) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
				$complect = $this->model_common_home->get_complect_by_number(3);
			} else if ($people >= 4) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
				$complect = $this->model_common_home->get_complect_by_number(2);
			} else {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
				$complect = $this->model_common_home->get_complect_by_number(1);
			}
			$computed['Sv1'] = round_up(sqrt($computed['V'] / (5 * 3600)) * 4, 1);
			$computed['Sv2'] = round_up(sqrt($computed['V'] / (8 * 3600)) * 4, 1);
			$computed['Y'] = ceil($computed['L2'] * $computed['Sv2']);
			$computed['SUMM'] = ceil($computed['L1'] * $computed['Sv1'] + $computed['L2'] * $computed['Sv2']);
		}

		if ($data['room_type'] == 2) {
			$people = $data['workers'];
			$computed['S'] = $data['width'] * $data['length'];
			$computed['L1'] = $data['length'] * 2 - 1;
			$computed['L2'] = ceil(($data['floor'] - $data['myfloor']) * $data['height'] + 1);
			$computed['V'] = $people * 80;
			$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 275;
			if ($people >= 76) {
				$complect = $this->model_common_home->get_complect_by_number(13);
			} else if ($people >= 63) {
				$complect = $this->model_common_home->get_complect_by_number(12);
			} else if ($people >= 47) {
				$complect = $this->model_common_home->get_complect_by_number(11);
			} else if ($people >= 36) {
				$complect = $this->model_common_home->get_complect_by_number(10);
			} else if ($people >= 24) {
				$complect = $this->model_common_home->get_complect_by_number(9);
			} else if ($people >= 18) {
				$complect = $this->model_common_home->get_complect_by_number(8);
			} else if ($people >= 11) {
				$complect = $this->model_common_home->get_complect_by_number(7);
			} else {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 150;
				$complect = $this->model_common_home->get_complect_by_number(5);
			}
			$computed['Sv1'] = round_up(sqrt($computed['V'] / (5 * 3600)) * 4, 1);
			$computed['Sv2'] = round_up(sqrt($computed['V'] / (8 * 3600)) * 4, 1);
			$computed['Y'] = ceil($computed['L2'] * $computed['Sv2']);
			$computed['SUMM'] = ceil($computed['L1'] * $computed['Sv1'] + $computed['L2'] * $computed['Sv2']);
		}

		if ($data['room_type'] == 3) {
			$people = $data['guests'] + $data['workers'];
			$computed['S'] = $data['width'] * $data['length'];
			$computed['L1'] = $data['length'] * 2 - 1;
			$computed['L2'] = ceil(($data['floor'] - $data['myfloor']) * $data['height'] + 1);
			$computed['V'] = $data['workers'] * 20 * $data['guests'];
			$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
			if ($computed['V'] >= 751) {
				$complect = $this->model_common_home->get_complect_by_number(6);
			} else if ($computed['V'] >= 561) {
				$complect = $this->model_common_home->get_complect_by_number(5);
			} else if ($computed['V'] >= 381) {
				$complect = $this->model_common_home->get_complect_by_number(4);
			} else if ($computed['V'] >= 241) {
				$complect = $this->model_common_home->get_complect_by_number(3);
			} else if ($computed['V'] >= 151) {
				$complect = $this->model_common_home->get_complect_by_number(2);
			} else {
				$complect = $this->model_common_home->get_complect_by_number(1);
			}
			$computed['Sv1'] = round_up(sqrt($computed['V'] / (5 * 3600)) * 4, 1);
			$computed['Sv2'] = round_up(sqrt($computed['V'] / (8 * 3600)) * 4, 1);
			$computed['Y'] = ceil($computed['L2'] * $computed['Sv2']);
			$computed['SUMM'] = ceil($computed['L1'] * $computed['Sv1'] + $computed['L2'] * $computed['Sv2']);
		}

		if ($data['room_type'] == 4) {
			$people = $data['workers'];
			$computed['S'] = $data['width'] * $data['length'];
			$computed['L1'] = $data['length'] * 2 - 1;
			$computed['L2'] = ceil(($data['floor'] - $data['myfloor']) * $data['height'] + 1);
			$computed['V'] = $computed['S'] * $data['height'];
			$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 275;
			if ($computed['V'] > 6000) {
				$complect = $this->model_common_home->get_complect_by_number(13);
			} else if ($computed['V'] > 5000) {
				$complect = $this->model_common_home->get_complect_by_number(12);
			} else if ($computed['V'] > 3750) {
				$complect = $this->model_common_home->get_complect_by_number(11);
			} else if ($computed['V'] > 2800) {
				$complect = $this->model_common_home->get_complect_by_number(10);
			} else if ($computed['V'] > 1870) {
				$complect = $this->model_common_home->get_complect_by_number(9);
			} else if ($computed['V'] > 1400) {
				$complect = $this->model_common_home->get_complect_by_number(8);
			} else if ($computed['V'] > 1100) {
				$complect = $this->model_common_home->get_complect_by_number(7);
			} else if ($computed['V'] > 750) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 150;
				$complect = $this->model_common_home->get_complect_by_number(6);
			} else if ($computed['V'] > 560) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 150;
				$complect = $this->model_common_home->get_complect_by_number(5);
			} else if ($computed['V'] > 380) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 120;
				$complect = $this->model_common_home->get_complect_by_number(4);
			} else if ($computed['V'] > 250) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
				$complect = $this->model_common_home->get_complect_by_number(3);
			} else if ($computed['V'] > 150) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
				$complect = $this->model_common_home->get_complect_by_number(2);
			} else {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
				$complect = $this->model_common_home->get_complect_by_number(1);
			}
			$computed['Sv1'] = round_up(sqrt($computed['V'] / (5 * 3600)) * 4, 1);
			$computed['Sv2'] = round_up(sqrt($computed['V'] / (8 * 3600)) * 4, 1);
			$computed['Y'] = ceil($computed['L2'] * $computed['Sv2']);
			$computed['SUMM'] = ceil($computed['L1'] * $computed['Sv1'] + $computed['L2'] * $computed['Sv2']);
		}

		if ($data['room_type'] == 5) {
			$people = $data['workers'];
			$computed['S'] = $data['width'] * $data['length'];
			$computed['L1'] = $data['length'] * 2 - 1;
			$computed['L2'] = ceil(($data['floor'] - $data['myfloor']) * $data['height'] + 1);
			$computed['V'] = $computed['S'] * $data['height'];
			$computed['Vv'] = $computed['V'] * 4;
			$computed['Vp'] = $computed['V'] * 3;
			$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 275;
			if ($computed['V'] > 1500) {
				$complect = $this->model_common_home->get_complect_by_number(13);
			} else if ($computed['V'] > 1250) {
				$complect = $this->model_common_home->get_complect_by_number(12);
			} else if ($computed['V'] > 938) {
				$complect = $this->model_common_home->get_complect_by_number(11);
			} else if ($computed['V'] > 700) {
				$complect = $this->model_common_home->get_complect_by_number(10);
			} else if ($computed['V'] > 478) {
				$complect = $this->model_common_home->get_complect_by_number(9);
			} else if ($computed['V'] > 350) {
				$complect = $this->model_common_home->get_complect_by_number(8);
			} else if ($computed['V'] > 238) {
				$complect = $this->model_common_home->get_complect_by_number(7);
			} else if ($computed['V'] > 188) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 150;
				$complect = $this->model_common_home->get_complect_by_number(6);
			} else if ($computed['V'] > 140) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 150;
				$complect = $this->model_common_home->get_complect_by_number(5);
			} else if ($computed['V'] > 93) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 120;
				$complect = $this->model_common_home->get_complect_by_number(4);
			} else if ($computed['V'] > 59) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
				$complect = $this->model_common_home->get_complect_by_number(3);
			} else if ($computed['V'] > 37) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
				$complect = $this->model_common_home->get_complect_by_number(2);
			} else {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
				$complect = $this->model_common_home->get_complect_by_number(1);
			}
			$computed['Sv1'] = round_up(sqrt($computed['Vv'] / (5 * 3600)) * 4, 1);
			$computed['Sv2'] = round_up(sqrt($computed['Vv'] / (8 * 3600)) * 4, 1);
			$computed['Y'] = ceil($computed['L2'] * $computed['Sv2']);
			$computed['SUMM'] = ceil($computed['L1'] * $computed['Sv1'] + $computed['L2'] * $computed['Sv2']);
		}

		if ($data['room_type'] == 6) {
			$people = $data['workers'];
			$computed['S'] = $data['width'] * $data['length'];
			$computed['L1'] = $data['length'] * 2 - 1;
			$computed['L2'] = ceil(($data['floor'] - $data['myfloor']) * $data['height'] + 1);
			$computed['V'] = $computed['S'] * $data['height'];
			$computed['Vv'] = $computed['V'] * 4;
			$computed['Vp'] = $computed['V'] * 3;
			$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 275;
			if ($computed['V'] > 2000) {
				$complect = $this->model_common_home->get_complect_by_number(13);
			} else if ($computed['V'] > 1665) {
				$complect = $this->model_common_home->get_complect_by_number(12);
			} else if ($computed['V'] > 1250) {
				$complect = $this->model_common_home->get_complect_by_number(11);
			} else if ($computed['V'] > 933) {
				$complect = $this->model_common_home->get_complect_by_number(10);
			} else if ($computed['V'] > 623) {
				$complect = $this->model_common_home->get_complect_by_number(9);
			} else if ($computed['V'] > 466) {
				$complect = $this->model_common_home->get_complect_by_number(8);
			} else if ($computed['V'] > 316) {
				$complect = $this->model_common_home->get_complect_by_number(7);
			} else if ($computed['V'] > 250) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 150;
				$complect = $this->model_common_home->get_complect_by_number(6);
			} else if ($computed['V'] > 186) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 150;
				$complect = $this->model_common_home->get_complect_by_number(5);
			} else if ($computed['V'] > 125) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 120;
				$complect = $this->model_common_home->get_complect_by_number(4);
			} else if ($computed['V'] > 79) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
				$complect = $this->model_common_home->get_complect_by_number(3);
			} else if ($computed['V'] > 49) {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
				$complect = $this->model_common_home->get_complect_by_number(2);
			} else {
				$computed['P'] = ($computed['L1'] * 1.5 + $computed['L2'] * 3.5) * 1.3 + 105;
				$complect = $this->model_common_home->get_complect_by_number(1);
			}
			$computed['Sv1'] = round_up(sqrt($computed['Vv'] / (5 * 3600)) * 4, 1);
			$computed['Sv2'] = round_up(sqrt($computed['Vv'] / (8 * 3600)) * 4, 1);
			$computed['Y'] = ceil($computed['L2'] * $computed['Sv2']);
			$computed['SUMM'] = ceil($computed['L1'] * $computed['Sv1'] + $computed['L2'] * $computed['Sv2']);
		}

		$json = array();
		$json['name'] = $complect[0]['name'];
		$matches = array();
		preg_match('/№\d+/', $complect[0]['name'], $matches);
		$json['num'] = $matches[0];
		$json['complect_id'] = $complect[0]['complect_id'];
		$json['data'] = array_merge($data, $computed);
		$json['complects'] = $complect;
		$complect_products = $this->model_common_home->get_complect_nomenclatures($complect[0]['complect_id']);
		foreach ($complect_products as $product) {
			$complect_data[$product['product_id']]['quantity'] = $product['quantity']; 
		}
		$products = $this->model_extension_module_trade_import->getProducts(array_column($complect_products, 'product_id'));
		$json['sum'] = 0;
		foreach ($products as $key => $product) {
			$quantity = $complect_data[$product['product_id']]['quantity'];
			switch ($this->model_extension_module_trade_import->get_product_group_code_by_id($product['product_id'])) {
				case "13683c84-de8b-4527-bf66-8fff6d292e6c":
					$quantity = $computed['SUMM'];
					break;
				case "2d5942ee-8287-4026-9b32-34f0ac87c49c":
					$quantity = $computed['Y'];
					break;
				default:
					break;
			}
			$json['sum'] += $quantity * $product['price'];
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function get_complect_info() {
		$post_data = $this->request->post;
		$data = array();
		$complect_data = array();
		$this->load->model('common/home');
		$this->load->model('extension/module/trade_import');
		$complect_products = $this->model_common_home->get_complect_nomenclatures($post_data['complect_id']);
		foreach ($complect_products as $product) {
			$complect_data[$product['product_id']]['quantity'] = $product['quantity']; 
		}
		$products = $this->model_extension_module_trade_import->getProducts(array_column($complect_products, 'product_id'));
		foreach ($products as $key => $product) {
			$data['products'][$key]['name'] = $product['name'];
			$data['products'][$key]['quantity'] = $complect_data[$product['product_id']]['quantity'];
			if ($this->model_extension_module_trade_import->get_product_group_code_by_id($product['product_id']) == "13683c84-de8b-4527-bf66-8fff6d292e6c") {
					$data['products'][$key]['quantity'] = $post_data['SUMM'];
			}
			if ($this->model_extension_module_trade_import->get_product_group_code_by_id($product['product_id']) == "2d5942ee-8287-4026-9b32-34f0ac87c49c") {
					$data['products'][$key]['quantity'] = $post_data['Y'];
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function get_complect_nomenclatures() {

		$post_data = $this->request->post;
		$this->load->model('common/home');
		$this->load->model('extension/module/trade_import');

		$complect_nomenclatures = $this->model_common_home->get_complect_nomenclatures($post_data['complect_id']);
		foreach ($complect_nomenclatures as $key => $nomenclature) {
			if ($nomenclature['ignore_quantity']) {
				if ($this->model_extension_module_trade_import->get_product_group_code_by_id($nomenclature['product_id']) == "13683c84-de8b-4527-bf66-8fff6d292e6c") {
					$this->cart->add($nomenclature['product_id'], $post_data['SUMM']);
				}
				if ($this->model_extension_module_trade_import->get_product_group_code_by_id($nomenclature['product_id']) == "2d5942ee-8287-4026-9b32-34f0ac87c49c") {
					$this->cart->add($nomenclature['product_id'], $post_data['Y']);
				}
			} else {
				$this->cart->add($nomenclature['product_id'], $nomenclature['quantity']);
			}
		}

		$order_data = array();

		$totals = array();
		$taxes = $this->cart->getTaxes();
		$total = 0;

		// Because __call can not keep var references so we put them into an array.
		$total_data = array(
			'totals' => &$totals,
			'taxes'  => &$taxes,
			'total'  => &$total
		);

		$this->load->model('setting/extension');

		$sort_order = array();

		$results = $this->model_setting_extension->getExtensions('total');

		foreach ($results as $key => $value) {
			$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
		}

		array_multisort($sort_order, SORT_ASC, $results);

		foreach ($results as $result) {
			if ($this->config->get('total_' . $result['code'] . '_status')) {
				$this->load->model('extension/total/' . $result['code']);

				// We have to put the totals in an array so that they pass by reference.
				$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
			}
		}

		$sort_order = array();

		foreach ($totals as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $totals);

		$order_data['totals'] = $totals;

		$this->load->language('checkout/checkout');

		$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
		$order_data['store_id'] = $this->config->get('config_store_id');
		$order_data['store_name'] = $this->config->get('config_name');

		if ($order_data['store_id']) {
			$order_data['store_url'] = $this->config->get('config_url');
		} else {
			if ($this->request->server['HTTPS']) {
				$order_data['store_url'] = HTTPS_SERVER;
			} else {
				$order_data['store_url'] = HTTP_SERVER;
			}
		}
		$order_data['customer_id'] = 0;
		$order_data['customer_group_id'] = 1;
		$order_data['firstname'] = $post_data['FIO'];
		$order_data['email'] = $post_data['email'];
		$order_data['telephone'] = $post_data['phone'];
		$order_data['lastname'] = '';
		$order_data['custom_field'] = array();

		$order_data['payment_firstname'] = 'ФИО: ' . $order_data['firstname'] . "\n";
		$order_data['payment_lastname'] = 'E-mail: ' . $order_data['email'] . "\n" . 'Телефон: ' . $post_data['phone'] . "\n" . 'ИНН: ' . $post_data['inn'] . "\n" . 'Название объекта: ' . $post_data['object_name'] . "\n" . 'Адрес объекта: ' . $post_data['address'] . "\n";
		$order_data['payment_company'] = '';
		$order_data['payment_address_1'] = '';
		$order_data['payment_address_2'] = '';
		$order_data['payment_city'] = '';
		$order_data['payment_postcode'] = '';
		$order_data['payment_zone'] = '';
		$order_data['payment_zone_id'] = '';
		$order_data['payment_country'] = '';
		$order_data['payment_country_id'] = '';
		$order_data['payment_address_format'] = '';
		$order_data['payment_custom_field'] = array();
		$order_data['payment_method'] = '';
		$order_data['payment_code'] = '';

		$order_data['shipping_firstname'] = '';
		$order_data['shipping_lastname'] = '';
		$order_data['shipping_company'] = '';
		$order_data['shipping_address_1'] = '';
		$order_data['shipping_address_2'] = '';
		$order_data['shipping_city'] = '';
		$order_data['shipping_postcode'] = '';
		$order_data['shipping_zone'] = '';
		$order_data['shipping_zone_id'] = '';
		$order_data['shipping_country'] = '';
		$order_data['shipping_country_id'] = '';
		$order_data['shipping_address_format'] = '';
		$order_data['shipping_custom_field'] = array();
		$order_data['shipping_method'] = '';
		$order_data['shipping_code'] = '';

		$order_data['products'] = array();

		foreach ($this->cart->getProducts() as $product) {
			$option_data = array();

			foreach ($product['option'] as $option) {
				$option_data[] = array(
					'product_option_id'       => $option['product_option_id'],
					'product_option_value_id' => $option['product_option_value_id'],
					'option_id'               => $option['option_id'],
					'option_value_id'         => $option['option_value_id'],
					'name'                    => $option['name'],
					'value'                   => $option['value'],
					'type'                    => $option['type']
				);
			}

			$order_data['products'][] = array(
				'product_id' => $product['product_id'],
				'name'       => $product['name'],
				'model'      => $product['model'],
				'option'     => $option_data,
				'download'   => $product['download'],
				'quantity'   => $product['quantity'],
				'subtract'   => $product['subtract'],
				'price'      => $product['price'],
				'total'      => $product['total'],
				'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
				'reward'     => $product['reward']
			);
		}

		$order_data['comment'] = '';
		$order_data['total'] = $total_data['total'];

		if (isset($this->request->cookie['tracking'])) {
			$order_data['tracking'] = $this->request->cookie['tracking'];

			$subtotal = $this->cart->getSubTotal();

			// Affiliate
			$affiliate_info = $this->model_account_customer->getAffiliateByTracking($this->request->cookie['tracking']);

			if ($affiliate_info) {
				$order_data['affiliate_id'] = $affiliate_info['customer_id'];
				$order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
			} else {
				$order_data['affiliate_id'] = 0;
				$order_data['commission'] = 0;
			}

			// Marketing
			$this->load->model('checkout/marketing');

			$marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

			if ($marketing_info) {
				$order_data['marketing_id'] = $marketing_info['marketing_id'];
			} else {
				$order_data['marketing_id'] = 0;
			}
		} else {
			$order_data['affiliate_id'] = 0;
			$order_data['commission'] = 0;
			$order_data['marketing_id'] = 0;
			$order_data['tracking'] = '';
		}

		$order_data['language_id'] = $this->config->get('config_language_id');
		$order_data['currency_id'] = $this->currency->getId($this->session->data['currency']);
		$order_data['currency_code'] = $this->session->data['currency'];
		$order_data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
		$order_data['ip'] = $this->request->server['REMOTE_ADDR'];

		if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
			$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
		} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
			$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
		} else {
			$order_data['forwarded_ip'] = '';
		}

		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
		} else {
			$order_data['user_agent'] = '';
		}

		if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
			$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
		} else {
			$order_data['accept_language'] = '';
		}

		$this->load->model('checkout/order');

		$this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);

		$this->load->model('tool/upload');

		$data['products'] = array();

		foreach ($this->cart->getProducts() as $product) {
			$option_data = array();

			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
				);
			}

			$recurring = '';

			if ($product['recurring']) {
				$frequencies = array(
					'day'        => $this->language->get('text_day'),
					'week'       => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month'      => $this->language->get('text_month'),
					'year'       => $this->language->get('text_year'),
				);

				if ($product['recurring']['trial']) {
					$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
				}

				if ($product['recurring']['duration']) {
					$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
				} else {
					$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
				}
			}

			$data['products'][] = array(
				'cart_id'    => $product['cart_id'],
				'product_id' => $product['product_id'],
				'name'       => $product['name'],
				'model'      => $product['model'],
				'option'     => $option_data,
				'recurring'  => $recurring,
				'quantity'   => $product['quantity'],
				'subtract'   => $product['subtract'],
				'price'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
				'total'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], $this->session->data['currency']),
				'href'       => $this->url->link('product/product', 'product_id=' . $product['product_id'])
			);
		}

		// Gift Voucher
		$data['vouchers'] = array();

		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $voucher) {
				$data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency'])
				);
			}
		}

		$data['totals'] = array();

		foreach ($order_data['totals'] as $total) {
			$data['totals'][] = array(
				'title' => $total['title'],
				'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
			);
		}

		$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_cod_order_status_id'));

		if ($this->config->get('module_trade_import_enable_order')) {
			//Connect to Trade and send order
			$time = time();
	    	$date = date('c', $time);
	    	$order_data = array(
	    		'firstname' => $post_data['FIO'],
	    		'email' => $post_data['email'],
	    		'telephone' => 'null'
	    	);
			$order_data['address'] = $this->model_extension_module_trade_import->get_order_address($this->session->data['order_id']);
			$order_data['address'] = empty($order_data['address']) ? 'null' : $order_data['address'];
	    	$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
			$order_data['store_id'] = $this->config->get('config_store_id');
			if ($order_data['store_id']) {
				$order_data['store_url'] = parse_url($this->config->get('config_url'), PHP_URL_HOST);
			} else {
				$order_data['store_url'] = parse_url(HTTP_SERVER, PHP_URL_HOST);
			}

	    	$data = array();
	    	$data['orders'] = array();
	    	$data['orders'][] = array(
	    		'contractor'	=> array(
	    			'phone'		=> $order_data['telephone'],
	    			'email'		=> $order_data['email'],
	    			'name'		=> $order_data['firstname'],
	    			'address'	=> $order_data['address'],
	    		),
	    		'date' 			=> $date,
	    		'description'	=> "{$post_data['room_name']} - {$post_data['name']}\n" . 'E-mail: ' . $order_data['email'] . "\n" . 'Телефон: ' . $post_data['phone'] . "\n" . 'ИНН: ' . $post_data['inn'] . "\n" . 'Название объекта: ' . $post_data['object_name'] . "\n" . 'Адрес объекта: ' . $post_data['address'] . "\n",
	    	);
	    	$data['orders'][0]['goods'] = array();
	    	foreach ($this->cart->getProducts() as $product) {
				foreach ($product['option'] as $option) {
					if ($option['option_id'] == $this->model_extension_module_trade_import->get_optionid_by_code($this->model_extension_module_trade_import->get_product_code_by_id($product['product_id']))) {
						$characteristic_uuid = $this->model_extension_module_trade_import->get_option_value_code_by_id($product['option_value_id']);
						break;
					}
				}

	    		$data['orders'][0]['goods'][] = array(
	    			'nomenclature_uuid'		=> $this->model_extension_module_trade_import->get_product_code_by_id($product['product_id']),
	    			'characteristic_uuid'	=> isset($characteristic_uuid) ? $characteristic_uuid : NULL,
	    			'shipment_uuid'			=> NULL,
	    			'quantity'				=> (double)$product['quantity'],
	    			'price'					=> (double)$product['price'],
	    			'discount'				=> (double)0.00,
	    			'total'					=> (double)$product['total']
	    		);
	    	}
	    	if (!file_exists("trade_import")) {
	    		mkdir("trade_import", 0777, true);
	    	}
	    	$order_url = $this->config->get('module_trade_import_order_address');
	    	$url = $this->config->get('module_trade_import_code');
	    	$token = $this->config->get('module_trade_import_token');
	    	$old_api_token = $this->config->get('module_trade_import_order_token');
	    	if (!$this->config->get('module_trade_import_enable_old_api')) {
		    	$data_string = json_encode(array('token' => $token));
		    	$ch = curl_init();
				$header = array();
				$header[] = "Content-Type: application/json";
				$header[] = "UUID: " . $token;
				$header[] = "Timestamp: " . $date;
				$header[] = "Authorization: " . hash("sha512", $token . $time);
				$header[] = "Content-Length: " . strlen($data_string);
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    	$response = curl_exec($ch);
		    	if ($response === false) {
					echo 'Curl error: ', curl_error($ch), "\n";
					file_put_contents("trade_import/error_" . $date . ".log", curl_error($ch) . "\n" . $response);
					$this->model_extension_module_trade_import->add_order($data_string, $this->session->data['order_id'], 'ERROR: ' . $date . ".log");
		        	curl_close($ch);
		        	return 0;
		        } else {
		        	$response_decoded = json_decode($response, true);
	                $access_token = $response_decoded['access_token'];
	                curl_close($ch);
	                $data_string = json_encode($data);
	                $ch = curl_init();
	                $header = array();
	                $header[] = "Content-Type: application/json";
	                $header[] = "Authorization: Bearer " . $access_token;
	                curl_setopt($ch, CURLOPT_URL, $order_url);
	                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	                $response = curl_exec($ch);
	                curl_close($ch);
		        }
		    } else {
		    	$data_string = json_encode($data);
		    	$ch = curl_init();
				$header = array();
				$header[] = "Content-Type: application/json";
				$header[] = "UUID: " . $old_api_token;
				$header[] = "Timestamp: " . $date;
				$header[] = "Authorization: " . hash("sha512", $old_api_token . $time);
				$header[] = "Content-Length: " . strlen($data_string);
				curl_setopt($ch, CURLOPT_URL, $order_url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    	$response = curl_exec($ch);
		    	curl_close($ch);
		    }
	        if (json_decode($response) !== NULL) {
	        	$this->model_extension_module_trade_import->add_order($data_string, $this->session->data['order_id'], $response);
	        } else {
	        	$this->model_extension_module_trade_import->add_order($data_string, $this->session->data['order_id'], 'ERROR: ' . $date . ".log");
	        	file_put_contents("trade_import/error_" . $date . ".log", $response);
	        }
		}

		$this->cart->clear();

		unset($this->session->data['order_id']);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}
}