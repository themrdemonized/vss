<?php
class ControllerExtensionModuleTradeImport extends Controller {
    private $categories = array();
    private $products = array();
    private $complects = array();
    private $stock_checkout;
    private $access_token;

    //Main function for cron exec
    public function index() {
        $this->load->model('setting/setting');
        date_default_timezone_set($this->config->get('module_trade_import_time_zone'));
        if (($this->config->get('module_trade_import_enable_sync') == 1) && (time() - strtotime($this->config->get('module_trade_import_sync_schedule')) >= 0)) {
            $this->load->model('extension/module/trade_import');
            $this->model_extension_module_trade_import->editSettingValue('module_trade_import', 'module_trade_import_sync_schedule', date("Y-m-d H:i:00", strtotime("+" . str_replace("_", " ",  $this->config->get('module_trade_import_sync_period')), time())));
            ini_set('max_execution_time', 3600);
            ini_set('memory_limit', '512M');    
            $response = $this->connect();
            if (!$response) {
                return 0;
            }
            if ($this->config->get('module_trade_import_save_json')) {
            file_put_contents('records.json', $response);
        }
        $groups_unsorted = json_decode($this->get_groups($response), true);
        if (!$groups_unsorted) {
            echo 'Invalid JSON, unable to get groups.', "\n";
            echo $response;
            $this->model_extension_module_trade_import->add_operation(NULL, 0);
            return 0;
        }
        $nomenclatures = json_decode($this->get_nomenclatures($response), true);
        if (!$nomenclatures) {
            echo 'Invalid JSON, unable to get nomenclatures.', "\n";
            echo $response;
            $this->model_extension_module_trade_import->add_operation(NULL, 0);
            return 0;
        }
        $groups = $this->organize_groups($groups_unsorted['groups']['values']);
        $timestamp = $this->get_timestamp($response);
        unset($response);
        echo memory_get_usage(), ' bytes.', "\n";
        $category_template = $this->set_category_template();
        $top_group_uuid = array_filter(explode(",", $this->config->get('module_trade_import_top_category')));
        $complects = array_flip(array_filter(explode(",", $this->config->get('module_trade_import_complects_group'))));
        $complects_ignore_quantity = array_flip(array_filter(explode(",", $this->config->get('module_trade_import_complects_ignore_quantity'))));
        $ignore_category = array_merge(array_flip(array_filter(explode(",", $this->config->get('module_trade_import_ignore_category')))), $complects);
        $this->stock_checkout = !$this->config->get('config_stock_checkout');
        if ($this->config->get('module_trade_import_add_category')) {
            if (empty($top_group_uuid)) {
                foreach ($groups as $group) {
                    if (!isset($ignore_category[$group[0]])) {
                        $this->set_category($group, $category_template);
                    }
                }
            } else {
                foreach ($groups as $group) {
                    if (in_array($group[0], $top_group_uuid)) {
                        foreach ($group[5] as $g) {
                            if (!isset($ignore_category[$g[0]])) {
                                $this->set_category($g, $category_template);
                            }
                        }
                    }
                }
            }
            $this->model_extension_module_trade_import->add_multiple_categories($this->categories, true);
        }
        $category_codes = $this->model_extension_module_trade_import->get_category_codes();
        $product_template = $this->set_product_template();
        if ($this->config->get('module_trade_import_add_product')) {
            foreach ($nomenclatures['nomenclatures']['values'] as $nomenclature) {
                $this->set_products($nomenclature, $product_template, $category_codes);
                if (empty($complects)) {
                    $this->set_complects($nomenclature, $complects_ignore_quantity);
                } else {
                    if (isset($complects[$nomenclature[5]])) {
                        $this->set_complects($nomenclature, $complects_ignore_quantity);
                    }
                }
            }
            $this->model_extension_module_trade_import->add_multiple_products($this->products, $this->config->get('module_trade_import_add_separate_products'), $this->config->get('module_trade_import_delete_product'), true);
            $this->model_extension_module_trade_import->add_multiple_complects($this->complects, true);
        }
        if ($this->config->get('module_trade_import_hide_product') && !$this->config->get('module_trade_import_add_product')) {
            $this->model_extension_module_trade_import->hide_products();
        } else if (!$this->config->get('module_trade_import_hide_product') && !$this->config->get('module_trade_import_add_product')) {
            $this->model_extension_module_trade_import->show_products();
        }
        if ($this->config->get('module_trade_import_delete_category')) {
            $this->model_extension_module_trade_import->delete_multiple_categories(array_flip(array_column($groups_unsorted['groups']['values'], 0)), true);
        }
        if ($this->config->get('module_trade_import_hide_category')) {
            $this->hide_categories();
        }
        $this->model_extension_module_trade_import->add_operation($timestamp, 1);
        }
    }

//JSON Process
    private function open_path($path) {
        $folder_path = dirname($path);
        if (!is_dir($folder_path)) {
            mkdir($folder_path, 0777, true);
        }
        return fopen($path, 'w+');
    }

    private function replace_between($str, $needle_start, $needle_end, $replacement) {
        $pos = strpos($str, $needle_start);
        $start = $pos === false ? 0 : $pos + strlen($needle_start);
        $pos = strpos($str, $needle_end, $start);
        $end = $pos === false ? strlen($str) : $pos;
        return substr_replace($str, $replacement, $start, $end - $start);
    }

    private function get_groups($response) {
        $string = $this->replace_between($response, '{', '"groups"', '');
        $string = $this->replace_between($string, ']],"deleted"', chr(NULL), '}}');
        $string = str_replace(']],"deleted"', ']]', $string);
        return $string;
    }   

    private function organize_groups($arr, $parent_key = '', $path = '') {
        $branch = array();
        foreach ($arr as $element) {
            $p = $path . "/" . $element[0];
            if ($element[3] == $parent_key) {
                $children = $this->organize_groups($arr, $element[0], $p);
                if ($children) {
                    $element[5] = $children;
                }
                $element[6] = $p;
                $branch[] = $element;
            }
        }
        return $branch;
    }   

    private function get_nomenclatures($response) {
        $string = $this->replace_between($response, '{', '"nomenclatures"', '');
        $string = $this->replace_between($string, ']],"deleted"', NULL, '}}');
        $string = str_replace(']],"deleted"', ']]', $string);
        return $string;
    }

    private function get_prices($response) {
        $string = $this->replace_between($response, '{', '"price_types"', '');
        $string = $this->replace_between($string, ']],"deleted"', NULL, '}}');
        $string = str_replace(']],"deleted"', ']]', $string);
        return $string;
    }

    private function get_timestamp($response) {
        $string = $this->replace_between($response, '{', '"timestamp"', '');
        $timestamp = json_decode($string, true);
        return $timestamp['timestamp'];
    }

    private function get_seo($string) {
        $cyr = ['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я','А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'];
        $lat = ['a','b','v','g','d','e','io','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','','y','','e','yu','ya','a','b','v','g','d','e','io','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','','y','','e','yu','ya'];
        $symb = ['_','/','\\',' ','.',',','<','>','"',"'",'«','»','+',"&nbsp;&nbsp;&gt;&nbsp;&nbsp;","&quot;"];
        $seo = strtolower(str_replace($cyr, $lat, $string));
        $seo = str_replace($symb, '-', $seo);
        $seo = preg_replace('/^\-+|\-+$|\-+(?=\-)/', '', $seo);
        return $seo;
    }

    private function get_category_name($name) {
        $category_name = explode('&nbsp;&nbsp;&gt;&nbsp;&nbsp;', $name);
        return end($category_name);
    }

    private function get_category_path($name) {
        $category_path = str_replace('&nbsp;&nbsp;&gt;&nbsp;&nbsp;', '/', $name); 
        return $category_path;
    }

    private function get_categories() {
        $categories = array();
        foreach ($this->model_extension_module_trade_import->getCategories(array()) as $category) {
            $categories[] = array(
                'category_id'   => $category['category_id'],
                'name'          => $this->get_category_name($category['name']),
                'parent_id'     => $category['parent_id'],
                'path'          => $this->get_category_path($category['name']),
                'original_path' => $category['name'],
            );
        }
        return $categories;
    }

    private function organize_categories($arr, $parent_key = 0) {
        $branch = array();
        foreach ($arr as $element) {
            if ($element['parent_id'] == $parent_key) {
                $children = $this->organize_categories($arr, $element['category_id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }

//Categories
    private function set_category_template() {
        $data = array(
            'parent_category_id' => $this->config->get('module_trade_import_parent_id') !== NULL ? $this->config->get('module_trade_import_parent_id') : 0,
            'parent_category_code' => array_filter(explode(",", $this->config->get('module_trade_import_top_category'))),
            'store' => $this->model_extension_module_trade_import->get_store_name(),
            'category_description' => array(
                1 => array(
                    'name'              => '',
                    'meta_h1'           => '',
                    'description'       => '',
                    'meta_title'        => '',
                    'meta_description'  => '',
                    'meta_keyword'      => ''
                )
            ),
            'category_store' => array(
                0 => 0
            ),
            'column'        => 1,
            'sort_order'    => 0,
            'status'        => 1,
            'noindex'       => 1,
            'category_seo_url' => array(
                0 => array(
                    1 => ''
                )
            ),
            'parent_id' => 0,
            'top' => 0,
            'code' => '',
            'path' => ''
        );
        return $data;
    }

    private function set_category($arr, &$data) {
        $data['category_description'][1]['name'] = $arr[1];
        $data['category_description'][1]['meta_h1'] = $arr[1];
        $data['category_description'][1]['description'] = $arr[1];
        $data['category_description'][1]['meta_title'] = 'Купить ' . $arr[1] . ' в ' . $data['store'] . ' по лучшей цене';
        $data['category_description'][1]['meta_description'] = 'Покупайте ' . $arr[1] . ' в магазине ' . $data['store'] . ' по лучшей цене';
        $data['category_description'][1]['meta_keyword'] = str_replace('/',',',$arr[4]) . ','. $data['store'];
        $data['category_seo_url'][0][1] = $this->get_seo($arr[4]);
        $data['code'] = $arr[0];
        $data['path'] = substr($arr[6], 1);
        $this->categories[$arr[0]] = $data;
        if (isset($arr[5])) {
            foreach ($arr[5] as $sub_category) {
                $this->set_category($sub_category, $data);
            }
        }
    }

    private function hide_category($arr) {
        if (isset($arr['children'])) {
            foreach ($arr['children'] as $children) {
                if ($this->hide_category($children) == 1) {
                    $not_empty = 1;
                }
            }
        }
        $data = $this->model_extension_module_trade_import->getProductsByCategoryId($arr['category_id']);
        foreach ($data as $product) {
            if ($product['status']) {
                $this->model_extension_module_trade_import->show_category($arr['category_id']);
                return 1;
            }
        }
        if (!isset($not_empty)) {
            echo 'Category ', $arr['path'], ' is empty. Hiding.', "\n";
            $this->model_extension_module_trade_import->hide_category($arr['category_id']);
        } else {
            $this->model_extension_module_trade_import->show_category($arr['category_id']);
            return 1;
        }
    }

    private function hide_categories() {
        foreach ($this->organize_categories($this->get_categories()) as $category) {
            $this->hide_category($category);
        }
    }

//Products
    private function set_product_template() {
        $price_map = array();
        $maps = array_filter(explode(";", $this->config->get('module_trade_import_price_map')));
        if (!empty($maps)) {
            foreach ($maps as $map) {
                $t = explode(':', $map);
                $price_map[$t[0]] = explode(',', $t[1]);
            }
        }
        $data = array(
            'store'     => $this->model_extension_module_trade_import->get_store_name(),
            'parent_category_id' => $this->config->get('module_trade_import_parent_id'),
            'parent_category_code' => array_filter(explode(",", $this->config->get('module_trade_import_top_category'))),
            'price_uuid'=> $this->config->get('module_trade_import_price'),
            'price_map' => $price_map,
            'hide_product' => $this->config->get('module_trade_import_hide_product'),
            'server'    => $this->config->get('module_trade_import_server'),
            'separate'  => $this->config->get('module_trade_import_add_separate_products'),
            'model'     => '',
            'sku'       => '',
            'jan'       => NULL,
            'isbn'      => NULL,
            'mpn'       => NULL,
            'upc'       => NULL,
            'location'  => NULL,
            'manufacturer_id'   => NULL,
            'points'    => NULL,
            'weight'    => NULL,
            'weight_class_id'   => NULL,
            'length'    => NULL,
            'width'     => NULL,
            'height'    => NULL,
            'length_class_id'   => NULL,
            'tax_class_id'      => NULL,
            'image'     => NULL,
            'minimum'   => 1,
            'subtract'  => $this->stock_checkout,
            'stock_status_id'   => 5,
            'shipping'  => 1,
            'noindex'   => 1,
            'date_available'    => date("Y-m-d"),
            'product_category' => array(),
            'product_store' => array(
                0   => 0
            ),
            'product_image' => array(),
            'sort_order' => 0,
            'product_description' => array(
                1 => array(
                    'name'  => '',
                    'description' => '',
                    'meta_h1' => '',
                    'meta_title' => '',
                    'meta_description' => '',
                    'meta_keyword' => '',
                    'tag' => ''
                )
            ),
            'quantity'  => 0,
            'price'     => 0,
            'ean'       => NULL,
            'product_seo_url' => array(),
            'product_special' => array(),
            'status'    => 0,
            'product_option' => array(),
            'code' => '',
            'option_data' => array()
        );
        return $data;
    }

    private function set_products($arr, &$data, $category_codes, $force_add = false) {
        if (isset($category_codes[$arr[5]]) || $data['parent_category_id'] || $force_add) {
            $data['model'] = isset($arr[2]) ? $arr[2] : $arr[3];
            $data['sku'] = $arr[2];
            $data['code'] =  $arr[0];
            $data['group_code'] = $arr[5];
            $data['product_category'] = $this->categories[$arr[5]]['path'];
            $data['product_description'][1]['name'] = $arr[3];
            $data['product_description'][1]['description'] = $arr[4];
            $data['product_description'][1]['meta_h1'] = $arr[3];
            $data['product_description'][1]['meta_title'] = 'Купить ' . $arr[3] . ' в ' . $data['store'] . ' по лучшей цене';
            $data['product_description'][1]['meta_description'] = 'Покупайте ' . $arr[3] . ' в магазине ' . $data['store'] . ' по лучшей цене';
            $data['product_description'][1]['meta_keyword'] = $arr[3] . (isset($arr[2]) ? ',' . $arr[2] : NULL) . ',' . $data['store'];
            $data['product_description'][1]['tag'] = $arr[3] . (isset($arr[2]) ? ',' . $arr[2] : NULL);
            $data['quantity'] = 0;
            if (isset($arr[19])) {
                foreach ($arr[19] as $quantity) {
                    $data['quantity'] += (int)$quantity[0];
                }
            }
            $data['product_special'] = NULL;
            $data['price'] = 0;
            if (isset($arr[20])) {
                foreach ($arr[20] as $price) {
                    if ($price[2] == $data['price_uuid']) {
                        $data['price'] = $price[0];
                    }
                    if (isset($data['price_map'][$price[2]])) {
                        foreach($data['price_map'][$price[2]] as $key => $customer) {
                            $data['product_special'][$key]['customer_group_id'] = $customer;
                            $data['product_special'][$key]['priority'] = 1;
                            $data['product_special'][$key]['price'] = $price[0];
                            $data['product_special'][$key]['date_start'] = date("Y-m-d H:i:s");
                            $data['product_special'][$key]['date_end'] = date("Y-m-d H:i:s", time() + 604800);
                        }
                    }
                }
            }
            $data['ean'] = NULL;
            if (isset($arr[18])) {
                $data['ean'] = $arr[18][0][0];
            }
            $data['product_seo_url'][0][1] = $this->get_seo($arr[3]);
            if ($data['hide_product']) {
                if (($data['quantity'] > 0) && ($data['price'] > 0)) {
                    $data['status'] = 1;
                } else {
                    echo 'Product ', $arr[3], ' is empty, null price or no category. Hiding.', "\n";
                    $data['status'] = 0;
                }
            } else {
                $data['status'] = 1;
            }
            $data['option_data'] = array();
            $data['product_option'] = array();
            if (isset($arr[16])) {
                $data['option_data'][0] = array(
                    'option_description'    => array(
                        1   => array(
                            'name' => $arr[3]
                        )
                    ),
                    'type'  => 'select',
                    'sort_order'    => 0
                );
                $data['product_option'][0]['name'] = $arr[3];
                $data['product_option'][0]['type'] = 'select';
                $data['product_option'][0]['required'] = 1;
                foreach ($arr[16] as $key => $attr) {
                    $data['option_data'][0]['option_value'][$key]['image'] = NULL;
                    $data['option_data'][0]['option_value'][$key]['sort_order'] = $key;
                    $data['option_data'][0]['option_value'][$key]['code'] = $attr[0];
                    $data['option_data'][0]['option_value'][$key]['option_value_description'][1]['name'] = $attr[1];
                    if ($data['separate']) {
                        $data['option_data'][0]['option_value'][$key]['name'] = $arr[3] . " " . $attr[1];
                        $data['option_data'][0]['option_value'][$key]['quantity'] = 0;
                        $data['option_data'][0]['option_value'][$key]['price'] = $data['price'];
                        $data['option_data'][0]['option_value'][$key]['product_description'][1]['name'] = $arr[3] . " " . $attr[1];
                        $data['option_data'][0]['option_value'][$key]['product_description'][1]['description'] = $arr[4];
                        $data['option_data'][0]['option_value'][$key]['product_description'][1]['meta_h1'] = $arr[3] . " " . $attr[1];
                        $data['option_data'][0]['option_value'][$key]['product_description'][1]['meta_title'] = 'Купить ' . $arr[3] . " " . $attr[1] . ' в ' . $data['store'] . ' по лучшей цене';
                        $data['option_data'][0]['option_value'][$key]['product_description'][1]['meta_description'] = 'Покупайте ' . $arr[3] . " " . $attr[1] . ' в магазине ' . $data['store'] . ' по лучшей цене';
                        $data['option_data'][0]['option_value'][$key]['product_description'][1]['meta_keyword'] = $arr[3] . " " . $attr[1] . "," . $arr[3] . (isset($arr[2]) ? ',' . $arr[2] : NULL) . ',' . $data['store'];
                        $data['option_data'][0]['option_value'][$key]['product_description'][1]['tag'] = $arr[3] . " " . $attr[1] . "," . $arr[3] . (isset($arr[2]) ? ',' . $arr[2] : NULL);
                        $data['option_data'][0]['option_value'][$key]['product_seo_url'][0][1] = $this->get_seo($arr[3] . " " . $attr[1]);
                    }
                    $data['product_option'][0]['product_option_value'][$key]['code'] = $attr[0];
                    $data['product_option'][0]['product_option_value'][$key]['subtract'] = $this->stock_checkout;
                    $data['product_option'][0]['product_option_value'][$key]['price_prefix'] = '=';
                    $data['product_option'][0]['product_option_value'][$key]['points_prefix'] = '+';
                    $data['product_option'][0]['product_option_value'][$key]['weight_prefix'] = '+';
                    $data['product_option'][0]['product_option_value'][$key]['points'] = NULL;
                    $data['product_option'][0]['product_option_value'][$key]['weight'] = NULL;
                    $data['product_option'][0]['product_option_value'][$key]['quantity'] = 0;
                    $data['product_option'][0]['product_option_value'][$key]['price'] = $data['price'];
                    if (isset($arr[20])) {
                        foreach ($arr[20] as $price) {
                            if ($price[1] == $attr[0]) {
                                if ($price[2] == $data['price_uuid']) {
                                    $data['option_data'][0]['option_value'][$key]['price'] = $data['separate'] ? $price[0] : NULL;
                                    $data['product_option'][0]['product_option_value'][$key]['price'] = $price[0];
                                    break;
                                }
                                if (isset($data['price_map'][$price[2]]) && $data['separate']) {
                                    foreach($data['price_map'][$price[2]] as $key => $customer) {
                                        $data['option_data'][0]['option_value'][$key]['product_special'][$key]['customer_group_id'] = $customer;
                                        $data['option_data'][0]['option_value'][$key]['product_special'][$key]['priority'] = 1;
                                        $data['option_data'][0]['option_value'][$key]['product_special'][$key]['price'] = $price[0];
                                        $data['option_data'][0]['option_value'][$key]['product_special'][$key]['date_start'] = date("Y-m-d H:i:s");
                                        $data['option_data'][0]['option_value'][$key]['product_special'][$key]['date_end'] = date("Y-m-d H:i:s", time() + 604800);
                                    }
                                }
                            }
                        }
                    }
                    if (isset($arr[19])) {
                        foreach ($arr[19] as $quantity) {
                            if ($quantity[1] == $attr[0]) {
                                if ($data['separate']) {
                                    $data['option_data'][0]['option_value'][$key]['quantity'] += $quantity[0];
                                }
                                $data['product_option'][0]['product_option_value'][$key]['quantity'] += $quantity[0];
                            }
                        }
                    }
                    if ($data['separate']) {
                        if ($data['hide_product']) {
                            if (($data['option_data'][0]['option_value'][$key]['quantity'] > 0) && ($data['option_data'][0]['option_value'][$key]['price'] > 0)) {
                                $data['option_data'][0]['option_value'][$key]['status'] = 1;
                            } else {
                                echo 'Product ', $data['option_data'][0]['option_value'][$key]['name'], ' is empty, null price or no category. Hiding.', "\n";
                                $data['option_data'][0]['option_value'][$key]['status'] = 0;
                            }
                        } else {
                            $data['option_data'][0]['option_value'][$key]['status'] = 1;
                        }
                    }
                }
            }
            $this->products[$arr[0]] = $data;
        }
    }

    private function set_complects($arr, $ignore_quantity) {
        $conditions = explode(",", $arr[4]);
        $data = array(
            'complect_uuid' => $arr[0],
            'name' => $arr[3],
            'min_air' => trim(explode("-", $conditions[0])[0]),
            'max_air' => trim(explode("-", $conditions[0])[1]),
            'min_pressure' => trim(explode("-", $conditions[1])[0]),
            'max_pressure' => trim(explode("-", $conditions[1])[1])
        );
        $data['nomenclatures'] = array();
        foreach ($arr[21] as $key => $nomenclature) {
            $data['nomenclatures'][$key]['product_id'] = $this->model_extension_module_trade_import->get_productid_by_code($nomenclature[1]);
            $data['nomenclatures'][$key]['quantity'] = $nomenclature[0];
            $data['nomenclatures'][$key]['ignore_quantity'] = isset($ignore_quantity[$nomenclature[1]]) ? true : false;
        }
        $this->complects[$arr[0]] = $data;
    }

//Connect to Trade
    private function connect() {   
        if ($this->config->get('module_trade_import_local_json')) {
            $response = file_get_contents('records.json');
            if ($response === false) {
                echo 'Unable to open file.';
                $this->model_extension_module_trade_import->add_operation(NULL, 0);
                return 0;
            } else {
                echo 'File opened.', "\n";
            }
        } else {
            if (!$this->config->get('module_trade_import_enable_old_api')) {
                $url = $this->config->get('module_trade_import_code');
                $nomenclature_url = $this->config->get('module_trade_import_nomenclature');
                $token = $this->config->get('module_trade_import_token');
                $time = time();
                $date = date('c', $time);
                $ch = curl_init();
                $header = array();
                $data_string = json_encode(array('token' => $token));
                $header[] = "Content-Type: application/json";
                $header[] = "UUID: " . $token;
                $header[] = "Timestamp: " . $date;
                $header[] = "Authorization: " . hash("sha512", $token . $time);
                $header[] = "Content-Length: " . strlen($data_string);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                $response = curl_exec($ch);
                if ($response === false) {
                    echo 'Curl error: ', curl_error($ch), "\n";
                    curl_close($ch);
                    echo $response;
                    $this->model_extension_module_trade_import->add_operation(NULL, 0);
                    return 0;
                } else {
                    echo $url, " connection successful.", "\n";
                    $response_decoded = json_decode($response, true);
                    $this->access_token = $response_decoded['access_token'];
                    curl_close($ch);
                    $ch = curl_init();
                    $header = array();
                    $header[] = "Content-Type: application/json";
                    $header[] = "Authorization: Bearer " . $this->access_token;
                    curl_setopt($ch, CURLOPT_URL, $nomenclature_url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    $response = curl_exec($ch);
                    curl_close($ch);
                }
            } else {
                $url = $this->config->get('module_trade_import_old_api_address');
                $token = $this->config->get('module_trade_import_old_api_token');
                $ch = curl_init();
                $header = array();
                $header[] = "Content-Type: application/json";
                $header[] = "Authorization: " . $token;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $response = curl_exec($ch);
                if ($response === false) {
                    echo 'Curl error: ', curl_error($ch), "\n";
                    curl_close($ch);
                    echo $response;
                    $this->model_extension_module_trade_import->add_operation(NULL, 0);
                    return 0;
                } else {
                    echo $url, " connection successful. Getting file.", "\n";
                }
                curl_close($ch);
            }
        }
        return $response;
    }
}