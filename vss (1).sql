-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 07 2021 г., 10:47
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `vss`
--

-- --------------------------------------------------------

--
-- Структура таблицы `oc_address`
--

CREATE TABLE `oc_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT 0,
  `zone_id` int(11) NOT NULL DEFAULT 0,
  `custom_field` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_api`
--

CREATE TABLE `oc_api` (
  `api_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_api`
--

INSERT INTO `oc_api` (`api_id`, `username`, `key`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Default', 'LOul2zvCyJlx5YEZAFGM4JaihDvEb0Khw2FlM1nDP1z19c3EvDig20MW8uPgplhf31WEnpi3VNQmHom673bnTcieJmvbklhzcd0J5wdSrMOHXZO4Jy7x0yhctfOr3eAOCJSBE6aQpwdbapD2Vt3CLGEn6KYnVJxB4PvKo5Koavy1mW6Mco3cnUXOeQ4XxhHJbiOtmeElrBker2b3xsbzcjsW0wq2KKyBzdi41LlqQZcGw2P48rMt4bH8xOEJOHCi', 1, '2020-03-10 16:35:43', '2020-03-10 16:35:43');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_api_ip`
--

CREATE TABLE `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_api_ip`
--

INSERT INTO `oc_api_ip` (`api_ip_id`, `api_id`, `ip`) VALUES
(1, 1, '192.168.99.1'),
(2, 1, '127.0.0.1');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_api_session`
--

CREATE TABLE `oc_api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_api_session`
--

INSERT INTO `oc_api_session` (`api_session_id`, `api_id`, `session_id`, `ip`, `date_added`, `date_modified`) VALUES
(28, 1, 'e9511a65a7293a5ae64207b415', '127.0.0.1', '2021-01-21 12:07:29', '2021-01-21 12:07:29'),
(27, 1, '59bf43d07eed31e53f72ebfeb1', '127.0.0.1', '2021-01-01 16:01:45', '2021-01-01 16:01:45'),
(26, 1, '52f12c456662eea7c91dd98a63', '127.0.0.1', '2020-12-09 16:15:04', '2020-12-09 16:15:04'),
(25, 1, 'fd4326ca922f3d2bd94a7878d9', '127.0.0.1', '2020-07-21 00:40:31', '2020-07-21 00:40:31'),
(24, 1, '3ff51a01d4353c3cda5ac05143', '127.0.0.1', '2020-07-06 17:08:41', '2020-07-06 17:08:41'),
(23, 1, '2c9e8e92d0e484e62628894898', '127.0.0.1', '2020-07-06 17:03:34', '2020-07-06 17:04:12');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article`
--

CREATE TABLE `oc_article` (
  `article_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `date_available` date NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `article_review` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `noindex` tinyint(1) NOT NULL DEFAULT 1,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int(5) NOT NULL DEFAULT 0,
  `gstatus` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_description`
--

CREATE TABLE `oc_article_description` (
  `article_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `tag` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_image`
--

CREATE TABLE `oc_article_image` (
  `article_image_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_related`
--

CREATE TABLE `oc_article_related` (
  `article_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_related_mn`
--

CREATE TABLE `oc_article_related_mn` (
  `article_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_related_product`
--

CREATE TABLE `oc_article_related_product` (
  `article_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_article_related_product`
--

INSERT INTO `oc_article_related_product` (`article_id`, `product_id`) VALUES
(30, 123),
(31, 123),
(43, 123),
(45, 123);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_related_wb`
--

CREATE TABLE `oc_article_related_wb` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_to_blog_category`
--

CREATE TABLE `oc_article_to_blog_category` (
  `article_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL,
  `main_blog_category` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_to_download`
--

CREATE TABLE `oc_article_to_download` (
  `article_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_to_layout`
--

CREATE TABLE `oc_article_to_layout` (
  `article_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_to_store`
--

CREATE TABLE `oc_article_to_store` (
  `article_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute`
--

CREATE TABLE `oc_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_description`
--

CREATE TABLE `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_group`
--

CREATE TABLE `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_group_description`
--

CREATE TABLE `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_banner`
--

CREATE TABLE `oc_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_banner`
--

INSERT INTO `oc_banner` (`banner_id`, `name`, `status`) VALUES
(6, 'HP Products', 1),
(7, 'Home Page Slideshow', 1),
(8, 'Manufacturers', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_banner_image`
--

INSERT INTO `oc_banner_image` (`banner_image_id`, `banner_id`, `language_id`, `title`, `link`, `image`, `sort_order`) VALUES
(100, 7, 1, 'MacBookAir', '', 'catalog/demo/banners/MacBookAir.jpg', 0),
(103, 6, 1, 'HP Banner', 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'catalog/demo/compaq_presario.jpg', 0),
(113, 8, 1, 'Disney', '', 'catalog/demo/manufacturer/disney.png', 0),
(112, 8, 1, 'Dell', '', 'catalog/demo/manufacturer/dell.png', 0),
(111, 8, 1, 'Harley Davidson', '', 'catalog/demo/manufacturer/harley.png', 0),
(110, 8, 1, 'Canon', '', 'catalog/demo/manufacturer/canon.png', 0),
(109, 8, 1, 'Burger King', '', 'catalog/demo/manufacturer/burgerking.png', 0),
(108, 8, 1, 'Coca Cola', '', 'catalog/demo/manufacturer/cocacola.png', 0),
(107, 8, 1, 'Sony', '', 'catalog/demo/manufacturer/sony.png', 0),
(99, 7, 1, 'iPhone 6', 'index.php?route=product/product&amp;path=57&amp;product_id=49', 'catalog/demo/banners/iPhone6.jpg', 0),
(106, 8, 1, 'RedBull', '', 'catalog/demo/manufacturer/redbull.png', 0),
(105, 8, 1, 'NFL', '', 'catalog/demo/manufacturer/nfl.png', 0),
(101, 7, 2, 'iPhone 6', 'index.php?route=product/product&amp;path=57&amp;product_id=49', 'catalog/demo/banners/iPhone6.jpg', 0),
(102, 7, 2, 'MacBookAir', '', 'catalog/demo/banners/MacBookAir.jpg', 0),
(104, 6, 2, 'HP Banner', 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'catalog/demo/compaq_presario.jpg', 0),
(114, 8, 1, 'Starbucks', '', 'catalog/demo/manufacturer/starbucks.png', 0),
(115, 8, 1, 'Nintendo', '', 'catalog/demo/manufacturer/nintendo.png', 0),
(116, 8, 2, 'NFL', '', 'catalog/demo/manufacturer/nfl.png', 0),
(117, 8, 2, 'RedBull', '', 'catalog/demo/manufacturer/redbull.png', 0),
(118, 8, 2, 'Sony', '', 'catalog/demo/manufacturer/sony.png', 0),
(119, 8, 2, 'Coca Cola', '', 'catalog/demo/manufacturer/cocacola.png', 0),
(120, 8, 2, 'Burger King', '', 'catalog/demo/manufacturer/burgerking.png', 0),
(121, 8, 2, 'Canon', '', 'catalog/demo/manufacturer/canon.png', 0),
(122, 8, 2, 'Harley Davidson', '', 'catalog/demo/manufacturer/harley.png', 0),
(123, 8, 2, 'Dell', '', 'catalog/demo/manufacturer/dell.png', 0),
(124, 8, 2, 'Disney', '', 'catalog/demo/manufacturer/disney.png', 0),
(125, 8, 2, 'Starbucks', '', 'catalog/demo/manufacturer/starbucks.png', 0),
(126, 8, 2, 'Nintendo', '', 'catalog/demo/manufacturer/nintendo.png', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category`
--

CREATE TABLE `oc_blog_category` (
  `blog_category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT 1,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category_description`
--

CREATE TABLE `oc_blog_category_description` (
  `blog_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category_path`
--

CREATE TABLE `oc_blog_category_path` (
  `blog_category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category_to_layout`
--

CREATE TABLE `oc_blog_category_to_layout` (
  `blog_category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category_to_store`
--

CREATE TABLE `oc_blog_category_to_store` (
  `blog_category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cart`
--

CREATE TABLE `oc_cart` (
  `cart_id` int(11) UNSIGNED NOT NULL,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category`
--

CREATE TABLE `oc_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`, `noindex`) VALUES
(50, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(51, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(52, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(53, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(54, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(55, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(56, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(57, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(58, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(59, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(60, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(61, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(62, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(63, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(64, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(65, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(66, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(67, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(68, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(69, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(70, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(71, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(72, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(73, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(74, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(75, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(76, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(77, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(78, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(79, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(80, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(81, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1),
(82, NULL, 0, 1, 1, 0, 1, '2020-07-14 23:37:31', '2021-01-01 16:02:01', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category_description`
--

INSERT INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(82, 1, 'Вставка карманная', 'Вставка карманная', 'Купить Вставка карманная в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка карманная в магазине ВентСтройСнаб по лучшей цене', 'Вставка карманная,ВентСтройСнаб', 'Вставка карманная'),
(81, 1, 'Обратные клапаны', 'Обратные клапаны', 'Купить Обратные клапаны в ВентСтройСнаб по лучшей цене', 'Покупайте Обратные клапаны в магазине ВентСтройСнаб по лучшей цене', 'Обратные клапаны,ВентСтройСнаб', 'Обратные клапаны'),
(80, 1, 'Решетки', 'Решетки', 'Купить Решетки в ВентСтройСнаб по лучшей цене', 'Покупайте Решетки в магазине ВентСтройСнаб по лучшей цене', 'Решетки,ВентСтройСнаб', 'Решетки'),
(79, 1, 'Частотные преобразователи', 'Частотные преобразователи', 'Купить Частотные преобразователи в ВентСтройСнаб по лучшей цене', 'Покупайте Частотные преобразователи в магазине ВентСтройСнаб по лучшей цене', 'Частотные преобразователи,ВентСтройСнаб', 'Частотные преобразователи'),
(78, 1, 'Шумоглушители', 'Шумоглушители', 'Купить Шумоглушители в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушители в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушители,ВентСтройСнаб', 'Шумоглушители'),
(71, 1, 'Датчики', 'Датчики', 'Купить Датчики в ВентСтройСнаб по лучшей цене', 'Покупайте Датчики в магазине ВентСтройСнаб по лучшей цене', 'Датчики,ВентСтройСнаб', 'Датчики'),
(72, 1, 'Воздушный клапан', 'Воздушный клапан', 'Купить Воздушный клапан в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан,ВентСтройСнаб', 'Воздушный клапан'),
(77, 1, 'Щиты управления', 'Щиты управления', 'Купить Щиты управления в ВентСтройСнаб по лучшей цене', 'Покупайте Щиты управления в магазине ВентСтройСнаб по лучшей цене', 'Щиты управления,ВентСтройСнаб', 'Щиты управления'),
(76, 1, 'Электрические нагреватели', 'Электрические нагреватели', 'Купить Электрические нагреватели в ВентСтройСнаб по лучшей цене', 'Покупайте Электрические нагреватели в магазине ВентСтройСнаб по лучшей цене', 'Электрические нагреватели,ВентСтройСнаб', 'Электрические нагреватели'),
(75, 1, 'Вентиляторы', 'Вентиляторы', 'Купить Вентиляторы в ВентСтройСнаб по лучшей цене', 'Покупайте Вентиляторы в магазине ВентСтройСнаб по лучшей цене', 'Вентиляторы,ВентСтройСнаб', 'Вентиляторы'),
(73, 1, 'Воздуховоды', 'Воздуховоды', 'Купить Воздуховоды в ВентСтройСнаб по лучшей цене', 'Покупайте Воздуховоды в магазине ВентСтройСнаб по лучшей цене', 'Воздуховоды,ВентСтройСнаб', 'Воздуховоды'),
(74, 1, 'Нагреватели', 'Нагреватели', 'Купить Нагреватели в ВентСтройСнаб по лучшей цене', 'Покупайте Нагреватели в магазине ВентСтройСнаб по лучшей цене', 'Нагреватели,ВентСтройСнаб', 'Нагреватели'),
(64, 1, 'Панель управления', 'Панель управления', 'Купить Панель управления в ВентСтройСнаб по лучшей цене', 'Покупайте Панель управления в магазине ВентСтройСнаб по лучшей цене', 'Панель управления,ВентСтройСнаб', 'Панель управления'),
(65, 1, 'Комплект NEMA', 'Комплект NEMA', 'Купить Комплект NEMA в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект NEMA в магазине ВентСтройСнаб по лучшей цене', 'Комплект NEMA,ВентСтройСнаб', 'Комплект NEMA'),
(66, 1, 'Противопожарные клапаны', 'Противопожарные клапаны', 'Купить Противопожарные клапаны в ВентСтройСнаб по лучшей цене', 'Покупайте Противопожарные клапаны в магазине ВентСтройСнаб по лучшей цене', 'Противопожарные клапаны,ВентСтройСнаб', 'Противопожарные клапаны'),
(67, 1, 'Фильтры', 'Фильтры', 'Купить Фильтры в ВентСтройСнаб по лучшей цене', 'Покупайте Фильтры в магазине ВентСтройСнаб по лучшей цене', 'Фильтры,ВентСтройСнаб', 'Фильтры'),
(68, 1, 'Зонты', 'Зонты', 'Купить Зонты в ВентСтройСнаб по лучшей цене', 'Покупайте Зонты в магазине ВентСтройСнаб по лучшей цене', 'Зонты,ВентСтройСнаб', 'Зонты'),
(61, 1, 'Регуляторы скорости', 'Регуляторы скорости', 'Купить Регуляторы скорости в ВентСтройСнаб по лучшей цене', 'Покупайте Регуляторы скорости в магазине ВентСтройСнаб по лучшей цене', 'Регуляторы скорости,ВентСтройСнаб', 'Регуляторы скорости'),
(70, 1, 'Дифузоры', 'Дифузоры', 'Купить Дифузоры в ВентСтройСнаб по лучшей цене', 'Покупайте Дифузоры в магазине ВентСтройСнаб по лучшей цене', 'Дифузоры,ВентСтройСнаб', 'Дифузоры'),
(69, 1, 'Дроссель клапаны', 'Дроссель клапаны', 'Купить Дроссель клапаны в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапаны в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапаны,ВентСтройСнаб', 'Дроссель клапаны'),
(63, 1, 'Помпы', 'Помпы', 'Купить Помпы в ВентСтройСнаб по лучшей цене', 'Покупайте Помпы в магазине ВентСтройСнаб по лучшей цене', 'Помпы,ВентСтройСнаб', 'Помпы'),
(62, 1, 'Пульт управления', 'Пульт управления', 'Купить Пульт управления в ВентСтройСнаб по лучшей цене', 'Покупайте Пульт управления в магазине ВентСтройСнаб по лучшей цене', 'Пульт управления,ВентСтройСнаб', 'Пульт управления'),
(60, 1, 'Рекуператоры', 'Рекуператоры', 'Купить Рекуператоры в ВентСтройСнаб по лучшей цене', 'Покупайте Рекуператоры в магазине ВентСтройСнаб по лучшей цене', 'Рекуператоры,ВентСтройСнаб', 'Рекуператоры'),
(58, 1, 'Термостаты', 'Термостаты', 'Купить Термостаты в ВентСтройСнаб по лучшей цене', 'Покупайте Термостаты в магазине ВентСтройСнаб по лучшей цене', 'Термостаты,ВентСтройСнаб', 'Термостаты'),
(56, 1, 'Электроприводы', 'Электроприводы', 'Купить Электроприводы в ВентСтройСнаб по лучшей цене', 'Покупайте Электроприводы в магазине ВентСтройСнаб по лучшей цене', 'Электроприводы,ВентСтройСнаб', 'Электроприводы'),
(59, 1, 'Смесительные узлы', 'Смесительные узлы', 'Купить Смесительные узлы в ВентСтройСнаб по лучшей цене', 'Покупайте Смесительные узлы в магазине ВентСтройСнаб по лучшей цене', 'Смесительные узлы,ВентСтройСнаб', 'Смесительные узлы'),
(57, 1, 'Фильтр-боксы', 'Фильтр-боксы', 'Купить Фильтр-боксы в ВентСтройСнаб по лучшей цене', 'Покупайте Фильтр-боксы в магазине ВентСтройСнаб по лучшей цене', 'Фильтр-боксы,ВентСтройСнаб', 'Фильтр-боксы'),
(51, 1, 'Расходный материал', 'Расходный материал', 'Купить Расходный материал в ВентСтройСнаб по лучшей цене', 'Покупайте Расходный материал в магазине ВентСтройСнаб по лучшей цене', 'Расходный материал,ВентСтройСнаб', 'Расходный материал'),
(55, 1, 'Вставка фильтрующая', 'Вставка фильтрующая', 'Купить Вставка фильтрующая в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка фильтрующая в магазине ВентСтройСнаб по лучшей цене', 'Вставка фильтрующая,ВентСтройСнаб', 'Вставка фильтрующая'),
(53, 1, 'Теплоизоляция', 'Теплоизоляция', 'Купить Теплоизоляция в ВентСтройСнаб по лучшей цене', 'Покупайте Теплоизоляция в магазине ВентСтройСнаб по лучшей цене', 'Теплоизоляция,ВентСтройСнаб', 'Теплоизоляция'),
(54, 1, 'Вставка гибкая', 'Вставка гибкая', 'Купить Вставка гибкая в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка гибкая в магазине ВентСтройСнаб по лучшей цене', 'Вставка гибкая,ВентСтройСнаб', 'Вставка гибкая'),
(52, 1, 'Провода', 'Провода', 'Купить Провода в ВентСтройСнаб по лучшей цене', 'Покупайте Провода в магазине ВентСтройСнаб по лучшей цене', 'Провода,ВентСтройСнаб', 'Провода'),
(50, 1, 'Зонты вытяжные', 'Зонты вытяжные', 'Купить Зонты вытяжные в ВентСтройСнаб по лучшей цене', 'Покупайте Зонты вытяжные в магазине ВентСтройСнаб по лучшей цене', 'Зонты вытяжные,ВентСтройСнаб', 'Зонты вытяжные');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_filter`
--

CREATE TABLE `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_path`
--

CREATE TABLE `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category_path`
--

INSERT INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(82, 82, 0),
(81, 81, 0),
(80, 80, 0),
(79, 79, 0),
(78, 78, 0),
(77, 77, 0),
(76, 76, 0),
(75, 75, 0),
(74, 74, 0),
(73, 73, 0),
(72, 72, 0),
(71, 71, 0),
(70, 70, 0),
(69, 69, 0),
(68, 68, 0),
(67, 67, 0),
(66, 66, 0),
(65, 65, 0),
(64, 64, 0),
(63, 63, 0),
(62, 62, 0),
(61, 61, 0),
(60, 60, 0),
(59, 59, 0),
(58, 58, 0),
(57, 57, 0),
(56, 56, 0),
(55, 55, 0),
(54, 54, 0),
(53, 53, 0),
(52, 52, 0),
(51, 51, 0),
(50, 50, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_to_layout`
--

CREATE TABLE `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_to_store`
--

CREATE TABLE `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category_to_store`
--

INSERT INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 0),
(77, 0),
(78, 0),
(79, 0),
(80, 0),
(81, 0),
(82, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_country`
--

CREATE TABLE `oc_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Армения', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Азербайджан', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Белоруссия (Беларусь)', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Грузия', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Казахстан', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Киргизия (Кыргызстан)', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Молдова', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Российская Федерация', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Таджикистан', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Туркменистан', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Украина', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Узбекистан', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon`
--

CREATE TABLE `oc_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_coupon`
--

INSERT INTO `oc_coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`) VALUES
(4, '-10% Discount', '2222', 'P', '10.0000', 0, 0, '0.0000', '2014-01-01', '2020-01-01', 10, '10', 0, '2009-01-27 13:55:03'),
(5, 'Free Shipping', '3333', 'P', '0.0000', 0, 1, '100.0000', '2014-01-01', '2014-02-01', 10, '10', 0, '2009-03-14 21:13:53'),
(6, '-10.00 Discount', '1111', 'F', '10.0000', 0, 0, '10.0000', '2014-01-01', '2020-01-01', 100000, '10000', 0, '2009-03-14 21:15:18');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon_category`
--

CREATE TABLE `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon_history`
--

CREATE TABLE `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon_product`
--

CREATE TABLE `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_currency`
--

CREATE TABLE `oc_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` double(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Рубль', 'RUB', '', ' ₽', '2', 1.00000000, 1, '2021-01-21 02:07:29'),
(2, 'US Dollar', 'USD', '$', '', '2', 0.01697793, 1, '2017-07-19 21:28:21'),
(3, 'Euro', 'EUR', '', '€', '2', 0.01476363, 1, '2017-07-19 21:28:21'),
(4, 'Гривна', 'UAH', '', 'грн.', '2', 0.44016022, 1, '2017-07-19 21:28:21');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer`
--

CREATE TABLE `oc_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `language_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text DEFAULT NULL,
  `wishlist` text DEFAULT NULL,
  `newsletter` tinyint(1) NOT NULL DEFAULT 0,
  `address_id` int(11) NOT NULL DEFAULT 0,
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `code` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_activity`
--

CREATE TABLE `oc_customer_activity` (
  `customer_activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_affiliate`
--

CREATE TABLE `oc_customer_affiliate` (
  `customer_id` int(11) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT 0.00,
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `custom_field` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_approval`
--

CREATE TABLE `oc_customer_approval` (
  `customer_approval_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` varchar(9) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_group`
--

CREATE TABLE `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_group`
--

INSERT INTO `oc_customer_group` (`customer_group_id`, `approval`, `sort_order`) VALUES
(1, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_group_description`
--

CREATE TABLE `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_group_description`
--

INSERT INTO `oc_customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Default', 'test');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_history`
--

CREATE TABLE `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_ip`
--

CREATE TABLE `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_login`
--

CREATE TABLE `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_online`
--

CREATE TABLE `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_reward`
--

CREATE TABLE `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `order_id` int(11) NOT NULL DEFAULT 0,
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_search`
--

CREATE TABLE `oc_customer_search` (
  `customer_search_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_transaction`
--

CREATE TABLE `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_wishlist`
--

CREATE TABLE `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field`
--

CREATE TABLE `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_customer_group`
--

CREATE TABLE `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_description`
--

CREATE TABLE `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_value`
--

CREATE TABLE `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_value_description`
--

CREATE TABLE `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_download`
--

CREATE TABLE `oc_download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_download_description`
--

CREATE TABLE `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_event`
--

CREATE TABLE `oc_event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_event`
--

INSERT INTO `oc_event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(1, 'activity_customer_add', 'catalog/model/account/customer/addCustomer/after', 'event/activity/addCustomer', 1, 0),
(2, 'activity_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'event/activity/editCustomer', 1, 0),
(3, 'activity_customer_password', 'catalog/model/account/customer/editPassword/after', 'event/activity/editPassword', 1, 0),
(4, 'activity_customer_forgotten', 'catalog/model/account/customer/editCode/after', 'event/activity/forgotten', 1, 0),
(5, 'activity_transaction', 'catalog/model/account/customer/addTransaction/after', 'event/activity/addTransaction', 1, 0),
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 0),
(7, 'activity_address_add', 'catalog/model/account/address/addAddress/after', 'event/activity/addAddress', 1, 0),
(8, 'activity_address_edit', 'catalog/model/account/address/editAddress/after', 'event/activity/editAddress', 1, 0),
(9, 'activity_address_delete', 'catalog/model/account/address/deleteAddress/after', 'event/activity/deleteAddress', 1, 0),
(10, 'activity_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'event/activity/addAffiliate', 1, 0),
(11, 'activity_affiliate_edit', 'catalog/model/account/customer/editAffiliate/after', 'event/activity/editAffiliate', 1, 0),
(12, 'activity_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'event/activity/addOrderHistory', 1, 0),
(13, 'activity_return_add', 'catalog/model/account/return/addReturn/after', 'event/activity/addReturn', 1, 0),
(14, 'mail_transaction', 'catalog/model/account/customer/addTransaction/after', 'mail/transaction', 1, 0),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 0),
(16, 'mail_customer_add', 'catalog/model/account/customer/addCustomer/after', 'mail/register', 1, 0),
(17, 'mail_customer_alert', 'catalog/model/account/customer/addCustomer/after', 'mail/register/alert', 1, 0),
(18, 'mail_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate', 1, 0),
(19, 'mail_affiliate_alert', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate/alert', 1, 0),
(20, 'mail_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 1, 0),
(21, 'mail_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order', 1, 0),
(22, 'mail_order_alert', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order/alert', 1, 0),
(23, 'statistics_review_add', 'catalog/model/catalog/review/addReview/after', 'event/statistics/addReview', 1, 0),
(24, 'statistics_return_add', 'catalog/model/account/return/addReturn/after', 'event/statistics/addReturn', 1, 0),
(25, 'statistics_order_history', 'catalog/model/checkout/order/addOrderHistory/after', 'event/statistics/addOrderHistory', 1, 0),
(26, 'admin_mail_affiliate_approve', 'admin/model/customer/customer_approval/approveAffiliate/after', 'mail/affiliate/approve', 1, 0),
(27, 'admin_mail_affiliate_deny', 'admin/model/customer/customer_approval/denyAffiliate/after', 'mail/affiliate/deny', 1, 0),
(28, 'admin_mail_customer_approve', 'admin/model/customer/customer_approval/approveCustomer/after', 'mail/customer/approve', 1, 0),
(29, 'admin_mail_customer_deny', 'admin/model/customer/customer_approval/denyCustomer/after', 'mail/customer/deny', 1, 0),
(30, 'admin_mail_reward', 'admin/model/customer/customer/addReward/after', 'mail/reward', 1, 0),
(31, 'admin_mail_transaction', 'admin/model/customer/customer/addTransaction/after', 'mail/transaction', 1, 0),
(32, 'admin_mail_return', 'admin/model/sale/return/addReturn/after', 'mail/return', 1, 0),
(33, 'admin_mail_forgotten', 'admin/model/user/user/editCode/after', 'mail/forgotten', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_extension`
--

CREATE TABLE `oc_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(3, 'total', 'sub_total'),
(5, 'total', 'total'),
(48, 'shipping', 'free'),
(49, 'module', 'manager'),
(17, 'payment', 'free_checkout'),
(47, 'module', 'trade_import'),
(20, 'theme', 'default'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_extension_install`
--

CREATE TABLE `oc_extension_install` (
  `extension_install_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_extension_install`
--

INSERT INTO `oc_extension_install` (`extension_install_id`, `extension_download_id`, `filename`, `date_added`) VALUES
(2, 0, 'localcopy_oc3.ocmod.zip', '2020-03-18 15:27:49'),
(3, 0, 'order_manager_oc3.ocmod.zip', '2020-03-18 15:28:01'),
(4, 0, 'opencart3.0fixes.ocmod.zip', '2020-07-06 17:06:23'),
(5, 0, 'sessions_gc.ocmod.zip', '2020-07-06 17:07:16');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_extension_path`
--

CREATE TABLE `oc_extension_path` (
  `extension_path_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_extension_path`
--

INSERT INTO `oc_extension_path` (`extension_path_id`, `extension_install_id`, `path`, `date_added`) VALUES
(1, 3, 'admin/controller/dashboard', '2020-03-18 15:28:01'),
(2, 3, 'admin/view/fontawesome', '2020-03-18 15:28:01'),
(3, 3, 'admin/controller/dashboard/manager.php', '2020-03-18 15:28:01'),
(4, 3, 'admin/model/sale/manager.php', '2020-03-18 15:28:01'),
(5, 3, 'admin/view/fontawesome/css', '2020-03-18 15:28:01'),
(6, 3, 'admin/view/fontawesome/fonts', '2020-03-18 15:28:01'),
(7, 3, 'admin/view/fontawesome/less', '2020-03-18 15:28:01'),
(8, 3, 'admin/view/fontawesome/scss', '2020-03-18 15:28:01'),
(9, 3, 'admin/view/stylesheet/manager.css', '2020-03-18 15:28:01'),
(10, 3, 'admin/view/template/dashboard', '2020-03-18 15:28:01'),
(11, 3, 'admin/controller/extension/module/manager.php', '2020-03-18 15:28:01'),
(12, 3, 'admin/view/fontawesome/css/font-awesome.css', '2020-03-18 15:28:01'),
(13, 3, 'admin/view/fontawesome/css/font-awesome.min.css', '2020-03-18 15:28:01'),
(14, 3, 'admin/view/fontawesome/fonts/FontAwesome.otf', '2020-03-18 15:28:01'),
(15, 3, 'admin/view/fontawesome/fonts/fontawesome-webfont.eot', '2020-03-18 15:28:01'),
(16, 3, 'admin/view/fontawesome/fonts/fontawesome-webfont.svg', '2020-03-18 15:28:01'),
(17, 3, 'admin/view/fontawesome/fonts/fontawesome-webfont.ttf', '2020-03-18 15:28:01'),
(18, 3, 'admin/view/fontawesome/fonts/fontawesome-webfont.woff', '2020-03-18 15:28:01'),
(19, 3, 'admin/view/fontawesome/fonts/fontawesome-webfont.woff2', '2020-03-18 15:28:01'),
(20, 3, 'admin/view/fontawesome/less/animated.less', '2020-03-18 15:28:01'),
(21, 3, 'admin/view/fontawesome/less/bordered-pulled.less', '2020-03-18 15:28:01'),
(22, 3, 'admin/view/fontawesome/less/core.less', '2020-03-18 15:28:01'),
(23, 3, 'admin/view/fontawesome/less/fixed-width.less', '2020-03-18 15:28:01'),
(24, 3, 'admin/view/fontawesome/less/font-awesome.less', '2020-03-18 15:28:01'),
(25, 3, 'admin/view/fontawesome/less/icons.less', '2020-03-18 15:28:01'),
(26, 3, 'admin/view/fontawesome/less/larger.less', '2020-03-18 15:28:01'),
(27, 3, 'admin/view/fontawesome/less/list.less', '2020-03-18 15:28:01'),
(28, 3, 'admin/view/fontawesome/less/mixins.less', '2020-03-18 15:28:01'),
(29, 3, 'admin/view/fontawesome/less/path.less', '2020-03-18 15:28:01'),
(30, 3, 'admin/view/fontawesome/less/rotated-flipped.less', '2020-03-18 15:28:01'),
(31, 3, 'admin/view/fontawesome/less/stacked.less', '2020-03-18 15:28:01'),
(32, 3, 'admin/view/fontawesome/less/variables.less', '2020-03-18 15:28:01'),
(33, 3, 'admin/view/fontawesome/scss/_animated.scss', '2020-03-18 15:28:01'),
(34, 3, 'admin/view/fontawesome/scss/_bordered-pulled.scss', '2020-03-18 15:28:01'),
(35, 3, 'admin/view/fontawesome/scss/_core.scss', '2020-03-18 15:28:01'),
(36, 3, 'admin/view/fontawesome/scss/_fixed-width.scss', '2020-03-18 15:28:01'),
(37, 3, 'admin/view/fontawesome/scss/_icons.scss', '2020-03-18 15:28:01'),
(38, 3, 'admin/view/fontawesome/scss/_larger.scss', '2020-03-18 15:28:01'),
(39, 3, 'admin/view/fontawesome/scss/_list.scss', '2020-03-18 15:28:01'),
(40, 3, 'admin/view/fontawesome/scss/_mixins.scss', '2020-03-18 15:28:01'),
(41, 3, 'admin/view/fontawesome/scss/_path.scss', '2020-03-18 15:28:01'),
(42, 3, 'admin/view/fontawesome/scss/_rotated-flipped.scss', '2020-03-18 15:28:01'),
(43, 3, 'admin/view/fontawesome/scss/_stacked.scss', '2020-03-18 15:28:01'),
(44, 3, 'admin/view/fontawesome/scss/_variables.scss', '2020-03-18 15:28:01'),
(45, 3, 'admin/view/fontawesome/scss/font-awesome.scss', '2020-03-18 15:28:01'),
(46, 3, 'admin/view/template/dashboard/manager.twig', '2020-03-18 15:28:01'),
(47, 3, 'admin/language/en-gb/extension/module/manager.php', '2020-03-18 15:28:01'),
(48, 3, 'admin/language/ru-ru/extension/module/manager.php', '2020-03-18 15:28:01'),
(49, 3, 'admin/view/template/extension/module/manager.twig', '2020-03-18 15:28:01'),
(50, 4, 'admin/controller/event/compatibility.php', '2020-07-06 17:06:23'),
(51, 4, 'catalog/controller/event/compatibility.php', '2020-07-06 17:06:23'),
(52, 4, 'admin/controller/extension/module/template_switcher.php', '2020-07-06 17:06:23'),
(53, 4, 'catalog/controller/extension/module/template_switcher.php', '2020-07-06 17:06:23'),
(54, 4, 'admin/language/en-gb/extension/module/template_switcher.php', '2020-07-06 17:06:23'),
(55, 4, 'admin/view/template/extension/module/template_switcher.twig', '2020-07-06 17:06:23');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter`
--

CREATE TABLE `oc_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter_description`
--

CREATE TABLE `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter_group`
--

CREATE TABLE `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter_group_description`
--

CREATE TABLE `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_geo_zone`
--

CREATE TABLE `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_added`, `date_modified`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2009-01-06 23:26:25', '2010-02-26 22:33:24'),
(4, 'UK Shipping', 'UK Shipping Zones', '2009-06-23 01:14:53', '2010-12-15 15:18:13');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information`
--

CREATE TABLE `oc_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT 0,
  `sort_order` int(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `noindex` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information`
--

INSERT INTO `oc_information` (`information_id`, `bottom`, `sort_order`, `status`, `noindex`) VALUES
(3, 1, 3, 1, 1),
(4, 1, 1, 1, 0),
(5, 1, 4, 1, 1),
(6, 1, 2, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information_description`
--

CREATE TABLE `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` mediumtext NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information_description`
--

INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(4, 1, 'О нас', '&lt;p&gt;О нас&lt;br&gt;&lt;/p&gt;\r\n', '', '', '', ''),
(5, 1, 'Условия соглашения', '&lt;p&gt;\r\n	Условия соглашения&lt;/p&gt;\r\n', '', '', '', ''),
(3, 1, 'Политика безопасности', '&lt;p&gt;\r\n	Политика безопасности&lt;/p&gt;\r\n', '', '', '', ''),
(6, 1, 'Информация о доставке', '&lt;p&gt;\r\n	Информация о доставке&lt;/p&gt;\r\n', '', '', '', ''),
(4, 2, 'About Us', '&lt;p&gt;About Us&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(6, 2, 'Delivery Information', '&lt;p&gt;Delivery Information&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(5, 2, 'Terms &amp; Conditions', '&lt;p&gt;Terms &amp;amp; Conditions&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(3, 2, 'Privacy Policy', '&lt;p&gt;Privacy Policy&lt;br&gt;&lt;/p&gt;', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information_to_layout`
--

CREATE TABLE `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information_to_store`
--

CREATE TABLE `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information_to_store`
--

INSERT INTO `oc_information_to_store` (`information_id`, `store_id`) VALUES
(3, 0),
(4, 0),
(5, 0),
(6, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_language`
--

CREATE TABLE `oc_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'Russian', 'ru-ru', 'ru_RU.UTF-8,ru_RU,russian', 'gb.png', 'english', 1, 1),
(2, 'English', 'en-gb', 'en-US,en_US.UTF-8,en_US,en-gb,english', '', '', 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_layout`
--

CREATE TABLE `oc_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Главная'),
(2, 'Товар'),
(3, 'Категория'),
(4, 'По-умолчанию'),
(5, 'Список Производителей'),
(6, 'Аккаунт'),
(7, 'Оформление заказа'),
(8, 'Контакты'),
(9, 'Карта сайта'),
(10, 'Партнерская программа'),
(11, 'Информация'),
(12, 'Сравнение'),
(13, 'Поиск'),
(14, 'Блог'),
(15, 'Категории Блога'),
(16, 'Статьи Блога'),
(17, 'Страница Производителя');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_layout_module`
--

CREATE TABLE `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(20, 5, '0', 'column_left', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_layout_route`
--

CREATE TABLE `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(38, 6, 0, 'account/%'),
(17, 10, 0, 'affiliate/%'),
(44, 3, 0, 'product/category'),
(42, 1, 0, 'common/home'),
(20, 2, 0, 'product/product'),
(24, 11, 0, 'information/information'),
(23, 7, 0, 'checkout/%'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap'),
(34, 4, 0, ''),
(45, 5, 0, 'product/manufacturer'),
(52, 12, 0, 'product/compare'),
(53, 13, 0, 'product/search'),
(57, 14, 0, 'blog/latest'),
(58, 15, 0, 'blog/category'),
(56, 16, 0, 'blog/article'),
(63, 17, 0, 'product/manufacturer/info');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Сантиметр', 'см'),
(1, 2, 'Centimeter', 'cm'),
(2, 1, 'Миллиметр', 'мм'),
(2, 2, 'Millimeter', 'mm'),
(3, 1, 'Дюйм', 'in'),
(3, 2, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_location`
--

CREATE TABLE `oc_location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer_description`
--

CREATE TABLE `oc_manufacturer_description` (
  `manufacturer_id` int(11) NOT NULL DEFAULT 0,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `description` text NOT NULL,
  `description3` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer_to_layout`
--

CREATE TABLE `oc_manufacturer_to_layout` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer_to_store`
--

CREATE TABLE `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_marketing`
--

CREATE TABLE `oc_marketing` (
  `marketing_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_modification`
--

CREATE TABLE `oc_modification` (
  `modification_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_modification`
--

INSERT INTO `oc_modification` (`modification_id`, `extension_install_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`) VALUES
(1, 2, 'Localcopy OCMOD Install Fix', 'localcopy-oc3', 'opencart3x.ru', '1.0', 'https://opencart3x.ru', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n  <name>Localcopy OCMOD Install Fix</name>\n  <code>localcopy-oc3</code>\n  <version>1.0</version>\n  <author>opencart3x.ru</author>\n  <link>https://opencart3x.ru</link>\n\n  <file path=\"admin/controller/marketplace/install.php\">\n	<operation>\n      <search>\n        <![CDATA[if ($safe) {]]>\n      </search>\n      <add position=\"before\">\n        <![CDATA[		\n		    $safe = true;\n		    ]]>\n      </add>\n    </operation>\n    <operation>\n      <search>\n        <![CDATA[if (is_dir($file) && !is_dir($path)) {]]>\n      </search>\n      <add position=\"before\">\n        <![CDATA[		\n			  if ($path == \'\') {\n  				$app_root = explode(\'/\',DIR_APPLICATION);\n  				unset($app_root[count($app_root)-2]);\n  				$app_root = implode(\'/\',$app_root);\n  				$path = $app_root . $destination;\n			  }\n		    ]]>\n      </add>\n    </operation>\n  </file> \n</modification>\n', 1, '2020-03-18 15:27:49'),
(2, 3, 'Order Manager', 'Order-Manager', 'opencart3x.ru', '3.0', 'https://opencart3x.ru', '<modification>\r\n	<name>Order Manager</name>\r\n	<code>Order-Manager</code>\r\n	<version>3.0</version>\r\n	<author>opencart3x.ru</author>\r\n	<link>https://opencart3x.ru</link>\r\n\r\n	<file path=\"admin/controller/common/dashboard.php\">	\r\n		<operation>\r\n			<search><![CDATA[$data[\'footer\'] = $this->load->controller(\'common/footer\');]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			if ($this->config->get(\'module_manager_status\')) {\r\n				$data[\'manager\'] = $this->load->controller(\'dashboard/manager\');\r\n				\r\n				$data[\'module_manager_status\'] = $this->config->get(\'module_manager_status\');\r\n				$data[\'module_manager_hide_dashboard\'] = $this->config->get(\'module_manager_hide_dashboard\');\r\n\r\n				if (isset($this->session->data[\'module_manager_success\'])) {\r\n					$data[\'module_manager_success\'] = $this->session->data[\'module_manager_success\'];\r\n					unset($this->session->data[\'module_manager_success\']);\r\n				} else {\r\n					$data[\'module_manager_success\'] = \"\";\r\n				}\r\n\r\n				if (isset($this->session->data[\'module_manager_error\'])) {\r\n					$data[\'module_manager_error\'] = $this->session->data[\'module_manager_error\'];\r\n					unset($this->session->data[\'module_manager_error\']);\r\n				} else {\r\n					$data[\'module_manager_error\'] = \"\";\r\n				}\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<file path=\"admin/view/template/common/dashboard.twig\">\r\n		<operation>\r\n			<search><![CDATA[{% if error_install %}]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			{% if module_manager_status  and  module_manager_status %} \r\n				{% if module_manager_success  and  module_manager_success %} \r\n  					<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> {{ module_manager_success }} \r\n      					<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    				</div>\r\n  				{% endif %} \r\n  			\r\n				{% if module_manager_error  and  module_manager_error %} \r\n  					<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> {{ module_manager_error }} \r\n      					<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    				</div>\r\n  				{% endif %} \r\n  			{% endif %} \r\n  						\r\n			]]></add>\r\n		</operation>  				\r\n		<operation>\r\n			<search><![CDATA[{% for row in rows %}]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			{% if module_manager_status  and  module_manager_status %} \r\n				{{ manager }} \r\n			{% endif %} \r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[<div class=\"row\">]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n\r\n			<div class=\"row\" {% if module_manager_hide_dashboard  and  module_manager_hide_dashboard %} {{ \' style=\"display: none;\"\' }} {% endif %} >\r\n			\r\n			]]></add>\r\n		</operation>								\r\n	</file>\r\n\r\n	<!-- Admin: Customer -->\r\n\r\n	<file path=\"admin/controller/customer/customer.php\">	\r\n		<operation>\r\n			<search><![CDATA[$this->response->redirect($this->url->link(]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'return\'])) {\r\n				$this->response->redirect($this->url->link(\'common/dashboard\', base64_decode($this->request->get[\'return\']), true));\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'customer/customer_form\']]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'return\'])) {\r\n				$url = \'user_token=\'.$this->session->data[\'user_token\'].\'&return=\'.$this->request->get[\'return\'];\r\n				\r\n				if (!isset($this->request->get[\'customer_id\'])) {\r\n					$data[\'action\'] = $this->url->link(\'customer/customer/add\', $url, true);\r\n				} else {\r\n					$data[\'action\'] = $this->url->link(\'customer/customer/edit\', $url.\'&customer_id=\'.$this->request->get[\'customer_id\'], true);\r\n				}\r\n\r\n				$data[\'cancel\'] = $this->url->link(\'common/dashboard\', base64_decode($this->request->get[\'return\']), true);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>				\r\n	</file>\r\n\r\n	<!-- Admin: Order -->\r\n\r\n	<file path=\"admin/controller/sale/order.php\">	\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'sale/order_form\']]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'return\'])) {\r\n				$data[\'cancel\'] = $this->url->link(\'common/dashboard\', base64_decode($this->request->get[\'return\']), true);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'sale/order_info\']]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'return\'])) {\r\n				$data[\'cancel\'] = $this->url->link(\'common/dashboard\', base64_decode($this->request->get[\'return\']), true);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/api/login.php\">	\r\n		<operation error=\"skip\">\r\n			<search><![CDATA[if (!in_array($this->request->server[\'REMOTE_ADDR\'], $ip_data)) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			$ip_data[] = $this->request->server[\'REMOTE_ADDR\'];\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<file path=\"catalog/controller/api/*.php\">	\r\n		<operation error=\"skip\">\r\n			<search><![CDATA[if (!isset($this->session->data[\'api_id\'])) {]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			\r\n			if (isset($this->session->data[\'opencart3x\'])) {\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n</modification>', 1, '2020-03-18 15:28:01'),
(3, 4, 'OpenCart 3.0 Compatibility Fixes', 'compatibility_fixes', 'The Krotek', '3.1.0', 'https://thekrotek.com', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n	<name>OpenCart 3.0 Compatibility Fixes</name>\r\n	<code>compatibility_fixes</code>\r\n	<version>3.1.0</version>\r\n	<author>The Krotek</author>\r\n    <link>https://thekrotek.com</link>\r\n 			\r\n	<!-- System: Loader -->\r\n\r\n	<file path=\"system/engine/loader.php\">\r\n		<operation>\r\n			<search><![CDATA[if (!$this->registry->has(\'model_\']]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			$this->registry->get(\'event\')->trigger(\'model/\'.$route.\'/before\', array(&$route));\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[throw new \\Exception(\'Error: Could not load model \']]></search>\r\n			<add position=\"after\" offset=\"1\"><![CDATA[\r\n			\r\n			$this->registry->get(\'event\')->trigger(\'model/\'.$route.\'/after\', array(&$route));\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[include_once($file);]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			\r\n			include_once(modification($file));\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<!-- System: Language -->\r\n\r\n	<file path=\"system/library/language.php\">\r\n		<operation>\r\n			<search><![CDATA[private $default = \'en-gb\';]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			\r\n			public $default = \'en-gb\';\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[public function load(]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n	\r\n			public function load($filename, $key = \'\')\r\n			{\r\n				if (!$key) {\r\n					$_ = array();\r\n\r\n					$file = DIR_LANGUAGE.\'english/\'.$filename.\'.php\';\r\n					$old_file = DIR_LANGUAGE.\'english/\'.str_replace(\'extension/\', \'\', $filename).\'.php\';\r\n		\r\n					if (is_file($file)) require(modification($file));\r\n					elseif (is_file($old_file)) require(modification($old_file));\r\n			\r\n					$file = DIR_LANGUAGE.$this->default.\'/\'.$filename.\'.php\';\r\n					$old_file = DIR_LANGUAGE.$this->default.\'/\'.str_replace(\'extension/\', \'\', $filename).\'.php\';\r\n		\r\n					if (is_file($file)) require(modification($file));\r\n					elseif (is_file($old_file)) require(modification($old_file));\r\n								\r\n					$file = DIR_LANGUAGE.$this->directory.\'/\'.$filename.\'.php\';\r\n					$old_file = DIR_LANGUAGE.$this->directory.\'/\'.str_replace(\'extension/\', \'\', $filename).\'.php\';\r\n		\r\n					if (is_file($file)) require(modification($file));\r\n					elseif (is_file($old_file)) require(modification($old_file));\r\n								\r\n					$this->data = array_merge($this->data, $_);\r\n				} else {\r\n					$this->data[$key] = new Language($this->directory);\r\n					$this->data[$key]->load($filename);\r\n				}\r\n		\r\n				return $this->data;\r\n			}\r\n				\r\n			public function loadDefault(]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<!-- System: Config -->\r\n	\r\n	<file path=\"system/config/admin.php\">\r\n		<operation>\r\n			<search><![CDATA[$_[\'action_event\'] = array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			\'model/*/before\' => array(\r\n				\'event/compatibility/beforemodel\'\r\n			),\r\n			\r\n			\'model/*/after\' => array(\r\n				\'event/compatibility/aftermodel\'\r\n			),\r\n	\r\n			\'language/*/after\' => array(\r\n				\'event/compatibility/language\',\r\n				\'event/translation\'\r\n			),\r\n				\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'controller/*/before\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			\'event/compatibility/controller\',\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'controller/*/after\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n\r\n			\'event/compatibility/controller\',\r\n			\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	\r\n	<file path=\"system/config/catalog.php\">\r\n		<operation>\r\n			<search><![CDATA[$_[\'action_event\'] = array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			\'model/*/before\' => array(\r\n				\'event/compatibility/beforemodel\'\r\n			),\r\n			\r\n			\'model/*/after\' => array(\r\n				\'event/compatibility/aftermodel\'\r\n			),\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'controller/*/before\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			\'event/compatibility/controller\',\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'controller/*/after\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n\r\n			\'event/compatibility/controller\',\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'language/*/after\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n						\r\n			\'event/compatibility/language\',\r\n					\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	   \r\n	<!-- Admin: Extensions -->\r\n\r\n	<file path=\"admin/controller/extension/extension/{analytics,captcha,dashboard,feed,fraud,menu,module,payment,report,shipping,theme,total}*.php\">\r\n		<operation>\r\n			<search><![CDATA[$this->load->controller(\'extension]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'extension\'])) {\r\n				$type = strtolower(str_replace(\'ControllerExtensionExtension\', \'\', __CLASS__));\r\n\r\n				if (__FUNCTION__ == \'install\') {\r\n					$this->model_user_user_group->addPermission($this->user->getGroupId(), \'access\', $type.\'/\'.$this->request->get[\'extension\']);\r\n					$this->model_user_user_group->addPermission($this->user->getGroupId(), \'modify\', $type.\'/\'.$this->request->get[\'extension\']);\r\n				}\r\n\r\n				$this->load->controller($type.\'/\'.$this->request->get[\'extension\'].\'/\'.__FUNCTION__);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'extension/extension]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($data[\'extensions\'])) {\r\n				$data[\'extensions\'] = array_unique($data[\'extensions\'], SORT_REGULAR);\r\n			\r\n				usort($data[\'extensions\'], function($a, $b){ return strcmp($a[\"name\"], $b[\"name\"]); });\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($files) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			$type = strtolower(str_replace(\'ControllerExtensionExtension\', \'\', __CLASS__));\r\n			\r\n			$files = glob(DIR_APPLICATION . \'controller/{extension/\'.$type.\',\'.$type.\'}/*.php\', GLOB_BRACE);\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<!-- Admin: User Groups -->\r\n\r\n	<file path=\"admin/controller/user/user_permission.php\">\r\n		<operation>\r\n			<search><![CDATA[ControllerUserUserPermission extends Controller {]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			private function checkPermissions(&$permissions)\r\n			{\r\n				$folders = array(\'analytics\', \'captcha\', \'dashboard\', \'feed\', \'fraud\', \'module\', \'payment\', \'shipping\', \'theme\', \'total\');\r\n				\r\n				foreach ($permissions as $type => $extensions) {\r\n					foreach ($extensions as $route) {\r\n						$route = explode(\'/\', $route);\r\n						\r\n						if (($route[0] != \'extension\') && in_array($route[0], $folders) && is_file(DIR_APPLICATION.\'controller/\'.$route[0].\'/\'.$route[1].\'.php\')) {\r\n							$permissions[$type][] = \'extension/\'.$route[0].\'/\'.$route[1];\r\n						}\r\n					}\r\n					\r\n					sort($permissions[$type]);\r\n				}\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->model_user_user_group->addUserGroup(]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (isset($this->request->post[\'permission\'])) {\r\n				$this->checkPermissions($this->request->post[\'permission\']);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->model_user_user_group->editUserGroup(]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (isset($this->request->post[\'permission\'])) {\r\n				$this->checkPermissions($this->request->post[\'permission\']);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n		\r\n	<!-- Admin: Menu -->\r\n\r\n	<file path=\"admin/controller/common/column_left.php\">\r\n		<operation>\r\n			<search><![CDATA[if ($this->user->hasPermission(\'access\', \'marketplace/extension\')) {]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			$this->load->language(\'marketplace/marketplace\');			\r\n			\r\n			$this->load->language(\'extension/extension/analytics\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'heading_title\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=analytics\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/captcha\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'heading_title\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=captcha\', true),\r\n				\'children\' => array());\r\n\r\n			$this->load->language(\'extension/extension/dashboard\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_dashboard\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=dashboard\', true),\r\n				\'children\' => array());\r\n				\r\n			$this->load->language(\'extension/extension/feed\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_feed\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=feed\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/fraud\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'heading_title\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=fraud\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/menu\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_menu\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=menu\', true),\r\n				\'children\' => array());\r\n				\r\n			$this->load->language(\'extension/extension/module\');\r\n										\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_module\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=module\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/payment\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_payment\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=payment\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/report\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_report\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=report\', true),\r\n				\'children\' => array());\r\n				\r\n			$this->load->language(\'extension/extension/shipping\');\r\n										\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_shipping\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=shipping\', true),\r\n				\'children\' => array());\r\n				\r\n			$this->load->language(\'extension/extension/theme\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_theme\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=theme\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/total\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_total\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=total\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'common/column_left\');\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($this->user->hasPermission(\'access\', \'marketplace/modification\')) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($extension)) {\r\n				$sorted = array_values($marketplace);\r\n				$element = end($sorted);\r\n			\r\n				$element[\'href\'] = \'\';\r\n				$element[\'children\'] = $extension;\r\n				\r\n				$marketplace[count($marketplace) - 1] = $element;\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<!-- Catalog: Checkout -->\r\n	\r\n	<file path=\"catalog/controller/checkout/cart.php\">\r\n		<operation>\r\n			<search><![CDATA[if ($files) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			$files = array_merge($files, glob(DIR_APPLICATION.\'controller/total/*.php\'));\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($files) {]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			$modules = array();\r\n			\r\n			]]></add>\r\n		</operation>			\r\n		<operation>\r\n			<search><![CDATA[if ($result) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n				$modules[] = basename($file, \'.php\');\r\n			}\r\n\r\n			foreach (array_unique($modules) as $module) {\r\n				$result = $this->load->controller(\'extension/total/\'.$module);\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n				\r\n</modification>', 1, '2020-07-06 17:06:23'),
(4, 5, 'GC Sessions Basic', '6629190803', 'alex cherednichenko', '3.x', 'https://www.alcher.info', '﻿<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<modification>\n	<name>GC Sessions Basic</name>\n	<code>6629190803</code>\n	<version>3.x</version>\n	<author>alex cherednichenko</author>\n	<link>https://www.alcher.info</link>\n\n	<file path=\"system/library/session.php\">\n		<operation>\n			<search>\n				<![CDATA[\n	public function close() {\n				]]>\n			</search>\n			<add position=\"replace\" offset=\"2\">\n				<![CDATA[\n	public function close() {\n		$this->adaptor->close($this->session_id, $this->data);\n	}\n				]]>\n			</add>\n		</operation>\n	</file>\n\n	<file path=\"system/library/session/db.php\">\n		<operation>\n			<search>\n				<![CDATA[\n		$query = $this->db->query(\"SELECT `data` FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\' AND expire > \" . (int)time());\n				]]>\n			</search>\n			<add position=\"replace\">\n				<![CDATA[\n		$query = $this->db->query(\"SELECT `data` FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\' AND expire > DATE_SUB(NOW(), INTERVAL \" . $this->expire . \" SECOND);\");\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search>\n				<![CDATA[\n		public function gc($expire) {\n				]]>\n			</search>\n			<add position=\"before\">\n				<![CDATA[\n		public function close( $session_id, $data ) {\n			$this->write($session_id, $data);\n			$this->gc($this->expire);\n		}\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search>\n				<![CDATA[\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE expire < \" . ((int)time() + $expire));\n				]]>\n			</search>\n			<add position=\"replace\">\n				<![CDATA[\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE expire < DATE_SUB(NOW(), INTERVAL \" . ((int)$expire) . \" SECOND);\");\n				]]>\n			</add>\n		</operation>\n\n	</file>\n\n<!-- InnoDB -->\n<!--\nCREATE TABLE `oc_session` (\n  `session_id` varchar(32) NOT NULL,\n  `data` text NOT NULL,\n  `ip_address` varchar(255) DEFAULT NULL,\n  `user_agent` tinytext,\n  `expire` datetime NOT NULL,\n  PRIMARY KEY (`session_id`)\n) ENGINE=InnoDB DEFAULT CHARSET=utf8\n-->\n<!--\n	<file path=\"system/library/session/db.php\">\n		<operation>\n			<search position=\"before\">\n				<![CDATA[\n		public function gc($expire) {\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		public function close( $session_id, $data ) {\n			$this->write($session_id, $data);\n			$this->gc($this->expire);\n		}\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search position=\"replace\">\n				<![CDATA[\n		$query = $this->db->query(\"SELECT `data` FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\' AND expire > DATE_SUB(NOW(), INTERVAL \" . $this->expire . \" SECOND);\");\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		$this->db->query(\"START TRANSACTION READ ONLY\");\n		$query = $this->db->query(\"SELECT `data` FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\' AND expire > \" . (int)time());\n		$this->db->query(\"COMMIT\");\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search position=\"replace\">\n				<![CDATA[\n		$this->db->query(\"REPLACE INTO `\" . DB_PREFIX . \"session` SET session_id = \'\" . $this->db->escape($session_id) . \"\', `data` = \'\" . $this->db->escape(json_encode($data)) . \"\', expire = \'\" . $this->db->escape(date(\'Y-m-d H:i:s\', time() + $this->expire)) . \"\'\");\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		$this->db->query(\"START TRANSACTION\");\n		$this->db->query(\"REPLACE INTO `\" . DB_PREFIX . \"session` SET session_id = \'\" . $this->db->escape($session_id) . \"\', `data` = \'\" . $this->db->escape(json_encode($data)) . \"\', user_agent = \'\" . $this->db->escape($_SERVER[\'HTTP_USER_AGENT\']) . \"\', ip_address = \'\".  $this->db->escape($_SERVER[\'REMOTE_ADDR\']) . \"\', expire = \'\" . $this->db->escape(date(\'Y-m-d H:i:s\', time() + $this->expire)) . \"\'\");\n		$this->db->query(\"COMMIT\");\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search position=\"replace\">\n				<![CDATA[\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\'\");\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		$this->db->query(\"START TRANSACTION\");\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\'\");\n		$this->db->query(\"COMMIT\");\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search position=\"replace\">\n				<![CDATA[\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE expire < \" . ((int)time() + $expire));\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		$this->db->query(\"START TRANSACTION\");\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE expire < DATE_SUB(NOW(), INTERVAL \" . $expire . \" SECOND);\"); // + 86400\n		$this->db->query(\"COMMIT\");\n				]]>\n			</add>\n		</operation>\n\n	</file>\n-->\n</modification>', 1, '2020-07-06 17:07:17');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_module`
--

CREATE TABLE `oc_module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option`
--

CREATE TABLE `oc_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_description`
--

CREATE TABLE `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_value`
--

CREATE TABLE `oc_option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_value_description`
--

CREATE TABLE `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order`
--

CREATE TABLE `oc_order` (
  `order_id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT 0,
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `customer_group_id` int(11) NOT NULL DEFAULT 0,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `order_status_id` int(11) NOT NULL DEFAULT 0,
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT 1.00000000,
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order`
--

INSERT INTO `oc_order` (`order_id`, `invoice_no`, `invoice_prefix`, `store_id`, `store_name`, `store_url`, `customer_id`, `customer_group_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `custom_field`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_country_id`, `payment_zone`, `payment_zone_id`, `payment_address_format`, `payment_custom_field`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_country_id`, `shipping_zone`, `shipping_zone_id`, `shipping_address_format`, `shipping_custom_field`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `marketing_id`, `tracking`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `date_added`, `date_modified`) VALUES
(39, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'тест', '', 'demonized@4ait.ru', '', '', '[]', 'ФИО: тест\n', 'E-mail: demonized@4ait.ru', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '223523.2000', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2020-07-10 18:27:55', '2020-07-10 18:27:56'),
(40, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'тест', '', 'demonized@4ait.ru', '', '', '[]', 'ФИО: тест\n', 'E-mail: demonized@4ait.ru', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2020-07-14 04:43:52', '2020-07-14 04:43:53'),
(41, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'тест', '', 'demonized@4ait.ru', '', '', '[]', 'ФИО: тест\n', 'E-mail: demonized@4ait.ru', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2020-07-14 04:44:31', '2020-07-14 04:44:31'),
(42, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'тест', '', 'demonized@4ait.ru', '', '', '[]', 'ФИО: тест\n', 'E-mail: demonized@4ait.ru', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2020-07-14 04:45:44', '2020-07-14 04:45:44'),
(43, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'тест', '', 'demonized@4ait.ru', '', '', '[]', 'ФИО: тест\n', 'E-mail: demonized@4ait.ru', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '97306.0500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2020-07-14 04:52:26', '2020-07-14 04:52:27'),
(44, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'тест', '', 'demonized@4ait.ru', '', '', '[]', 'ФИО: тест\n', 'E-mail: demonized@4ait.ru', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2021-01-21 07:05:36', '2021-01-21 07:05:37'),
(45, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'тест', '', 'demonized@4ait.ru', '', '', '[]', 'ФИО: тест\n', 'E-mail: demonized@4ait.ru', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2021-01-21 07:06:09', '2021-01-21 07:06:10'),
(46, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'хер', '', 'khui@ss.com', '', '', '[]', 'ФИО: хер\n', 'E-mail: khui@ss.com', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '90406.0500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2021-01-21 07:08:16', '2021-01-21 07:08:17'),
(47, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'хер', '', 'khui@ss.com', '', '', '[]', 'ФИО: хер\n', 'E-mail: khui@ss.com', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2021-01-21 07:10:44', '2021-01-21 07:10:45'),
(48, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'тест', '', 'khui@ss.com', '', '', '[]', 'ФИО: тест\n', 'E-mail: khui@ss.com', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2021-01-21 07:12:40', '2021-01-21 07:12:40'),
(49, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'тест', '', 'khui@ss.com', '', '', '[]', 'ФИО: тест\n', 'E-mail: khui@ss.com', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2021-01-21 07:14:10', '2021-01-21 07:14:11'),
(50, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, '432535235235b32', '', 'khui@ss.com', '', '', '[]', 'ФИО: 432535235235b32\n', 'E-mail: khui@ss.com', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2021-01-21 07:16:47', '2021-01-21 07:16:47'),
(51, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'тест232', '', 'fdsf', '', '', '[]', 'ФИО: тест232\n', 'E-mail: fdsf', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2021-01-21 07:17:35', '2021-01-21 07:17:35'),
(52, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'хер', '', 'khui@ss.com', '', '', '[]', 'ФИО: хер\n', 'E-mail: khui@ss.com', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2021-01-21 07:19:53', '2021-01-21 07:19:53'),
(53, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'тест', '', 'demonized@4ait.ru', '', '', '[]', 'ФИО: тест\n', 'E-mail: demonized@4ait.ru', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2021-01-21 07:21:33', '2021-01-21 07:21:34'),
(54, 0, 'INV-2020-00', 0, 'ВентСтройСнаб', 'http://vss/', 0, 1, 'тест', '', 'demonized@4ait.ru', '', '', '[]', 'ФИО: тест\n', 'E-mail: demonized@4ait.ru', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', '82435.7500', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7', '2021-01-21 07:23:08', '2021-01-21 07:23:08');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_history`
--

CREATE TABLE `oc_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT 0,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order_history`
--

INSERT INTO `oc_order_history` (`order_history_id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_added`) VALUES
(63, 40, 1, 0, '', '2020-07-14 04:43:53'),
(64, 41, 1, 0, '', '2020-07-14 04:44:31'),
(62, 39, 1, 0, '', '2020-07-10 18:27:56'),
(65, 42, 1, 0, '', '2020-07-14 04:45:44'),
(66, 43, 1, 0, '', '2020-07-14 04:52:27'),
(67, 44, 1, 0, '', '2021-01-21 07:05:37'),
(68, 45, 1, 0, '', '2021-01-21 07:06:10'),
(69, 46, 1, 0, '', '2021-01-21 07:08:17'),
(70, 47, 1, 0, '', '2021-01-21 07:10:45'),
(71, 48, 1, 0, '', '2021-01-21 07:12:40'),
(72, 49, 1, 0, '', '2021-01-21 07:14:11'),
(73, 50, 1, 0, '', '2021-01-21 07:16:47'),
(74, 51, 1, 0, '', '2021-01-21 07:17:35'),
(75, 52, 1, 0, '', '2021-01-21 07:19:53'),
(76, 53, 1, 0, '', '2021-01-21 07:21:34'),
(77, 54, 1, 0, '', '2021-01-21 07:23:08');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_option`
--

CREATE TABLE `oc_order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_product`
--

CREATE TABLE `oc_order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `total` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `tax` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `reward` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order_product`
--

INSERT INTO `oc_order_product` (`order_product_id`, `order_id`, `product_id`, `name`, `model`, `quantity`, `price`, `total`, `tax`, `reward`) VALUES
(735, 41, 222, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(736, 41, 108, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(734, 41, 100, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0),
(733, 41, 273, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(732, 41, 203, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(731, 40, 190, 'Вентилятор VK 100', 'Вентилятор VK 100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(730, 40, 117, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(729, 40, 312, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(728, 40, 74, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(726, 40, 142, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(727, 40, 86, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(725, 40, 204, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(724, 40, 167, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(723, 40, 215, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(722, 40, 61, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(721, 40, 278, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(720, 40, 108, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(719, 40, 222, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(718, 40, 100, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0),
(717, 40, 273, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(716, 40, 203, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(715, 39, 194, 'Вентилятор VKP 60x30-4D (380В)', 'Вентилятор VKP 60x30-4D (380В)', 2, '26913.5000', '53827.0000', '0.0000', 0),
(714, 39, 117, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(713, 39, 312, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(712, 39, 306, 'Панель управления LCP для FC-051(132B0101)', 'Панель управления LCP для FC-051(132B0101)', 2, '1163.7500', '2327.5000', '0.0000', 0),
(711, 39, 320, 'Воздушный клапан ZAP 60-30', 'Воздушный клапан ZAP 60-30', 2, '4085.0000', '8170.0000', '0.0000', 0),
(710, 39, 73, 'Вставка кассетная фильтрующая ФВКас 600-300-100-G3', 'Вставка кассетная фильтрующая ФВКас 600-300-100-G3', 1, '1453.5000', '1453.5000', '0.0000', 0),
(709, 39, 101, 'Вставка гибкая FKR 60-30', 'Вставка гибкая FKR 60-30', 4, '1311.0000', '5244.0000', '0.0000', 0),
(708, 39, 61, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(707, 39, 215, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(706, 39, 154, 'Комплект NEMA1-M2 (для FC-051 2,2 кВт)', 'Комплект NEMA1-M2 (для FC-051 2,2 кВт)', 2, '746.7000', '1493.4000', '0.0000', 0),
(705, 39, 324, 'Частотный преобразователь FC-051P2K2-2,2кВт, 5,3А (3)', 'Частотный преобразователь FC-051P2K2-2,2кВт, 5,3А (3)', 2, '11637.5000', '23275.0000', '0.0000', 0),
(704, 39, 267, 'Эл/нагреватель для прямоугольных каналов EHR 600*300-30', 'Эл/нагреватель для прямоугольных каналов EHR 600*300-30', 1, '19912.0000', '19912.0000', '0.0000', 0),
(703, 39, 356, 'Фильтр-бокс (корпус) FBRr 600x300', 'Фильтр-бокс (корпус) FBRr 600x300', 1, '0.0000', '0.0000', '0.0000', 0),
(702, 39, 120, 'Шумоглушитель RA 60-30 (L 1000 мм)', 'Шумоглушитель RA 60-30 (L 1000 мм)', 1, '6213.0000', '6213.0000', '0.0000', 0),
(701, 39, 176, 'Решетка Z/H 300x150 (с клапаном OBD)', 'Решетка Z/H 300x150 (с клапаном OBD)', 12, '500.6500', '6007.8000', '0.0000', 0),
(695, 39, 203, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 4, '175.7500', '703.0000', '0.0000', 0),
(696, 39, 273, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 21, '1150.0000', '24150.0000', '0.0000', 0),
(697, 39, 86, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(698, 39, 326, 'Щит управления (ПВ)30,0/3.3Е', 'Щит управления (ПВ)30,0/3.3Е', 1, '32300.0000', '32300.0000', '0.0000', 0),
(699, 39, 302, 'Зонт из оцинкованной стали для вентиляционных шахт 300х300', 'Зонт из оцинкованной стали для вентиляционных шахт 300х300', 1, '1800.0000', '1800.0000', '0.0000', 0),
(700, 39, 129, 'Решетка наружная ВР-Н3 700х400 (без защелки, без КРВ, без монтажных отверстий)', 'Решетка наружная ВР-Н3 700х400 (без защелки, без КРВ, без монтаж', 1, '2099.5000', '2099.5000', '0.0000', 0),
(746, 41, 117, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(745, 41, 312, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(744, 41, 74, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(743, 41, 86, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(742, 41, 142, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(741, 41, 204, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(740, 41, 167, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(738, 41, 61, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(739, 41, 215, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(737, 41, 278, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(765, 43, 273, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 8, '1150.0000', '9200.0000', '0.0000', 0),
(764, 43, 203, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(763, 42, 190, 'Вентилятор VK 100', 'Вентилятор VK 100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(762, 42, 117, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(760, 42, 74, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(761, 42, 312, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(759, 42, 86, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(758, 42, 142, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(757, 42, 204, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(756, 42, 167, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(754, 42, 61, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(755, 42, 215, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(747, 41, 190, 'Вентилятор VK 100', 'Вентилятор VK 100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(748, 42, 203, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(749, 42, 273, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(750, 42, 100, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0),
(751, 42, 222, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(752, 42, 108, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(753, 42, 278, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(800, 45, 328, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(799, 45, 354, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(798, 45, 50, 'Вентилятор д.100', 'Вентилятор д.100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(797, 45, 77, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(796, 45, 352, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(795, 44, 360, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0),
(794, 44, 219, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(766, 43, 86, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(767, 43, 193, 'Кассетный фильтр (корпус с материалом) BKS 160', 'Кассетный фильтр (корпус с материалом) BKS 160', 1, '1605.5000', '1605.5000', '0.0000', 0),
(768, 43, 50, 'Шумоглушитель CA 160/600', 'Шумоглушитель CA 160/600', 2, '2593.5000', '5187.0000', '0.0000', 0),
(769, 43, 108, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(770, 43, 222, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 3, '418.0000', '1254.0000', '0.0000', 0),
(771, 43, 243, 'Зонт из оцинкованной стали для вентиляционных шахт 200х200', 'Зонт из оцинкованной стали для вентиляционных шахт 200х200', 1, '1400.0000', '1400.0000', '0.0000', 0),
(772, 43, 215, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(773, 43, 117, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(774, 43, 61, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(775, 43, 121, 'Воздушный клапан DCGAr 160', 'Воздушный клапан DCGAr 160', 2, '950.0000', '1900.0000', '0.0000', 0),
(776, 43, 210, 'Эл/нагреватель для круглого канала ЕНС 160-6,0/3', 'Эл/нагреватель для круглого канала ЕНС 160-6,0/3', 1, '6083.8000', '6083.8000', '0.0000', 0),
(777, 43, 74, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(778, 43, 312, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(779, 43, 130, 'Вентилятор VK 160', 'Вентилятор VK 160', 2, '4427.0000', '8854.0000', '0.0000', 0),
(780, 44, 352, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(781, 44, 77, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(782, 44, 50, 'Вентилятор д.100', 'Вентилятор д.100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(783, 44, 354, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(784, 44, 328, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(785, 44, 297, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(786, 44, 357, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(787, 44, 283, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(788, 44, 80, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(789, 44, 346, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(790, 44, 115, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(791, 44, 112, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(792, 44, 134, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(793, 44, 251, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(809, 45, 251, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(801, 45, 297, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(802, 45, 357, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(803, 45, 283, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(804, 45, 80, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(805, 45, 346, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(806, 45, 115, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(807, 45, 112, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(808, 45, 134, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(827, 46, 52, 'Вентилятор д.160', 'Вентилятор д.160', 2, '4427.0000', '8854.0000', '0.0000', 0),
(828, 47, 352, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(825, 46, 285, 'Шумоглушитель CA 160/600', 'Шумоглушитель CA 160/600', 2, '2593.5000', '5187.0000', '0.0000', 0),
(826, 46, 165, 'Кассетный фильтр (корпус с материалом) BKS 160', 'Кассетный фильтр (корпус с материалом) BKS 160', 1, '1605.5000', '1605.5000', '0.0000', 0),
(824, 46, 251, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(823, 46, 219, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 3, '418.0000', '1254.0000', '0.0000', 0),
(810, 45, 219, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(811, 45, 360, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0),
(812, 46, 352, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(813, 46, 77, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 2, '1150.0000', '2300.0000', '0.0000', 0),
(814, 46, 354, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(815, 46, 357, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(816, 46, 328, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(817, 46, 297, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(818, 46, 307, 'Эл/нагреватель для круглого канала ЕНС 160-6,0/3', 'Эл/нагреватель для круглого канала ЕНС 160-6,0/3', 1, '6083.8000', '6083.8000', '0.0000', 0),
(819, 46, 82, 'Воздушный клапан DCGAr 160', 'Воздушный клапан DCGAr 160', 2, '950.0000', '1900.0000', '0.0000', 0),
(820, 46, 112, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(821, 46, 115, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(822, 46, 137, 'Зонт из оцинкованной стали для вентиляционных шахт 200х200', 'Зонт из оцинкованной стали для вентиляционных шахт 200х200', 1, '1400.0000', '1400.0000', '0.0000', 0),
(875, 49, 360, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0),
(829, 47, 77, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(830, 47, 50, 'Вентилятор д.100', 'Вентилятор д.100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(831, 47, 354, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(832, 47, 328, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(833, 47, 297, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(834, 47, 357, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(835, 47, 283, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(836, 47, 80, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(837, 47, 346, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(838, 47, 115, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(839, 47, 112, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(840, 47, 134, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(841, 47, 251, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(842, 47, 219, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(843, 47, 360, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0),
(844, 48, 352, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(845, 48, 77, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(846, 48, 50, 'Вентилятор д.100', 'Вентилятор д.100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(847, 48, 354, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(848, 48, 328, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(849, 48, 297, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(850, 48, 357, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(851, 48, 283, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(852, 48, 80, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(853, 48, 346, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(854, 48, 115, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(855, 48, 112, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(856, 48, 134, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(857, 48, 251, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(858, 48, 219, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(859, 48, 360, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0),
(860, 49, 352, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(861, 49, 77, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(862, 49, 50, 'Вентилятор д.100', 'Вентилятор д.100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(863, 49, 354, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(864, 49, 328, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(865, 49, 297, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(866, 49, 357, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(867, 49, 283, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(868, 49, 80, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(869, 49, 346, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(870, 49, 115, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(871, 49, 112, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(872, 49, 134, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(873, 49, 251, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(874, 49, 219, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(876, 50, 352, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(877, 50, 77, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(878, 50, 50, 'Вентилятор д.100', 'Вентилятор д.100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(879, 50, 354, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(880, 50, 328, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(881, 50, 297, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(882, 50, 357, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(883, 50, 283, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(884, 50, 80, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(885, 50, 346, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(886, 50, 115, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(887, 50, 112, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(888, 50, 134, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(889, 50, 251, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(890, 50, 219, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(891, 50, 360, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0),
(892, 51, 352, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(893, 51, 77, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(894, 51, 50, 'Вентилятор д.100', 'Вентилятор д.100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(895, 51, 354, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(896, 51, 328, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(897, 51, 297, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(898, 51, 357, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(899, 51, 283, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(900, 51, 80, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(901, 51, 346, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(902, 51, 115, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(903, 51, 112, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(904, 51, 134, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(905, 51, 251, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(906, 51, 219, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(907, 51, 360, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0),
(908, 52, 352, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(909, 52, 77, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(910, 52, 50, 'Вентилятор д.100', 'Вентилятор д.100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(911, 52, 354, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(912, 52, 328, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(913, 52, 297, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(914, 52, 357, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(915, 52, 283, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(916, 52, 80, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(917, 52, 346, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(918, 52, 115, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(919, 52, 112, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(920, 52, 134, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(921, 52, 251, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(922, 52, 219, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(923, 52, 360, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0),
(924, 53, 352, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(925, 53, 77, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(926, 53, 50, 'Вентилятор д.100', 'Вентилятор д.100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(927, 53, 354, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(928, 53, 328, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(929, 53, 297, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(930, 53, 357, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(931, 53, 283, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(932, 53, 80, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(933, 53, 346, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(934, 53, 115, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(935, 53, 112, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(936, 53, 134, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(937, 53, 251, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(938, 53, 219, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(939, 53, 360, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0),
(940, 54, 352, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 1, '175.7500', '175.7500', '0.0000', 0),
(941, 54, 77, 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Воздуховоды из оцинкованной стали, 0,5 мм', 1, '1150.0000', '1150.0000', '0.0000', 0),
(942, 54, 50, 'Вентилятор д.100', 'Вентилятор д.100', 2, '3410.5000', '6821.0000', '0.0000', 0),
(943, 54, 354, 'Комплект проводов №1', 'Комплект проводов №1', 1, '8000.0000', '8000.0000', '0.0000', 0),
(944, 54, 328, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 2, '6754.5000', '13509.0000', '0.0000', 0),
(945, 54, 297, 'Щит управления (ПВ)12,0/1.1Е', 'Щит управления (ПВ)12,0/1.1Е', 1, '26500.0000', '26500.0000', '0.0000', 0),
(946, 54, 357, 'Комплект расходных материалов №1', 'Комплект расходных материалов №1', 1, '7500.0000', '7500.0000', '0.0000', 0),
(947, 54, 283, 'Шумоглушитель CA 100/600', 'Шумоглушитель CA 100/600', 2, '1966.5000', '3933.0000', '0.0000', 0),
(948, 54, 80, 'Воздушный клапан DCGAr 100', 'Воздушный клапан DCGAr 100', 2, '788.5000', '1577.0000', '0.0000', 0),
(949, 54, 346, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 1, '3135.0000', '3135.0000', '0.0000', 0),
(950, 54, 115, 'Датчик температуры канальный HTF-PT1000', 'Датчик температуры канальный HTF-PT1000', 1, '1605.5000', '1605.5000', '0.0000', 0),
(951, 54, 112, 'Датчик давления DPS 500', 'Датчик давления DPS 500', 3, '1311.0000', '3933.0000', '0.0000', 0),
(952, 54, 134, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 1, '1100.0000', '1100.0000', '0.0000', 0),
(953, 54, 251, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', 1, '598.5000', '598.5000', '0.0000', 0),
(954, 54, 219, 'Решетка Z/H 200x100 (с клапаном OBD)', 'Решетка Z/H 200x100 (с клапаном OBD)', 4, '418.0000', '1672.0000', '0.0000', 0),
(955, 54, 360, 'Кассетный фильтр (корпус с материалом) BKS 100', 'Кассетный фильтр (корпус с материалом) BKS 100', 1, '1226.0000', '1226.0000', '0.0000', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_recurring`
--

CREATE TABLE `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_recurring_transaction`
--

CREATE TABLE `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_shipment`
--

CREATE TABLE `oc_order_shipment` (
  `order_shipment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `shipping_courier_id` varchar(255) NOT NULL DEFAULT '',
  `tracking_number` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_status`
--

CREATE TABLE `oc_order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(2, 1, 'В обработке'),
(3, 1, 'Доставлено'),
(7, 1, 'Отменено'),
(5, 1, 'Сделка завершена'),
(8, 1, 'Возврат'),
(9, 1, 'Отмена и аннулирование'),
(10, 1, 'Неудавшийся'),
(11, 1, 'Возмещенный'),
(12, 1, 'Полностью измененный'),
(13, 1, 'Полный возврат'),
(1, 1, 'Ожидание'),
(15, 1, 'Обработано'),
(14, 1, 'Истекло'),
(2, 2, 'Processing'),
(8, 2, 'Denied'),
(11, 2, 'Refunded'),
(3, 2, 'Shipped'),
(10, 2, 'Failed'),
(1, 2, 'Pending'),
(9, 2, 'Canceled Reversal'),
(7, 2, 'Canceled'),
(12, 2, 'Reversed'),
(13, 2, 'Chargeback'),
(5, 2, 'Complete'),
(14, 2, 'Expired'),
(16, 1, 'Анулированный'),
(16, 2, 'Voided'),
(15, 2, 'Processed');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_total`
--

CREATE TABLE `oc_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order_total`
--

INSERT INTO `oc_order_total` (`order_total_id`, `order_id`, `code`, `title`, `value`, `sort_order`) VALUES
(94, 47, 'total', 'Всего', '82435.7500', 9),
(93, 47, 'sub_total', 'Итого', '82435.7500', 1),
(77, 39, 'sub_total', 'Итого', '223523.2000', 1),
(78, 39, 'total', 'Всего', '223523.2000', 9),
(79, 40, 'sub_total', 'Итого', '82435.7500', 1),
(80, 40, 'total', 'Всего', '82435.7500', 9),
(81, 41, 'sub_total', 'Итого', '82435.7500', 1),
(82, 41, 'total', 'Всего', '82435.7500', 9),
(83, 42, 'sub_total', 'Итого', '82435.7500', 1),
(84, 42, 'total', 'Всего', '82435.7500', 9),
(85, 43, 'sub_total', 'Итого', '97306.0500', 1),
(86, 43, 'total', 'Всего', '97306.0500', 9),
(87, 44, 'sub_total', 'Итого', '82435.7500', 1),
(88, 44, 'total', 'Всего', '82435.7500', 9),
(89, 45, 'sub_total', 'Итого', '82435.7500', 1),
(90, 45, 'total', 'Всего', '82435.7500', 9),
(91, 46, 'sub_total', 'Итого', '90406.0500', 1),
(92, 46, 'total', 'Всего', '90406.0500', 9),
(95, 48, 'sub_total', 'Итого', '82435.7500', 1),
(96, 48, 'total', 'Всего', '82435.7500', 9),
(97, 49, 'sub_total', 'Итого', '82435.7500', 1),
(98, 49, 'total', 'Всего', '82435.7500', 9),
(99, 50, 'sub_total', 'Итого', '82435.7500', 1),
(100, 50, 'total', 'Всего', '82435.7500', 9),
(101, 51, 'sub_total', 'Итого', '82435.7500', 1),
(102, 51, 'total', 'Всего', '82435.7500', 9),
(103, 52, 'sub_total', 'Итого', '82435.7500', 1),
(104, 52, 'total', 'Всего', '82435.7500', 9),
(105, 53, 'sub_total', 'Итого', '82435.7500', 1),
(106, 53, 'total', 'Всего', '82435.7500', 9),
(107, 54, 'sub_total', 'Итого', '82435.7500', 1),
(108, 54, 'total', 'Всего', '82435.7500', 9);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_voucher`
--

CREATE TABLE `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product`
--

CREATE TABLE `oc_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT 0,
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT 1,
  `price` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `points` int(8) NOT NULL DEFAULT 0,
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT 0.00000000,
  `weight_class_id` int(11) NOT NULL DEFAULT 0,
  `length` decimal(15,8) NOT NULL DEFAULT 0.00000000,
  `width` decimal(15,8) NOT NULL DEFAULT 0.00000000,
  `height` decimal(15,8) NOT NULL DEFAULT 0.00000000,
  `length_class_id` int(11) NOT NULL DEFAULT 0,
  `subtract` tinyint(1) NOT NULL DEFAULT 1,
  `minimum` int(11) NOT NULL DEFAULT 1,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `viewed` int(5) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product`
--

INSERT INTO `oc_product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`, `noindex`) VALUES
(50, 'Вентилятор д.100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3410.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(51, 'Вентилятор д. 125', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3705.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(52, 'Вентилятор д.160', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4427.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(53, 'Вентилятор д. 200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5177.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(54, 'Вентилятор д. 250', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6175.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(55, 'Вентилятор д. 315', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '7400.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(56, 'Вентилятор 50x30-4D (380В)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '18050.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(57, 'Вентилятор 60x30-4D (380В)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '26913.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(58, 'Вентилятор 60х35-4D (380В)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '30143.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(59, 'Вентилятор 70х40-4D (380В)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '35558.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(60, 'Вентилятор 80х50-4D (380В)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '53912.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(61, 'Вентилятор 90х50-4D (380В)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '58282.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(62, 'Водяной нагреватель д.315/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '13500.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(63, 'Водяной нагреватель д.50х30/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5690.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(64, 'Водяной нагреватель д. 60х30/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '7115.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(65, 'Водяной нагреватель д. 60х35/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '8075.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(66, 'Водяной нагреватель д. 70х40/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '10440.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(67, 'Водяной нагреватель WWP 80х50/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '21500.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(68, 'Водяной нагреватель WWP 90х50/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '22500.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(69, 'Водяной нагреватель д.100х50/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '26000.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(70, 'Водяной нагреватель  д.50х30/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6669.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(71, 'Водяной нагреватель д. 60x30/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '7923.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(72, 'Водяной нагреватель д. 60x35/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '10440.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(73, 'Водяной нагреватель WWP 70х40/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '16000.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(74, 'Водяной нагреватель WWP 80х50/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '23000.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(75, 'Водяной нагреватель WWP 90х50/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '24500.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(76, 'Водяной нагреватель д.100х50/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '28000.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(77, 'Воздуховоды из оцинкованной стали, 0,5 мм', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1150.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(78, 'Воздуховоды из оцинкованной стали, 0,7 мм', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1290.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(79, 'Воздуховоды из оцинкованной стали, 1 мм', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1740.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(80, 'Воздушный клапан DCGAr 100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '788.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(81, 'Воздушный клапан DCGAr 125', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '845.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(82, 'Воздушный клапан DCGAr 160', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '950.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(83, 'Воздушный клапан DCGAr 200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1121.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(84, 'Воздушный клапан DCGAr 250', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1406.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(85, 'Воздушный клапан DCGAr 315', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1567.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(86, 'Воздушный клапан ZAP 50-30', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3743.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(87, 'Воздушный клапан ZAP 60-30', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4085.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(88, 'Воздушный клапан ZAP 60-35', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4446.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(89, 'Воздушный клапан ZAP 70-40', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5339.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(90, 'Воздушный клапан ZAP 80-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6726.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(91, 'Воздушный клапан ZAP 90-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '7315.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(92, 'Воздушный клапан ZAP 100-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '8787.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(93, 'Вставка карманная фильтрующая FBR 50-30', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3277.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(94, 'Вставка карманная фильтрующая FBR 60-30', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3277.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(95, 'Вставка карманная фильтрующая FBR 60-35', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3277.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(96, 'Вставка карманная фильтрующая FBR 70-40', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5890.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(97, 'Вставка карманная фильтрующая FBR 80-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5244.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(98, 'Вставка карманная фильтрующая FBR 90-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5510.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(99, 'Вставка карманная фильтрующая FBR 100-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5899.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(100, 'Гибкие воздуховоды армированные AF102 10м', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '256.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(101, 'Гибкие воздуховоды армированные AF127 10м', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '351.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(102, 'Гибкие воздуховоды армированные AF160 10м', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '475.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(103, 'Гибкие воздуховоды армированные AF203 10м', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '665.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(104, 'Гибкие воздуховоды армированные AF254 10м', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '940.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(105, 'Гибкие воздуховоды армированные AF315 10м', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1216.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(106, 'Гибкие воздуховоды изолированные ISO 102', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1263.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(107, 'Гибкие воздуховоды изолированные ISO 127', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1349.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(108, 'Гибкие воздуховоды изолированные ISO 160', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1738.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(109, 'Гибкие воздуховоды изолированные ISO 203', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2175.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(110, 'Гибкие воздуховоды изолированные ISO 254', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2612.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(111, 'Гибкие воздуховоды изолированные ISO 315', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3331.6500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(112, 'Датчик давления DPS 500', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1311.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(113, 'Датчик температуры воды погружной ЕТF01-PT 1000', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1966.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(114, 'Датчик температуры канальный ETF-1144/99-AN-NTC (для щитов BM-Mi', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1596.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(115, 'Датчик температуры канальный HTF-PT1000', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1605.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(116, 'Датчик температуры канальный  250мм PT1000 (бескорпусный К02)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1225.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(117, 'Диффузор DVA d100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '118.7500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(118, 'Диффузор DVA d125', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '137.7500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(119, 'Диффузор DVA d160', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '199.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(120, 'Диффузор DVA d200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '228.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(121, 'Диффузор DVA d250', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '522.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(122, 'Дроссель клапан с ручным управлением 125', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '500.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(123, 'Дроссель клапан с ручным управлением 160', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '550.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(124, 'Дроссель клапан с ручным управлением 200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '600.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(125, 'Дроссель клапан с ручным управлением 250', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '650.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(126, 'Дроссель клапан с ручным управлением 315', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '700.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(127, 'Дроссель клапан с ручным управлением 100x100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '500.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(128, 'Дроссель клапан с ручным управлением 150x100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '550.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(129, 'Дроссель клапан с ручным управлением 150x150', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '600.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(130, 'Дроссель клапан с ручным управлением 200x100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '600.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(131, 'Дроссель клапан с ручным управлением 200x150', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '650.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(132, 'Дроссель клапан с ручным управлением 200x200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '700.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(133, 'Дроссель клапан с ручным управлением 250x250', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '750.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(134, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1100.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(135, 'Зонт из оцинкованной стали для вентиляционных шахт 200х100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1200.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(136, 'Зонт из оцинкованной стали для вентиляционных шахт 200х150', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1300.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(137, 'Зонт из оцинкованной стали для вентиляционных шахт 200х200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1400.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(138, 'Зонт из оцинкованной стали для вентиляционных шахт 250x250', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1500.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(139, 'Зонт из оцинкованной стали для вентиляционных шахт 300х150', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1600.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(140, 'Зонт из оцинкованной стали для вентиляционных шахт 300х200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1700.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(141, 'Зонт из оцинкованной стали для вентиляционных шахт 300х300', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1800.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(142, 'Зонт из оцинкованной стали для вентиляционных шахт 400х200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1900.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(143, 'Зонт из оцинкованной стали для вентиляционных шахт 400х250', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2000.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(144, 'Зонт из оцинкованной стали для вентиляционных шахт 500х300', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2100.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(145, 'Зонт из оцинкованной стали для вентиляционных шахт 600х300', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2200.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(146, 'Зонт из оцинкованной стали для вентиляционных шахт 700х400', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2300.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(147, 'Зонт из оцинкованной стали для вентиляционных шахт 800х500', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2400.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(148, 'Зонт из оцинкованной стали для вентиляционных шахт 900х500', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2500.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(149, 'Зонт из оцинкованной стали для вентиляционных шахт 1000х500', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2600.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(150, 'Карманный корпус фильтра BKP 50-30', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4645.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(151, 'Карманный корпус фильтра BКР 60-30', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4655.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(152, 'Карманный корпус фильтра BKP 60-35', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5320.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(153, 'Карманный корпус фильтра BKP 70-40', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5890.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(154, 'Карманный корпус фильтра BКР 80-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6175.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(155, 'Карманный корпус фильтра BKP 90-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '7077.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(156, 'Карманный корпус фильтра BKP 100-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '7077.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(157, 'Кассетный корпус фильтра BKSP 50-30', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4512.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(158, 'Кассетный корпус фильтра BKSP 60-30', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4655.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(159, 'Кассетный корпус фильтра BKSP 60-35', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5225.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(160, 'Кассетный корпус фильтра BKSP 70-40', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5605.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(161, 'Кассетный корпус фильтра BKSP 80-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6080.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(162, 'Кассетный корпус фильтра BKSP 90-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6555.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(163, 'Кассетный корпус фильтра BKSP 100-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '7125.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(164, 'Кассетный фильтр (корпус с материалом) BKS 125', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1358.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(165, 'Кассетный фильтр (корпус с материалом) BKS 160', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1605.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(166, 'Кассетный фильтр (корпус с материалом) BKS 200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1729.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(167, 'Кассетный фильтр (корпус с материалом) BKS 250', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1938.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(168, 'Кассетный фильтр (корпус с материалом) BKS 315', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2308.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(169, 'Клапан КПС-1м(60)-НО-МВ(220)-100x100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9462.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(170, 'Клапан КПС-1м(60)-НО-МВ(220)-150x100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9462.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(171, 'Клапан КПС-1м(60)-НО-МВ(220)-150x150', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9462.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(172, 'Клапан КПС-1м(60)-НО-МВ(220)-200х100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9462.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(173, 'Клапан КПС-1м(60)-НО-МВ(220)-200х150', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9462.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(174, 'Клапан КПС-1м(60)-НО-МВ(220)-200х200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9462.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(175, 'Клапан КПС-1м(60)-НО-МВ(220)-250х250', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9557.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(176, 'Клапан КПС-1м(60)-НО-МВ(220)-300х150', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9766.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(177, 'Клапан КПС-1м(60)-НО-МВ(220)-300х200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9766.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(178, 'Клапан КПС-1м(60)-НО-МВ(220)-300х300', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9766.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(179, 'Комплект NEMA1-M1 (для FC-051 0,75 кВт)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '608.9500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(180, 'Комплект NEMA1-M2 (для FC-051 1,5 кВт)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '608.9500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(181, 'Комплект NEMA1-M2 (для FC-051 2,2 кВт)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '746.7000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(182, 'Комплект NEMA1-M3 (для FC-051 3 кВт)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '746.7000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(183, 'Комплект NEMA1-M3 (для FC-051 4 кВт)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '746.7000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(184, 'Комплект NEMA1-M3 (для FC-051 5,5 кВт)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '643.1500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(185, 'Комплект NEMA1-M3 (для FC-051 7,5 кВт)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '643.1500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(186, 'Комплект NEMA1-M4 (для FC-051 11 кВт)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1330.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(187, 'Комплект NEMA1-M4 (для FC-051 15,0 кВт)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1330.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(188, 'Обратный клапан RSK 100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '342.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(189, 'Обратный клапан RSK 125', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '380.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(190, 'Обратный клапан RSK 160', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '437.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(191, 'Обратный клапан RSK 200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '703.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(192, 'Обратный клапан RSK 250', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '874.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(193, 'Обратный клапан RSK 315', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '945.2500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(194, 'Панель управления LCP для FC-051(132B0101)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1163.7500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(195, 'Помпа Aspen HI-flow MAX 1.7L наливная', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5215.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(196, 'Помпа Siccom Eco Flowatch  13л/ч 10 м', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2992.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(197, 'Помпа Siccom Maxi Eco Flowatch 40л/ч 10 м', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4940.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(198, 'Пульт управления Shuft ARC 121', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(199, 'Регулятор скорости 5-ти ступенчатый с термозащитой SRE-E-1.5-T', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5571.7500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(200, 'Регулятор скорости MTY 2.5 ON (в корпусе)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1510.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(201, 'Рекуператор пластинчатый RHPr 500-300', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '21745.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(202, 'Рекуператор пластинчатый RHPr 600-300', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '24130.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(203, 'Рекуператор пластинчатый RHPr 600-350', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '31383.2500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(204, 'Рекуператор пластинчатый RHPr 700-400', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '41053.3000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(205, 'Рекуператор  пластинчатый RHPr 800-500', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '56007.2500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(206, 'Рекуператор  пластинчатый RHPr 900-500', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '80275.9500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(207, 'Рекуператор  пластинчатый RHPr 1000-500', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '95019.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(208, 'Решетка  CGTN 125 (с сеткой)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '342.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(209, 'Решетка CGTN 160 (с сеткой)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '532.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(210, 'Решетка CGTN 200 (с сеткой)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '731.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(211, 'Решетка CGTN 250 (с сеткой)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1140.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(212, 'Решетка  CGTN 315 (с сеткой)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1852.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(213, 'Решетка Z/H 100x100 (с клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '380.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(214, 'Решетка Z/H 100x100 (,без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '218.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(215, 'Решетка Z/H 150х100 (с  клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '408.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(216, 'Решетка Z/H 150х100 (Без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '218.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(217, 'Решетка Z/H 150х150 (Без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '256.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(218, 'Решетка Z/H 150x150 (с клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '465.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(219, 'Решетка Z/H 200x100 (с клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '418.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(220, 'Решетка Z/H 200х100 (Без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '247.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(221, 'Решетка Z/H 200х200 (Без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '342.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(222, 'Решетка Z/H 250х150 (с клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '560.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(223, 'Решетка Z/H 250х150 (Без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '294.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(224, 'Решетка Z/H 300х100 (С клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '422.7500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(225, 'Решетка Z/H 300x150 (с клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '500.6500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(226, 'Решетка Z/H 300х100 (с клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '541.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(227, 'Решетка Z/H 300х100 (Без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '323.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(228, 'Решетка Z/H 300х150 (Без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '342.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(229, 'Решетка Z/H 300х200 (с клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '731.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(230, 'Решетка Z/H 300х200 (без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '418.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(231, 'Решетка Z/H 300х300 (Без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '475.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(232, 'Решетка Z/H 300х300 (С клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '950.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(233, 'Решетка Z/H 400x100 (с клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '494.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(234, 'Решетка Z/H 400х100 (Без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '389.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(235, 'Решетка Z/H 400х150 (С клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '589.9500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(236, 'Решетка Z/H 400х200 (Без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '437.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(237, 'Решетка Z/H 400х200 (С клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '855.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(238, 'Решетка Z/H 500x200 (с клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '978.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(239, 'Решетка Z/H 500x200 (без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '494.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(240, 'Решетка Z/H 500х300 (Без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '717.2500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(241, 'Решетка Z/H 500х300 (С клапаном OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1216.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(242, 'Решетка Z/H 600х350 (Без клапана OBD)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '969.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(243, 'Решетка 4АПН 300х300', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '603.2500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(244, 'Решетка 4АПН 450х450', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '936.7000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(245, 'Решетка 4АПН 600х600', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1330.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(246, 'Решетка наружная IGC 125', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '342.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(247, 'Решетка наружная IGC 160', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '532.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1);
INSERT INTO `oc_product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`, `noindex`) VALUES
(248, 'Решетка наружная IGC 200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '807.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(249, 'Решетка наружная IGC 250', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1064.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(250, 'Решетка наружная IGC 315', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1548.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(251, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтаж', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '598.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(252, 'Решетка наружная ВР-Н3 500х300 (без защелки, без КРВ, без монтаж', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1311.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(253, 'Решетка наружная ВР-Н3 600x300 (без защелки, без КРВ, без монтаж', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1501.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(254, 'Решетка наружная ВР-Н3 600x350 (без защелки, без КРВ, без монтаж', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1691.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(255, 'Решетка наружная ВР-Н3 700х400 (без защелки, без КРВ, без монтаж', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2099.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(256, 'Решетка наружная ВР-Н3 800х500 (без защелки, без КРВ)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2422.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(257, 'Решетка наружная ВР-Н3 900х500 (без защелки, без КРВ)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2745.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(258, 'Решетка наружная ВР-Н3 1000х500 (без защелки, без КРВ)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3097.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(259, 'Смесительный узел с электроприводом GRUNER с плавным управлением', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '29025.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(260, 'Смесительный узел с электроприводом GRUNER с плавным управлением', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '33075.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(261, 'Смесительный узел с электроприводом GRUNER с плавным управлением', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '33075.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(262, 'Смесительный узел с электроприводом GRUNER с плавным управлением', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '33075.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(263, 'Смесительный узел с электроприводом GRUNER с плавным управлением', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '37665.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(264, 'Термостат КР61-1 (1м.)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3534.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(265, 'Термостат КР61-3 (3м.) крепление в комплекте', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3885.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(266, 'Термостат КР61-6 (6м.) крепление в комплекте', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3980.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(267, 'Фильтр-бокс (корпус) FBRr 500x300', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(268, 'Фильтр-бокс (корпус) FBRr 600x300', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(269, 'Фильтр-бокс (корпус) FBRr 600x350', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(270, 'Фильтр-бокс (корпус) FBRr 700x400', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(271, 'Фильтр-бокс (корпус) FBRr 800x500', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(272, 'Фильтр-бокс (корпус) FBRr 900x500', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(273, 'Фильтр-бокс (корпус) FBRr 1000x500', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(274, 'Частотный преобразователь FC-051P1K75-0,75кВт 4,2 A (1)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '8645.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(275, 'Частотный преобразователь FC-051P1K5-1,5кВт, 3,7А  (3)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '13300.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(276, 'Частотный преобразователь FC-051P1K5-1,5кВт, 6,8А  (1)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9310.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(277, 'Частотный преобразователь FC-051P2K2-2,2кВт, 5,3А (3)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '11637.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(278, 'Частотный преобразователь FC-051P2K2-2,2кВт, 9,6А (1)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '11970.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(279, 'Частотный преобразователь FC-051P3K0-3кВт,7,2 А (3)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '13965.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(280, 'Частотный преобразователь FC-051P4K0-4кВт,9 А', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '16625.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(281, 'Частотный преобразователь FC-051P5K5-5,5кВт,12 А (3)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '22610.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(282, 'Частотный преобразователь FC-051P7K5-7,5кВт,15,5 А', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '27265.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(283, 'Шумоглушитель CA 100/600', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1966.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(284, 'Шумоглушитель CA 125/600', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2166.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(285, 'Шумоглушитель CA 160/600', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '2593.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(286, 'Шумоглушитель CA 250/600', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3619.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(287, 'Шумоглушитель CA 315/600', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4341.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(288, 'Шумоглушитель CA 200/900', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(289, 'Шумоглушитель CA 250/900', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(290, 'Шумоглушитель RA 50-30 (L 1000 мм)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5747.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(291, 'Шумоглушитель RA 60-30 (L 1000 мм)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6213.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(292, 'Шумоглушитель RA 60-35 (L 1000 мм)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6768.7500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(293, 'Шумоглушитель RA 70-40 (L 1000 мм)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '8673.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(294, 'Шумоглушитель RA 80-50 (L 1000 мм)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9975.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(295, 'Шумоглушитель RA 90-50 (L 1000 мм)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '11115.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(296, 'Шумоглушитель RA 1000-50 (L 1000 мм)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '12160.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(297, 'Щит управления (ПВ)12,0/1.1Е', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '26500.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(298, 'Щит управления (ПВ)30,0/3.3Е', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '32300.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(299, 'Щит управления (ПВ)60,0/3.3Е', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '61100.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(300, 'Эл/нагреватель для круглого канала ЕНС 125-1,2/1', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3182.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(301, 'Эл/нагреватель для круглого канала ЕНС 125-1,8/1', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4099.2500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(302, 'Эл/нагреватель для круглого канала ЕНС 160-1,2/1', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3947.2500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(303, 'Эл/нагреватель для круглого канала ЕНС 160-2,4/1', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4170.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(304, 'Эл/нагреватель для круглого канала ЕНС 160-3,0/1', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4004.2500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(305, 'Эл/нагреватель для круглого канала ЕНС 160-3,0/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5215.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(306, 'Эл/нагреватель для круглого канала ЕНС 160-5,0/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4797.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(307, 'Эл/нагреватель для круглого канала ЕНС 160-6,0/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6083.8000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(308, 'Эл/нагреватель для круглого канала ЕНС 200-2,4/1', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4265.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(309, 'Эл/нагреватель для круглого канала ЕНС 200-3,0/1', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4503.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(310, 'Эл/нагреватель для круглого канала ЕНС 200-5,0/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5434.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(311, 'Эл/нагреватель для круглого канала ЕНС 200-6,0/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6103.7500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(312, 'Эл/нагреватель для круглого канала ЕНС 200-6,0/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6184.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(313, 'Эл/нагреватель для круглого канала ЕНС 250-3,0/1', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5277.2500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(314, 'Эл/нагреватель для круглого канала ЕНС 250-6,0/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6146.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(315, 'Эл/нагреватель для круглого канала ЕНС 250-6,0/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6246.2500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(316, 'Эл/нагреватель для круглого канала ЕНС 250-9,0/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6863.7500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(317, 'Эл/нагреватель для круглого канала ЕНС 250-12,0/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '8740.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(318, 'Эл/нагреватель для круглого канала ЕНС 315-6,0/2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6175.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(319, 'Эл/нагреватель для круглого канала ЕНС 315-9,0/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '7476.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(320, 'Эл/нагреватель для круглого канала ЕНС 315-12,0/3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '8987.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(321, 'Электронагреватель NEK 315-15', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(322, 'Эл/нагреватель для прямоугольных каналов EHR 600*300-22,5', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '16150.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(323, 'Эл/нагреватель для прямоугольных каналов EHR 600*300-30', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '19912.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(324, 'Эл/нагреватель для прямоугольных каналов EHR 600*350-30', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '19779.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(325, 'Электропривод GRUNER 225-230Т-05 (4-6 Нм) 2-3 позиц', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4740.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(326, 'Электропривод GRUNER 341-230-05 (5 Н*м) с возвр. пружиной', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '9025.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(327, 'Эл.привод воздушной заслонки Siemens GDB 331.1E ( 5 Нм), 230В, 3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '5913.7500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(328, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '6754.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(329, 'Электропривод  VA05N230 (5 Н/м) без возвр. пружины (М)', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '4170.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(330, 'Вставка кассетная фильтрующая FKS 100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '237.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(331, 'Вставка кассетная фильтрующая FKS 160', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '332.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(332, 'Вставка кассетная фильтрующая FKS 200', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '361.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(333, 'Вставка кассетная фильтрующая FKS 250', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '399.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(334, 'Вставка кассетная фильтрующая FKS 315', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '446.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(335, 'Вставка кассетная фильтрующая ФВКас 500-300-100-G3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1358.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(336, 'Вставка кассетная фильтрующая ФВКас 600-300-100-G3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1453.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(337, 'Вставка кассетная фильтрующая ФВКас 600-350-100-G3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1453.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(338, 'Вставка кассетная фильтрующая ФВКас 700-400-100-G3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1710.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(339, 'Вставка гибкая FKR 50-30', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1311.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(340, 'Вставка гибкая FKR 60-30', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1311.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(341, 'Вставка гибкая FKR 60-35', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1311.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(342, 'Вставка гибкая FKR 70-40', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1311.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(343, 'Вставка гибкая FKR 80-50', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1966.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(344, 'Дроссель клапан с ручным управлением 100', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '450.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(345, 'Шумоглушитель CA 200/600', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3030.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(346, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '3135.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(347, 'Эл/нагреватель для круглого канала ЕНR 500-300-22,5', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(348, 'Эл/нагреватель для круглого канала ЕНR 600-350-45', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(349, 'Эл/нагреватель для круглого канала ЕНR 700-400-60', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(350, 'Эл/нагреватель для круглого канала ЕНR 800-500-60', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '0.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(351, 'Эл.привод воздушной заслонки GCA 121.1E (18 Нм),24В, 2-х позицио', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '14212.9500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(352, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '175.7500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(353, 'Теплоизолирующий материал \"Магнофлекс\" б=20 мм', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '436.0500', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(354, 'Комплект проводов №1', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '8000.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(355, 'Комплект проводов №2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '15000.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(356, 'Комплект проводов №3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '21000.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(357, 'Комплект расходных материалов №1', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '7500.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(358, 'Комплект расходных материалов №2', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '12000.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(359, 'Комплект расходных материалов №3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '15000.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(360, 'Кассетный фильтр (корпус с материалом) BKS 100', '', '', '', '', '', '', '', 0, 5, NULL, 0, 1, '1226.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(361, 'Эл/нагреватель для прямоугольного канала ЕНR 500-300-22,5', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '16283.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(362, 'Эл/нагреватель для прямоугольного канала ЕНR 600-350-45', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '26600.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(363, 'Эл/нагреватель для прямоугольного канала ЕНR 700-400-60', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '33183.5000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(364, 'Эл/нагреватель для прямоугольного канала ЕНR 800-500-60', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '34599.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1),
(365, 'Вставка кассетная фильтрующая ФВКас 800-500-100-G3', '', '', '', '', '', '', '', 1, 5, NULL, 0, 1, '1796.0000', 0, 0, '2020-07-14', '0.00000000', 0, '0.00000000', '0.00000000', '0.00000000', 0, 0, 1, 0, 1, 0, '2020-07-14 23:37:31', '2021-01-01 16:02:02', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_attribute`
--

CREATE TABLE `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_description`
--

INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(365, 1, 'Вставка кассетная фильтрующая ФВКас 800-500-100-G3', '', 'Вставка кассетная фильтрующая ФВКас 800-500-100-G3', 'Купить Вставка кассетная фильтрующая ФВКас 800-500-100-G3 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка кассетная фильтрующая ФВКас 800-500-100-G3 в магазине ВентСтройСнаб по лучшей цене', 'Вставка кассетная фильтрующая ФВКас 800-500-100-G3,ВентСтройСнаб', 'Вставка кассетная фильтрующая ФВКас 800-500-100-G3'),
(364, 1, 'Эл/нагреватель для прямоугольного канала ЕНR 800-500-60', '', 'Эл/нагреватель для прямоугольного канала ЕНR 800-500-60', 'Купить Эл/нагреватель для прямоугольного канала ЕНR 800-500-60 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для прямоугольного канала ЕНR 800-500-60 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для прямоугольного канала ЕНR 800-500-60,ВентСтройСнаб', 'Эл/нагреватель для прямоугольного канала ЕНR 800-500-60'),
(358, 1, 'Комплект расходных материалов №2', '', 'Комплект расходных материалов №2', 'Купить Комплект расходных материалов №2 в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект расходных материалов №2 в магазине ВентСтройСнаб по лучшей цене', 'Комплект расходных материалов №2,ВентСтройСнаб', 'Комплект расходных материалов №2'),
(359, 1, 'Комплект расходных материалов №3', '', 'Комплект расходных материалов №3', 'Купить Комплект расходных материалов №3 в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект расходных материалов №3 в магазине ВентСтройСнаб по лучшей цене', 'Комплект расходных материалов №3,ВентСтройСнаб', 'Комплект расходных материалов №3'),
(360, 1, 'Кассетный фильтр (корпус с материалом) BKS 100', '', 'Кассетный фильтр (корпус с материалом) BKS 100', 'Купить Кассетный фильтр (корпус с материалом) BKS 100 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный фильтр (корпус с материалом) BKS 100 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный фильтр (корпус с материалом) BKS 100,ВентСтройСнаб', 'Кассетный фильтр (корпус с материалом) BKS 100'),
(361, 1, 'Эл/нагреватель для прямоугольного канала ЕНR 500-300-22,5', '', 'Эл/нагреватель для прямоугольного канала ЕНR 500-300-22,5', 'Купить Эл/нагреватель для прямоугольного канала ЕНR 500-300-22,5 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для прямоугольного канала ЕНR 500-300-22,5 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для прямоугольного канала ЕНR 500-300-22,5,ВентСтройСнаб', 'Эл/нагреватель для прямоугольного канала ЕНR 500-300-22,5'),
(362, 1, 'Эл/нагреватель для прямоугольного канала ЕНR 600-350-45', '', 'Эл/нагреватель для прямоугольного канала ЕНR 600-350-45', 'Купить Эл/нагреватель для прямоугольного канала ЕНR 600-350-45 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для прямоугольного канала ЕНR 600-350-45 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для прямоугольного канала ЕНR 600-350-45,ВентСтройСнаб', 'Эл/нагреватель для прямоугольного канала ЕНR 600-350-45'),
(363, 1, 'Эл/нагреватель для прямоугольного канала ЕНR 700-400-60', '', 'Эл/нагреватель для прямоугольного канала ЕНR 700-400-60', 'Купить Эл/нагреватель для прямоугольного канала ЕНR 700-400-60 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для прямоугольного канала ЕНR 700-400-60 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для прямоугольного канала ЕНR 700-400-60,ВентСтройСнаб', 'Эл/нагреватель для прямоугольного канала ЕНR 700-400-60'),
(355, 1, 'Комплект проводов №2', '', 'Комплект проводов №2', 'Купить Комплект проводов №2 в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект проводов №2 в магазине ВентСтройСнаб по лучшей цене', 'Комплект проводов №2,ВентСтройСнаб', 'Комплект проводов №2'),
(356, 1, 'Комплект проводов №3', '', 'Комплект проводов №3', 'Купить Комплект проводов №3 в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект проводов №3 в магазине ВентСтройСнаб по лучшей цене', 'Комплект проводов №3,ВентСтройСнаб', 'Комплект проводов №3'),
(357, 1, 'Комплект расходных материалов №1', '', 'Комплект расходных материалов №1', 'Купить Комплект расходных материалов №1 в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект расходных материалов №1 в магазине ВентСтройСнаб по лучшей цене', 'Комплект расходных материалов №1,ВентСтройСнаб', 'Комплект расходных материалов №1'),
(353, 1, 'Теплоизолирующий материал \"Магнофлекс\" б=20 мм', '', 'Теплоизолирующий материал \"Магнофлекс\" б=20 мм', 'Купить Теплоизолирующий материал \"Магнофлекс\" б=20 мм в ВентСтройСнаб по лучшей цене', 'Покупайте Теплоизолирующий материал \"Магнофлекс\" б=20 мм в магазине ВентСтройСнаб по лучшей цене', 'Теплоизолирующий материал \"Магнофлекс\" б=20 мм,ВентСтройСнаб', 'Теплоизолирующий материал \"Магнофлекс\" б=20 мм'),
(354, 1, 'Комплект проводов №1', '', 'Комплект проводов №1', 'Купить Комплект проводов №1 в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект проводов №1 в магазине ВентСтройСнаб по лучшей цене', 'Комплект проводов №1,ВентСтройСнаб', 'Комплект проводов №1'),
(348, 1, 'Эл/нагреватель для круглого канала ЕНR 600-350-45', '', 'Эл/нагреватель для круглого канала ЕНR 600-350-45', 'Купить Эл/нагреватель для круглого канала ЕНR 600-350-45 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНR 600-350-45 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНR 600-350-45,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНR 600-350-45'),
(349, 1, 'Эл/нагреватель для круглого канала ЕНR 700-400-60', '', 'Эл/нагреватель для круглого канала ЕНR 700-400-60', 'Купить Эл/нагреватель для круглого канала ЕНR 700-400-60 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНR 700-400-60 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНR 700-400-60,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНR 700-400-60'),
(350, 1, 'Эл/нагреватель для круглого канала ЕНR 800-500-60', '', 'Эл/нагреватель для круглого канала ЕНR 800-500-60', 'Купить Эл/нагреватель для круглого канала ЕНR 800-500-60 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНR 800-500-60 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНR 800-500-60,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНR 800-500-60'),
(351, 1, 'Эл.привод воздушной заслонки GCA 121.1E (18 Нм),24В, 2-х позиционный', '', 'Эл.привод воздушной заслонки GCA 121.1E (18 Нм),24В, 2-х позиционный', 'Купить Эл.привод воздушной заслонки GCA 121.1E (18 Нм),24В, 2-х позиционный в ВентСтройСнаб по лучшей цене', 'Покупайте Эл.привод воздушной заслонки GCA 121.1E (18 Нм),24В, 2-х позиционный в магазине ВентСтройСнаб по лучшей цене', 'Эл.привод воздушной заслонки GCA 121.1E (18 Нм),24В, 2-х позиционный,ВентСтройСнаб', 'Эл.привод воздушной заслонки GCA 121.1E (18 Нм),24В, 2-х позиционный'),
(352, 1, 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', '', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм', 'Купить Теплоизолирующий материал \"Магнофлекс\" б=10 мм в ВентСтройСнаб по лучшей цене', 'Покупайте Теплоизолирующий материал \"Магнофлекс\" б=10 мм в магазине ВентСтройСнаб по лучшей цене', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм,ВентСтройСнаб', 'Теплоизолирующий материал \"Магнофлекс\" б=10 мм'),
(345, 1, 'Шумоглушитель CA 200/600', '', 'Шумоглушитель CA 200/600', 'Купить Шумоглушитель CA 200/600 в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель CA 200/600 в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель CA 200/600,ВентСтройСнаб', 'Шумоглушитель CA 200/600'),
(346, 1, 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', '', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1', 'Купить Эл/нагреватель для круглого канала ЕНС 100-2,4/1 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 100-2,4/1 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 100-2,4/1'),
(347, 1, 'Эл/нагреватель для круглого канала ЕНR 500-300-22,5', '', 'Эл/нагреватель для круглого канала ЕНR 500-300-22,5', 'Купить Эл/нагреватель для круглого канала ЕНR 500-300-22,5 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНR 500-300-22,5 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНR 500-300-22,5,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНR 500-300-22,5'),
(344, 1, 'Дроссель клапан с ручным управлением 100', '', 'Дроссель клапан с ручным управлением 100', 'Купить Дроссель клапан с ручным управлением 100 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 100 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 100,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 100'),
(342, 1, 'Вставка гибкая FKR 70-40', '', 'Вставка гибкая FKR 70-40', 'Купить Вставка гибкая FKR 70-40 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка гибкая FKR 70-40 в магазине ВентСтройСнаб по лучшей цене', 'Вставка гибкая FKR 70-40,ВентСтройСнаб', 'Вставка гибкая FKR 70-40'),
(343, 1, 'Вставка гибкая FKR 80-50', '', 'Вставка гибкая FKR 80-50', 'Купить Вставка гибкая FKR 80-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка гибкая FKR 80-50 в магазине ВентСтройСнаб по лучшей цене', 'Вставка гибкая FKR 80-50,ВентСтройСнаб', 'Вставка гибкая FKR 80-50'),
(341, 1, 'Вставка гибкая FKR 60-35', '', 'Вставка гибкая FKR 60-35', 'Купить Вставка гибкая FKR 60-35 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка гибкая FKR 60-35 в магазине ВентСтройСнаб по лучшей цене', 'Вставка гибкая FKR 60-35,ВентСтройСнаб', 'Вставка гибкая FKR 60-35'),
(339, 1, 'Вставка гибкая FKR 50-30', '', 'Вставка гибкая FKR 50-30', 'Купить Вставка гибкая FKR 50-30 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка гибкая FKR 50-30 в магазине ВентСтройСнаб по лучшей цене', 'Вставка гибкая FKR 50-30,ВентСтройСнаб', 'Вставка гибкая FKR 50-30'),
(340, 1, 'Вставка гибкая FKR 60-30', '', 'Вставка гибкая FKR 60-30', 'Купить Вставка гибкая FKR 60-30 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка гибкая FKR 60-30 в магазине ВентСтройСнаб по лучшей цене', 'Вставка гибкая FKR 60-30,ВентСтройСнаб', 'Вставка гибкая FKR 60-30'),
(337, 1, 'Вставка кассетная фильтрующая ФВКас 600-350-100-G3', '', 'Вставка кассетная фильтрующая ФВКас 600-350-100-G3', 'Купить Вставка кассетная фильтрующая ФВКас 600-350-100-G3 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка кассетная фильтрующая ФВКас 600-350-100-G3 в магазине ВентСтройСнаб по лучшей цене', 'Вставка кассетная фильтрующая ФВКас 600-350-100-G3,ВентСтройСнаб', 'Вставка кассетная фильтрующая ФВКас 600-350-100-G3'),
(338, 1, 'Вставка кассетная фильтрующая ФВКас 700-400-100-G3', '', 'Вставка кассетная фильтрующая ФВКас 700-400-100-G3', 'Купить Вставка кассетная фильтрующая ФВКас 700-400-100-G3 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка кассетная фильтрующая ФВКас 700-400-100-G3 в магазине ВентСтройСнаб по лучшей цене', 'Вставка кассетная фильтрующая ФВКас 700-400-100-G3,ВентСтройСнаб', 'Вставка кассетная фильтрующая ФВКас 700-400-100-G3'),
(336, 1, 'Вставка кассетная фильтрующая ФВКас 600-300-100-G3', '', 'Вставка кассетная фильтрующая ФВКас 600-300-100-G3', 'Купить Вставка кассетная фильтрующая ФВКас 600-300-100-G3 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка кассетная фильтрующая ФВКас 600-300-100-G3 в магазине ВентСтройСнаб по лучшей цене', 'Вставка кассетная фильтрующая ФВКас 600-300-100-G3,ВентСтройСнаб', 'Вставка кассетная фильтрующая ФВКас 600-300-100-G3'),
(335, 1, 'Вставка кассетная фильтрующая ФВКас 500-300-100-G3', '', 'Вставка кассетная фильтрующая ФВКас 500-300-100-G3', 'Купить Вставка кассетная фильтрующая ФВКас 500-300-100-G3 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка кассетная фильтрующая ФВКас 500-300-100-G3 в магазине ВентСтройСнаб по лучшей цене', 'Вставка кассетная фильтрующая ФВКас 500-300-100-G3,ВентСтройСнаб', 'Вставка кассетная фильтрующая ФВКас 500-300-100-G3'),
(334, 1, 'Вставка кассетная фильтрующая FKS 315', '', 'Вставка кассетная фильтрующая FKS 315', 'Купить Вставка кассетная фильтрующая FKS 315 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка кассетная фильтрующая FKS 315 в магазине ВентСтройСнаб по лучшей цене', 'Вставка кассетная фильтрующая FKS 315,ВентСтройСнаб', 'Вставка кассетная фильтрующая FKS 315'),
(333, 1, 'Вставка кассетная фильтрующая FKS 250', '', 'Вставка кассетная фильтрующая FKS 250', 'Купить Вставка кассетная фильтрующая FKS 250 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка кассетная фильтрующая FKS 250 в магазине ВентСтройСнаб по лучшей цене', 'Вставка кассетная фильтрующая FKS 250,ВентСтройСнаб', 'Вставка кассетная фильтрующая FKS 250'),
(328, 1, 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', '', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной', 'Купить Электропривод VA05S230 (5 Нм) c возвр. пружиной в ВентСтройСнаб по лучшей цене', 'Покупайте Электропривод VA05S230 (5 Нм) c возвр. пружиной в магазине ВентСтройСнаб по лучшей цене', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной,ВентСтройСнаб', 'Электропривод VA05S230 (5 Нм) c возвр. пружиной'),
(329, 1, 'Электропривод  VA05N230 (5 Н/м) без возвр. пружины (М)', '', 'Электропривод  VA05N230 (5 Н/м) без возвр. пружины (М)', 'Купить Электропривод  VA05N230 (5 Н/м) без возвр. пружины (М) в ВентСтройСнаб по лучшей цене', 'Покупайте Электропривод  VA05N230 (5 Н/м) без возвр. пружины (М) в магазине ВентСтройСнаб по лучшей цене', 'Электропривод  VA05N230 (5 Н/м) без возвр. пружины (М),ВентСтройСнаб', 'Электропривод  VA05N230 (5 Н/м) без возвр. пружины (М)'),
(330, 1, 'Вставка кассетная фильтрующая FKS 100', '', 'Вставка кассетная фильтрующая FKS 100', 'Купить Вставка кассетная фильтрующая FKS 100 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка кассетная фильтрующая FKS 100 в магазине ВентСтройСнаб по лучшей цене', 'Вставка кассетная фильтрующая FKS 100,ВентСтройСнаб', 'Вставка кассетная фильтрующая FKS 100'),
(331, 1, 'Вставка кассетная фильтрующая FKS 160', '', 'Вставка кассетная фильтрующая FKS 160', 'Купить Вставка кассетная фильтрующая FKS 160 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка кассетная фильтрующая FKS 160 в магазине ВентСтройСнаб по лучшей цене', 'Вставка кассетная фильтрующая FKS 160,ВентСтройСнаб', 'Вставка кассетная фильтрующая FKS 160'),
(332, 1, 'Вставка кассетная фильтрующая FKS 200', '', 'Вставка кассетная фильтрующая FKS 200', 'Купить Вставка кассетная фильтрующая FKS 200 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка кассетная фильтрующая FKS 200 в магазине ВентСтройСнаб по лучшей цене', 'Вставка кассетная фильтрующая FKS 200,ВентСтройСнаб', 'Вставка кассетная фильтрующая FKS 200'),
(326, 1, 'Электропривод GRUNER 341-230-05 (5 Н*м) с возвр. пружиной', '', 'Электропривод GRUNER 341-230-05 (5 Н*м) с возвр. пружиной', 'Купить Электропривод GRUNER 341-230-05 (5 Н*м) с возвр. пружиной в ВентСтройСнаб по лучшей цене', 'Покупайте Электропривод GRUNER 341-230-05 (5 Н*м) с возвр. пружиной в магазине ВентСтройСнаб по лучшей цене', 'Электропривод GRUNER 341-230-05 (5 Н*м) с возвр. пружиной,ВентСтройСнаб', 'Электропривод GRUNER 341-230-05 (5 Н*м) с возвр. пружиной'),
(327, 1, 'Эл.привод воздушной заслонки Siemens GDB 331.1E ( 5 Нм), 230В, 3-х позиционный', '', 'Эл.привод воздушной заслонки Siemens GDB 331.1E ( 5 Нм), 230В, 3-х позиционный', 'Купить Эл.привод воздушной заслонки Siemens GDB 331.1E ( 5 Нм), 230В, 3-х позиционный в ВентСтройСнаб по лучшей цене', 'Покупайте Эл.привод воздушной заслонки Siemens GDB 331.1E ( 5 Нм), 230В, 3-х позиционный в магазине ВентСтройСнаб по лучшей цене', 'Эл.привод воздушной заслонки Siemens GDB 331.1E ( 5 Нм), 230В, 3-х позиционный,ВентСтройСнаб', 'Эл.привод воздушной заслонки Siemens GDB 331.1E ( 5 Нм), 230В, 3-х позиционный'),
(325, 1, 'Электропривод GRUNER 225-230Т-05 (4-6 Нм) 2-3 позиц', '', 'Электропривод GRUNER 225-230Т-05 (4-6 Нм) 2-3 позиц', 'Купить Электропривод GRUNER 225-230Т-05 (4-6 Нм) 2-3 позиц в ВентСтройСнаб по лучшей цене', 'Покупайте Электропривод GRUNER 225-230Т-05 (4-6 Нм) 2-3 позиц в магазине ВентСтройСнаб по лучшей цене', 'Электропривод GRUNER 225-230Т-05 (4-6 Нм) 2-3 позиц,ВентСтройСнаб', 'Электропривод GRUNER 225-230Т-05 (4-6 Нм) 2-3 позиц'),
(324, 1, 'Эл/нагреватель для прямоугольных каналов EHR 600*350-30', '', 'Эл/нагреватель для прямоугольных каналов EHR 600*350-30', 'Купить Эл/нагреватель для прямоугольных каналов EHR 600*350-30 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для прямоугольных каналов EHR 600*350-30 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для прямоугольных каналов EHR 600*350-30,ВентСтройСнаб', 'Эл/нагреватель для прямоугольных каналов EHR 600*350-30'),
(322, 1, 'Эл/нагреватель для прямоугольных каналов EHR 600*300-22,5', '', 'Эл/нагреватель для прямоугольных каналов EHR 600*300-22,5', 'Купить Эл/нагреватель для прямоугольных каналов EHR 600*300-22,5 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для прямоугольных каналов EHR 600*300-22,5 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для прямоугольных каналов EHR 600*300-22,5,ВентСтройСнаб', 'Эл/нагреватель для прямоугольных каналов EHR 600*300-22,5'),
(323, 1, 'Эл/нагреватель для прямоугольных каналов EHR 600*300-30', '', 'Эл/нагреватель для прямоугольных каналов EHR 600*300-30', 'Купить Эл/нагреватель для прямоугольных каналов EHR 600*300-30 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для прямоугольных каналов EHR 600*300-30 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для прямоугольных каналов EHR 600*300-30,ВентСтройСнаб', 'Эл/нагреватель для прямоугольных каналов EHR 600*300-30'),
(321, 1, 'Электронагреватель NEK 315-15', '', 'Электронагреватель NEK 315-15', 'Купить Электронагреватель NEK 315-15 в ВентСтройСнаб по лучшей цене', 'Покупайте Электронагреватель NEK 315-15 в магазине ВентСтройСнаб по лучшей цене', 'Электронагреватель NEK 315-15,ВентСтройСнаб', 'Электронагреватель NEK 315-15'),
(319, 1, 'Эл/нагреватель для круглого канала ЕНС 315-9,0/3', '', 'Эл/нагреватель для круглого канала ЕНС 315-9,0/3', 'Купить Эл/нагреватель для круглого канала ЕНС 315-9,0/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 315-9,0/3 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 315-9,0/3,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 315-9,0/3'),
(320, 1, 'Эл/нагреватель для круглого канала ЕНС 315-12,0/3', '', 'Эл/нагреватель для круглого канала ЕНС 315-12,0/3', 'Купить Эл/нагреватель для круглого канала ЕНС 315-12,0/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 315-12,0/3 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 315-12,0/3,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 315-12,0/3'),
(318, 1, 'Эл/нагреватель для круглого канала ЕНС 315-6,0/2', '', 'Эл/нагреватель для круглого канала ЕНС 315-6,0/2', 'Купить Эл/нагреватель для круглого канала ЕНС 315-6,0/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 315-6,0/2 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 315-6,0/2,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 315-6,0/2'),
(316, 1, 'Эл/нагреватель для круглого канала ЕНС 250-9,0/3', '', 'Эл/нагреватель для круглого канала ЕНС 250-9,0/3', 'Купить Эл/нагреватель для круглого канала ЕНС 250-9,0/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 250-9,0/3 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 250-9,0/3,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 250-9,0/3'),
(317, 1, 'Эл/нагреватель для круглого канала ЕНС 250-12,0/3', '', 'Эл/нагреватель для круглого канала ЕНС 250-12,0/3', 'Купить Эл/нагреватель для круглого канала ЕНС 250-12,0/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 250-12,0/3 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 250-12,0/3,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 250-12,0/3'),
(314, 1, 'Эл/нагреватель для круглого канала ЕНС 250-6,0/2', '', 'Эл/нагреватель для круглого канала ЕНС 250-6,0/2', 'Купить Эл/нагреватель для круглого канала ЕНС 250-6,0/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 250-6,0/2 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 250-6,0/2,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 250-6,0/2'),
(315, 1, 'Эл/нагреватель для круглого канала ЕНС 250-6,0/3', '', 'Эл/нагреватель для круглого канала ЕНС 250-6,0/3', 'Купить Эл/нагреватель для круглого канала ЕНС 250-6,0/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 250-6,0/3 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 250-6,0/3,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 250-6,0/3'),
(313, 1, 'Эл/нагреватель для круглого канала ЕНС 250-3,0/1', '', 'Эл/нагреватель для круглого канала ЕНС 250-3,0/1', 'Купить Эл/нагреватель для круглого канала ЕНС 250-3,0/1 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 250-3,0/1 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 250-3,0/1,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 250-3,0/1'),
(312, 1, 'Эл/нагреватель для круглого канала ЕНС 200-6,0/3', '', 'Эл/нагреватель для круглого канала ЕНС 200-6,0/3', 'Купить Эл/нагреватель для круглого канала ЕНС 200-6,0/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 200-6,0/3 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 200-6,0/3,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 200-6,0/3'),
(311, 1, 'Эл/нагреватель для круглого канала ЕНС 200-6,0/2', '', 'Эл/нагреватель для круглого канала ЕНС 200-6,0/2', 'Купить Эл/нагреватель для круглого канала ЕНС 200-6,0/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 200-6,0/2 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 200-6,0/2,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 200-6,0/2'),
(308, 1, 'Эл/нагреватель для круглого канала ЕНС 200-2,4/1', '', 'Эл/нагреватель для круглого канала ЕНС 200-2,4/1', 'Купить Эл/нагреватель для круглого канала ЕНС 200-2,4/1 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 200-2,4/1 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 200-2,4/1,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 200-2,4/1'),
(309, 1, 'Эл/нагреватель для круглого канала ЕНС 200-3,0/1', '', 'Эл/нагреватель для круглого канала ЕНС 200-3,0/1', 'Купить Эл/нагреватель для круглого канала ЕНС 200-3,0/1 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 200-3,0/1 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 200-3,0/1,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 200-3,0/1'),
(310, 1, 'Эл/нагреватель для круглого канала ЕНС 200-5,0/2', '', 'Эл/нагреватель для круглого канала ЕНС 200-5,0/2', 'Купить Эл/нагреватель для круглого канала ЕНС 200-5,0/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 200-5,0/2 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 200-5,0/2,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 200-5,0/2'),
(305, 1, 'Эл/нагреватель для круглого канала ЕНС 160-3,0/2', '', 'Эл/нагреватель для круглого канала ЕНС 160-3,0/2', 'Купить Эл/нагреватель для круглого канала ЕНС 160-3,0/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 160-3,0/2 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 160-3,0/2,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 160-3,0/2'),
(306, 1, 'Эл/нагреватель для круглого канала ЕНС 160-5,0/2', '', 'Эл/нагреватель для круглого канала ЕНС 160-5,0/2', 'Купить Эл/нагреватель для круглого канала ЕНС 160-5,0/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 160-5,0/2 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 160-5,0/2,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 160-5,0/2'),
(307, 1, 'Эл/нагреватель для круглого канала ЕНС 160-6,0/3', '', 'Эл/нагреватель для круглого канала ЕНС 160-6,0/3', 'Купить Эл/нагреватель для круглого канала ЕНС 160-6,0/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 160-6,0/3 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 160-6,0/3,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 160-6,0/3'),
(303, 1, 'Эл/нагреватель для круглого канала ЕНС 160-2,4/1', '', 'Эл/нагреватель для круглого канала ЕНС 160-2,4/1', 'Купить Эл/нагреватель для круглого канала ЕНС 160-2,4/1 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 160-2,4/1 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 160-2,4/1,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 160-2,4/1'),
(304, 1, 'Эл/нагреватель для круглого канала ЕНС 160-3,0/1', '', 'Эл/нагреватель для круглого канала ЕНС 160-3,0/1', 'Купить Эл/нагреватель для круглого канала ЕНС 160-3,0/1 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 160-3,0/1 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 160-3,0/1,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 160-3,0/1'),
(301, 1, 'Эл/нагреватель для круглого канала ЕНС 125-1,8/1', '', 'Эл/нагреватель для круглого канала ЕНС 125-1,8/1', 'Купить Эл/нагреватель для круглого канала ЕНС 125-1,8/1 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 125-1,8/1 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 125-1,8/1,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 125-1,8/1'),
(302, 1, 'Эл/нагреватель для круглого канала ЕНС 160-1,2/1', '', 'Эл/нагреватель для круглого канала ЕНС 160-1,2/1', 'Купить Эл/нагреватель для круглого канала ЕНС 160-1,2/1 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 160-1,2/1 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 160-1,2/1,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 160-1,2/1'),
(300, 1, 'Эл/нагреватель для круглого канала ЕНС 125-1,2/1', '', 'Эл/нагреватель для круглого канала ЕНС 125-1,2/1', 'Купить Эл/нагреватель для круглого канала ЕНС 125-1,2/1 в ВентСтройСнаб по лучшей цене', 'Покупайте Эл/нагреватель для круглого канала ЕНС 125-1,2/1 в магазине ВентСтройСнаб по лучшей цене', 'Эл/нагреватель для круглого канала ЕНС 125-1,2/1,ВентСтройСнаб', 'Эл/нагреватель для круглого канала ЕНС 125-1,2/1'),
(298, 1, 'Щит управления (ПВ)30,0/3.3Е', '', 'Щит управления (ПВ)30,0/3.3Е', 'Купить Щит управления (ПВ)30,0/3.3Е в ВентСтройСнаб по лучшей цене', 'Покупайте Щит управления (ПВ)30,0/3.3Е в магазине ВентСтройСнаб по лучшей цене', 'Щит управления (ПВ)30,0/3.3Е,ВентСтройСнаб', 'Щит управления (ПВ)30,0/3.3Е'),
(299, 1, 'Щит управления (ПВ)60,0/3.3Е', '', 'Щит управления (ПВ)60,0/3.3Е', 'Купить Щит управления (ПВ)60,0/3.3Е в ВентСтройСнаб по лучшей цене', 'Покупайте Щит управления (ПВ)60,0/3.3Е в магазине ВентСтройСнаб по лучшей цене', 'Щит управления (ПВ)60,0/3.3Е,ВентСтройСнаб', 'Щит управления (ПВ)60,0/3.3Е'),
(295, 1, 'Шумоглушитель RA 90-50 (L 1000 мм)', '', 'Шумоглушитель RA 90-50 (L 1000 мм)', 'Купить Шумоглушитель RA 90-50 (L 1000 мм) в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель RA 90-50 (L 1000 мм) в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель RA 90-50 (L 1000 мм),ВентСтройСнаб', 'Шумоглушитель RA 90-50 (L 1000 мм)'),
(296, 1, 'Шумоглушитель RA 1000-50 (L 1000 мм)', '', 'Шумоглушитель RA 1000-50 (L 1000 мм)', 'Купить Шумоглушитель RA 1000-50 (L 1000 мм) в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель RA 1000-50 (L 1000 мм) в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель RA 1000-50 (L 1000 мм),ВентСтройСнаб', 'Шумоглушитель RA 1000-50 (L 1000 мм)'),
(297, 1, 'Щит управления (ПВ)12,0/1.1Е', '', 'Щит управления (ПВ)12,0/1.1Е', 'Купить Щит управления (ПВ)12,0/1.1Е в ВентСтройСнаб по лучшей цене', 'Покупайте Щит управления (ПВ)12,0/1.1Е в магазине ВентСтройСнаб по лучшей цене', 'Щит управления (ПВ)12,0/1.1Е,ВентСтройСнаб', 'Щит управления (ПВ)12,0/1.1Е'),
(294, 1, 'Шумоглушитель RA 80-50 (L 1000 мм)', '', 'Шумоглушитель RA 80-50 (L 1000 мм)', 'Купить Шумоглушитель RA 80-50 (L 1000 мм) в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель RA 80-50 (L 1000 мм) в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель RA 80-50 (L 1000 мм),ВентСтройСнаб', 'Шумоглушитель RA 80-50 (L 1000 мм)'),
(292, 1, 'Шумоглушитель RA 60-35 (L 1000 мм)', '', 'Шумоглушитель RA 60-35 (L 1000 мм)', 'Купить Шумоглушитель RA 60-35 (L 1000 мм) в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель RA 60-35 (L 1000 мм) в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель RA 60-35 (L 1000 мм),ВентСтройСнаб', 'Шумоглушитель RA 60-35 (L 1000 мм)'),
(293, 1, 'Шумоглушитель RA 70-40 (L 1000 мм)', '', 'Шумоглушитель RA 70-40 (L 1000 мм)', 'Купить Шумоглушитель RA 70-40 (L 1000 мм) в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель RA 70-40 (L 1000 мм) в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель RA 70-40 (L 1000 мм),ВентСтройСнаб', 'Шумоглушитель RA 70-40 (L 1000 мм)'),
(291, 1, 'Шумоглушитель RA 60-30 (L 1000 мм)', '', 'Шумоглушитель RA 60-30 (L 1000 мм)', 'Купить Шумоглушитель RA 60-30 (L 1000 мм) в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель RA 60-30 (L 1000 мм) в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель RA 60-30 (L 1000 мм),ВентСтройСнаб', 'Шумоглушитель RA 60-30 (L 1000 мм)'),
(285, 1, 'Шумоглушитель CA 160/600', '', 'Шумоглушитель CA 160/600', 'Купить Шумоглушитель CA 160/600 в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель CA 160/600 в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель CA 160/600,ВентСтройСнаб', 'Шумоглушитель CA 160/600'),
(286, 1, 'Шумоглушитель CA 250/600', '', 'Шумоглушитель CA 250/600', 'Купить Шумоглушитель CA 250/600 в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель CA 250/600 в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель CA 250/600,ВентСтройСнаб', 'Шумоглушитель CA 250/600'),
(287, 1, 'Шумоглушитель CA 315/600', '', 'Шумоглушитель CA 315/600', 'Купить Шумоглушитель CA 315/600 в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель CA 315/600 в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель CA 315/600,ВентСтройСнаб', 'Шумоглушитель CA 315/600'),
(288, 1, 'Шумоглушитель CA 200/900', '', 'Шумоглушитель CA 200/900', 'Купить Шумоглушитель CA 200/900 в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель CA 200/900 в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель CA 200/900,ВентСтройСнаб', 'Шумоглушитель CA 200/900'),
(289, 1, 'Шумоглушитель CA 250/900', '', 'Шумоглушитель CA 250/900', 'Купить Шумоглушитель CA 250/900 в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель CA 250/900 в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель CA 250/900,ВентСтройСнаб', 'Шумоглушитель CA 250/900'),
(290, 1, 'Шумоглушитель RA 50-30 (L 1000 мм)', '', 'Шумоглушитель RA 50-30 (L 1000 мм)', 'Купить Шумоглушитель RA 50-30 (L 1000 мм) в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель RA 50-30 (L 1000 мм) в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель RA 50-30 (L 1000 мм),ВентСтройСнаб', 'Шумоглушитель RA 50-30 (L 1000 мм)'),
(283, 1, 'Шумоглушитель CA 100/600', '', 'Шумоглушитель CA 100/600', 'Купить Шумоглушитель CA 100/600 в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель CA 100/600 в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель CA 100/600,ВентСтройСнаб', 'Шумоглушитель CA 100/600'),
(284, 1, 'Шумоглушитель CA 125/600', '', 'Шумоглушитель CA 125/600', 'Купить Шумоглушитель CA 125/600 в ВентСтройСнаб по лучшей цене', 'Покупайте Шумоглушитель CA 125/600 в магазине ВентСтройСнаб по лучшей цене', 'Шумоглушитель CA 125/600,ВентСтройСнаб', 'Шумоглушитель CA 125/600'),
(282, 1, 'Частотный преобразователь FC-051P7K5-7,5кВт,15,5 А', '', 'Частотный преобразователь FC-051P7K5-7,5кВт,15,5 А', 'Купить Частотный преобразователь FC-051P7K5-7,5кВт,15,5 А в ВентСтройСнаб по лучшей цене', 'Покупайте Частотный преобразователь FC-051P7K5-7,5кВт,15,5 А в магазине ВентСтройСнаб по лучшей цене', 'Частотный преобразователь FC-051P7K5-7,5кВт,15,5 А,ВентСтройСнаб', 'Частотный преобразователь FC-051P7K5-7,5кВт,15,5 А'),
(281, 1, 'Частотный преобразователь FC-051P5K5-5,5кВт,12 А (3)', '', 'Частотный преобразователь FC-051P5K5-5,5кВт,12 А (3)', 'Купить Частотный преобразователь FC-051P5K5-5,5кВт,12 А (3) в ВентСтройСнаб по лучшей цене', 'Покупайте Частотный преобразователь FC-051P5K5-5,5кВт,12 А (3) в магазине ВентСтройСнаб по лучшей цене', 'Частотный преобразователь FC-051P5K5-5,5кВт,12 А (3),ВентСтройСнаб', 'Частотный преобразователь FC-051P5K5-5,5кВт,12 А (3)'),
(279, 1, 'Частотный преобразователь FC-051P3K0-3кВт,7,2 А (3)', '', 'Частотный преобразователь FC-051P3K0-3кВт,7,2 А (3)', 'Купить Частотный преобразователь FC-051P3K0-3кВт,7,2 А (3) в ВентСтройСнаб по лучшей цене', 'Покупайте Частотный преобразователь FC-051P3K0-3кВт,7,2 А (3) в магазине ВентСтройСнаб по лучшей цене', 'Частотный преобразователь FC-051P3K0-3кВт,7,2 А (3),ВентСтройСнаб', 'Частотный преобразователь FC-051P3K0-3кВт,7,2 А (3)'),
(280, 1, 'Частотный преобразователь FC-051P4K0-4кВт,9 А', '', 'Частотный преобразователь FC-051P4K0-4кВт,9 А', 'Купить Частотный преобразователь FC-051P4K0-4кВт,9 А в ВентСтройСнаб по лучшей цене', 'Покупайте Частотный преобразователь FC-051P4K0-4кВт,9 А в магазине ВентСтройСнаб по лучшей цене', 'Частотный преобразователь FC-051P4K0-4кВт,9 А,ВентСтройСнаб', 'Частотный преобразователь FC-051P4K0-4кВт,9 А'),
(277, 1, 'Частотный преобразователь FC-051P2K2-2,2кВт, 5,3А (3)', '', 'Частотный преобразователь FC-051P2K2-2,2кВт, 5,3А (3)', 'Купить Частотный преобразователь FC-051P2K2-2,2кВт, 5,3А (3) в ВентСтройСнаб по лучшей цене', 'Покупайте Частотный преобразователь FC-051P2K2-2,2кВт, 5,3А (3) в магазине ВентСтройСнаб по лучшей цене', 'Частотный преобразователь FC-051P2K2-2,2кВт, 5,3А (3),ВентСтройСнаб', 'Частотный преобразователь FC-051P2K2-2,2кВт, 5,3А (3)'),
(278, 1, 'Частотный преобразователь FC-051P2K2-2,2кВт, 9,6А (1)', '', 'Частотный преобразователь FC-051P2K2-2,2кВт, 9,6А (1)', 'Купить Частотный преобразователь FC-051P2K2-2,2кВт, 9,6А (1) в ВентСтройСнаб по лучшей цене', 'Покупайте Частотный преобразователь FC-051P2K2-2,2кВт, 9,6А (1) в магазине ВентСтройСнаб по лучшей цене', 'Частотный преобразователь FC-051P2K2-2,2кВт, 9,6А (1),ВентСтройСнаб', 'Частотный преобразователь FC-051P2K2-2,2кВт, 9,6А (1)'),
(271, 1, 'Фильтр-бокс (корпус) FBRr 800x500', '', 'Фильтр-бокс (корпус) FBRr 800x500', 'Купить Фильтр-бокс (корпус) FBRr 800x500 в ВентСтройСнаб по лучшей цене', 'Покупайте Фильтр-бокс (корпус) FBRr 800x500 в магазине ВентСтройСнаб по лучшей цене', 'Фильтр-бокс (корпус) FBRr 800x500,ВентСтройСнаб', 'Фильтр-бокс (корпус) FBRr 800x500'),
(272, 1, 'Фильтр-бокс (корпус) FBRr 900x500', '', 'Фильтр-бокс (корпус) FBRr 900x500', 'Купить Фильтр-бокс (корпус) FBRr 900x500 в ВентСтройСнаб по лучшей цене', 'Покупайте Фильтр-бокс (корпус) FBRr 900x500 в магазине ВентСтройСнаб по лучшей цене', 'Фильтр-бокс (корпус) FBRr 900x500,ВентСтройСнаб', 'Фильтр-бокс (корпус) FBRr 900x500'),
(273, 1, 'Фильтр-бокс (корпус) FBRr 1000x500', '', 'Фильтр-бокс (корпус) FBRr 1000x500', 'Купить Фильтр-бокс (корпус) FBRr 1000x500 в ВентСтройСнаб по лучшей цене', 'Покупайте Фильтр-бокс (корпус) FBRr 1000x500 в магазине ВентСтройСнаб по лучшей цене', 'Фильтр-бокс (корпус) FBRr 1000x500,ВентСтройСнаб', 'Фильтр-бокс (корпус) FBRr 1000x500'),
(274, 1, 'Частотный преобразователь FC-051P1K75-0,75кВт 4,2 A (1)', '', 'Частотный преобразователь FC-051P1K75-0,75кВт 4,2 A (1)', 'Купить Частотный преобразователь FC-051P1K75-0,75кВт 4,2 A (1) в ВентСтройСнаб по лучшей цене', 'Покупайте Частотный преобразователь FC-051P1K75-0,75кВт 4,2 A (1) в магазине ВентСтройСнаб по лучшей цене', 'Частотный преобразователь FC-051P1K75-0,75кВт 4,2 A (1),ВентСтройСнаб', 'Частотный преобразователь FC-051P1K75-0,75кВт 4,2 A (1)'),
(275, 1, 'Частотный преобразователь FC-051P1K5-1,5кВт, 3,7А  (3)', '', 'Частотный преобразователь FC-051P1K5-1,5кВт, 3,7А  (3)', 'Купить Частотный преобразователь FC-051P1K5-1,5кВт, 3,7А  (3) в ВентСтройСнаб по лучшей цене', 'Покупайте Частотный преобразователь FC-051P1K5-1,5кВт, 3,7А  (3) в магазине ВентСтройСнаб по лучшей цене', 'Частотный преобразователь FC-051P1K5-1,5кВт, 3,7А  (3),ВентСтройСнаб', 'Частотный преобразователь FC-051P1K5-1,5кВт, 3,7А  (3)'),
(276, 1, 'Частотный преобразователь FC-051P1K5-1,5кВт, 6,8А  (1)', '', 'Частотный преобразователь FC-051P1K5-1,5кВт, 6,8А  (1)', 'Купить Частотный преобразователь FC-051P1K5-1,5кВт, 6,8А  (1) в ВентСтройСнаб по лучшей цене', 'Покупайте Частотный преобразователь FC-051P1K5-1,5кВт, 6,8А  (1) в магазине ВентСтройСнаб по лучшей цене', 'Частотный преобразователь FC-051P1K5-1,5кВт, 6,8А  (1),ВентСтройСнаб', 'Частотный преобразователь FC-051P1K5-1,5кВт, 6,8А  (1)'),
(270, 1, 'Фильтр-бокс (корпус) FBRr 700x400', '', 'Фильтр-бокс (корпус) FBRr 700x400', 'Купить Фильтр-бокс (корпус) FBRr 700x400 в ВентСтройСнаб по лучшей цене', 'Покупайте Фильтр-бокс (корпус) FBRr 700x400 в магазине ВентСтройСнаб по лучшей цене', 'Фильтр-бокс (корпус) FBRr 700x400,ВентСтройСнаб', 'Фильтр-бокс (корпус) FBRr 700x400'),
(267, 1, 'Фильтр-бокс (корпус) FBRr 500x300', '', 'Фильтр-бокс (корпус) FBRr 500x300', 'Купить Фильтр-бокс (корпус) FBRr 500x300 в ВентСтройСнаб по лучшей цене', 'Покупайте Фильтр-бокс (корпус) FBRr 500x300 в магазине ВентСтройСнаб по лучшей цене', 'Фильтр-бокс (корпус) FBRr 500x300,ВентСтройСнаб', 'Фильтр-бокс (корпус) FBRr 500x300'),
(268, 1, 'Фильтр-бокс (корпус) FBRr 600x300', '', 'Фильтр-бокс (корпус) FBRr 600x300', 'Купить Фильтр-бокс (корпус) FBRr 600x300 в ВентСтройСнаб по лучшей цене', 'Покупайте Фильтр-бокс (корпус) FBRr 600x300 в магазине ВентСтройСнаб по лучшей цене', 'Фильтр-бокс (корпус) FBRr 600x300,ВентСтройСнаб', 'Фильтр-бокс (корпус) FBRr 600x300'),
(269, 1, 'Фильтр-бокс (корпус) FBRr 600x350', '', 'Фильтр-бокс (корпус) FBRr 600x350', 'Купить Фильтр-бокс (корпус) FBRr 600x350 в ВентСтройСнаб по лучшей цене', 'Покупайте Фильтр-бокс (корпус) FBRr 600x350 в магазине ВентСтройСнаб по лучшей цене', 'Фильтр-бокс (корпус) FBRr 600x350,ВентСтройСнаб', 'Фильтр-бокс (корпус) FBRr 600x350'),
(265, 1, 'Термостат КР61-3 (3м.) крепление в комплекте', '', 'Термостат КР61-3 (3м.) крепление в комплекте', 'Купить Термостат КР61-3 (3м.) крепление в комплекте в ВентСтройСнаб по лучшей цене', 'Покупайте Термостат КР61-3 (3м.) крепление в комплекте в магазине ВентСтройСнаб по лучшей цене', 'Термостат КР61-3 (3м.) крепление в комплекте,ВентСтройСнаб', 'Термостат КР61-3 (3м.) крепление в комплекте'),
(266, 1, 'Термостат КР61-6 (6м.) крепление в комплекте', '', 'Термостат КР61-6 (6м.) крепление в комплекте', 'Купить Термостат КР61-6 (6м.) крепление в комплекте в ВентСтройСнаб по лучшей цене', 'Покупайте Термостат КР61-6 (6м.) крепление в комплекте в магазине ВентСтройСнаб по лучшей цене', 'Термостат КР61-6 (6м.) крепление в комплекте,ВентСтройСнаб', 'Термостат КР61-6 (6м.) крепление в комплекте'),
(262, 1, 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-60-6,3-С24^', '', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-60-6,3-С24^', 'Купить Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-60-6,3-С24^ в ВентСтройСнаб по лучшей цене', 'Покупайте Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-60-6,3-С24^ в магазине ВентСтройСнаб по лучшей цене', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-60-6,3-С24^,ВентСтройСнаб', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-60-6,3-С24^'),
(263, 1, 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-80-6,3-С24^', '', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-80-6,3-С24^', 'Купить Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-80-6,3-С24^ в ВентСтройСнаб по лучшей цене', 'Покупайте Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-80-6,3-С24^ в магазине ВентСтройСнаб по лучшей цене', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-80-6,3-С24^,ВентСтройСнаб', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-80-6,3-С24^'),
(264, 1, 'Термостат КР61-1 (1м.)', '', 'Термостат КР61-1 (1м.)', 'Купить Термостат КР61-1 (1м.) в ВентСтройСнаб по лучшей цене', 'Покупайте Термостат КР61-1 (1м.) в магазине ВентСтройСнаб по лучшей цене', 'Термостат КР61-1 (1м.),ВентСтройСнаб', 'Термостат КР61-1 (1м.)'),
(260, 1, 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-2.5-С24^-ТС(без байпаса)', '', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-2.5-С24^-ТС(без байпаса)', 'Купить Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-2.5-С24^-ТС(без байпаса) в ВентСтройСнаб по лучшей цене', 'Покупайте Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-2.5-С24^-ТС(без байпаса) в магазине ВентСтройСнаб по лучшей цене', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-2.5-С24^-ТС(без байпаса),ВентСтройСнаб', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-2.5-С24^-ТС(без байпаса)'),
(261, 1, 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-4.0-С24^', '', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-4.0-С24^', 'Купить Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-4.0-С24^ в ВентСтройСнаб по лучшей цене', 'Покупайте Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-4.0-С24^ в магазине ВентСтройСнаб по лучшей цене', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-4.0-С24^,ВентСтройСнаб', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-4.0-С24^'),
(259, 1, 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-1.6-С24^', '', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-1.6-С24^', 'Купить Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-1.6-С24^ в ВентСтройСнаб по лучшей цене', 'Покупайте Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-1.6-С24^ в магазине ВентСтройСнаб по лучшей цене', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-1.6-С24^,ВентСтройСнаб', 'Смесительный узел с электроприводом GRUNER с плавным управлением MST 25-40-1.6-С24^'),
(258, 1, 'Решетка наружная ВР-Н3 1000х500 (без защелки, без КРВ)', '', 'Решетка наружная ВР-Н3 1000х500 (без защелки, без КРВ)', 'Купить Решетка наружная ВР-Н3 1000х500 (без защелки, без КРВ) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная ВР-Н3 1000х500 (без защелки, без КРВ) в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная ВР-Н3 1000х500 (без защелки, без КРВ),ВентСтройСнаб', 'Решетка наружная ВР-Н3 1000х500 (без защелки, без КРВ)'),
(256, 1, 'Решетка наружная ВР-Н3 800х500 (без защелки, без КРВ)', '', 'Решетка наружная ВР-Н3 800х500 (без защелки, без КРВ)', 'Купить Решетка наружная ВР-Н3 800х500 (без защелки, без КРВ) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная ВР-Н3 800х500 (без защелки, без КРВ) в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная ВР-Н3 800х500 (без защелки, без КРВ),ВентСтройСнаб', 'Решетка наружная ВР-Н3 800х500 (без защелки, без КРВ)'),
(257, 1, 'Решетка наружная ВР-Н3 900х500 (без защелки, без КРВ)', '', 'Решетка наружная ВР-Н3 900х500 (без защелки, без КРВ)', 'Купить Решетка наружная ВР-Н3 900х500 (без защелки, без КРВ) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная ВР-Н3 900х500 (без защелки, без КРВ) в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная ВР-Н3 900х500 (без защелки, без КРВ),ВентСтройСнаб', 'Решетка наружная ВР-Н3 900х500 (без защелки, без КРВ)'),
(255, 1, 'Решетка наружная ВР-Н3 700х400 (без защелки, без КРВ, без монтажных отверстий)', '', 'Решетка наружная ВР-Н3 700х400 (без защелки, без КРВ, без монтажных отверстий)', 'Купить Решетка наружная ВР-Н3 700х400 (без защелки, без КРВ, без монтажных отверстий) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная ВР-Н3 700х400 (без защелки, без КРВ, без монтажных отверстий) в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная ВР-Н3 700х400 (без защелки, без КРВ, без монтажных отверстий),ВентСтройСнаб', 'Решетка наружная ВР-Н3 700х400 (без защелки, без КРВ, без монтажных отверстий)'),
(254, 1, 'Решетка наружная ВР-Н3 600x350 (без защелки, без КРВ, без монтажных отверстий)', '', 'Решетка наружная ВР-Н3 600x350 (без защелки, без КРВ, без монтажных отверстий)', 'Купить Решетка наружная ВР-Н3 600x350 (без защелки, без КРВ, без монтажных отверстий) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная ВР-Н3 600x350 (без защелки, без КРВ, без монтажных отверстий) в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная ВР-Н3 600x350 (без защелки, без КРВ, без монтажных отверстий),ВентСтройСнаб', 'Решетка наружная ВР-Н3 600x350 (без защелки, без КРВ, без монтажных отверстий)'),
(253, 1, 'Решетка наружная ВР-Н3 600x300 (без защелки, без КРВ, без монтажный отверстий)', '', 'Решетка наружная ВР-Н3 600x300 (без защелки, без КРВ, без монтажный отверстий)', 'Купить Решетка наружная ВР-Н3 600x300 (без защелки, без КРВ, без монтажный отверстий) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная ВР-Н3 600x300 (без защелки, без КРВ, без монтажный отверстий) в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная ВР-Н3 600x300 (без защелки, без КРВ, без монтажный отверстий),ВентСтройСнаб', 'Решетка наружная ВР-Н3 600x300 (без защелки, без КРВ, без монтажный отверстий)'),
(252, 1, 'Решетка наружная ВР-Н3 500х300 (без защелки, без КРВ, без монтажных отверстий)', '', 'Решетка наружная ВР-Н3 500х300 (без защелки, без КРВ, без монтажных отверстий)', 'Купить Решетка наружная ВР-Н3 500х300 (без защелки, без КРВ, без монтажных отверстий) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная ВР-Н3 500х300 (без защелки, без КРВ, без монтажных отверстий) в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная ВР-Н3 500х300 (без защелки, без КРВ, без монтажных отверстий),ВентСтройСнаб', 'Решетка наружная ВР-Н3 500х300 (без защелки, без КРВ, без монтажных отверстий)'),
(251, 1, 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', '', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)', 'Купить Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий) в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий),ВентСтройСнаб', 'Решетка наружная ВР-Н3 200x200 (без защелки, без КРВ, без монтажный отверстий)'),
(249, 1, 'Решетка наружная IGC 250', '', 'Решетка наружная IGC 250', 'Купить Решетка наружная IGC 250 в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная IGC 250 в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная IGC 250,ВентСтройСнаб', 'Решетка наружная IGC 250'),
(250, 1, 'Решетка наружная IGC 315', '', 'Решетка наружная IGC 315', 'Купить Решетка наружная IGC 315 в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная IGC 315 в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная IGC 315,ВентСтройСнаб', 'Решетка наружная IGC 315'),
(248, 1, 'Решетка наружная IGC 200', '', 'Решетка наружная IGC 200', 'Купить Решетка наружная IGC 200 в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная IGC 200 в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная IGC 200,ВентСтройСнаб', 'Решетка наружная IGC 200'),
(245, 1, 'Решетка 4АПН 600х600', '', 'Решетка 4АПН 600х600', 'Купить Решетка 4АПН 600х600 в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка 4АПН 600х600 в магазине ВентСтройСнаб по лучшей цене', 'Решетка 4АПН 600х600,ВентСтройСнаб', 'Решетка 4АПН 600х600'),
(246, 1, 'Решетка наружная IGC 125', '', 'Решетка наружная IGC 125', 'Купить Решетка наружная IGC 125 в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная IGC 125 в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная IGC 125,ВентСтройСнаб', 'Решетка наружная IGC 125'),
(247, 1, 'Решетка наружная IGC 160', '', 'Решетка наружная IGC 160', 'Купить Решетка наружная IGC 160 в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка наружная IGC 160 в магазине ВентСтройСнаб по лучшей цене', 'Решетка наружная IGC 160,ВентСтройСнаб', 'Решетка наружная IGC 160'),
(241, 1, 'Решетка Z/H 500х300 (С клапаном OBD)', '', 'Решетка Z/H 500х300 (С клапаном OBD)', 'Купить Решетка Z/H 500х300 (С клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 500х300 (С клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 500х300 (С клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 500х300 (С клапаном OBD)'),
(242, 1, 'Решетка Z/H 600х350 (Без клапана OBD)', '', 'Решетка Z/H 600х350 (Без клапана OBD)', 'Купить Решетка Z/H 600х350 (Без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 600х350 (Без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 600х350 (Без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 600х350 (Без клапана OBD)');
INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(243, 1, 'Решетка 4АПН 300х300', '', 'Решетка 4АПН 300х300', 'Купить Решетка 4АПН 300х300 в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка 4АПН 300х300 в магазине ВентСтройСнаб по лучшей цене', 'Решетка 4АПН 300х300,ВентСтройСнаб', 'Решетка 4АПН 300х300'),
(244, 1, 'Решетка 4АПН 450х450', '', 'Решетка 4АПН 450х450', 'Купить Решетка 4АПН 450х450 в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка 4АПН 450х450 в магазине ВентСтройСнаб по лучшей цене', 'Решетка 4АПН 450х450,ВентСтройСнаб', 'Решетка 4АПН 450х450'),
(240, 1, 'Решетка Z/H 500х300 (Без клапана OBD)', '', 'Решетка Z/H 500х300 (Без клапана OBD)', 'Купить Решетка Z/H 500х300 (Без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 500х300 (Без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 500х300 (Без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 500х300 (Без клапана OBD)'),
(238, 1, 'Решетка Z/H 500x200 (с клапаном OBD)', '', 'Решетка Z/H 500x200 (с клапаном OBD)', 'Купить Решетка Z/H 500x200 (с клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 500x200 (с клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 500x200 (с клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 500x200 (с клапаном OBD)'),
(239, 1, 'Решетка Z/H 500x200 (без клапана OBD)', '', 'Решетка Z/H 500x200 (без клапана OBD)', 'Купить Решетка Z/H 500x200 (без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 500x200 (без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 500x200 (без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 500x200 (без клапана OBD)'),
(237, 1, 'Решетка Z/H 400х200 (С клапаном OBD)', '', 'Решетка Z/H 400х200 (С клапаном OBD)', 'Купить Решетка Z/H 400х200 (С клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 400х200 (С клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 400х200 (С клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 400х200 (С клапаном OBD)'),
(235, 1, 'Решетка Z/H 400х150 (С клапаном OBD)', '', 'Решетка Z/H 400х150 (С клапаном OBD)', 'Купить Решетка Z/H 400х150 (С клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 400х150 (С клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 400х150 (С клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 400х150 (С клапаном OBD)'),
(236, 1, 'Решетка Z/H 400х200 (Без клапана OBD)', '', 'Решетка Z/H 400х200 (Без клапана OBD)', 'Купить Решетка Z/H 400х200 (Без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 400х200 (Без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 400х200 (Без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 400х200 (Без клапана OBD)'),
(234, 1, 'Решетка Z/H 400х100 (Без клапана OBD)', '', 'Решетка Z/H 400х100 (Без клапана OBD)', 'Купить Решетка Z/H 400х100 (Без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 400х100 (Без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 400х100 (Без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 400х100 (Без клапана OBD)'),
(233, 1, 'Решетка Z/H 400x100 (с клапаном OBD)', '', 'Решетка Z/H 400x100 (с клапаном OBD)', 'Купить Решетка Z/H 400x100 (с клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 400x100 (с клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 400x100 (с клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 400x100 (с клапаном OBD)'),
(231, 1, 'Решетка Z/H 300х300 (Без клапана OBD)', '', 'Решетка Z/H 300х300 (Без клапана OBD)', 'Купить Решетка Z/H 300х300 (Без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 300х300 (Без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 300х300 (Без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 300х300 (Без клапана OBD)'),
(232, 1, 'Решетка Z/H 300х300 (С клапаном OBD)', '', 'Решетка Z/H 300х300 (С клапаном OBD)', 'Купить Решетка Z/H 300х300 (С клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 300х300 (С клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 300х300 (С клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 300х300 (С клапаном OBD)'),
(227, 1, 'Решетка Z/H 300х100 (Без клапана OBD)', '', 'Решетка Z/H 300х100 (Без клапана OBD)', 'Купить Решетка Z/H 300х100 (Без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 300х100 (Без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 300х100 (Без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 300х100 (Без клапана OBD)'),
(228, 1, 'Решетка Z/H 300х150 (Без клапана OBD)', '', 'Решетка Z/H 300х150 (Без клапана OBD)', 'Купить Решетка Z/H 300х150 (Без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 300х150 (Без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 300х150 (Без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 300х150 (Без клапана OBD)'),
(229, 1, 'Решетка Z/H 300х200 (с клапаном OBD)', '', 'Решетка Z/H 300х200 (с клапаном OBD)', 'Купить Решетка Z/H 300х200 (с клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 300х200 (с клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 300х200 (с клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 300х200 (с клапаном OBD)'),
(230, 1, 'Решетка Z/H 300х200 (без клапана OBD)', '', 'Решетка Z/H 300х200 (без клапана OBD)', 'Купить Решетка Z/H 300х200 (без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 300х200 (без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 300х200 (без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 300х200 (без клапана OBD)'),
(225, 1, 'Решетка Z/H 300x150 (с клапаном OBD)', '', 'Решетка Z/H 300x150 (с клапаном OBD)', 'Купить Решетка Z/H 300x150 (с клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 300x150 (с клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 300x150 (с клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 300x150 (с клапаном OBD)'),
(226, 1, 'Решетка Z/H 300х100 (с клапаном OBD)', '', 'Решетка Z/H 300х100 (с клапаном OBD)', 'Купить Решетка Z/H 300х100 (с клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 300х100 (с клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 300х100 (с клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 300х100 (с клапаном OBD)'),
(224, 1, 'Решетка Z/H 300х100 (С клапаном OBD)', '', 'Решетка Z/H 300х100 (С клапаном OBD)', 'Купить Решетка Z/H 300х100 (С клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 300х100 (С клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 300х100 (С клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 300х100 (С клапаном OBD)'),
(223, 1, 'Решетка Z/H 250х150 (Без клапана OBD)', '', 'Решетка Z/H 250х150 (Без клапана OBD)', 'Купить Решетка Z/H 250х150 (Без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 250х150 (Без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 250х150 (Без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 250х150 (Без клапана OBD)'),
(222, 1, 'Решетка Z/H 250х150 (с клапаном OBD)', '', 'Решетка Z/H 250х150 (с клапаном OBD)', 'Купить Решетка Z/H 250х150 (с клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 250х150 (с клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 250х150 (с клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 250х150 (с клапаном OBD)'),
(221, 1, 'Решетка Z/H 200х200 (Без клапана OBD)', '', 'Решетка Z/H 200х200 (Без клапана OBD)', 'Купить Решетка Z/H 200х200 (Без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 200х200 (Без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 200х200 (Без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 200х200 (Без клапана OBD)'),
(220, 1, 'Решетка Z/H 200х100 (Без клапана OBD)', '', 'Решетка Z/H 200х100 (Без клапана OBD)', 'Купить Решетка Z/H 200х100 (Без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 200х100 (Без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 200х100 (Без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 200х100 (Без клапана OBD)'),
(219, 1, 'Решетка Z/H 200x100 (с клапаном OBD)', '', 'Решетка Z/H 200x100 (с клапаном OBD)', 'Купить Решетка Z/H 200x100 (с клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 200x100 (с клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 200x100 (с клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 200x100 (с клапаном OBD)'),
(217, 1, 'Решетка Z/H 150х150 (Без клапана OBD)', '', 'Решетка Z/H 150х150 (Без клапана OBD)', 'Купить Решетка Z/H 150х150 (Без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 150х150 (Без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 150х150 (Без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 150х150 (Без клапана OBD)'),
(218, 1, 'Решетка Z/H 150x150 (с клапаном OBD)', '', 'Решетка Z/H 150x150 (с клапаном OBD)', 'Купить Решетка Z/H 150x150 (с клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 150x150 (с клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 150x150 (с клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 150x150 (с клапаном OBD)'),
(216, 1, 'Решетка Z/H 150х100 (Без клапана OBD)', '', 'Решетка Z/H 150х100 (Без клапана OBD)', 'Купить Решетка Z/H 150х100 (Без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 150х100 (Без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 150х100 (Без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 150х100 (Без клапана OBD)'),
(215, 1, 'Решетка Z/H 150х100 (с  клапаном OBD)', '', 'Решетка Z/H 150х100 (с  клапаном OBD)', 'Купить Решетка Z/H 150х100 (с  клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 150х100 (с  клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 150х100 (с  клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 150х100 (с  клапаном OBD)'),
(214, 1, 'Решетка Z/H 100x100 (,без клапана OBD)', '', 'Решетка Z/H 100x100 (,без клапана OBD)', 'Купить Решетка Z/H 100x100 (,без клапана OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 100x100 (,без клапана OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 100x100 (,без клапана OBD),ВентСтройСнаб', 'Решетка Z/H 100x100 (,без клапана OBD)'),
(213, 1, 'Решетка Z/H 100x100 (с клапаном OBD)', '', 'Решетка Z/H 100x100 (с клапаном OBD)', 'Купить Решетка Z/H 100x100 (с клапаном OBD) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка Z/H 100x100 (с клапаном OBD) в магазине ВентСтройСнаб по лучшей цене', 'Решетка Z/H 100x100 (с клапаном OBD),ВентСтройСнаб', 'Решетка Z/H 100x100 (с клапаном OBD)'),
(212, 1, 'Решетка  CGTN 315 (с сеткой)', '', 'Решетка  CGTN 315 (с сеткой)', 'Купить Решетка  CGTN 315 (с сеткой) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка  CGTN 315 (с сеткой) в магазине ВентСтройСнаб по лучшей цене', 'Решетка  CGTN 315 (с сеткой),ВентСтройСнаб', 'Решетка  CGTN 315 (с сеткой)'),
(211, 1, 'Решетка CGTN 250 (с сеткой)', '', 'Решетка CGTN 250 (с сеткой)', 'Купить Решетка CGTN 250 (с сеткой) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка CGTN 250 (с сеткой) в магазине ВентСтройСнаб по лучшей цене', 'Решетка CGTN 250 (с сеткой),ВентСтройСнаб', 'Решетка CGTN 250 (с сеткой)'),
(209, 1, 'Решетка CGTN 160 (с сеткой)', '', 'Решетка CGTN 160 (с сеткой)', 'Купить Решетка CGTN 160 (с сеткой) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка CGTN 160 (с сеткой) в магазине ВентСтройСнаб по лучшей цене', 'Решетка CGTN 160 (с сеткой),ВентСтройСнаб', 'Решетка CGTN 160 (с сеткой)'),
(210, 1, 'Решетка CGTN 200 (с сеткой)', '', 'Решетка CGTN 200 (с сеткой)', 'Купить Решетка CGTN 200 (с сеткой) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка CGTN 200 (с сеткой) в магазине ВентСтройСнаб по лучшей цене', 'Решетка CGTN 200 (с сеткой),ВентСтройСнаб', 'Решетка CGTN 200 (с сеткой)'),
(207, 1, 'Рекуператор  пластинчатый RHPr 1000-500', '', 'Рекуператор  пластинчатый RHPr 1000-500', 'Купить Рекуператор  пластинчатый RHPr 1000-500 в ВентСтройСнаб по лучшей цене', 'Покупайте Рекуператор  пластинчатый RHPr 1000-500 в магазине ВентСтройСнаб по лучшей цене', 'Рекуператор  пластинчатый RHPr 1000-500,ВентСтройСнаб', 'Рекуператор  пластинчатый RHPr 1000-500'),
(208, 1, 'Решетка  CGTN 125 (с сеткой)', '', 'Решетка  CGTN 125 (с сеткой)', 'Купить Решетка  CGTN 125 (с сеткой) в ВентСтройСнаб по лучшей цене', 'Покупайте Решетка  CGTN 125 (с сеткой) в магазине ВентСтройСнаб по лучшей цене', 'Решетка  CGTN 125 (с сеткой),ВентСтройСнаб', 'Решетка  CGTN 125 (с сеткой)'),
(206, 1, 'Рекуператор  пластинчатый RHPr 900-500', '', 'Рекуператор  пластинчатый RHPr 900-500', 'Купить Рекуператор  пластинчатый RHPr 900-500 в ВентСтройСнаб по лучшей цене', 'Покупайте Рекуператор  пластинчатый RHPr 900-500 в магазине ВентСтройСнаб по лучшей цене', 'Рекуператор  пластинчатый RHPr 900-500,ВентСтройСнаб', 'Рекуператор  пластинчатый RHPr 900-500'),
(204, 1, 'Рекуператор пластинчатый RHPr 700-400', '', 'Рекуператор пластинчатый RHPr 700-400', 'Купить Рекуператор пластинчатый RHPr 700-400 в ВентСтройСнаб по лучшей цене', 'Покупайте Рекуператор пластинчатый RHPr 700-400 в магазине ВентСтройСнаб по лучшей цене', 'Рекуператор пластинчатый RHPr 700-400,ВентСтройСнаб', 'Рекуператор пластинчатый RHPr 700-400'),
(205, 1, 'Рекуператор  пластинчатый RHPr 800-500', '', 'Рекуператор  пластинчатый RHPr 800-500', 'Купить Рекуператор  пластинчатый RHPr 800-500 в ВентСтройСнаб по лучшей цене', 'Покупайте Рекуператор  пластинчатый RHPr 800-500 в магазине ВентСтройСнаб по лучшей цене', 'Рекуператор  пластинчатый RHPr 800-500,ВентСтройСнаб', 'Рекуператор  пластинчатый RHPr 800-500'),
(203, 1, 'Рекуператор пластинчатый RHPr 600-350', '', 'Рекуператор пластинчатый RHPr 600-350', 'Купить Рекуператор пластинчатый RHPr 600-350 в ВентСтройСнаб по лучшей цене', 'Покупайте Рекуператор пластинчатый RHPr 600-350 в магазине ВентСтройСнаб по лучшей цене', 'Рекуператор пластинчатый RHPr 600-350,ВентСтройСнаб', 'Рекуператор пластинчатый RHPr 600-350'),
(202, 1, 'Рекуператор пластинчатый RHPr 600-300', '', 'Рекуператор пластинчатый RHPr 600-300', 'Купить Рекуператор пластинчатый RHPr 600-300 в ВентСтройСнаб по лучшей цене', 'Покупайте Рекуператор пластинчатый RHPr 600-300 в магазине ВентСтройСнаб по лучшей цене', 'Рекуператор пластинчатый RHPr 600-300,ВентСтройСнаб', 'Рекуператор пластинчатый RHPr 600-300'),
(200, 1, 'Регулятор скорости MTY 2.5 ON (в корпусе)', '', 'Регулятор скорости MTY 2.5 ON (в корпусе)', 'Купить Регулятор скорости MTY 2.5 ON (в корпусе) в ВентСтройСнаб по лучшей цене', 'Покупайте Регулятор скорости MTY 2.5 ON (в корпусе) в магазине ВентСтройСнаб по лучшей цене', 'Регулятор скорости MTY 2.5 ON (в корпусе),ВентСтройСнаб', 'Регулятор скорости MTY 2.5 ON (в корпусе)'),
(201, 1, 'Рекуператор пластинчатый RHPr 500-300', '', 'Рекуператор пластинчатый RHPr 500-300', 'Купить Рекуператор пластинчатый RHPr 500-300 в ВентСтройСнаб по лучшей цене', 'Покупайте Рекуператор пластинчатый RHPr 500-300 в магазине ВентСтройСнаб по лучшей цене', 'Рекуператор пластинчатый RHPr 500-300,ВентСтройСнаб', 'Рекуператор пластинчатый RHPr 500-300'),
(198, 1, 'Пульт управления Shuft ARC 121', '', 'Пульт управления Shuft ARC 121', 'Купить Пульт управления Shuft ARC 121 в ВентСтройСнаб по лучшей цене', 'Покупайте Пульт управления Shuft ARC 121 в магазине ВентСтройСнаб по лучшей цене', 'Пульт управления Shuft ARC 121,ВентСтройСнаб', 'Пульт управления Shuft ARC 121'),
(199, 1, 'Регулятор скорости 5-ти ступенчатый с термозащитой SRE-E-1.5-T', '', 'Регулятор скорости 5-ти ступенчатый с термозащитой SRE-E-1.5-T', 'Купить Регулятор скорости 5-ти ступенчатый с термозащитой SRE-E-1.5-T в ВентСтройСнаб по лучшей цене', 'Покупайте Регулятор скорости 5-ти ступенчатый с термозащитой SRE-E-1.5-T в магазине ВентСтройСнаб по лучшей цене', 'Регулятор скорости 5-ти ступенчатый с термозащитой SRE-E-1.5-T,ВентСтройСнаб', 'Регулятор скорости 5-ти ступенчатый с термозащитой SRE-E-1.5-T'),
(195, 1, 'Помпа Aspen HI-flow MAX 1.7L наливная', '', 'Помпа Aspen HI-flow MAX 1.7L наливная', 'Купить Помпа Aspen HI-flow MAX 1.7L наливная в ВентСтройСнаб по лучшей цене', 'Покупайте Помпа Aspen HI-flow MAX 1.7L наливная в магазине ВентСтройСнаб по лучшей цене', 'Помпа Aspen HI-flow MAX 1.7L наливная,ВентСтройСнаб', 'Помпа Aspen HI-flow MAX 1.7L наливная'),
(196, 1, 'Помпа Siccom Eco Flowatch  13л/ч 10 м', '', 'Помпа Siccom Eco Flowatch  13л/ч 10 м', 'Купить Помпа Siccom Eco Flowatch  13л/ч 10 м в ВентСтройСнаб по лучшей цене', 'Покупайте Помпа Siccom Eco Flowatch  13л/ч 10 м в магазине ВентСтройСнаб по лучшей цене', 'Помпа Siccom Eco Flowatch  13л/ч 10 м,ВентСтройСнаб', 'Помпа Siccom Eco Flowatch  13л/ч 10 м'),
(197, 1, 'Помпа Siccom Maxi Eco Flowatch 40л/ч 10 м', '', 'Помпа Siccom Maxi Eco Flowatch 40л/ч 10 м', 'Купить Помпа Siccom Maxi Eco Flowatch 40л/ч 10 м в ВентСтройСнаб по лучшей цене', 'Покупайте Помпа Siccom Maxi Eco Flowatch 40л/ч 10 м в магазине ВентСтройСнаб по лучшей цене', 'Помпа Siccom Maxi Eco Flowatch 40л/ч 10 м,ВентСтройСнаб', 'Помпа Siccom Maxi Eco Flowatch 40л/ч 10 м'),
(194, 1, 'Панель управления LCP для FC-051(132B0101)', '', 'Панель управления LCP для FC-051(132B0101)', 'Купить Панель управления LCP для FC-051(132B0101) в ВентСтройСнаб по лучшей цене', 'Покупайте Панель управления LCP для FC-051(132B0101) в магазине ВентСтройСнаб по лучшей цене', 'Панель управления LCP для FC-051(132B0101),ВентСтройСнаб', 'Панель управления LCP для FC-051(132B0101)'),
(193, 1, 'Обратный клапан RSK 315', '', 'Обратный клапан RSK 315', 'Купить Обратный клапан RSK 315 в ВентСтройСнаб по лучшей цене', 'Покупайте Обратный клапан RSK 315 в магазине ВентСтройСнаб по лучшей цене', 'Обратный клапан RSK 315,ВентСтройСнаб', 'Обратный клапан RSK 315'),
(192, 1, 'Обратный клапан RSK 250', '', 'Обратный клапан RSK 250', 'Купить Обратный клапан RSK 250 в ВентСтройСнаб по лучшей цене', 'Покупайте Обратный клапан RSK 250 в магазине ВентСтройСнаб по лучшей цене', 'Обратный клапан RSK 250,ВентСтройСнаб', 'Обратный клапан RSK 250'),
(191, 1, 'Обратный клапан RSK 200', '', 'Обратный клапан RSK 200', 'Купить Обратный клапан RSK 200 в ВентСтройСнаб по лучшей цене', 'Покупайте Обратный клапан RSK 200 в магазине ВентСтройСнаб по лучшей цене', 'Обратный клапан RSK 200,ВентСтройСнаб', 'Обратный клапан RSK 200'),
(190, 1, 'Обратный клапан RSK 160', '', 'Обратный клапан RSK 160', 'Купить Обратный клапан RSK 160 в ВентСтройСнаб по лучшей цене', 'Покупайте Обратный клапан RSK 160 в магазине ВентСтройСнаб по лучшей цене', 'Обратный клапан RSK 160,ВентСтройСнаб', 'Обратный клапан RSK 160'),
(188, 1, 'Обратный клапан RSK 100', '', 'Обратный клапан RSK 100', 'Купить Обратный клапан RSK 100 в ВентСтройСнаб по лучшей цене', 'Покупайте Обратный клапан RSK 100 в магазине ВентСтройСнаб по лучшей цене', 'Обратный клапан RSK 100,ВентСтройСнаб', 'Обратный клапан RSK 100'),
(189, 1, 'Обратный клапан RSK 125', '', 'Обратный клапан RSK 125', 'Купить Обратный клапан RSK 125 в ВентСтройСнаб по лучшей цене', 'Покупайте Обратный клапан RSK 125 в магазине ВентСтройСнаб по лучшей цене', 'Обратный клапан RSK 125,ВентСтройСнаб', 'Обратный клапан RSK 125'),
(183, 1, 'Комплект NEMA1-M3 (для FC-051 4 кВт)', '', 'Комплект NEMA1-M3 (для FC-051 4 кВт)', 'Купить Комплект NEMA1-M3 (для FC-051 4 кВт) в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект NEMA1-M3 (для FC-051 4 кВт) в магазине ВентСтройСнаб по лучшей цене', 'Комплект NEMA1-M3 (для FC-051 4 кВт),ВентСтройСнаб', 'Комплект NEMA1-M3 (для FC-051 4 кВт)'),
(184, 1, 'Комплект NEMA1-M3 (для FC-051 5,5 кВт)', '', 'Комплект NEMA1-M3 (для FC-051 5,5 кВт)', 'Купить Комплект NEMA1-M3 (для FC-051 5,5 кВт) в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект NEMA1-M3 (для FC-051 5,5 кВт) в магазине ВентСтройСнаб по лучшей цене', 'Комплект NEMA1-M3 (для FC-051 5,5 кВт),ВентСтройСнаб', 'Комплект NEMA1-M3 (для FC-051 5,5 кВт)'),
(185, 1, 'Комплект NEMA1-M3 (для FC-051 7,5 кВт)', '', 'Комплект NEMA1-M3 (для FC-051 7,5 кВт)', 'Купить Комплект NEMA1-M3 (для FC-051 7,5 кВт) в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект NEMA1-M3 (для FC-051 7,5 кВт) в магазине ВентСтройСнаб по лучшей цене', 'Комплект NEMA1-M3 (для FC-051 7,5 кВт),ВентСтройСнаб', 'Комплект NEMA1-M3 (для FC-051 7,5 кВт)'),
(186, 1, 'Комплект NEMA1-M4 (для FC-051 11 кВт)', '', 'Комплект NEMA1-M4 (для FC-051 11 кВт)', 'Купить Комплект NEMA1-M4 (для FC-051 11 кВт) в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект NEMA1-M4 (для FC-051 11 кВт) в магазине ВентСтройСнаб по лучшей цене', 'Комплект NEMA1-M4 (для FC-051 11 кВт),ВентСтройСнаб', 'Комплект NEMA1-M4 (для FC-051 11 кВт)'),
(187, 1, 'Комплект NEMA1-M4 (для FC-051 15,0 кВт)', '', 'Комплект NEMA1-M4 (для FC-051 15,0 кВт)', 'Купить Комплект NEMA1-M4 (для FC-051 15,0 кВт) в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект NEMA1-M4 (для FC-051 15,0 кВт) в магазине ВентСтройСнаб по лучшей цене', 'Комплект NEMA1-M4 (для FC-051 15,0 кВт),ВентСтройСнаб', 'Комплект NEMA1-M4 (для FC-051 15,0 кВт)'),
(181, 1, 'Комплект NEMA1-M2 (для FC-051 2,2 кВт)', '', 'Комплект NEMA1-M2 (для FC-051 2,2 кВт)', 'Купить Комплект NEMA1-M2 (для FC-051 2,2 кВт) в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект NEMA1-M2 (для FC-051 2,2 кВт) в магазине ВентСтройСнаб по лучшей цене', 'Комплект NEMA1-M2 (для FC-051 2,2 кВт),ВентСтройСнаб', 'Комплект NEMA1-M2 (для FC-051 2,2 кВт)'),
(182, 1, 'Комплект NEMA1-M3 (для FC-051 3 кВт)', '', 'Комплект NEMA1-M3 (для FC-051 3 кВт)', 'Купить Комплект NEMA1-M3 (для FC-051 3 кВт) в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект NEMA1-M3 (для FC-051 3 кВт) в магазине ВентСтройСнаб по лучшей цене', 'Комплект NEMA1-M3 (для FC-051 3 кВт),ВентСтройСнаб', 'Комплект NEMA1-M3 (для FC-051 3 кВт)'),
(179, 1, 'Комплект NEMA1-M1 (для FC-051 0,75 кВт)', '', 'Комплект NEMA1-M1 (для FC-051 0,75 кВт)', 'Купить Комплект NEMA1-M1 (для FC-051 0,75 кВт) в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект NEMA1-M1 (для FC-051 0,75 кВт) в магазине ВентСтройСнаб по лучшей цене', 'Комплект NEMA1-M1 (для FC-051 0,75 кВт),ВентСтройСнаб', 'Комплект NEMA1-M1 (для FC-051 0,75 кВт)'),
(180, 1, 'Комплект NEMA1-M2 (для FC-051 1,5 кВт)', '', 'Комплект NEMA1-M2 (для FC-051 1,5 кВт)', 'Купить Комплект NEMA1-M2 (для FC-051 1,5 кВт) в ВентСтройСнаб по лучшей цене', 'Покупайте Комплект NEMA1-M2 (для FC-051 1,5 кВт) в магазине ВентСтройСнаб по лучшей цене', 'Комплект NEMA1-M2 (для FC-051 1,5 кВт),ВентСтройСнаб', 'Комплект NEMA1-M2 (для FC-051 1,5 кВт)'),
(178, 1, 'Клапан КПС-1м(60)-НО-МВ(220)-300х300', '', 'Клапан КПС-1м(60)-НО-МВ(220)-300х300', 'Купить Клапан КПС-1м(60)-НО-МВ(220)-300х300 в ВентСтройСнаб по лучшей цене', 'Покупайте Клапан КПС-1м(60)-НО-МВ(220)-300х300 в магазине ВентСтройСнаб по лучшей цене', 'Клапан КПС-1м(60)-НО-МВ(220)-300х300,ВентСтройСнаб', 'Клапан КПС-1м(60)-НО-МВ(220)-300х300'),
(175, 1, 'Клапан КПС-1м(60)-НО-МВ(220)-250х250', '', 'Клапан КПС-1м(60)-НО-МВ(220)-250х250', 'Купить Клапан КПС-1м(60)-НО-МВ(220)-250х250 в ВентСтройСнаб по лучшей цене', 'Покупайте Клапан КПС-1м(60)-НО-МВ(220)-250х250 в магазине ВентСтройСнаб по лучшей цене', 'Клапан КПС-1м(60)-НО-МВ(220)-250х250,ВентСтройСнаб', 'Клапан КПС-1м(60)-НО-МВ(220)-250х250'),
(177, 1, 'Клапан КПС-1м(60)-НО-МВ(220)-300х200', '', 'Клапан КПС-1м(60)-НО-МВ(220)-300х200', 'Купить Клапан КПС-1м(60)-НО-МВ(220)-300х200 в ВентСтройСнаб по лучшей цене', 'Покупайте Клапан КПС-1м(60)-НО-МВ(220)-300х200 в магазине ВентСтройСнаб по лучшей цене', 'Клапан КПС-1м(60)-НО-МВ(220)-300х200,ВентСтройСнаб', 'Клапан КПС-1м(60)-НО-МВ(220)-300х200'),
(176, 1, 'Клапан КПС-1м(60)-НО-МВ(220)-300х150', '', 'Клапан КПС-1м(60)-НО-МВ(220)-300х150', 'Купить Клапан КПС-1м(60)-НО-МВ(220)-300х150 в ВентСтройСнаб по лучшей цене', 'Покупайте Клапан КПС-1м(60)-НО-МВ(220)-300х150 в магазине ВентСтройСнаб по лучшей цене', 'Клапан КПС-1м(60)-НО-МВ(220)-300х150,ВентСтройСнаб', 'Клапан КПС-1м(60)-НО-МВ(220)-300х150'),
(173, 1, 'Клапан КПС-1м(60)-НО-МВ(220)-200х150', '', 'Клапан КПС-1м(60)-НО-МВ(220)-200х150', 'Купить Клапан КПС-1м(60)-НО-МВ(220)-200х150 в ВентСтройСнаб по лучшей цене', 'Покупайте Клапан КПС-1м(60)-НО-МВ(220)-200х150 в магазине ВентСтройСнаб по лучшей цене', 'Клапан КПС-1м(60)-НО-МВ(220)-200х150,ВентСтройСнаб', 'Клапан КПС-1м(60)-НО-МВ(220)-200х150'),
(174, 1, 'Клапан КПС-1м(60)-НО-МВ(220)-200х200', '', 'Клапан КПС-1м(60)-НО-МВ(220)-200х200', 'Купить Клапан КПС-1м(60)-НО-МВ(220)-200х200 в ВентСтройСнаб по лучшей цене', 'Покупайте Клапан КПС-1м(60)-НО-МВ(220)-200х200 в магазине ВентСтройСнаб по лучшей цене', 'Клапан КПС-1м(60)-НО-МВ(220)-200х200,ВентСтройСнаб', 'Клапан КПС-1м(60)-НО-МВ(220)-200х200'),
(171, 1, 'Клапан КПС-1м(60)-НО-МВ(220)-150x150', '', 'Клапан КПС-1м(60)-НО-МВ(220)-150x150', 'Купить Клапан КПС-1м(60)-НО-МВ(220)-150x150 в ВентСтройСнаб по лучшей цене', 'Покупайте Клапан КПС-1м(60)-НО-МВ(220)-150x150 в магазине ВентСтройСнаб по лучшей цене', 'Клапан КПС-1м(60)-НО-МВ(220)-150x150,ВентСтройСнаб', 'Клапан КПС-1м(60)-НО-МВ(220)-150x150'),
(172, 1, 'Клапан КПС-1м(60)-НО-МВ(220)-200х100', '', 'Клапан КПС-1м(60)-НО-МВ(220)-200х100', 'Купить Клапан КПС-1м(60)-НО-МВ(220)-200х100 в ВентСтройСнаб по лучшей цене', 'Покупайте Клапан КПС-1м(60)-НО-МВ(220)-200х100 в магазине ВентСтройСнаб по лучшей цене', 'Клапан КПС-1м(60)-НО-МВ(220)-200х100,ВентСтройСнаб', 'Клапан КПС-1м(60)-НО-МВ(220)-200х100'),
(169, 1, 'Клапан КПС-1м(60)-НО-МВ(220)-100x100', '', 'Клапан КПС-1м(60)-НО-МВ(220)-100x100', 'Купить Клапан КПС-1м(60)-НО-МВ(220)-100x100 в ВентСтройСнаб по лучшей цене', 'Покупайте Клапан КПС-1м(60)-НО-МВ(220)-100x100 в магазине ВентСтройСнаб по лучшей цене', 'Клапан КПС-1м(60)-НО-МВ(220)-100x100,ВентСтройСнаб', 'Клапан КПС-1м(60)-НО-МВ(220)-100x100'),
(170, 1, 'Клапан КПС-1м(60)-НО-МВ(220)-150x100', '', 'Клапан КПС-1м(60)-НО-МВ(220)-150x100', 'Купить Клапан КПС-1м(60)-НО-МВ(220)-150x100 в ВентСтройСнаб по лучшей цене', 'Покупайте Клапан КПС-1м(60)-НО-МВ(220)-150x100 в магазине ВентСтройСнаб по лучшей цене', 'Клапан КПС-1м(60)-НО-МВ(220)-150x100,ВентСтройСнаб', 'Клапан КПС-1м(60)-НО-МВ(220)-150x100'),
(164, 1, 'Кассетный фильтр (корпус с материалом) BKS 125', '', 'Кассетный фильтр (корпус с материалом) BKS 125', 'Купить Кассетный фильтр (корпус с материалом) BKS 125 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный фильтр (корпус с материалом) BKS 125 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный фильтр (корпус с материалом) BKS 125,ВентСтройСнаб', 'Кассетный фильтр (корпус с материалом) BKS 125'),
(165, 1, 'Кассетный фильтр (корпус с материалом) BKS 160', '', 'Кассетный фильтр (корпус с материалом) BKS 160', 'Купить Кассетный фильтр (корпус с материалом) BKS 160 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный фильтр (корпус с материалом) BKS 160 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный фильтр (корпус с материалом) BKS 160,ВентСтройСнаб', 'Кассетный фильтр (корпус с материалом) BKS 160'),
(166, 1, 'Кассетный фильтр (корпус с материалом) BKS 200', '', 'Кассетный фильтр (корпус с материалом) BKS 200', 'Купить Кассетный фильтр (корпус с материалом) BKS 200 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный фильтр (корпус с материалом) BKS 200 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный фильтр (корпус с материалом) BKS 200,ВентСтройСнаб', 'Кассетный фильтр (корпус с материалом) BKS 200'),
(167, 1, 'Кассетный фильтр (корпус с материалом) BKS 250', '', 'Кассетный фильтр (корпус с материалом) BKS 250', 'Купить Кассетный фильтр (корпус с материалом) BKS 250 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный фильтр (корпус с материалом) BKS 250 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный фильтр (корпус с материалом) BKS 250,ВентСтройСнаб', 'Кассетный фильтр (корпус с материалом) BKS 250'),
(168, 1, 'Кассетный фильтр (корпус с материалом) BKS 315', '', 'Кассетный фильтр (корпус с материалом) BKS 315', 'Купить Кассетный фильтр (корпус с материалом) BKS 315 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный фильтр (корпус с материалом) BKS 315 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный фильтр (корпус с материалом) BKS 315,ВентСтройСнаб', 'Кассетный фильтр (корпус с материалом) BKS 315'),
(160, 1, 'Кассетный корпус фильтра BKSP 70-40', '', 'Кассетный корпус фильтра BKSP 70-40', 'Купить Кассетный корпус фильтра BKSP 70-40 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный корпус фильтра BKSP 70-40 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный корпус фильтра BKSP 70-40,ВентСтройСнаб', 'Кассетный корпус фильтра BKSP 70-40'),
(161, 1, 'Кассетный корпус фильтра BKSP 80-50', '', 'Кассетный корпус фильтра BKSP 80-50', 'Купить Кассетный корпус фильтра BKSP 80-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный корпус фильтра BKSP 80-50 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный корпус фильтра BKSP 80-50,ВентСтройСнаб', 'Кассетный корпус фильтра BKSP 80-50'),
(162, 1, 'Кассетный корпус фильтра BKSP 90-50', '', 'Кассетный корпус фильтра BKSP 90-50', 'Купить Кассетный корпус фильтра BKSP 90-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный корпус фильтра BKSP 90-50 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный корпус фильтра BKSP 90-50,ВентСтройСнаб', 'Кассетный корпус фильтра BKSP 90-50'),
(163, 1, 'Кассетный корпус фильтра BKSP 100-50', '', 'Кассетный корпус фильтра BKSP 100-50', 'Купить Кассетный корпус фильтра BKSP 100-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный корпус фильтра BKSP 100-50 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный корпус фильтра BKSP 100-50,ВентСтройСнаб', 'Кассетный корпус фильтра BKSP 100-50'),
(158, 1, 'Кассетный корпус фильтра BKSP 60-30', '', 'Кассетный корпус фильтра BKSP 60-30', 'Купить Кассетный корпус фильтра BKSP 60-30 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный корпус фильтра BKSP 60-30 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный корпус фильтра BKSP 60-30,ВентСтройСнаб', 'Кассетный корпус фильтра BKSP 60-30'),
(159, 1, 'Кассетный корпус фильтра BKSP 60-35', '', 'Кассетный корпус фильтра BKSP 60-35', 'Купить Кассетный корпус фильтра BKSP 60-35 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный корпус фильтра BKSP 60-35 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный корпус фильтра BKSP 60-35,ВентСтройСнаб', 'Кассетный корпус фильтра BKSP 60-35'),
(155, 1, 'Карманный корпус фильтра BKP 90-50', '', 'Карманный корпус фильтра BKP 90-50', 'Купить Карманный корпус фильтра BKP 90-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Карманный корпус фильтра BKP 90-50 в магазине ВентСтройСнаб по лучшей цене', 'Карманный корпус фильтра BKP 90-50,ВентСтройСнаб', 'Карманный корпус фильтра BKP 90-50'),
(156, 1, 'Карманный корпус фильтра BKP 100-50', '', 'Карманный корпус фильтра BKP 100-50', 'Купить Карманный корпус фильтра BKP 100-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Карманный корпус фильтра BKP 100-50 в магазине ВентСтройСнаб по лучшей цене', 'Карманный корпус фильтра BKP 100-50,ВентСтройСнаб', 'Карманный корпус фильтра BKP 100-50'),
(157, 1, 'Кассетный корпус фильтра BKSP 50-30', '', 'Кассетный корпус фильтра BKSP 50-30', 'Купить Кассетный корпус фильтра BKSP 50-30 в ВентСтройСнаб по лучшей цене', 'Покупайте Кассетный корпус фильтра BKSP 50-30 в магазине ВентСтройСнаб по лучшей цене', 'Кассетный корпус фильтра BKSP 50-30,ВентСтройСнаб', 'Кассетный корпус фильтра BKSP 50-30'),
(154, 1, 'Карманный корпус фильтра BКР 80-50', '', 'Карманный корпус фильтра BКР 80-50', 'Купить Карманный корпус фильтра BКР 80-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Карманный корпус фильтра BКР 80-50 в магазине ВентСтройСнаб по лучшей цене', 'Карманный корпус фильтра BКР 80-50,ВентСтройСнаб', 'Карманный корпус фильтра BКР 80-50'),
(152, 1, 'Карманный корпус фильтра BKP 60-35', '', 'Карманный корпус фильтра BKP 60-35', 'Купить Карманный корпус фильтра BKP 60-35 в ВентСтройСнаб по лучшей цене', 'Покупайте Карманный корпус фильтра BKP 60-35 в магазине ВентСтройСнаб по лучшей цене', 'Карманный корпус фильтра BKP 60-35,ВентСтройСнаб', 'Карманный корпус фильтра BKP 60-35'),
(150, 1, 'Карманный корпус фильтра BKP 50-30', '', 'Карманный корпус фильтра BKP 50-30', 'Купить Карманный корпус фильтра BKP 50-30 в ВентСтройСнаб по лучшей цене', 'Покупайте Карманный корпус фильтра BKP 50-30 в магазине ВентСтройСнаб по лучшей цене', 'Карманный корпус фильтра BKP 50-30,ВентСтройСнаб', 'Карманный корпус фильтра BKP 50-30'),
(151, 1, 'Карманный корпус фильтра BКР 60-30', '', 'Карманный корпус фильтра BКР 60-30', 'Купить Карманный корпус фильтра BКР 60-30 в ВентСтройСнаб по лучшей цене', 'Покупайте Карманный корпус фильтра BКР 60-30 в магазине ВентСтройСнаб по лучшей цене', 'Карманный корпус фильтра BКР 60-30,ВентСтройСнаб', 'Карманный корпус фильтра BКР 60-30'),
(153, 1, 'Карманный корпус фильтра BKP 70-40', '', 'Карманный корпус фильтра BKP 70-40', 'Купить Карманный корпус фильтра BKP 70-40 в ВентСтройСнаб по лучшей цене', 'Покупайте Карманный корпус фильтра BKP 70-40 в магазине ВентСтройСнаб по лучшей цене', 'Карманный корпус фильтра BKP 70-40,ВентСтройСнаб', 'Карманный корпус фильтра BKP 70-40'),
(148, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 900х500', '', 'Зонт из оцинкованной стали для вентиляционных шахт 900х500', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 900х500 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 900х500 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 900х500,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 900х500'),
(149, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 1000х500', '', 'Зонт из оцинкованной стали для вентиляционных шахт 1000х500', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 1000х500 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 1000х500 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 1000х500,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 1000х500'),
(147, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 800х500', '', 'Зонт из оцинкованной стали для вентиляционных шахт 800х500', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 800х500 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 800х500 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 800х500,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 800х500'),
(145, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 600х300', '', 'Зонт из оцинкованной стали для вентиляционных шахт 600х300', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 600х300 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 600х300 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 600х300,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 600х300'),
(146, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 700х400', '', 'Зонт из оцинкованной стали для вентиляционных шахт 700х400', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 700х400 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 700х400 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 700х400,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 700х400'),
(143, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 400х250', '', 'Зонт из оцинкованной стали для вентиляционных шахт 400х250', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 400х250 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 400х250 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 400х250,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 400х250'),
(144, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 500х300', '', 'Зонт из оцинкованной стали для вентиляционных шахт 500х300', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 500х300 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 500х300 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 500х300,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 500х300'),
(141, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 300х300', '', 'Зонт из оцинкованной стали для вентиляционных шахт 300х300', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 300х300 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 300х300 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 300х300,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 300х300'),
(142, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 400х200', '', 'Зонт из оцинкованной стали для вентиляционных шахт 400х200', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 400х200 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 400х200 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 400х200,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 400х200'),
(139, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 300х150', '', 'Зонт из оцинкованной стали для вентиляционных шахт 300х150', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 300х150 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 300х150 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 300х150,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 300х150'),
(140, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 300х200', '', 'Зонт из оцинкованной стали для вентиляционных шахт 300х200', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 300х200 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 300х200 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 300х200,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 300х200'),
(137, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 200х200', '', 'Зонт из оцинкованной стали для вентиляционных шахт 200х200', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 200х200 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 200х200 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 200х200,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 200х200'),
(138, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 250x250', '', 'Зонт из оцинкованной стали для вентиляционных шахт 250x250', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 250x250 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 250x250 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 250x250,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 250x250'),
(136, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 200х150', '', 'Зонт из оцинкованной стали для вентиляционных шахт 200х150', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 200х150 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 200х150 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 200х150,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 200х150'),
(135, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 200х100', '', 'Зонт из оцинкованной стали для вентиляционных шахт 200х100', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 200х100 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 200х100 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 200х100,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 200х100'),
(134, 1, 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', '', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150', 'Купить Зонт из оцинкованной стали для вентиляционных шахт 150х150 в ВентСтройСнаб по лучшей цене', 'Покупайте Зонт из оцинкованной стали для вентиляционных шахт 150х150 в магазине ВентСтройСнаб по лучшей цене', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150,ВентСтройСнаб', 'Зонт из оцинкованной стали для вентиляционных шахт 150х150'),
(133, 1, 'Дроссель клапан с ручным управлением 250x250', '', 'Дроссель клапан с ручным управлением 250x250', 'Купить Дроссель клапан с ручным управлением 250x250 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 250x250 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 250x250,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 250x250'),
(132, 1, 'Дроссель клапан с ручным управлением 200x200', '', 'Дроссель клапан с ручным управлением 200x200', 'Купить Дроссель клапан с ручным управлением 200x200 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 200x200 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 200x200,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 200x200'),
(131, 1, 'Дроссель клапан с ручным управлением 200x150', '', 'Дроссель клапан с ручным управлением 200x150', 'Купить Дроссель клапан с ручным управлением 200x150 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 200x150 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 200x150,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 200x150'),
(129, 1, 'Дроссель клапан с ручным управлением 150x150', '', 'Дроссель клапан с ручным управлением 150x150', 'Купить Дроссель клапан с ручным управлением 150x150 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 150x150 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 150x150,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 150x150'),
(130, 1, 'Дроссель клапан с ручным управлением 200x100', '', 'Дроссель клапан с ручным управлением 200x100', 'Купить Дроссель клапан с ручным управлением 200x100 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 200x100 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 200x100,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 200x100'),
(128, 1, 'Дроссель клапан с ручным управлением 150x100', '', 'Дроссель клапан с ручным управлением 150x100', 'Купить Дроссель клапан с ручным управлением 150x100 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 150x100 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 150x100,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 150x100'),
(126, 1, 'Дроссель клапан с ручным управлением 315', '', 'Дроссель клапан с ручным управлением 315', 'Купить Дроссель клапан с ручным управлением 315 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 315 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 315,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 315'),
(127, 1, 'Дроссель клапан с ручным управлением 100x100', '', 'Дроссель клапан с ручным управлением 100x100', 'Купить Дроссель клапан с ручным управлением 100x100 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 100x100 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 100x100,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 100x100'),
(125, 1, 'Дроссель клапан с ручным управлением 250', '', 'Дроссель клапан с ручным управлением 250', 'Купить Дроссель клапан с ручным управлением 250 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 250 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 250,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 250'),
(124, 1, 'Дроссель клапан с ручным управлением 200', '', 'Дроссель клапан с ручным управлением 200', 'Купить Дроссель клапан с ручным управлением 200 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 200 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 200,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 200'),
(123, 1, 'Дроссель клапан с ручным управлением 160', '', 'Дроссель клапан с ручным управлением 160', 'Купить Дроссель клапан с ручным управлением 160 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 160 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 160,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 160'),
(120, 1, 'Диффузор DVA d200', '', 'Диффузор DVA d200', 'Купить Диффузор DVA d200 в ВентСтройСнаб по лучшей цене', 'Покупайте Диффузор DVA d200 в магазине ВентСтройСнаб по лучшей цене', 'Диффузор DVA d200,ВентСтройСнаб', 'Диффузор DVA d200'),
(121, 1, 'Диффузор DVA d250', '', 'Диффузор DVA d250', 'Купить Диффузор DVA d250 в ВентСтройСнаб по лучшей цене', 'Покупайте Диффузор DVA d250 в магазине ВентСтройСнаб по лучшей цене', 'Диффузор DVA d250,ВентСтройСнаб', 'Диффузор DVA d250'),
(122, 1, 'Дроссель клапан с ручным управлением 125', '', 'Дроссель клапан с ручным управлением 125', 'Купить Дроссель клапан с ручным управлением 125 в ВентСтройСнаб по лучшей цене', 'Покупайте Дроссель клапан с ручным управлением 125 в магазине ВентСтройСнаб по лучшей цене', 'Дроссель клапан с ручным управлением 125,ВентСтройСнаб', 'Дроссель клапан с ручным управлением 125'),
(118, 1, 'Диффузор DVA d125', '', 'Диффузор DVA d125', 'Купить Диффузор DVA d125 в ВентСтройСнаб по лучшей цене', 'Покупайте Диффузор DVA d125 в магазине ВентСтройСнаб по лучшей цене', 'Диффузор DVA d125,ВентСтройСнаб', 'Диффузор DVA d125'),
(119, 1, 'Диффузор DVA d160', '', 'Диффузор DVA d160', 'Купить Диффузор DVA d160 в ВентСтройСнаб по лучшей цене', 'Покупайте Диффузор DVA d160 в магазине ВентСтройСнаб по лучшей цене', 'Диффузор DVA d160,ВентСтройСнаб', 'Диффузор DVA d160'),
(117, 1, 'Диффузор DVA d100', '', 'Диффузор DVA d100', 'Купить Диффузор DVA d100 в ВентСтройСнаб по лучшей цене', 'Покупайте Диффузор DVA d100 в магазине ВентСтройСнаб по лучшей цене', 'Диффузор DVA d100,ВентСтройСнаб', 'Диффузор DVA d100'),
(115, 1, 'Датчик температуры канальный HTF-PT1000', '', 'Датчик температуры канальный HTF-PT1000', 'Купить Датчик температуры канальный HTF-PT1000 в ВентСтройСнаб по лучшей цене', 'Покупайте Датчик температуры канальный HTF-PT1000 в магазине ВентСтройСнаб по лучшей цене', 'Датчик температуры канальный HTF-PT1000,ВентСтройСнаб', 'Датчик температуры канальный HTF-PT1000'),
(116, 1, 'Датчик температуры канальный  250мм PT1000 (бескорпусный К02)', '', 'Датчик температуры канальный  250мм PT1000 (бескорпусный К02)', 'Купить Датчик температуры канальный  250мм PT1000 (бескорпусный К02) в ВентСтройСнаб по лучшей цене', 'Покупайте Датчик температуры канальный  250мм PT1000 (бескорпусный К02) в магазине ВентСтройСнаб по лучшей цене', 'Датчик температуры канальный  250мм PT1000 (бескорпусный К02),ВентСтройСнаб', 'Датчик температуры канальный  250мм PT1000 (бескорпусный К02)'),
(114, 1, 'Датчик температуры канальный ETF-1144/99-AN-NTC (для щитов BM-Mini)', '', 'Датчик температуры канальный ETF-1144/99-AN-NTC (для щитов BM-Mini)', 'Купить Датчик температуры канальный ETF-1144/99-AN-NTC (для щитов BM-Mini) в ВентСтройСнаб по лучшей цене', 'Покупайте Датчик температуры канальный ETF-1144/99-AN-NTC (для щитов BM-Mini) в магазине ВентСтройСнаб по лучшей цене', 'Датчик температуры канальный ETF-1144/99-AN-NTC (для щитов BM-Mini),ВентСтройСнаб', 'Датчик температуры канальный ETF-1144/99-AN-NTC (для щитов BM-Mini)'),
(113, 1, 'Датчик температуры воды погружной ЕТF01-PT 1000', '', 'Датчик температуры воды погружной ЕТF01-PT 1000', 'Купить Датчик температуры воды погружной ЕТF01-PT 1000 в ВентСтройСнаб по лучшей цене', 'Покупайте Датчик температуры воды погружной ЕТF01-PT 1000 в магазине ВентСтройСнаб по лучшей цене', 'Датчик температуры воды погружной ЕТF01-PT 1000,ВентСтройСнаб', 'Датчик температуры воды погружной ЕТF01-PT 1000'),
(112, 1, 'Датчик давления DPS 500', '', 'Датчик давления DPS 500', 'Купить Датчик давления DPS 500 в ВентСтройСнаб по лучшей цене', 'Покупайте Датчик давления DPS 500 в магазине ВентСтройСнаб по лучшей цене', 'Датчик давления DPS 500,ВентСтройСнаб', 'Датчик давления DPS 500'),
(111, 1, 'Гибкие воздуховоды изолированные ISO 315', '', 'Гибкие воздуховоды изолированные ISO 315', 'Купить Гибкие воздуховоды изолированные ISO 315 в ВентСтройСнаб по лучшей цене', 'Покупайте Гибкие воздуховоды изолированные ISO 315 в магазине ВентСтройСнаб по лучшей цене', 'Гибкие воздуховоды изолированные ISO 315,ВентСтройСнаб', 'Гибкие воздуховоды изолированные ISO 315'),
(110, 1, 'Гибкие воздуховоды изолированные ISO 254', '', 'Гибкие воздуховоды изолированные ISO 254', 'Купить Гибкие воздуховоды изолированные ISO 254 в ВентСтройСнаб по лучшей цене', 'Покупайте Гибкие воздуховоды изолированные ISO 254 в магазине ВентСтройСнаб по лучшей цене', 'Гибкие воздуховоды изолированные ISO 254,ВентСтройСнаб', 'Гибкие воздуховоды изолированные ISO 254'),
(109, 1, 'Гибкие воздуховоды изолированные ISO 203', '', 'Гибкие воздуховоды изолированные ISO 203', 'Купить Гибкие воздуховоды изолированные ISO 203 в ВентСтройСнаб по лучшей цене', 'Покупайте Гибкие воздуховоды изолированные ISO 203 в магазине ВентСтройСнаб по лучшей цене', 'Гибкие воздуховоды изолированные ISO 203,ВентСтройСнаб', 'Гибкие воздуховоды изолированные ISO 203');
INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(107, 1, 'Гибкие воздуховоды изолированные ISO 127', '', 'Гибкие воздуховоды изолированные ISO 127', 'Купить Гибкие воздуховоды изолированные ISO 127 в ВентСтройСнаб по лучшей цене', 'Покупайте Гибкие воздуховоды изолированные ISO 127 в магазине ВентСтройСнаб по лучшей цене', 'Гибкие воздуховоды изолированные ISO 127,ВентСтройСнаб', 'Гибкие воздуховоды изолированные ISO 127'),
(108, 1, 'Гибкие воздуховоды изолированные ISO 160', '', 'Гибкие воздуховоды изолированные ISO 160', 'Купить Гибкие воздуховоды изолированные ISO 160 в ВентСтройСнаб по лучшей цене', 'Покупайте Гибкие воздуховоды изолированные ISO 160 в магазине ВентСтройСнаб по лучшей цене', 'Гибкие воздуховоды изолированные ISO 160,ВентСтройСнаб', 'Гибкие воздуховоды изолированные ISO 160'),
(103, 1, 'Гибкие воздуховоды армированные AF203 10м', '', 'Гибкие воздуховоды армированные AF203 10м', 'Купить Гибкие воздуховоды армированные AF203 10м в ВентСтройСнаб по лучшей цене', 'Покупайте Гибкие воздуховоды армированные AF203 10м в магазине ВентСтройСнаб по лучшей цене', 'Гибкие воздуховоды армированные AF203 10м,ВентСтройСнаб', 'Гибкие воздуховоды армированные AF203 10м'),
(104, 1, 'Гибкие воздуховоды армированные AF254 10м', '', 'Гибкие воздуховоды армированные AF254 10м', 'Купить Гибкие воздуховоды армированные AF254 10м в ВентСтройСнаб по лучшей цене', 'Покупайте Гибкие воздуховоды армированные AF254 10м в магазине ВентСтройСнаб по лучшей цене', 'Гибкие воздуховоды армированные AF254 10м,ВентСтройСнаб', 'Гибкие воздуховоды армированные AF254 10м'),
(105, 1, 'Гибкие воздуховоды армированные AF315 10м', '', 'Гибкие воздуховоды армированные AF315 10м', 'Купить Гибкие воздуховоды армированные AF315 10м в ВентСтройСнаб по лучшей цене', 'Покупайте Гибкие воздуховоды армированные AF315 10м в магазине ВентСтройСнаб по лучшей цене', 'Гибкие воздуховоды армированные AF315 10м,ВентСтройСнаб', 'Гибкие воздуховоды армированные AF315 10м'),
(106, 1, 'Гибкие воздуховоды изолированные ISO 102', '', 'Гибкие воздуховоды изолированные ISO 102', 'Купить Гибкие воздуховоды изолированные ISO 102 в ВентСтройСнаб по лучшей цене', 'Покупайте Гибкие воздуховоды изолированные ISO 102 в магазине ВентСтройСнаб по лучшей цене', 'Гибкие воздуховоды изолированные ISO 102,ВентСтройСнаб', 'Гибкие воздуховоды изолированные ISO 102'),
(102, 1, 'Гибкие воздуховоды армированные AF160 10м', '', 'Гибкие воздуховоды армированные AF160 10м', 'Купить Гибкие воздуховоды армированные AF160 10м в ВентСтройСнаб по лучшей цене', 'Покупайте Гибкие воздуховоды армированные AF160 10м в магазине ВентСтройСнаб по лучшей цене', 'Гибкие воздуховоды армированные AF160 10м,ВентСтройСнаб', 'Гибкие воздуховоды армированные AF160 10м'),
(101, 1, 'Гибкие воздуховоды армированные AF127 10м', '', 'Гибкие воздуховоды армированные AF127 10м', 'Купить Гибкие воздуховоды армированные AF127 10м в ВентСтройСнаб по лучшей цене', 'Покупайте Гибкие воздуховоды армированные AF127 10м в магазине ВентСтройСнаб по лучшей цене', 'Гибкие воздуховоды армированные AF127 10м,ВентСтройСнаб', 'Гибкие воздуховоды армированные AF127 10м'),
(98, 1, 'Вставка карманная фильтрующая FBR 90-50', '', 'Вставка карманная фильтрующая FBR 90-50', 'Купить Вставка карманная фильтрующая FBR 90-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка карманная фильтрующая FBR 90-50 в магазине ВентСтройСнаб по лучшей цене', 'Вставка карманная фильтрующая FBR 90-50,ВентСтройСнаб', 'Вставка карманная фильтрующая FBR 90-50'),
(99, 1, 'Вставка карманная фильтрующая FBR 100-50', '', 'Вставка карманная фильтрующая FBR 100-50', 'Купить Вставка карманная фильтрующая FBR 100-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка карманная фильтрующая FBR 100-50 в магазине ВентСтройСнаб по лучшей цене', 'Вставка карманная фильтрующая FBR 100-50,ВентСтройСнаб', 'Вставка карманная фильтрующая FBR 100-50'),
(100, 1, 'Гибкие воздуховоды армированные AF102 10м', '', 'Гибкие воздуховоды армированные AF102 10м', 'Купить Гибкие воздуховоды армированные AF102 10м в ВентСтройСнаб по лучшей цене', 'Покупайте Гибкие воздуховоды армированные AF102 10м в магазине ВентСтройСнаб по лучшей цене', 'Гибкие воздуховоды армированные AF102 10м,ВентСтройСнаб', 'Гибкие воздуховоды армированные AF102 10м'),
(97, 1, 'Вставка карманная фильтрующая FBR 80-50', '', 'Вставка карманная фильтрующая FBR 80-50', 'Купить Вставка карманная фильтрующая FBR 80-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка карманная фильтрующая FBR 80-50 в магазине ВентСтройСнаб по лучшей цене', 'Вставка карманная фильтрующая FBR 80-50,ВентСтройСнаб', 'Вставка карманная фильтрующая FBR 80-50'),
(96, 1, 'Вставка карманная фильтрующая FBR 70-40', '', 'Вставка карманная фильтрующая FBR 70-40', 'Купить Вставка карманная фильтрующая FBR 70-40 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка карманная фильтрующая FBR 70-40 в магазине ВентСтройСнаб по лучшей цене', 'Вставка карманная фильтрующая FBR 70-40,ВентСтройСнаб', 'Вставка карманная фильтрующая FBR 70-40'),
(95, 1, 'Вставка карманная фильтрующая FBR 60-35', '', 'Вставка карманная фильтрующая FBR 60-35', 'Купить Вставка карманная фильтрующая FBR 60-35 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка карманная фильтрующая FBR 60-35 в магазине ВентСтройСнаб по лучшей цене', 'Вставка карманная фильтрующая FBR 60-35,ВентСтройСнаб', 'Вставка карманная фильтрующая FBR 60-35'),
(92, 1, 'Воздушный клапан ZAP 100-50', '', 'Воздушный клапан ZAP 100-50', 'Купить Воздушный клапан ZAP 100-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан ZAP 100-50 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан ZAP 100-50,ВентСтройСнаб', 'Воздушный клапан ZAP 100-50'),
(93, 1, 'Вставка карманная фильтрующая FBR 50-30', '', 'Вставка карманная фильтрующая FBR 50-30', 'Купить Вставка карманная фильтрующая FBR 50-30 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка карманная фильтрующая FBR 50-30 в магазине ВентСтройСнаб по лучшей цене', 'Вставка карманная фильтрующая FBR 50-30,ВентСтройСнаб', 'Вставка карманная фильтрующая FBR 50-30'),
(94, 1, 'Вставка карманная фильтрующая FBR 60-30', '', 'Вставка карманная фильтрующая FBR 60-30', 'Купить Вставка карманная фильтрующая FBR 60-30 в ВентСтройСнаб по лучшей цене', 'Покупайте Вставка карманная фильтрующая FBR 60-30 в магазине ВентСтройСнаб по лучшей цене', 'Вставка карманная фильтрующая FBR 60-30,ВентСтройСнаб', 'Вставка карманная фильтрующая FBR 60-30'),
(91, 1, 'Воздушный клапан ZAP 90-50', '', 'Воздушный клапан ZAP 90-50', 'Купить Воздушный клапан ZAP 90-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан ZAP 90-50 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан ZAP 90-50,ВентСтройСнаб', 'Воздушный клапан ZAP 90-50'),
(89, 1, 'Воздушный клапан ZAP 70-40', '', 'Воздушный клапан ZAP 70-40', 'Купить Воздушный клапан ZAP 70-40 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан ZAP 70-40 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан ZAP 70-40,ВентСтройСнаб', 'Воздушный клапан ZAP 70-40'),
(90, 1, 'Воздушный клапан ZAP 80-50', '', 'Воздушный клапан ZAP 80-50', 'Купить Воздушный клапан ZAP 80-50 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан ZAP 80-50 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан ZAP 80-50,ВентСтройСнаб', 'Воздушный клапан ZAP 80-50'),
(88, 1, 'Воздушный клапан ZAP 60-35', '', 'Воздушный клапан ZAP 60-35', 'Купить Воздушный клапан ZAP 60-35 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан ZAP 60-35 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан ZAP 60-35,ВентСтройСнаб', 'Воздушный клапан ZAP 60-35'),
(83, 1, 'Воздушный клапан DCGAr 200', '', 'Воздушный клапан DCGAr 200', 'Купить Воздушный клапан DCGAr 200 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан DCGAr 200 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан DCGAr 200,ВентСтройСнаб', 'Воздушный клапан DCGAr 200'),
(84, 1, 'Воздушный клапан DCGAr 250', '', 'Воздушный клапан DCGAr 250', 'Купить Воздушный клапан DCGAr 250 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан DCGAr 250 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан DCGAr 250,ВентСтройСнаб', 'Воздушный клапан DCGAr 250'),
(85, 1, 'Воздушный клапан DCGAr 315', '', 'Воздушный клапан DCGAr 315', 'Купить Воздушный клапан DCGAr 315 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан DCGAr 315 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан DCGAr 315,ВентСтройСнаб', 'Воздушный клапан DCGAr 315'),
(86, 1, 'Воздушный клапан ZAP 50-30', '', 'Воздушный клапан ZAP 50-30', 'Купить Воздушный клапан ZAP 50-30 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан ZAP 50-30 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан ZAP 50-30,ВентСтройСнаб', 'Воздушный клапан ZAP 50-30'),
(87, 1, 'Воздушный клапан ZAP 60-30', '', 'Воздушный клапан ZAP 60-30', 'Купить Воздушный клапан ZAP 60-30 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан ZAP 60-30 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан ZAP 60-30,ВентСтройСнаб', 'Воздушный клапан ZAP 60-30'),
(81, 1, 'Воздушный клапан DCGAr 125', '', 'Воздушный клапан DCGAr 125', 'Купить Воздушный клапан DCGAr 125 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан DCGAr 125 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан DCGAr 125,ВентСтройСнаб', 'Воздушный клапан DCGAr 125'),
(82, 1, 'Воздушный клапан DCGAr 160', '', 'Воздушный клапан DCGAr 160', 'Купить Воздушный клапан DCGAr 160 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан DCGAr 160 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан DCGAr 160,ВентСтройСнаб', 'Воздушный клапан DCGAr 160'),
(80, 1, 'Воздушный клапан DCGAr 100', '', 'Воздушный клапан DCGAr 100', 'Купить Воздушный клапан DCGAr 100 в ВентСтройСнаб по лучшей цене', 'Покупайте Воздушный клапан DCGAr 100 в магазине ВентСтройСнаб по лучшей цене', 'Воздушный клапан DCGAr 100,ВентСтройСнаб', 'Воздушный клапан DCGAr 100'),
(79, 1, 'Воздуховоды из оцинкованной стали, 1 мм', '', 'Воздуховоды из оцинкованной стали, 1 мм', 'Купить Воздуховоды из оцинкованной стали, 1 мм в ВентСтройСнаб по лучшей цене', 'Покупайте Воздуховоды из оцинкованной стали, 1 мм в магазине ВентСтройСнаб по лучшей цене', 'Воздуховоды из оцинкованной стали, 1 мм,ВентСтройСнаб', 'Воздуховоды из оцинкованной стали, 1 мм'),
(78, 1, 'Воздуховоды из оцинкованной стали, 0,7 мм', '', 'Воздуховоды из оцинкованной стали, 0,7 мм', 'Купить Воздуховоды из оцинкованной стали, 0,7 мм в ВентСтройСнаб по лучшей цене', 'Покупайте Воздуховоды из оцинкованной стали, 0,7 мм в магазине ВентСтройСнаб по лучшей цене', 'Воздуховоды из оцинкованной стали, 0,7 мм,ВентСтройСнаб', 'Воздуховоды из оцинкованной стали, 0,7 мм'),
(77, 1, 'Воздуховоды из оцинкованной стали, 0,5 мм', '', 'Воздуховоды из оцинкованной стали, 0,5 мм', 'Купить Воздуховоды из оцинкованной стали, 0,5 мм в ВентСтройСнаб по лучшей цене', 'Покупайте Воздуховоды из оцинкованной стали, 0,5 мм в магазине ВентСтройСнаб по лучшей цене', 'Воздуховоды из оцинкованной стали, 0,5 мм,ВентСтройСнаб', 'Воздуховоды из оцинкованной стали, 0,5 мм'),
(75, 1, 'Водяной нагреватель WWP 90х50/3', '', 'Водяной нагреватель WWP 90х50/3', 'Купить Водяной нагреватель WWP 90х50/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель WWP 90х50/3 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель WWP 90х50/3,ВентСтройСнаб', 'Водяной нагреватель WWP 90х50/3'),
(76, 1, 'Водяной нагреватель д.100х50/3', '', 'Водяной нагреватель д.100х50/3', 'Купить Водяной нагреватель д.100х50/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель д.100х50/3 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель д.100х50/3,ВентСтройСнаб', 'Водяной нагреватель д.100х50/3'),
(71, 1, 'Водяной нагреватель д. 60x30/3', '', 'Водяной нагреватель д. 60x30/3', 'Купить Водяной нагреватель д. 60x30/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель д. 60x30/3 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель д. 60x30/3,ВентСтройСнаб', 'Водяной нагреватель д. 60x30/3'),
(72, 1, 'Водяной нагреватель д. 60x35/3', '', 'Водяной нагреватель д. 60x35/3', 'Купить Водяной нагреватель д. 60x35/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель д. 60x35/3 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель д. 60x35/3,ВентСтройСнаб', 'Водяной нагреватель д. 60x35/3'),
(73, 1, 'Водяной нагреватель WWP 70х40/3', '', 'Водяной нагреватель WWP 70х40/3', 'Купить Водяной нагреватель WWP 70х40/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель WWP 70х40/3 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель WWP 70х40/3,ВентСтройСнаб', 'Водяной нагреватель WWP 70х40/3'),
(74, 1, 'Водяной нагреватель WWP 80х50/3', '', 'Водяной нагреватель WWP 80х50/3', 'Купить Водяной нагреватель WWP 80х50/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель WWP 80х50/3 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель WWP 80х50/3,ВентСтройСнаб', 'Водяной нагреватель WWP 80х50/3'),
(67, 1, 'Водяной нагреватель WWP 80х50/2', '', 'Водяной нагреватель WWP 80х50/2', 'Купить Водяной нагреватель WWP 80х50/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель WWP 80х50/2 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель WWP 80х50/2,ВентСтройСнаб', 'Водяной нагреватель WWP 80х50/2'),
(68, 1, 'Водяной нагреватель WWP 90х50/2', '', 'Водяной нагреватель WWP 90х50/2', 'Купить Водяной нагреватель WWP 90х50/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель WWP 90х50/2 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель WWP 90х50/2,ВентСтройСнаб', 'Водяной нагреватель WWP 90х50/2'),
(69, 1, 'Водяной нагреватель д.100х50/2', '', 'Водяной нагреватель д.100х50/2', 'Купить Водяной нагреватель д.100х50/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель д.100х50/2 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель д.100х50/2,ВентСтройСнаб', 'Водяной нагреватель д.100х50/2'),
(70, 1, 'Водяной нагреватель  д.50х30/3', '', 'Водяной нагреватель  д.50х30/3', 'Купить Водяной нагреватель  д.50х30/3 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель  д.50х30/3 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель  д.50х30/3,ВентСтройСнаб', 'Водяной нагреватель  д.50х30/3'),
(66, 1, 'Водяной нагреватель д. 70х40/2', '', 'Водяной нагреватель д. 70х40/2', 'Купить Водяной нагреватель д. 70х40/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель д. 70х40/2 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель д. 70х40/2,ВентСтройСнаб', 'Водяной нагреватель д. 70х40/2'),
(65, 1, 'Водяной нагреватель д. 60х35/2', '', 'Водяной нагреватель д. 60х35/2', 'Купить Водяной нагреватель д. 60х35/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель д. 60х35/2 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель д. 60х35/2,ВентСтройСнаб', 'Водяной нагреватель д. 60х35/2'),
(64, 1, 'Водяной нагреватель д. 60х30/2', '', 'Водяной нагреватель д. 60х30/2', 'Купить Водяной нагреватель д. 60х30/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель д. 60х30/2 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель д. 60х30/2,ВентСтройСнаб', 'Водяной нагреватель д. 60х30/2'),
(63, 1, 'Водяной нагреватель д.50х30/2', '', 'Водяной нагреватель д.50х30/2', 'Купить Водяной нагреватель д.50х30/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель д.50х30/2 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель д.50х30/2,ВентСтройСнаб', 'Водяной нагреватель д.50х30/2'),
(62, 1, 'Водяной нагреватель д.315/2', '', 'Водяной нагреватель д.315/2', 'Купить Водяной нагреватель д.315/2 в ВентСтройСнаб по лучшей цене', 'Покупайте Водяной нагреватель д.315/2 в магазине ВентСтройСнаб по лучшей цене', 'Водяной нагреватель д.315/2,ВентСтройСнаб', 'Водяной нагреватель д.315/2'),
(60, 1, 'Вентилятор 80х50-4D (380В)', '', 'Вентилятор 80х50-4D (380В)', 'Купить Вентилятор 80х50-4D (380В) в ВентСтройСнаб по лучшей цене', 'Покупайте Вентилятор 80х50-4D (380В) в магазине ВентСтройСнаб по лучшей цене', 'Вентилятор 80х50-4D (380В),ВентСтройСнаб', 'Вентилятор 80х50-4D (380В)'),
(61, 1, 'Вентилятор 90х50-4D (380В)', '', 'Вентилятор 90х50-4D (380В)', 'Купить Вентилятор 90х50-4D (380В) в ВентСтройСнаб по лучшей цене', 'Покупайте Вентилятор 90х50-4D (380В) в магазине ВентСтройСнаб по лучшей цене', 'Вентилятор 90х50-4D (380В),ВентСтройСнаб', 'Вентилятор 90х50-4D (380В)'),
(58, 1, 'Вентилятор 60х35-4D (380В)', '', 'Вентилятор 60х35-4D (380В)', 'Купить Вентилятор 60х35-4D (380В) в ВентСтройСнаб по лучшей цене', 'Покупайте Вентилятор 60х35-4D (380В) в магазине ВентСтройСнаб по лучшей цене', 'Вентилятор 60х35-4D (380В),ВентСтройСнаб', 'Вентилятор 60х35-4D (380В)'),
(59, 1, 'Вентилятор 70х40-4D (380В)', '', 'Вентилятор 70х40-4D (380В)', 'Купить Вентилятор 70х40-4D (380В) в ВентСтройСнаб по лучшей цене', 'Покупайте Вентилятор 70х40-4D (380В) в магазине ВентСтройСнаб по лучшей цене', 'Вентилятор 70х40-4D (380В),ВентСтройСнаб', 'Вентилятор 70х40-4D (380В)'),
(50, 1, 'Вентилятор д.100', '', 'Вентилятор д.100', 'Купить Вентилятор д.100 в ВентСтройСнаб по лучшей цене', 'Покупайте Вентилятор д.100 в магазине ВентСтройСнаб по лучшей цене', 'Вентилятор д.100,ВентСтройСнаб', 'Вентилятор д.100'),
(51, 1, 'Вентилятор д. 125', '', 'Вентилятор д. 125', 'Купить Вентилятор д. 125 в ВентСтройСнаб по лучшей цене', 'Покупайте Вентилятор д. 125 в магазине ВентСтройСнаб по лучшей цене', 'Вентилятор д. 125,ВентСтройСнаб', 'Вентилятор д. 125'),
(52, 1, 'Вентилятор д.160', '', 'Вентилятор д.160', 'Купить Вентилятор д.160 в ВентСтройСнаб по лучшей цене', 'Покупайте Вентилятор д.160 в магазине ВентСтройСнаб по лучшей цене', 'Вентилятор д.160,ВентСтройСнаб', 'Вентилятор д.160'),
(53, 1, 'Вентилятор д. 200', '', 'Вентилятор д. 200', 'Купить Вентилятор д. 200 в ВентСтройСнаб по лучшей цене', 'Покупайте Вентилятор д. 200 в магазине ВентСтройСнаб по лучшей цене', 'Вентилятор д. 200,ВентСтройСнаб', 'Вентилятор д. 200'),
(54, 1, 'Вентилятор д. 250', '', 'Вентилятор д. 250', 'Купить Вентилятор д. 250 в ВентСтройСнаб по лучшей цене', 'Покупайте Вентилятор д. 250 в магазине ВентСтройСнаб по лучшей цене', 'Вентилятор д. 250,ВентСтройСнаб', 'Вентилятор д. 250'),
(55, 1, 'Вентилятор д. 315', '', 'Вентилятор д. 315', 'Купить Вентилятор д. 315 в ВентСтройСнаб по лучшей цене', 'Покупайте Вентилятор д. 315 в магазине ВентСтройСнаб по лучшей цене', 'Вентилятор д. 315,ВентСтройСнаб', 'Вентилятор д. 315'),
(57, 1, 'Вентилятор 60x30-4D (380В)', '', 'Вентилятор 60x30-4D (380В)', 'Купить Вентилятор 60x30-4D (380В) в ВентСтройСнаб по лучшей цене', 'Покупайте Вентилятор 60x30-4D (380В) в магазине ВентСтройСнаб по лучшей цене', 'Вентилятор 60x30-4D (380В),ВентСтройСнаб', 'Вентилятор 60x30-4D (380В)'),
(56, 1, 'Вентилятор 50x30-4D (380В)', '', 'Вентилятор 50x30-4D (380В)', 'Купить Вентилятор 50x30-4D (380В) в ВентСтройСнаб по лучшей цене', 'Покупайте Вентилятор 50x30-4D (380В) в магазине ВентСтройСнаб по лучшей цене', 'Вентилятор 50x30-4D (380В),ВентСтройСнаб', 'Вентилятор 50x30-4D (380В)');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT 0,
  `priority` int(5) NOT NULL DEFAULT 1,
  `price` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_filter`
--

CREATE TABLE `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_option`
--

CREATE TABLE `oc_product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_option_value`
--

CREATE TABLE `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_recurring`
--

CREATE TABLE `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related`
--

CREATE TABLE `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related_article`
--

CREATE TABLE `oc_product_related_article` (
  `article_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related_mn`
--

CREATE TABLE `oc_product_related_mn` (
  `product_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related_wb`
--

CREATE TABLE `oc_product_related_wb` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_reward`
--

CREATE TABLE `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT 0,
  `customer_group_id` int(11) NOT NULL DEFAULT 0,
  `points` int(8) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_special`
--

CREATE TABLE `oc_product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT 1,
  `price` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `main_category` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_to_category`
--

INSERT INTO `oc_product_to_category` (`product_id`, `category_id`, `main_category`) VALUES
(365, 55, 1),
(364, 76, 1),
(363, 76, 1),
(362, 76, 1),
(361, 76, 1),
(360, 67, 1),
(359, 51, 1),
(358, 51, 1),
(357, 51, 1),
(356, 52, 1),
(355, 52, 1),
(354, 52, 1),
(353, 53, 1),
(352, 53, 1),
(351, 56, 1),
(350, 76, 1),
(349, 76, 1),
(348, 76, 1),
(347, 76, 1),
(346, 76, 1),
(345, 78, 1),
(344, 69, 1),
(343, 54, 1),
(342, 54, 1),
(341, 54, 1),
(340, 54, 1),
(339, 54, 1),
(338, 55, 1),
(337, 55, 1),
(336, 55, 1),
(335, 55, 1),
(334, 55, 1),
(333, 55, 1),
(332, 55, 1),
(331, 55, 1),
(330, 55, 1),
(329, 56, 1),
(328, 56, 1),
(327, 56, 1),
(326, 76, 1),
(325, 76, 1),
(324, 76, 1),
(323, 76, 1),
(322, 76, 1),
(321, 76, 1),
(320, 76, 1),
(319, 76, 1),
(318, 76, 1),
(317, 76, 1),
(316, 76, 1),
(315, 76, 1),
(314, 76, 1),
(313, 76, 1),
(312, 76, 1),
(311, 76, 1),
(310, 76, 1),
(309, 76, 1),
(308, 76, 1),
(307, 76, 1),
(306, 76, 1),
(305, 76, 1),
(304, 76, 1),
(303, 76, 1),
(302, 76, 1),
(301, 76, 1),
(300, 76, 1),
(299, 77, 1),
(298, 77, 1),
(297, 77, 1),
(296, 78, 1),
(295, 78, 1),
(294, 78, 1),
(293, 78, 1),
(292, 78, 1),
(291, 78, 1),
(290, 78, 1),
(289, 78, 1),
(288, 78, 1),
(287, 78, 1),
(286, 78, 1),
(285, 78, 1),
(284, 78, 1),
(283, 78, 1),
(282, 79, 1),
(281, 79, 1),
(280, 79, 1),
(279, 79, 1),
(278, 79, 1),
(277, 79, 1),
(276, 79, 1),
(275, 79, 1),
(274, 79, 1),
(273, 57, 1),
(272, 57, 1),
(271, 57, 1),
(270, 57, 1),
(269, 57, 1),
(268, 57, 1),
(267, 57, 1),
(266, 58, 1),
(265, 58, 1),
(264, 58, 1),
(263, 59, 1),
(262, 59, 1),
(261, 59, 1),
(260, 59, 1),
(259, 59, 1),
(258, 80, 1),
(257, 80, 1),
(256, 80, 1),
(255, 80, 1),
(254, 80, 1),
(253, 80, 1),
(252, 80, 1),
(251, 80, 1),
(250, 80, 1),
(249, 80, 1),
(248, 80, 1),
(247, 80, 1),
(246, 80, 1),
(245, 80, 1),
(244, 80, 1),
(243, 80, 1),
(242, 80, 1),
(241, 80, 1),
(240, 80, 1),
(239, 80, 1),
(238, 80, 1),
(237, 80, 1),
(236, 80, 1),
(235, 80, 1),
(234, 80, 1),
(233, 80, 1),
(232, 80, 1),
(231, 80, 1),
(230, 80, 1),
(229, 80, 1),
(228, 80, 1),
(227, 80, 1),
(226, 80, 1),
(225, 80, 1),
(224, 80, 1),
(223, 80, 1),
(222, 80, 1),
(221, 80, 1),
(220, 80, 1),
(219, 80, 1),
(218, 80, 1),
(217, 80, 1),
(216, 80, 1),
(215, 80, 1),
(214, 80, 1),
(213, 80, 1),
(212, 80, 1),
(211, 80, 1),
(210, 80, 1),
(209, 80, 1),
(208, 80, 1),
(207, 60, 1),
(206, 60, 1),
(205, 60, 1),
(204, 60, 1),
(203, 60, 1),
(202, 60, 1),
(201, 60, 1),
(200, 61, 1),
(199, 61, 1),
(198, 62, 1),
(197, 63, 1),
(196, 63, 1),
(195, 63, 1),
(194, 64, 1),
(193, 81, 1),
(192, 81, 1),
(191, 81, 1),
(190, 81, 1),
(189, 81, 1),
(188, 81, 1),
(187, 65, 1),
(186, 65, 1),
(185, 65, 1),
(184, 65, 1),
(183, 65, 1),
(182, 65, 1),
(181, 65, 1),
(180, 65, 1),
(179, 65, 1),
(178, 66, 1),
(177, 66, 1),
(176, 66, 1),
(175, 66, 1),
(174, 66, 1),
(173, 66, 1),
(172, 66, 1),
(171, 66, 1),
(170, 66, 1),
(169, 66, 1),
(168, 67, 1),
(167, 67, 1),
(166, 67, 1),
(165, 67, 1),
(164, 67, 1),
(163, 67, 1),
(162, 67, 1),
(161, 67, 1),
(160, 67, 1),
(159, 67, 1),
(158, 67, 1),
(157, 67, 1),
(156, 67, 1),
(155, 67, 1),
(154, 67, 1),
(153, 67, 1),
(152, 67, 1),
(151, 67, 1),
(150, 67, 1),
(149, 68, 1),
(148, 68, 1),
(147, 68, 1),
(146, 68, 1),
(145, 68, 1),
(144, 68, 1),
(143, 68, 1),
(142, 68, 1),
(141, 68, 1),
(140, 68, 1),
(139, 68, 1),
(138, 68, 1),
(137, 68, 1),
(136, 68, 1),
(135, 68, 1),
(134, 68, 1),
(133, 69, 1),
(132, 69, 1),
(131, 69, 1),
(130, 69, 1),
(129, 69, 1),
(128, 69, 1),
(127, 69, 1),
(126, 69, 1),
(125, 69, 1),
(124, 69, 1),
(123, 69, 1),
(122, 69, 1),
(121, 70, 1),
(120, 70, 1),
(119, 70, 1),
(118, 70, 1),
(117, 70, 1),
(116, 71, 1),
(115, 71, 1),
(114, 71, 1),
(113, 71, 1),
(112, 71, 1),
(111, 73, 1),
(110, 73, 1),
(109, 73, 1),
(108, 73, 1),
(107, 73, 1),
(106, 73, 1),
(105, 73, 1),
(104, 73, 1),
(103, 73, 1),
(102, 73, 1),
(101, 73, 1),
(100, 73, 1),
(99, 82, 1),
(98, 82, 1),
(97, 82, 1),
(96, 82, 1),
(95, 82, 1),
(94, 82, 1),
(93, 82, 1),
(92, 72, 1),
(91, 72, 1),
(90, 72, 1),
(89, 72, 1),
(88, 72, 1),
(87, 72, 1),
(86, 72, 1),
(85, 72, 1),
(84, 72, 1),
(83, 72, 1),
(82, 72, 1),
(81, 72, 1),
(80, 72, 1),
(79, 73, 1),
(78, 73, 1),
(77, 73, 1),
(76, 74, 1),
(75, 74, 1),
(74, 74, 1),
(73, 74, 1),
(72, 74, 1),
(71, 74, 1),
(70, 74, 1),
(69, 74, 1),
(68, 74, 1),
(67, 74, 1),
(66, 74, 1),
(65, 74, 1),
(64, 74, 1),
(63, 74, 1),
(62, 74, 1),
(61, 75, 1),
(60, 75, 1),
(59, 75, 1),
(58, 75, 1),
(57, 75, 1),
(56, 75, 1),
(55, 75, 1),
(54, 75, 1),
(53, 75, 1),
(52, 75, 1),
(51, 75, 1),
(50, 75, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_download`
--

CREATE TABLE `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_layout`
--

CREATE TABLE `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_store`
--

CREATE TABLE `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_to_store`
--

INSERT INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 0),
(77, 0),
(78, 0),
(79, 0),
(80, 0),
(81, 0),
(82, 0),
(83, 0),
(84, 0),
(85, 0),
(86, 0),
(87, 0),
(88, 0),
(89, 0),
(90, 0),
(91, 0),
(92, 0),
(93, 0),
(94, 0),
(95, 0),
(96, 0),
(97, 0),
(98, 0),
(99, 0),
(100, 0),
(101, 0),
(102, 0),
(103, 0),
(104, 0),
(105, 0),
(106, 0),
(107, 0),
(108, 0),
(109, 0),
(110, 0),
(111, 0),
(112, 0),
(113, 0),
(114, 0),
(115, 0),
(116, 0),
(117, 0),
(118, 0),
(119, 0),
(120, 0),
(121, 0),
(122, 0),
(123, 0),
(124, 0),
(125, 0),
(126, 0),
(127, 0),
(128, 0),
(129, 0),
(130, 0),
(131, 0),
(132, 0),
(133, 0),
(134, 0),
(135, 0),
(136, 0),
(137, 0),
(138, 0),
(139, 0),
(140, 0),
(141, 0),
(142, 0),
(143, 0),
(144, 0),
(145, 0),
(146, 0),
(147, 0),
(148, 0),
(149, 0),
(150, 0),
(151, 0),
(152, 0),
(153, 0),
(154, 0),
(155, 0),
(156, 0),
(157, 0),
(158, 0),
(159, 0),
(160, 0),
(161, 0),
(162, 0),
(163, 0),
(164, 0),
(165, 0),
(166, 0),
(167, 0),
(168, 0),
(169, 0),
(170, 0),
(171, 0),
(172, 0),
(173, 0),
(174, 0),
(175, 0),
(176, 0),
(177, 0),
(178, 0),
(179, 0),
(180, 0),
(181, 0),
(182, 0),
(183, 0),
(184, 0),
(185, 0),
(186, 0),
(187, 0),
(188, 0),
(189, 0),
(190, 0),
(191, 0),
(192, 0),
(193, 0),
(194, 0),
(195, 0),
(196, 0),
(197, 0),
(198, 0),
(199, 0),
(200, 0),
(201, 0),
(202, 0),
(203, 0),
(204, 0),
(205, 0),
(206, 0),
(207, 0),
(208, 0),
(209, 0),
(210, 0),
(211, 0),
(212, 0),
(213, 0),
(214, 0),
(215, 0),
(216, 0),
(217, 0),
(218, 0),
(219, 0),
(220, 0),
(221, 0),
(222, 0),
(223, 0),
(224, 0),
(225, 0),
(226, 0),
(227, 0),
(228, 0),
(229, 0),
(230, 0),
(231, 0),
(232, 0),
(233, 0),
(234, 0),
(235, 0),
(236, 0),
(237, 0),
(238, 0),
(239, 0),
(240, 0),
(241, 0),
(242, 0),
(243, 0),
(244, 0),
(245, 0),
(246, 0),
(247, 0),
(248, 0),
(249, 0),
(250, 0),
(251, 0),
(252, 0),
(253, 0),
(254, 0),
(255, 0),
(256, 0),
(257, 0),
(258, 0),
(259, 0),
(260, 0),
(261, 0),
(262, 0),
(263, 0),
(264, 0),
(265, 0),
(266, 0),
(267, 0),
(268, 0),
(269, 0),
(270, 0),
(271, 0),
(272, 0),
(273, 0),
(274, 0),
(275, 0),
(276, 0),
(277, 0),
(278, 0),
(279, 0),
(280, 0),
(281, 0),
(282, 0),
(283, 0),
(284, 0),
(285, 0),
(286, 0),
(287, 0),
(288, 0),
(289, 0),
(290, 0),
(291, 0),
(292, 0),
(293, 0),
(294, 0),
(295, 0),
(296, 0),
(297, 0),
(298, 0),
(299, 0),
(300, 0),
(301, 0),
(302, 0),
(303, 0),
(304, 0),
(305, 0),
(306, 0),
(307, 0),
(308, 0),
(309, 0),
(310, 0),
(311, 0),
(312, 0),
(313, 0),
(314, 0),
(315, 0),
(316, 0),
(317, 0),
(318, 0),
(319, 0),
(320, 0),
(321, 0),
(322, 0),
(323, 0),
(324, 0),
(325, 0),
(326, 0),
(327, 0),
(328, 0),
(329, 0),
(330, 0),
(331, 0),
(332, 0),
(333, 0),
(334, 0),
(335, 0),
(336, 0),
(337, 0),
(338, 0),
(339, 0),
(340, 0),
(341, 0),
(342, 0),
(343, 0),
(344, 0),
(345, 0),
(346, 0),
(347, 0),
(348, 0),
(349, 0),
(350, 0),
(351, 0),
(352, 0),
(353, 0),
(354, 0),
(355, 0),
(356, 0),
(357, 0),
(358, 0),
(359, 0),
(360, 0),
(361, 0),
(362, 0),
(363, 0),
(364, 0),
(365, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_recurring`
--

CREATE TABLE `oc_recurring` (
  `recurring_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_recurring_description`
--

CREATE TABLE `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return`
--

CREATE TABLE `oc_return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text DEFAULT NULL,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_action`
--

CREATE TABLE `oc_return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Возмещенный'),
(2, 1, 'Возврат средств'),
(3, 1, 'Отправлена замена'),
(1, 2, 'Refunded'),
(3, 2, 'Replacement Sent'),
(2, 2, 'Credit Issued');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_history`
--

CREATE TABLE `oc_return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_reason`
--

CREATE TABLE `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Получен неисправным (сломанным)'),
(1, 2, 'Dead On Arrival'),
(2, 1, 'Получен не тот (ошибочный) товар'),
(2, 2, 'Received Wrong Item'),
(3, 1, 'Заказан по ошибке'),
(3, 2, 'Order Error'),
(4, 1, 'Неисправен, пожалуйста укажите/приложите подробности'),
(4, 2, 'Faulty, please supply details'),
(5, 1, 'Другое (другая причина), пожалуйста укажите/приложите подробности'),
(5, 2, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_status`
--

CREATE TABLE `oc_return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'В ожидании'),
(3, 1, 'Выполнен'),
(2, 1, 'Ожидание товара'),
(1, 2, 'Pending'),
(2, 2, 'Awaiting Products'),
(3, 2, 'Complete');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_review`
--

CREATE TABLE `oc_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_review_article`
--

CREATE TABLE `oc_review_article` (
  `review_article_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_seo_url`
--

CREATE TABLE `oc_seo_url` (
  `seo_url_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_seo_url`
--

INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`) VALUES
(1, 0, 1, 'information_id=4', 'about_us'),
(2, 0, 1, 'common/home', ''),
(3, 0, 1, 'information_id=6', 'delivery'),
(4, 0, 1, 'information_id=3', 'privacy'),
(5, 0, 1, 'information_id=5', 'terms'),
(6, 0, 2, 'common/home', 'en'),
(7, 0, 2, 'information_id=6', 'en_delivery'),
(8, 0, 2, 'information_id=4', 'en_about_us'),
(9, 0, 2, 'information_id=3', 'en_privacy'),
(10, 0, 2, 'information_id=5', 'en_terms'),
(2486, 0, 1, 'category_id=82', 'vstavka-karmannaya'),
(2485, 0, 1, 'category_id=81', 'obratnye-klapany'),
(2484, 0, 1, 'category_id=80', 'reshetki'),
(2483, 0, 1, 'category_id=79', 'chastotnye-preobrazovateli'),
(2482, 0, 1, 'category_id=78', 'shumoglushiteli'),
(2481, 0, 1, 'category_id=77', 'schity-upravleniya'),
(2480, 0, 1, 'category_id=76', 'elektricheskie-nagrevateli'),
(2479, 0, 1, 'category_id=75', 'ventilyatory'),
(2478, 0, 1, 'category_id=74', 'nagrevateli'),
(2477, 0, 1, 'category_id=73', 'vozduhovody'),
(2476, 0, 1, 'category_id=72', 'vozdushnyj-klapan'),
(2475, 0, 1, 'category_id=71', 'datchiki'),
(2474, 0, 1, 'category_id=70', 'difuzory'),
(2473, 0, 1, 'category_id=69', 'drossel-klapany'),
(2472, 0, 1, 'category_id=68', 'zonty'),
(2471, 0, 1, 'category_id=67', 'filtry'),
(2470, 0, 1, 'category_id=66', 'protivopozharnye-klapany'),
(2469, 0, 1, 'category_id=65', 'komplekt-nema'),
(2468, 0, 1, 'category_id=64', 'panel-upravleniya'),
(2467, 0, 1, 'category_id=63', 'pompy'),
(2466, 0, 1, 'category_id=62', 'pult-upravleniya'),
(2465, 0, 1, 'category_id=61', 'regulyatory-skorosti'),
(2464, 0, 1, 'category_id=60', 'rekuperatory'),
(2463, 0, 1, 'category_id=59', 'smesitelnye-uzly'),
(2462, 0, 1, 'category_id=58', 'termostaty'),
(2461, 0, 1, 'category_id=57', 'filtr-boksy'),
(2460, 0, 1, 'category_id=56', 'elektroprivody'),
(2459, 0, 1, 'category_id=55', 'vstavka-filtruyuschaya'),
(2458, 0, 1, 'category_id=54', 'vstavka-gibkaya'),
(2457, 0, 1, 'category_id=53', 'teploizolyaciya'),
(2456, 0, 1, 'category_id=52', 'provoda'),
(2455, 0, 1, 'category_id=51', 'rashodnyj-material'),
(2454, 0, 1, 'category_id=50', 'zonty-vytyazhnye'),
(2802, 0, 1, 'product_id=365', 'vstavka-kassetnaya-filtruyuschaya-fvkas-800-500-100-g3'),
(2801, 0, 1, 'product_id=364', 'el-nagrevatel-dlya-pryamougolnogo-kanala-enr-800-500-60'),
(2800, 0, 1, 'product_id=363', 'el-nagrevatel-dlya-pryamougolnogo-kanala-enr-700-400-60'),
(2799, 0, 1, 'product_id=362', 'el-nagrevatel-dlya-pryamougolnogo-kanala-enr-600-350-45'),
(2798, 0, 1, 'product_id=361', 'el-nagrevatel-dlya-pryamougolnogo-kanala-enr-500-300-22-5'),
(2797, 0, 1, 'product_id=360', 'kassetnyj-filtr-(korpus-s-materialom)-bks-100'),
(2795, 0, 1, 'product_id=358', 'komplekt-rashodnyh-materialov-№2'),
(2796, 0, 1, 'product_id=359', 'komplekt-rashodnyh-materialov-№3'),
(2791, 0, 1, 'product_id=354', 'komplekt-provodov-№1'),
(2792, 0, 1, 'product_id=355', 'komplekt-provodov-№2'),
(2793, 0, 1, 'product_id=356', 'komplekt-provodov-№3'),
(2794, 0, 1, 'product_id=357', 'komplekt-rashodnyh-materialov-№1'),
(2790, 0, 1, 'product_id=353', 'teploizoliruyuschij-material-magnofleks-b=20-mm'),
(2785, 0, 1, 'product_id=348', 'el-nagrevatel-dlya-kruglogo-kanala-enr-600-350-45'),
(2786, 0, 1, 'product_id=349', 'el-nagrevatel-dlya-kruglogo-kanala-enr-700-400-60'),
(2787, 0, 1, 'product_id=350', 'el-nagrevatel-dlya-kruglogo-kanala-enr-800-500-60'),
(2788, 0, 1, 'product_id=351', 'el-privod-vozdushnoj-zaslonki-gca-121-1e-(18-nm)-24v-2-h-pozicionnyj'),
(2789, 0, 1, 'product_id=352', 'teploizoliruyuschij-material-magnofleks-b=10-mm'),
(2784, 0, 1, 'product_id=347', 'el-nagrevatel-dlya-kruglogo-kanala-enr-500-300-22-5'),
(2783, 0, 1, 'product_id=346', 'el-nagrevatel-dlya-kruglogo-kanala-ens-100-2-4-1'),
(2782, 0, 1, 'product_id=345', 'shumoglushitel-ca-200-600'),
(2781, 0, 1, 'product_id=344', 'drossel-klapan-s-ruchnym-upravleniem-100'),
(2780, 0, 1, 'product_id=343', 'vstavka-gibkaya-fkr-80-50'),
(2778, 0, 1, 'product_id=341', 'vstavka-gibkaya-fkr-60-35'),
(2779, 0, 1, 'product_id=342', 'vstavka-gibkaya-fkr-70-40'),
(2777, 0, 1, 'product_id=340', 'vstavka-gibkaya-fkr-60-30'),
(2775, 0, 1, 'product_id=338', 'vstavka-kassetnaya-filtruyuschaya-fvkas-700-400-100-g3'),
(2776, 0, 1, 'product_id=339', 'vstavka-gibkaya-fkr-50-30'),
(2774, 0, 1, 'product_id=337', 'vstavka-kassetnaya-filtruyuschaya-fvkas-600-350-100-g3'),
(2773, 0, 1, 'product_id=336', 'vstavka-kassetnaya-filtruyuschaya-fvkas-600-300-100-g3'),
(2772, 0, 1, 'product_id=335', 'vstavka-kassetnaya-filtruyuschaya-fvkas-500-300-100-g3'),
(2771, 0, 1, 'product_id=334', 'vstavka-kassetnaya-filtruyuschaya-fks-315'),
(2770, 0, 1, 'product_id=333', 'vstavka-kassetnaya-filtruyuschaya-fks-250'),
(2769, 0, 1, 'product_id=332', 'vstavka-kassetnaya-filtruyuschaya-fks-200'),
(2768, 0, 1, 'product_id=331', 'vstavka-kassetnaya-filtruyuschaya-fks-160'),
(2766, 0, 1, 'product_id=329', 'elektroprivod-va05n230-(5-n-m)-bez-vozvr-pruzhiny-(m)'),
(2767, 0, 1, 'product_id=330', 'vstavka-kassetnaya-filtruyuschaya-fks-100'),
(2765, 0, 1, 'product_id=328', 'elektroprivod-va05s230-(5-nm)-c-vozvr-pruzhinoj'),
(2757, 0, 1, 'product_id=320', 'el-nagrevatel-dlya-kruglogo-kanala-ens-315-12-0-3'),
(2764, 0, 1, 'product_id=327', 'el-privod-vozdushnoj-zaslonki-siemens-gdb-331-1e-(-5-nm)-230v-3-h-pozicionnyj'),
(2763, 0, 1, 'product_id=326', 'elektroprivod-gruner-341-230-05-(5-n*m)-s-vozvr-pruzhinoj'),
(2762, 0, 1, 'product_id=325', 'elektroprivod-gruner-225-230t-05-(4-6-nm)-2-3-pozic'),
(2761, 0, 1, 'product_id=324', 'el-nagrevatel-dlya-pryamougolnyh-kanalov-ehr-600*350-30'),
(2760, 0, 1, 'product_id=323', 'el-nagrevatel-dlya-pryamougolnyh-kanalov-ehr-600*300-30'),
(2759, 0, 1, 'product_id=322', 'el-nagrevatel-dlya-pryamougolnyh-kanalov-ehr-600*300-22-5'),
(2758, 0, 1, 'product_id=321', 'elektronagrevatel-nek-315-15'),
(2756, 0, 1, 'product_id=319', 'el-nagrevatel-dlya-kruglogo-kanala-ens-315-9-0-3'),
(2755, 0, 1, 'product_id=318', 'el-nagrevatel-dlya-kruglogo-kanala-ens-315-6-0-2'),
(2754, 0, 1, 'product_id=317', 'el-nagrevatel-dlya-kruglogo-kanala-ens-250-12-0-3'),
(2753, 0, 1, 'product_id=316', 'el-nagrevatel-dlya-kruglogo-kanala-ens-250-9-0-3'),
(2752, 0, 1, 'product_id=315', 'el-nagrevatel-dlya-kruglogo-kanala-ens-250-6-0-3'),
(2751, 0, 1, 'product_id=314', 'el-nagrevatel-dlya-kruglogo-kanala-ens-250-6-0-2'),
(2750, 0, 1, 'product_id=313', 'el-nagrevatel-dlya-kruglogo-kanala-ens-250-3-0-1'),
(2749, 0, 1, 'product_id=312', 'el-nagrevatel-dlya-kruglogo-kanala-ens-200-6-0-3'),
(2748, 0, 1, 'product_id=311', 'el-nagrevatel-dlya-kruglogo-kanala-ens-200-6-0-2'),
(2747, 0, 1, 'product_id=310', 'el-nagrevatel-dlya-kruglogo-kanala-ens-200-5-0-2'),
(2746, 0, 1, 'product_id=309', 'el-nagrevatel-dlya-kruglogo-kanala-ens-200-3-0-1'),
(2745, 0, 1, 'product_id=308', 'el-nagrevatel-dlya-kruglogo-kanala-ens-200-2-4-1'),
(2744, 0, 1, 'product_id=307', 'el-nagrevatel-dlya-kruglogo-kanala-ens-160-6-0-3'),
(2743, 0, 1, 'product_id=306', 'el-nagrevatel-dlya-kruglogo-kanala-ens-160-5-0-2'),
(2742, 0, 1, 'product_id=305', 'el-nagrevatel-dlya-kruglogo-kanala-ens-160-3-0-2'),
(2741, 0, 1, 'product_id=304', 'el-nagrevatel-dlya-kruglogo-kanala-ens-160-3-0-1'),
(2740, 0, 1, 'product_id=303', 'el-nagrevatel-dlya-kruglogo-kanala-ens-160-2-4-1'),
(2738, 0, 1, 'product_id=301', 'el-nagrevatel-dlya-kruglogo-kanala-ens-125-1-8-1'),
(2739, 0, 1, 'product_id=302', 'el-nagrevatel-dlya-kruglogo-kanala-ens-160-1-2-1'),
(2737, 0, 1, 'product_id=300', 'el-nagrevatel-dlya-kruglogo-kanala-ens-125-1-2-1'),
(2736, 0, 1, 'product_id=299', 'schit-upravleniya-(pv)60-0-3-3e'),
(2734, 0, 1, 'product_id=297', 'schit-upravleniya-(pv)12-0-1-1e'),
(2735, 0, 1, 'product_id=298', 'schit-upravleniya-(pv)30-0-3-3e'),
(2733, 0, 1, 'product_id=296', 'shumoglushitel-ra-1000-50-(l-1000-mm)'),
(2732, 0, 1, 'product_id=295', 'shumoglushitel-ra-90-50-(l-1000-mm)'),
(2731, 0, 1, 'product_id=294', 'shumoglushitel-ra-80-50-(l-1000-mm)'),
(2730, 0, 1, 'product_id=293', 'shumoglushitel-ra-70-40-(l-1000-mm)'),
(2729, 0, 1, 'product_id=292', 'shumoglushitel-ra-60-35-(l-1000-mm)'),
(2728, 0, 1, 'product_id=291', 'shumoglushitel-ra-60-30-(l-1000-mm)'),
(2727, 0, 1, 'product_id=290', 'shumoglushitel-ra-50-30-(l-1000-mm)'),
(2726, 0, 1, 'product_id=289', 'shumoglushitel-ca-250-900'),
(2725, 0, 1, 'product_id=288', 'shumoglushitel-ca-200-900'),
(2724, 0, 1, 'product_id=287', 'shumoglushitel-ca-315-600'),
(2723, 0, 1, 'product_id=286', 'shumoglushitel-ca-250-600'),
(2722, 0, 1, 'product_id=285', 'shumoglushitel-ca-160-600'),
(2721, 0, 1, 'product_id=284', 'shumoglushitel-ca-125-600'),
(2719, 0, 1, 'product_id=282', 'chastotnyj-preobrazovatel-fc-051p7k5-7-5kvt-15-5-a'),
(2720, 0, 1, 'product_id=283', 'shumoglushitel-ca-100-600'),
(2718, 0, 1, 'product_id=281', 'chastotnyj-preobrazovatel-fc-051p5k5-5-5kvt-12-a-(3)'),
(2717, 0, 1, 'product_id=280', 'chastotnyj-preobrazovatel-fc-051p4k0-4kvt-9-a'),
(2715, 0, 1, 'product_id=278', 'chastotnyj-preobrazovatel-fc-051p2k2-2-2kvt-9-6a-(1)'),
(2716, 0, 1, 'product_id=279', 'chastotnyj-preobrazovatel-fc-051p3k0-3kvt-7-2-a-(3)'),
(2714, 0, 1, 'product_id=277', 'chastotnyj-preobrazovatel-fc-051p2k2-2-2kvt-5-3a-(3)'),
(2713, 0, 1, 'product_id=276', 'chastotnyj-preobrazovatel-fc-051p1k5-1-5kvt-6-8a-(1)'),
(2710, 0, 1, 'product_id=273', 'filtr-boks-(korpus)-fbrr-1000x500'),
(2712, 0, 1, 'product_id=275', 'chastotnyj-preobrazovatel-fc-051p1k5-1-5kvt-3-7a-(3)'),
(2711, 0, 1, 'product_id=274', 'chastotnyj-preobrazovatel-fc-051p1k75-0-75kvt-4-2-a-(1)'),
(2709, 0, 1, 'product_id=272', 'filtr-boks-(korpus)-fbrr-900x500'),
(2707, 0, 1, 'product_id=270', 'filtr-boks-(korpus)-fbrr-700x400'),
(2702, 0, 1, 'product_id=265', 'termostat-kr61-3-(3m-)-kreplenie-v-komplekte'),
(2703, 0, 1, 'product_id=266', 'termostat-kr61-6-(6m-)-kreplenie-v-komplekte'),
(2708, 0, 1, 'product_id=271', 'filtr-boks-(korpus)-fbrr-800x500'),
(2705, 0, 1, 'product_id=268', 'filtr-boks-(korpus)-fbrr-600x300'),
(2706, 0, 1, 'product_id=269', 'filtr-boks-(korpus)-fbrr-600x350'),
(2704, 0, 1, 'product_id=267', 'filtr-boks-(korpus)-fbrr-500x300'),
(2701, 0, 1, 'product_id=264', 'termostat-kr61-1-(1m-)'),
(2700, 0, 1, 'product_id=263', 'smesitelnyj-uzel-s-elektroprivodom-gruner-s-plavnym-upravleniem-mst-25-80-6-3-s24^'),
(2699, 0, 1, 'product_id=262', 'smesitelnyj-uzel-s-elektroprivodom-gruner-s-plavnym-upravleniem-mst-25-60-6-3-s24^'),
(2698, 0, 1, 'product_id=261', 'smesitelnyj-uzel-s-elektroprivodom-gruner-s-plavnym-upravleniem-mst-25-40-4-0-s24^'),
(2697, 0, 1, 'product_id=260', 'smesitelnyj-uzel-s-elektroprivodom-gruner-s-plavnym-upravleniem-mst-25-40-2-5-s24^-ts(bez-bajpasa)'),
(2693, 0, 1, 'product_id=256', 'reshetka-naruzhnaya-vr-n3-800h500-(bez-zaschelki-bez-krv)'),
(2694, 0, 1, 'product_id=257', 'reshetka-naruzhnaya-vr-n3-900h500-(bez-zaschelki-bez-krv)'),
(2696, 0, 1, 'product_id=259', 'smesitelnyj-uzel-s-elektroprivodom-gruner-s-plavnym-upravleniem-mst-25-40-1-6-s24^'),
(2695, 0, 1, 'product_id=258', 'reshetka-naruzhnaya-vr-n3-1000h500-(bez-zaschelki-bez-krv)'),
(2692, 0, 1, 'product_id=255', 'reshetka-naruzhnaya-vr-n3-700h400-(bez-zaschelki-bez-krv-bez-montazhnyh-otverstij)'),
(2690, 0, 1, 'product_id=253', 'reshetka-naruzhnaya-vr-n3-600x300-(bez-zaschelki-bez-krv-bez-montazhnyj-otverstij)'),
(2691, 0, 1, 'product_id=254', 'reshetka-naruzhnaya-vr-n3-600x350-(bez-zaschelki-bez-krv-bez-montazhnyh-otverstij)'),
(2688, 0, 1, 'product_id=251', 'reshetka-naruzhnaya-vr-n3-200x200-(bez-zaschelki-bez-krv-bez-montazhnyj-otverstij)'),
(2689, 0, 1, 'product_id=252', 'reshetka-naruzhnaya-vr-n3-500h300-(bez-zaschelki-bez-krv-bez-montazhnyh-otverstij)'),
(2687, 0, 1, 'product_id=250', 'reshetka-naruzhnaya-igc-315'),
(2686, 0, 1, 'product_id=249', 'reshetka-naruzhnaya-igc-250'),
(2685, 0, 1, 'product_id=248', 'reshetka-naruzhnaya-igc-200'),
(2684, 0, 1, 'product_id=247', 'reshetka-naruzhnaya-igc-160'),
(2683, 0, 1, 'product_id=246', 'reshetka-naruzhnaya-igc-125'),
(2682, 0, 1, 'product_id=245', 'reshetka-4apn-600h600'),
(2681, 0, 1, 'product_id=244', 'reshetka-4apn-450h450'),
(2680, 0, 1, 'product_id=243', 'reshetka-4apn-300h300'),
(2679, 0, 1, 'product_id=242', 'reshetka-z-h-600h350-(bez-klapana-obd)'),
(2678, 0, 1, 'product_id=241', 'reshetka-z-h-500h300-(s-klapanom-obd)'),
(2677, 0, 1, 'product_id=240', 'reshetka-z-h-500h300-(bez-klapana-obd)'),
(2676, 0, 1, 'product_id=239', 'reshetka-z-h-500x200-(bez-klapana-obd)'),
(2672, 0, 1, 'product_id=235', 'reshetka-z-h-400h150-(s-klapanom-obd)'),
(2673, 0, 1, 'product_id=236', 'reshetka-z-h-400h200-(bez-klapana-obd)'),
(2674, 0, 1, 'product_id=237', 'reshetka-z-h-400h200-(s-klapanom-obd)'),
(2675, 0, 1, 'product_id=238', 'reshetka-z-h-500x200-(s-klapanom-obd)'),
(2669, 0, 1, 'product_id=232', 'reshetka-z-h-300h300-(s-klapanom-obd)'),
(2671, 0, 1, 'product_id=234', 'reshetka-z-h-400h100-(bez-klapana-obd)'),
(2670, 0, 1, 'product_id=233', 'reshetka-z-h-400x100-(s-klapanom-obd)'),
(2668, 0, 1, 'product_id=231', 'reshetka-z-h-300h300-(bez-klapana-obd)'),
(2667, 0, 1, 'product_id=230', 'reshetka-z-h-300h200-(bez-klapana-obd)'),
(2666, 0, 1, 'product_id=229', 'reshetka-z-h-300h200-(s-klapanom-obd)'),
(2665, 0, 1, 'product_id=228', 'reshetka-z-h-300h150-(bez-klapana-obd)'),
(2664, 0, 1, 'product_id=227', 'reshetka-z-h-300h100-(bez-klapana-obd)'),
(2663, 0, 1, 'product_id=226', 'reshetka-z-h-300h100-(s-klapanom-obd)'),
(2662, 0, 1, 'product_id=225', 'reshetka-z-h-300x150-(s-klapanom-obd)'),
(2658, 0, 1, 'product_id=221', 'reshetka-z-h-200h200-(bez-klapana-obd)'),
(2659, 0, 1, 'product_id=222', 'reshetka-z-h-250h150-(s-klapanom-obd)'),
(2660, 0, 1, 'product_id=223', 'reshetka-z-h-250h150-(bez-klapana-obd)'),
(2661, 0, 1, 'product_id=224', 'reshetka-z-h-300h100-(s-klapanom-obd)'),
(2654, 0, 1, 'product_id=217', 'reshetka-z-h-150h150-(bez-klapana-obd)'),
(2655, 0, 1, 'product_id=218', 'reshetka-z-h-150x150-(s-klapanom-obd)'),
(2656, 0, 1, 'product_id=219', 'reshetka-z-h-200x100-(s-klapanom-obd)'),
(2657, 0, 1, 'product_id=220', 'reshetka-z-h-200h100-(bez-klapana-obd)'),
(2653, 0, 1, 'product_id=216', 'reshetka-z-h-150h100-(bez-klapana-obd)'),
(2652, 0, 1, 'product_id=215', 'reshetka-z-h-150h100-(s-klapanom-obd)'),
(2651, 0, 1, 'product_id=214', 'reshetka-z-h-100x100-(-bez-klapana-obd)'),
(2650, 0, 1, 'product_id=213', 'reshetka-z-h-100x100-(s-klapanom-obd)'),
(2649, 0, 1, 'product_id=212', 'reshetka-cgtn-315-(s-setkoj)'),
(2648, 0, 1, 'product_id=211', 'reshetka-cgtn-250-(s-setkoj)'),
(2647, 0, 1, 'product_id=210', 'reshetka-cgtn-200-(s-setkoj)'),
(2646, 0, 1, 'product_id=209', 'reshetka-cgtn-160-(s-setkoj)'),
(2645, 0, 1, 'product_id=208', 'reshetka-cgtn-125-(s-setkoj)'),
(2644, 0, 1, 'product_id=207', 'rekuperator-plastinchatyj-rhpr-1000-500'),
(2643, 0, 1, 'product_id=206', 'rekuperator-plastinchatyj-rhpr-900-500'),
(2642, 0, 1, 'product_id=205', 'rekuperator-plastinchatyj-rhpr-800-500'),
(2638, 0, 1, 'product_id=201', 'rekuperator-plastinchatyj-rhpr-500-300'),
(2639, 0, 1, 'product_id=202', 'rekuperator-plastinchatyj-rhpr-600-300'),
(2640, 0, 1, 'product_id=203', 'rekuperator-plastinchatyj-rhpr-600-350'),
(2641, 0, 1, 'product_id=204', 'rekuperator-plastinchatyj-rhpr-700-400'),
(2633, 0, 1, 'product_id=196', 'pompa-siccom-eco-flowatch-13l-ch-10-m'),
(2634, 0, 1, 'product_id=197', 'pompa-siccom-maxi-eco-flowatch-40l-ch-10-m'),
(2635, 0, 1, 'product_id=198', 'pult-upravleniya-shuft-arc-121'),
(2636, 0, 1, 'product_id=199', 'regulyator-skorosti-5-ti-stupenchatyj-s-termozaschitoj-sre-e-1-5-t'),
(2637, 0, 1, 'product_id=200', 'regulyator-skorosti-mty-2-5-on-(v-korpuse)'),
(2632, 0, 1, 'product_id=195', 'pompa-aspen-hi-flow-max-1-7l-nalivnaya'),
(2631, 0, 1, 'product_id=194', 'panel-upravleniya-lcp-dlya-fc-051(132b0101)'),
(2630, 0, 1, 'product_id=193', 'obratnyj-klapan-rsk-315'),
(2629, 0, 1, 'product_id=192', 'obratnyj-klapan-rsk-250'),
(2628, 0, 1, 'product_id=191', 'obratnyj-klapan-rsk-200'),
(2627, 0, 1, 'product_id=190', 'obratnyj-klapan-rsk-160'),
(2626, 0, 1, 'product_id=189', 'obratnyj-klapan-rsk-125'),
(2625, 0, 1, 'product_id=188', 'obratnyj-klapan-rsk-100'),
(2624, 0, 1, 'product_id=187', 'komplekt-nema1-m4-(dlya-fc-051-15-0-kvt)'),
(2623, 0, 1, 'product_id=186', 'komplekt-nema1-m4-(dlya-fc-051-11-kvt)'),
(2622, 0, 1, 'product_id=185', 'komplekt-nema1-m3-(dlya-fc-051-7-5-kvt)'),
(2621, 0, 1, 'product_id=184', 'komplekt-nema1-m3-(dlya-fc-051-5-5-kvt)'),
(2620, 0, 1, 'product_id=183', 'komplekt-nema1-m3-(dlya-fc-051-4-kvt)'),
(2619, 0, 1, 'product_id=182', 'komplekt-nema1-m3-(dlya-fc-051-3-kvt)'),
(2618, 0, 1, 'product_id=181', 'komplekt-nema1-m2-(dlya-fc-051-2-2-kvt)'),
(2617, 0, 1, 'product_id=180', 'komplekt-nema1-m2-(dlya-fc-051-1-5-kvt)'),
(2616, 0, 1, 'product_id=179', 'komplekt-nema1-m1-(dlya-fc-051-0-75-kvt)'),
(2615, 0, 1, 'product_id=178', 'klapan-kps-1m(60)-no-mv(220)-300h300'),
(2614, 0, 1, 'product_id=177', 'klapan-kps-1m(60)-no-mv(220)-300h200'),
(2613, 0, 1, 'product_id=176', 'klapan-kps-1m(60)-no-mv(220)-300h150'),
(2612, 0, 1, 'product_id=175', 'klapan-kps-1m(60)-no-mv(220)-250h250'),
(2611, 0, 1, 'product_id=174', 'klapan-kps-1m(60)-no-mv(220)-200h200'),
(2610, 0, 1, 'product_id=173', 'klapan-kps-1m(60)-no-mv(220)-200h150'),
(2609, 0, 1, 'product_id=172', 'klapan-kps-1m(60)-no-mv(220)-200h100'),
(2608, 0, 1, 'product_id=171', 'klapan-kps-1m(60)-no-mv(220)-150x150'),
(2607, 0, 1, 'product_id=170', 'klapan-kps-1m(60)-no-mv(220)-150x100'),
(2605, 0, 1, 'product_id=168', 'kassetnyj-filtr-(korpus-s-materialom)-bks-315'),
(2606, 0, 1, 'product_id=169', 'klapan-kps-1m(60)-no-mv(220)-100x100'),
(2604, 0, 1, 'product_id=167', 'kassetnyj-filtr-(korpus-s-materialom)-bks-250'),
(2603, 0, 1, 'product_id=166', 'kassetnyj-filtr-(korpus-s-materialom)-bks-200'),
(2602, 0, 1, 'product_id=165', 'kassetnyj-filtr-(korpus-s-materialom)-bks-160'),
(2601, 0, 1, 'product_id=164', 'kassetnyj-filtr-(korpus-s-materialom)-bks-125'),
(2600, 0, 1, 'product_id=163', 'kassetnyj-korpus-filtra-bksp-100-50'),
(2599, 0, 1, 'product_id=162', 'kassetnyj-korpus-filtra-bksp-90-50'),
(2598, 0, 1, 'product_id=161', 'kassetnyj-korpus-filtra-bksp-80-50'),
(2595, 0, 1, 'product_id=158', 'kassetnyj-korpus-filtra-bksp-60-30'),
(2597, 0, 1, 'product_id=160', 'kassetnyj-korpus-filtra-bksp-70-40'),
(2596, 0, 1, 'product_id=159', 'kassetnyj-korpus-filtra-bksp-60-35'),
(2594, 0, 1, 'product_id=157', 'kassetnyj-korpus-filtra-bksp-50-30'),
(2593, 0, 1, 'product_id=156', 'karmannyj-korpus-filtra-bkp-100-50'),
(2592, 0, 1, 'product_id=155', 'karmannyj-korpus-filtra-bkp-90-50'),
(2591, 0, 1, 'product_id=154', 'karmannyj-korpus-filtra-bkr-80-50'),
(2589, 0, 1, 'product_id=152', 'karmannyj-korpus-filtra-bkp-60-35'),
(2590, 0, 1, 'product_id=153', 'karmannyj-korpus-filtra-bkp-70-40'),
(2588, 0, 1, 'product_id=151', 'karmannyj-korpus-filtra-bkr-60-30'),
(2587, 0, 1, 'product_id=150', 'karmannyj-korpus-filtra-bkp-50-30'),
(2586, 0, 1, 'product_id=149', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-1000h500'),
(2585, 0, 1, 'product_id=148', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-900h500'),
(2584, 0, 1, 'product_id=147', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-800h500'),
(2583, 0, 1, 'product_id=146', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-700h400'),
(2582, 0, 1, 'product_id=145', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-600h300'),
(2581, 0, 1, 'product_id=144', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-500h300'),
(2579, 0, 1, 'product_id=142', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-400h200'),
(2580, 0, 1, 'product_id=143', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-400h250'),
(2578, 0, 1, 'product_id=141', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-300h300'),
(2577, 0, 1, 'product_id=140', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-300h200'),
(2574, 0, 1, 'product_id=137', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-200h200'),
(2575, 0, 1, 'product_id=138', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-250x250'),
(2576, 0, 1, 'product_id=139', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-300h150'),
(2573, 0, 1, 'product_id=136', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-200h150'),
(2572, 0, 1, 'product_id=135', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-200h100'),
(2571, 0, 1, 'product_id=134', 'zont-iz-ocinkovannoj-stali-dlya-ventilyacionnyh-shaht-150h150'),
(2570, 0, 1, 'product_id=133', 'drossel-klapan-s-ruchnym-upravleniem-250x250'),
(2569, 0, 1, 'product_id=132', 'drossel-klapan-s-ruchnym-upravleniem-200x200'),
(2568, 0, 1, 'product_id=131', 'drossel-klapan-s-ruchnym-upravleniem-200x150'),
(2567, 0, 1, 'product_id=130', 'drossel-klapan-s-ruchnym-upravleniem-200x100'),
(2566, 0, 1, 'product_id=129', 'drossel-klapan-s-ruchnym-upravleniem-150x150'),
(2565, 0, 1, 'product_id=128', 'drossel-klapan-s-ruchnym-upravleniem-150x100'),
(2564, 0, 1, 'product_id=127', 'drossel-klapan-s-ruchnym-upravleniem-100x100'),
(2563, 0, 1, 'product_id=126', 'drossel-klapan-s-ruchnym-upravleniem-315'),
(2562, 0, 1, 'product_id=125', 'drossel-klapan-s-ruchnym-upravleniem-250'),
(2561, 0, 1, 'product_id=124', 'drossel-klapan-s-ruchnym-upravleniem-200'),
(2560, 0, 1, 'product_id=123', 'drossel-klapan-s-ruchnym-upravleniem-160'),
(2559, 0, 1, 'product_id=122', 'drossel-klapan-s-ruchnym-upravleniem-125'),
(2558, 0, 1, 'product_id=121', 'diffuzor-dva-d250'),
(2557, 0, 1, 'product_id=120', 'diffuzor-dva-d200'),
(2553, 0, 1, 'product_id=116', 'datchik-temperatury-kanalnyj-250mm-pt1000-(beskorpusnyj-k02)'),
(2556, 0, 1, 'product_id=119', 'diffuzor-dva-d160'),
(2555, 0, 1, 'product_id=118', 'diffuzor-dva-d125'),
(2554, 0, 1, 'product_id=117', 'diffuzor-dva-d100'),
(2552, 0, 1, 'product_id=115', 'datchik-temperatury-kanalnyj-htf-pt1000'),
(2550, 0, 1, 'product_id=113', 'datchik-temperatury-vody-pogruzhnoj-etf01-pt-1000'),
(2551, 0, 1, 'product_id=114', 'datchik-temperatury-kanalnyj-etf-1144-99-an-ntc-(dlya-schitov-bm-mini)'),
(2549, 0, 1, 'product_id=112', 'datchik-davleniya-dps-500'),
(2545, 0, 1, 'product_id=108', 'gibkie-vozduhovody-izolirovannye-iso-160'),
(2546, 0, 1, 'product_id=109', 'gibkie-vozduhovody-izolirovannye-iso-203'),
(2547, 0, 1, 'product_id=110', 'gibkie-vozduhovody-izolirovannye-iso-254'),
(2548, 0, 1, 'product_id=111', 'gibkie-vozduhovody-izolirovannye-iso-315'),
(2544, 0, 1, 'product_id=107', 'gibkie-vozduhovody-izolirovannye-iso-127'),
(2543, 0, 1, 'product_id=106', 'gibkie-vozduhovody-izolirovannye-iso-102'),
(2542, 0, 1, 'product_id=105', 'gibkie-vozduhovody-armirovannye-af315-10m'),
(2541, 0, 1, 'product_id=104', 'gibkie-vozduhovody-armirovannye-af254-10m'),
(2540, 0, 1, 'product_id=103', 'gibkie-vozduhovody-armirovannye-af203-10m'),
(2539, 0, 1, 'product_id=102', 'gibkie-vozduhovody-armirovannye-af160-10m'),
(2538, 0, 1, 'product_id=101', 'gibkie-vozduhovody-armirovannye-af127-10m'),
(2537, 0, 1, 'product_id=100', 'gibkie-vozduhovody-armirovannye-af102-10m'),
(2536, 0, 1, 'product_id=99', 'vstavka-karmannaya-filtruyuschaya-fbr-100-50'),
(2535, 0, 1, 'product_id=98', 'vstavka-karmannaya-filtruyuschaya-fbr-90-50'),
(2534, 0, 1, 'product_id=97', 'vstavka-karmannaya-filtruyuschaya-fbr-80-50'),
(2533, 0, 1, 'product_id=96', 'vstavka-karmannaya-filtruyuschaya-fbr-70-40'),
(2532, 0, 1, 'product_id=95', 'vstavka-karmannaya-filtruyuschaya-fbr-60-35'),
(2531, 0, 1, 'product_id=94', 'vstavka-karmannaya-filtruyuschaya-fbr-60-30'),
(2530, 0, 1, 'product_id=93', 'vstavka-karmannaya-filtruyuschaya-fbr-50-30'),
(2529, 0, 1, 'product_id=92', 'vozdushnyj-klapan-zap-100-50'),
(2528, 0, 1, 'product_id=91', 'vozdushnyj-klapan-zap-90-50'),
(2527, 0, 1, 'product_id=90', 'vozdushnyj-klapan-zap-80-50'),
(2526, 0, 1, 'product_id=89', 'vozdushnyj-klapan-zap-70-40'),
(2525, 0, 1, 'product_id=88', 'vozdushnyj-klapan-zap-60-35'),
(2524, 0, 1, 'product_id=87', 'vozdushnyj-klapan-zap-60-30'),
(2523, 0, 1, 'product_id=86', 'vozdushnyj-klapan-zap-50-30'),
(2522, 0, 1, 'product_id=85', 'vozdushnyj-klapan-dcgar-315'),
(2521, 0, 1, 'product_id=84', 'vozdushnyj-klapan-dcgar-250'),
(2520, 0, 1, 'product_id=83', 'vozdushnyj-klapan-dcgar-200'),
(2519, 0, 1, 'product_id=82', 'vozdushnyj-klapan-dcgar-160'),
(2518, 0, 1, 'product_id=81', 'vozdushnyj-klapan-dcgar-125'),
(2517, 0, 1, 'product_id=80', 'vozdushnyj-klapan-dcgar-100'),
(2516, 0, 1, 'product_id=79', 'vozduhovody-iz-ocinkovannoj-stali-1-mm'),
(2515, 0, 1, 'product_id=78', 'vozduhovody-iz-ocinkovannoj-stali-0-7-mm'),
(2514, 0, 1, 'product_id=77', 'vozduhovody-iz-ocinkovannoj-stali-0-5-mm'),
(2513, 0, 1, 'product_id=76', 'vodyanoj-nagrevatel-d-100h50-3'),
(2512, 0, 1, 'product_id=75', 'vodyanoj-nagrevatel-wwp-90h50-3'),
(2511, 0, 1, 'product_id=74', 'vodyanoj-nagrevatel-wwp-80h50-3'),
(2510, 0, 1, 'product_id=73', 'vodyanoj-nagrevatel-wwp-70h40-3'),
(2509, 0, 1, 'product_id=72', 'vodyanoj-nagrevatel-d-60x35-3'),
(2508, 0, 1, 'product_id=71', 'vodyanoj-nagrevatel-d-60x30-3'),
(2507, 0, 1, 'product_id=70', 'vodyanoj-nagrevatel-d-50h30-3'),
(2506, 0, 1, 'product_id=69', 'vodyanoj-nagrevatel-d-100h50-2'),
(2502, 0, 1, 'product_id=65', 'vodyanoj-nagrevatel-d-60h35-2'),
(2505, 0, 1, 'product_id=68', 'vodyanoj-nagrevatel-wwp-90h50-2'),
(2504, 0, 1, 'product_id=67', 'vodyanoj-nagrevatel-wwp-80h50-2'),
(2503, 0, 1, 'product_id=66', 'vodyanoj-nagrevatel-d-70h40-2'),
(2501, 0, 1, 'product_id=64', 'vodyanoj-nagrevatel-d-60h30-2'),
(2500, 0, 1, 'product_id=63', 'vodyanoj-nagrevatel-d-50h30-2'),
(2499, 0, 1, 'product_id=62', 'vodyanoj-nagrevatel-d-315-2'),
(2498, 0, 1, 'product_id=61', 'ventilyator-90h50-4d-(380v)'),
(2497, 0, 1, 'product_id=60', 'ventilyator-80h50-4d-(380v)'),
(2496, 0, 1, 'product_id=59', 'ventilyator-70h40-4d-(380v)'),
(2495, 0, 1, 'product_id=58', 'ventilyator-60h35-4d-(380v)'),
(2494, 0, 1, 'product_id=57', 'ventilyator-60x30-4d-(380v)'),
(2493, 0, 1, 'product_id=56', 'ventilyator-50x30-4d-(380v)'),
(2492, 0, 1, 'product_id=55', 'ventilyator-d-315'),
(2491, 0, 1, 'product_id=54', 'ventilyator-d-250'),
(2490, 0, 1, 'product_id=53', 'ventilyator-d-200'),
(2489, 0, 1, 'product_id=52', 'ventilyator-d-160'),
(2488, 0, 1, 'product_id=51', 'ventilyator-d-125'),
(2487, 0, 1, 'product_id=50', 'ventilyator-d-100');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_session`
--

CREATE TABLE `oc_session` (
  `session_id` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_session`
--

INSERT INTO `oc_session` (`session_id`, `data`, `expire`) VALUES
('e9511a65a7293a5ae64207b415', '{\"api_id\":\"1\"}', '2021-01-24 02:07:29'),
('fec162da42b5086b14ac234a4c', '{\"language\":\"ru-ru\",\"currency\":\"RUB\",\"user_id\":\"1\",\"user_token\":\"plEKnHBmLaVfq2DR5VXUJvPeYiAXQwX4\",\"api_token\":\"e9511a65a7293a5ae64207b415\"}', '2021-01-24 02:25:04');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_setting`
--

CREATE TABLE `oc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(1152, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(1159, 0, 'config', 'config_seopro_addslash', '0', 0),
(1158, 0, 'config', 'config_seo_url_cache', '0', 0),
(1157, 0, 'config', 'config_seo_url_include_path', '0', 0),
(1153, 0, 'config', 'config_error_display', '1', 0),
(1154, 0, 'config', 'config_error_log', '1', 0),
(1155, 0, 'config', 'config_error_filename', 'error.log', 0),
(1156, 0, 'config', 'config_seo_pro', '0', 0),
(4, 0, 'voucher', 'total_voucher_sort_order', '8', 0),
(5, 0, 'voucher', 'total_voucher_status', '1', 0),
(1151, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(1150, 0, 'config', 'config_file_max_size', '300000', 0),
(1145, 0, 'config', 'config_compression', '7', 0),
(1146, 0, 'config', 'config_secure', '1', 0),
(1147, 0, 'config', 'config_password', '1', 0),
(1148, 0, 'config', 'config_shared', '0', 0),
(1149, 0, 'config', 'config_encryption', 'R9sGKgheezH4ZhxfoWT2GxkyBeARh3CF7LTAK6Nh8hUN5K6nqSPbbUnTBNS8xDC54JYGsD6z5vlK5Nq80gejdX5UuA6oIB4sHqhWFDT99GiL5ZMy3c5yGCWPJCRIoLXXBpYCSxWnMa1KwvzEqAeYBAE7zkKg6MPNghOLb7zlJjnfrrkC2j42UVCsxTVQYTc3Id2uBN9Zj4UgEmWuoTsLnDaGxAxXw56BzqFRkeJjCKFF7VnjkVlHVDaGxyfYaNRcUDTmTiBfWerhxLLvY4K9yjTHCSohjcStyI9QeKW6sHtisFH9nsirGQ2kzPWEVB2Tv84b4OnlOzznkbs9F5SSZWdJlfCN0z4wA6D5A8NSe6YTWB2fHAkhVHJUAdJAID40EXNY32YI9EnoKesusQGLQkjAeWpHwHw6AQijVv3lS2lHLJ5o6TMGUdkA4sGQrLk78ATGXEez5CyIAglhQsEU8uehrwlXy8Hmwd5tARXQ27IMkcVupKV5LaN7h95BtVrPrY0ZYavgFgTk0tEElQZkBcBzi5Sca7Lq9jNcLSJ6txs2lv5SIpSDdE1DZiqr4OV6LbcZcMzwsQiwdtEJovs7ZJblkcySt6f8qPgENJKWENoKphydTrZ7C2diJSlFeUsiiCbqwBs8N7FX67Osn7XBB1yKaDjy3zVSfl4x8gDc0Ys2kvLOQbJYr8aT8sWhyuhsNcnUCJKSX4p76m2XvmAE6fqNVQWxk3FD7VGxpZWGLprsLPnxlYHzOsIyQoq8WQVYIl8PqQ1Zn5ibdVnfwLFxgqVziJfbvaWdbV7OZqeDho0izzlQIzmAqVftnRVNj9ogvynkUgfw41YTzSr4JATaUhuGxHS8xgctvhi4enu79ABxoXcxN2LC4Ymg8FdBjCOvrq38YjN7xn4HStdn5WnVdotswvhqbxeEJXe6kNRSbMIgTQ4OSuiXC19Bv3vgl7KfKiGgq5J01D4BAE0KCJlUCxwy60paNvXeApBhWr7hBQ4Ge2g0Xjctvqw7qThOVCRH', 0),
(95, 0, 'payment_free_checkout', 'payment_free_checkout_status', '1', 0),
(96, 0, 'payment_free_checkout', 'free_checkout_order_status_id', '1', 0),
(97, 0, 'payment_free_checkout', 'payment_free_checkout_sort_order', '1', 0),
(98, 0, 'payment_cod', 'payment_cod_sort_order', '5', 0),
(99, 0, 'payment_cod', 'payment_cod_total', '0.01', 0),
(100, 0, 'payment_cod', 'payment_cod_order_status_id', '1', 0),
(101, 0, 'payment_cod', 'payment_cod_geo_zone_id', '0', 0),
(102, 0, 'payment_cod', 'payment_cod_status', '1', 0),
(670, 0, 'shipping_free', 'shipping_free_sort_order', '', 0),
(669, 0, 'shipping_free', 'shipping_free_status', '1', 0),
(668, 0, 'shipping_free', 'shipping_free_geo_zone_id', '0', 0),
(667, 0, 'shipping_free', 'shipping_free_total', '', 0),
(109, 0, 'total_sub_total', 'sub_total_sort_order', '1', 0),
(110, 0, 'total_sub_total', 'total_sub_total_status', '1', 0),
(112, 0, 'total_total', 'total_total_sort_order', '9', 0),
(113, 0, 'total_total', 'total_total_status', '1', 0),
(1144, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(1142, 0, 'config', 'config_maintenance', '0', 0),
(1143, 0, 'config', 'config_seo_url', '0', 0),
(861, 0, 'module_trade_import', 'module_trade_import_sync_schedule', '2021-01-02 16:02:00', 0),
(860, 0, 'module_trade_import', 'module_trade_import_add_one_product', '', 0),
(858, 0, 'module_trade_import', 'module_trade_import_hide_product', '0', 0),
(1141, 0, 'config', 'config_mail_alert_email', '', 0),
(125, 0, 'theme_default', 'theme_default_product_limit', '15', 0),
(126, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(127, 0, 'theme_default', 'theme_default_image_thumb_width', '228', 0),
(128, 0, 'theme_default', 'theme_default_image_thumb_height', '228', 0),
(129, 0, 'theme_default', 'theme_default_image_popup_width', '500', 0),
(130, 0, 'theme_default', 'theme_default_image_popup_height', '500', 0),
(131, 0, 'theme_default', 'theme_default_image_category_width', '80', 0),
(132, 0, 'theme_default', 'theme_default_image_category_height', '80', 0),
(133, 0, 'theme_default', 'theme_default_image_manufacturer_width', '80', 0),
(134, 0, 'theme_default', 'theme_default_image_manufacturer_height', '80', 0),
(135, 0, 'theme_default', 'theme_default_image_product_width', '228', 0),
(136, 0, 'theme_default', 'theme_default_image_product_height', '228', 0),
(137, 0, 'theme_default', 'theme_default_image_additional_width', '74', 0),
(138, 0, 'theme_default', 'theme_default_image_additional_height', '74', 0),
(139, 0, 'theme_default', 'theme_default_image_related_width', '200', 0),
(140, 0, 'theme_default', 'theme_default_image_related_height', '200', 0),
(141, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(142, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(143, 0, 'theme_default', 'theme_default_image_wishlist_width', '47', 0),
(144, 0, 'theme_default', 'theme_default_image_wishlist_height', '47', 0),
(145, 0, 'theme_default', 'theme_default_image_cart_height', '47', 0),
(146, 0, 'theme_default', 'theme_default_image_cart_width', '47', 0),
(147, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(148, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(149, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(150, 0, 'theme_default', 'theme_default_status', '1', 0),
(151, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(152, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(153, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(154, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(155, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(156, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(157, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(158, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(159, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(160, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(161, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(162, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(163, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(164, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(165, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(166, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(167, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(168, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(169, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(170, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(171, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(172, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(173, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(174, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(175, 0, 'report_customer_activity', 'report_customer_activity_status', '1', 0),
(176, 0, 'report_customer_activity', 'report_customer_activity_sort_order', '1', 0),
(177, 0, 'report_customer_order', 'report_customer_order_status', '1', 0),
(178, 0, 'report_customer_order', 'report_customer_order_sort_order', '2', 0),
(179, 0, 'report_customer_reward', 'report_customer_reward_status', '1', 0),
(180, 0, 'report_customer_reward', 'report_customer_reward_sort_order', '3', 0),
(181, 0, 'report_customer_search', 'report_customer_search_sort_order', '3', 0),
(182, 0, 'report_customer_search', 'report_customer_search_status', '1', 0),
(183, 0, 'report_customer_transaction', 'report_customer_transaction_status', '1', 0),
(184, 0, 'report_customer_transaction', 'report_customer_transaction_status_sort_order', '4', 0),
(185, 0, 'report_sale_tax', 'report_sale_tax_status', '1', 0),
(186, 0, 'report_sale_tax', 'report_sale_tax_sort_order', '5', 0),
(187, 0, 'report_sale_shipping', 'report_sale_shipping_status', '1', 0),
(188, 0, 'report_sale_shipping', 'report_sale_shipping_sort_order', '6', 0),
(189, 0, 'report_sale_return', 'report_sale_return_status', '1', 0),
(190, 0, 'report_sale_return', 'report_sale_return_sort_order', '7', 0),
(191, 0, 'report_sale_order', 'report_sale_order_status', '1', 0),
(192, 0, 'report_sale_order', 'report_sale_order_sort_order', '8', 0),
(193, 0, 'report_sale_coupon', 'report_sale_coupon_status', '1', 0),
(194, 0, 'report_sale_coupon', 'report_sale_coupon_sort_order', '9', 0),
(195, 0, 'report_product_viewed', 'report_product_viewed_status', '1', 0),
(196, 0, 'report_product_viewed', 'report_product_viewed_sort_order', '10', 0),
(197, 0, 'report_product_purchased', 'report_product_purchased_status', '1', 0),
(198, 0, 'report_product_purchased', 'report_product_purchased_sort_order', '11', 0),
(199, 0, 'report_marketing', 'report_marketing_status', '1', 0),
(200, 0, 'report_marketing', 'report_marketing_sort_order', '12', 0),
(227, 0, 'developer', 'developer_theme', '0', 0),
(203, 0, 'configblog', 'configblog_name', 'Блог', 0),
(204, 0, 'configblog', 'configblog_html_h1', 'Блог для интернет магазина на OpenCart', 0),
(205, 0, 'configblog', 'configblog_meta_title', 'Блог для интернет магазина на OpenCart', 0),
(206, 0, 'configblog', 'configblog_meta_description', 'Блог для интернет магазина на OpenCart', 0),
(207, 0, 'configblog', 'configblog_meta_keyword', 'Блог для интернет магазина на OpenCart', 0),
(208, 0, 'configblog', 'configblog_article_count', '1', 0),
(209, 0, 'configblog', 'configblog_article_limit', '20', 0),
(210, 0, 'configblog', 'configblog_article_description_length', '200', 0),
(211, 0, 'configblog', 'configblog_limit_admin', '20', 0),
(212, 0, 'configblog', 'configblog_blog_menu', '1', 0),
(213, 0, 'configblog', 'configblog_article_download', '1', 0),
(214, 0, 'configblog', 'configblog_review_status', '1', 0),
(215, 0, 'configblog', 'configblog_review_guest', '1', 0),
(216, 0, 'configblog', 'configblog_review_mail', '1', 0),
(217, 0, 'configblog', 'configblog_image_category_width', '50', 0),
(218, 0, 'configblog', 'configblog_image_category_height', '50', 0),
(219, 0, 'configblog', 'configblog_image_article_width', '150', 0),
(220, 0, 'configblog', 'configblog_image_article_height', '150', 0),
(221, 0, 'configblog', 'configblog_image_related_width', '200', 0),
(222, 0, 'configblog', 'configblog_image_related_height', '200', 0),
(859, 0, 'module_trade_import', 'module_trade_import_add_separate_products', '0', 0),
(1140, 0, 'config', 'config_mail_alert', '[\"order\"]', 1),
(1139, 0, 'config', 'config_mail_smtp_timeout', '5', 0),
(1138, 0, 'config', 'config_mail_smtp_port', '25', 0),
(1137, 0, 'config', 'config_mail_smtp_password', '', 0),
(1136, 0, 'config', 'config_mail_smtp_username', '', 0),
(1135, 0, 'config', 'config_mail_smtp_hostname', '', 0),
(1134, 0, 'config', 'config_mail_parameter', '-O DeliveryMode=b', 0),
(1133, 0, 'config', 'config_mail_engine', 'mail', 0),
(228, 0, 'developer', 'developer_sass', '1', 0),
(1131, 0, 'config', 'config_logo', '', 0),
(1132, 0, 'config', 'config_icon', '', 0),
(1130, 0, 'config', 'config_captcha_page', '[\"review\",\"return\",\"contact\"]', 1),
(1129, 0, 'config', 'config_captcha', '', 0),
(1128, 0, 'config', 'config_return_status_id', '2', 0),
(1127, 0, 'config', 'config_return_id', '0', 0),
(1126, 0, 'config', 'config_affiliate_id', '0', 0),
(1125, 0, 'config', 'config_affiliate_commission', '5', 0),
(1124, 0, 'config', 'config_affiliate_auto', '0', 0),
(1122, 0, 'config', 'config_affiliate_group_id', '1', 0),
(1123, 0, 'config', 'config_affiliate_approval', '0', 0),
(1120, 0, 'config', 'config_stock_warning', '0', 0),
(1121, 0, 'config', 'config_stock_checkout', '1', 0),
(1119, 0, 'config', 'config_stock_display', '0', 0),
(1118, 0, 'config', 'config_api_id', '1', 0),
(854, 0, 'module_trade_import', 'module_trade_import_hide_category', '0', 0),
(855, 0, 'module_trade_import', 'module_trade_import_sub_filters', '0', 0),
(856, 0, 'module_trade_import', 'module_trade_import_add_product', '1', 0),
(857, 0, 'module_trade_import', 'module_trade_import_delete_product', '1', 0),
(853, 0, 'module_trade_import', 'module_trade_import_delete_category', '1', 0),
(852, 0, 'module_trade_import', 'module_trade_import_add_category', '1', 0),
(851, 0, 'module_trade_import', 'module_trade_import_save_json', '1', 0),
(850, 0, 'module_trade_import', 'module_trade_import_local_json', '0', 0),
(849, 0, 'module_trade_import', 'module_trade_import_time_zone', 'Asia/Vladivostok', 0),
(848, 0, 'module_trade_import', 'module_trade_import_sync_period', '1_day', 0),
(842, 0, 'module_trade_import', 'module_trade_import_price_map', '', 0),
(843, 0, 'module_trade_import', 'module_trade_import_parent_id', '', 0),
(844, 0, 'module_trade_import', 'module_trade_import_ignore_category', '', 0),
(845, 0, 'module_trade_import', 'module_trade_import_complects_group', 'b39a7c22-a19b-48b8-9b58-0728299a462a', 0),
(846, 0, 'module_trade_import', 'module_trade_import_complects_ignore_quantity', '9a04e7d1-5eb3-4c92-8d22-e538be5eba3d,4312ff34-ea08-4a13-890d-52b3160238f8,f9d06603-9199-41c7-8e4d-dfe7ef084a75,fbfc8b3c-94df-4569-aea5-028abb224052,0ffbbbb7-7769-4395-a5cd-60f153255bc5', 0),
(847, 0, 'module_trade_import', 'module_trade_import_enable_sync', '0', 0),
(840, 0, 'module_trade_import', 'module_trade_import_price', '342f7b93-06f7-41ee-b012-908530a64275', 0),
(841, 0, 'module_trade_import', 'module_trade_import_top_category', '', 0),
(838, 0, 'module_trade_import', 'module_trade_import_order_address', 'https://ventstroysnab.4ait.ru/api/v1/orders', 0),
(839, 0, 'module_trade_import', 'module_trade_import_order_token', '', 0),
(837, 0, 'module_trade_import', 'module_trade_import_enable_order', '1', 0),
(836, 0, 'module_trade_import', 'module_trade_import_old_api_token', '', 0),
(835, 0, 'module_trade_import', 'module_trade_import_old_api_address', '', 0),
(834, 0, 'module_trade_import', 'module_trade_import_enable_old_api', '0', 0),
(833, 0, 'module_trade_import', 'module_trade_import_token', 'd1fdb793549cf292b69ec356e3a5f8ad291ced04', 0),
(832, 0, 'module_trade_import', 'module_trade_import_nomenclature', 'https://ventstroysnab.4ait.ru/api/v1/products', 0),
(830, 0, 'module_trade_import', 'module_trade_import_server', 'https://ventstroysnab.4ait.ru/', 0),
(831, 0, 'module_trade_import', 'module_trade_import_code', 'https://ventstroysnab.4ait.ru/api/v1/auth', 0),
(862, 0, 'module_manager', 'module_manager_buttons', '[\"history\",\"invoice\",\"shipping\",\"delete\",\"create\",\"minimize\",\"toggle\",\"filter\",\"clear\",\"edit_customer\",\"view_order\",\"edit_order\"]', 1),
(863, 0, 'module_manager', 'module_manager_columns', '[\"select\",\"order_id\",\"order_status_id\",\"customer\",\"recipient\",\"date_added\",\"date_modified\",\"products\",\"payment\",\"shipping\",\"subtotal\",\"total\",\"actions\"]', 1),
(864, 0, 'module_manager', 'module_manager_statuses', '{\"16\":{\"checked\":\"1\",\"color\":\"\"},\"2\":{\"checked\":\"1\",\"color\":\"\"},\"8\":{\"checked\":\"1\",\"color\":\"\"},\"11\":{\"checked\":\"1\",\"color\":\"\"},\"3\":{\"checked\":\"1\",\"color\":\"\"},\"14\":{\"checked\":\"1\",\"color\":\"\"},\"10\":{\"checked\":\"1\",\"color\":\"\"},\"15\":{\"checked\":\"1\",\"color\":\"\"},\"1\":{\"checked\":\"1\",\"color\":\"\"},\"9\":{\"checked\":\"1\",\"color\":\"\"},\"7\":{\"checked\":\"1\",\"color\":\"\"},\"12\":{\"checked\":\"1\",\"color\":\"\"},\"13\":{\"checked\":\"1\",\"color\":\"\"},\"5\":{\"checked\":\"1\",\"color\":\"\"},\"0\":{\"checked\":\"1\",\"color\":\"\"}}', 1),
(865, 0, 'module_manager', 'module_manager_payments', '{\"cod\":{\"color\":\"\"},\"free_checkout\":{\"color\":\"\"}}', 1),
(866, 0, 'module_manager', 'module_manager_shippings', '{\"free\":{\"color\":\"\"}}', 1),
(867, 0, 'module_manager', 'module_manager_mode', 'full', 0),
(868, 0, 'module_manager', 'module_manager_notice', '', 0),
(869, 0, 'module_manager', 'module_manager_hide_dashboard', '', 0),
(870, 0, 'module_manager', 'module_manager_filters', '', 0),
(871, 0, 'module_manager', 'module_manager_notify', '', 0),
(872, 0, 'module_manager', 'module_manager_default_limit', '10', 0),
(873, 0, 'module_manager', 'module_manager_default_links', '', 0),
(874, 0, 'module_manager', 'module_manager_name_format', 'firstname', 0),
(875, 0, 'module_manager', 'module_manager_address_format', '&lt;b&gt;{name}&lt;/b&gt;\r\nМагазин: {store}\r\n{company}\r\n&lt;a href=\'skype:+{telephone}?call\'&gt;{telephone}&lt;/a&gt;\r\n&lt;a href=\'mailto:{email}\'&gt;{email}&lt;/a&gt;\r\n{address}\r\n{country}, {city}, {zone}\r\n{postcode}\'', 0),
(876, 0, 'module_manager', 'module_manager_date_format', '', 0),
(877, 0, 'module_manager', 'module_manager_addips', '', 0),
(878, 0, 'module_manager', 'module_manager_status', '1', 0),
(1117, 0, 'config', 'config_fraud_status_id', '16', 0),
(1116, 0, 'config', 'config_complete_status', '[\"3\",\"5\"]', 1),
(1115, 0, 'config', 'config_processing_status', '[\"2\",\"3\",\"1\",\"12\",\"5\"]', 1),
(1114, 0, 'config', 'config_order_status_id', '1', 0),
(1113, 0, 'config', 'config_checkout_id', '0', 0),
(1112, 0, 'config', 'config_checkout_guest', '1', 0),
(1111, 0, 'config', 'config_cart_weight', '0', 0),
(1110, 0, 'config', 'config_invoice_prefix', 'INV-2020-00', 0),
(1109, 0, 'config', 'config_account_id', '0', 0),
(1108, 0, 'config', 'config_login_attempts', '5', 0),
(1107, 0, 'config', 'config_customer_price', '0', 0),
(1106, 0, 'config', 'config_customer_group_display', '[\"1\"]', 1),
(1105, 0, 'config', 'config_customer_group_id', '1', 0),
(1104, 0, 'config', 'config_customer_search', '0', 0),
(1103, 0, 'config', 'config_customer_activity', '0', 0),
(1102, 0, 'config', 'config_customer_online', '0', 0),
(1099, 0, 'config', 'config_tax', '0', 0),
(1100, 0, 'config', 'config_tax_default', '', 0),
(1101, 0, 'config', 'config_tax_customer', '', 0),
(1091, 0, 'config', 'config_length_class_id', '1', 0),
(1092, 0, 'config', 'config_weight_class_id', '1', 0),
(1093, 0, 'config', 'config_product_count', '0', 0),
(1094, 0, 'config', 'config_limit_admin', '20', 0),
(1095, 0, 'config', 'config_review_status', '0', 0),
(1096, 0, 'config', 'config_review_guest', '0', 0),
(1097, 0, 'config', 'config_voucher_min', '1', 0),
(1098, 0, 'config', 'config_voucher_max', '1000', 0),
(1090, 0, 'config', 'config_currency_auto', '1', 0),
(1087, 0, 'config', 'config_language', 'ru-ru', 0),
(1088, 0, 'config', 'config_admin_language', 'ru-ru', 0),
(1089, 0, 'config', 'config_currency', 'RUB', 0),
(1076, 0, 'config', 'config_address', 'Адрес', 0),
(1077, 0, 'config', 'config_geocode', '', 0),
(1078, 0, 'config', 'config_email', 'ventstroysnab@gmail.com', 0),
(1079, 0, 'config', 'config_telephone', '+7 (4212) 62-41-86', 0),
(1080, 0, 'config', 'config_telephone2', '', 0),
(1081, 0, 'config', 'config_fax', '', 0),
(1082, 0, 'config', 'config_image', '', 0),
(1083, 0, 'config', 'config_open', '', 0),
(1084, 0, 'config', 'config_comment', '', 0),
(1085, 0, 'config', 'config_country_id', '176', 0),
(1086, 0, 'config', 'config_zone_id', '2748', 0),
(1075, 0, 'config', 'config_owner', 'ВентСтройСнаб', 0),
(1074, 0, 'config', 'config_name', 'ВентСтройСнаб', 0),
(1073, 0, 'config', 'config_layout_id', '4', 0),
(1072, 0, 'config', 'config_theme', 'default', 0),
(1071, 0, 'config', 'config_meta_keyword', 'ВентСтройСнаб, вентиляция', 0),
(1069, 0, 'config', 'config_meta_title', 'ВентСтройСнаб', 0),
(1070, 0, 'config', 'config_meta_description', 'ВентСтройСнаб', 0),
(1160, 0, 'config', 'config_seopro_lowercase', '0', 0),
(1161, 0, 'config', 'config_page_postfix', '', 0),
(1162, 0, 'config', 'config_valide_param_flag', '0', 0),
(1163, 0, 'config', 'config_valide_params', 'tracking\r\nutm_source\r\nutm_campaign\r\nutm_medium\r\ntype\r\nsource\r\nblock\r\nposition\r\nkeyword\r\nyclid\r\ngclid', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_shipping_courier`
--

CREATE TABLE `oc_shipping_courier` (
  `shipping_courier_id` int(11) NOT NULL,
  `shipping_courier_code` varchar(255) NOT NULL DEFAULT '',
  `shipping_courier_name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_shipping_courier`
--

INSERT INTO `oc_shipping_courier` (`shipping_courier_id`, `shipping_courier_code`, `shipping_courier_name`) VALUES
(1, 'dhl', 'DHL'),
(2, 'fedex', 'Fedex'),
(3, 'ups', 'UPS'),
(4, 'royal-mail', 'Royal Mail'),
(5, 'usps', 'United States Postal Service'),
(6, 'auspost', 'Australia Post'),
(7, 'citylink', 'Citylink');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_statistics`
--

CREATE TABLE `oc_statistics` (
  `statistics_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `value` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_statistics`
--

INSERT INTO `oc_statistics` (`statistics_id`, `code`, `value`) VALUES
(1, 'order_sale', '7448734.4000'),
(2, 'order_processing', '0.0000'),
(3, 'order_complete', '0.0000'),
(4, 'order_other', '0.0000'),
(5, 'returns', '0.0000'),
(6, 'product', '0.0000'),
(7, 'review', '0.0000');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'В наличии'),
(8, 1, 'Предзаказ'),
(5, 1, 'Нет в наличии'),
(6, 1, 'Ожидание 2-3 дня'),
(7, 2, 'In Stock'),
(8, 2, 'Pre-Order'),
(5, 2, 'Out Of Stock'),
(6, 2, '2-3 Days');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_store`
--

CREATE TABLE `oc_store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_theme`
--

CREATE TABLE `oc_theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import`
--

CREATE TABLE `oc_trade_import` (
  `operation_id` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `json_timestamp` timestamp NULL DEFAULT NULL,
  `success` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oc_trade_import`
--

INSERT INTO `oc_trade_import` (`operation_id`, `timestamp`, `json_timestamp`, `success`) VALUES
(1, '2020-07-14 13:37:32', '2020-07-14 03:37:30', 1),
(2, '2020-07-14 13:37:46', '2020-07-14 03:37:44', 1),
(3, '2020-07-14 13:43:39', '2020-07-14 03:43:37', 1),
(4, '2020-07-14 13:48:17', '2020-07-14 03:48:15', 1),
(5, '2020-07-14 13:57:52', '2020-07-14 03:57:50', 1),
(6, '2020-12-09 06:19:52', '2020-12-08 20:19:52', 1),
(7, '2020-12-09 06:56:33', '2020-12-08 20:56:33', 1),
(8, '2021-01-01 06:02:02', '2020-12-31 20:01:21', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_category_codes`
--

CREATE TABLE `oc_trade_import_category_codes` (
  `category_id` int(11) NOT NULL,
  `group_uuid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oc_trade_import_category_codes`
--

INSERT INTO `oc_trade_import_category_codes` (`category_id`, `group_uuid`) VALUES
(50, '7de209aa-ec70-4db8-8178-e2f1d8f5cd00'),
(51, 'ee32f7aa-a9fb-474c-95ff-a3bee2ab58f1'),
(52, 'b0f82632-76f9-4527-9317-0c53de2ad253'),
(53, '2d5942ee-8287-4026-9b32-34f0ac87c49c'),
(54, '1883bcc5-6b6b-4dc2-b05b-5d139b915037'),
(55, '185546a2-6405-4824-9395-4f4dd841cbe7'),
(56, '4f4b76d3-1cc6-4c3b-a9df-565352c23792'),
(57, '0a73c5b2-5a21-4fbd-b63d-239041670c5f'),
(58, 'a34d61db-3733-4444-bb20-c67a79824874'),
(59, 'f5192646-626a-4161-8eee-12be2756611a'),
(60, '797f9048-363d-4eb7-9523-e972d37a2fdc'),
(61, 'b2e78c7f-1e51-4823-9f8b-391ac895a43f'),
(62, '6f47bb42-e7fd-448f-a9ae-4e9e0cd15f43'),
(63, '6da3740d-295f-4313-9e33-e6a7ca3c0aca'),
(64, '1d30352d-b0ea-4cbd-b26b-7e773ef64dec'),
(65, '011ccdb6-d7e5-49d8-b801-abcab152c5d2'),
(66, '43537669-9bf1-4bc4-841b-7c79c69e6be2'),
(67, 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(68, 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(69, 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(70, 'd62a47df-7d41-496c-9f91-3ccfa012f4e7'),
(71, '461b7616-9840-4e2f-bd53-7b679e72ed56'),
(72, '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(73, '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(74, '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(75, '32bdaf53-578d-46b6-ab7d-94940a279515'),
(76, '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(77, 'ac2ef08d-32f9-4c8d-98d9-8f46728f3478'),
(78, '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(79, '24146130-dbeb-4bc3-9639-3e87766d1c5c'),
(80, 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(81, '25aa6aba-b75a-410d-ae65-935dc34f57a6'),
(82, '56ca7bd0-fb0e-4db4-9145-859814194ceb');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_complect`
--

CREATE TABLE `oc_trade_import_complect` (
  `complect_id` int(11) NOT NULL,
  `complect_uuid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_air` int(10) DEFAULT NULL,
  `max_air` int(10) DEFAULT NULL,
  `min_pressure` int(10) DEFAULT NULL,
  `max_pressure` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oc_trade_import_complect`
--

INSERT INTO `oc_trade_import_complect` (`complect_id`, `complect_uuid`, `name`, `min_air`, `max_air`, `min_pressure`, `max_pressure`) VALUES
(1, '5e9bd905-55b6-4473-85eb-5329c085da3a', 'Пакет №1', 50, 149, 270, 119),
(2, '06b70358-1ada-4f35-9d61-b4cb4336690a', 'Пакет №2', 150, 239, 200, 120),
(3, 'd8bd691e-a3ea-457e-93af-e36a25a576b0', 'Пакет №3', 240, 449, 340, 201),
(4, '72358dea-c35c-4061-aef4-431dbce2503a', 'Пакет №4', 450, 700, 341, 201),
(5, '9b481215-d77c-463d-a65d-81fbb203418f', 'Пакет №5', 701, 840, 300, 201),
(6, 'e98cfa45-20ec-4e77-8c8e-e62f01df2702', 'Пакет №6', 841, 1100, 400, 301),
(7, 'acc380fe-665c-435a-b2a8-4eea948d49ec', 'Пакет №7', 800, 1399, 399, 330),
(8, '8847598e-1e85-47b0-bcfe-f13029fab48a', 'Пакет №8', 1400, 1999, 499, 400),
(9, '3203c4c2-e1a5-4249-81dd-a2167b463113', 'Пакет №9', 1500, 2999, 640, 500),
(10, '4bb41ba7-5051-464d-b79c-e9471ec35ab6', 'Пакет №10', 3000, 4400, 820, 641),
(11, '7c6af822-bd85-4712-8417-a88c260a8eb6', 'Пакет №11', 2000, 6000, 930, 821);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_complect_products`
--

CREATE TABLE `oc_trade_import_complect_products` (
  `complect_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `ignore_quantity` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oc_trade_import_complect_products`
--

INSERT INTO `oc_trade_import_complect_products` (`complect_id`, `product_id`, `quantity`, `ignore_quantity`) VALUES
(1, 50, 2, 0),
(1, 219, 4, 0),
(1, 251, 1, 0),
(1, 134, 1, 0),
(1, 112, 3, 0),
(1, 115, 1, 0),
(1, 346, 1, 0),
(1, 80, 2, 0),
(1, 283, 2, 0),
(1, 357, 1, 0),
(1, 297, 1, 0),
(1, 328, 2, 0),
(1, 354, 1, 0),
(1, 77, 1, 1),
(1, 352, 1, 1),
(1, 360, 1, 0),
(2, 51, 2, 0),
(2, 164, 1, 0),
(2, 284, 2, 0),
(2, 219, 3, 0),
(2, 251, 1, 0),
(2, 134, 1, 0),
(2, 115, 1, 0),
(2, 112, 3, 0),
(2, 77, 1, 1),
(2, 81, 2, 0),
(2, 301, 1, 0),
(2, 297, 1, 0),
(2, 328, 2, 0),
(2, 352, 1, 1),
(2, 357, 1, 0),
(2, 354, 1, 0),
(3, 52, 2, 0),
(3, 165, 1, 0),
(3, 285, 2, 0),
(3, 251, 1, 0),
(3, 219, 3, 0),
(3, 137, 1, 0),
(3, 115, 1, 0),
(3, 77, 1, 1),
(3, 112, 3, 0),
(3, 82, 2, 0),
(3, 307, 1, 0),
(3, 297, 1, 0),
(3, 328, 2, 0),
(3, 357, 1, 0),
(3, 354, 1, 0),
(3, 352, 1, 1),
(4, 53, 2, 0),
(4, 166, 1, 0),
(4, 288, 2, 0),
(4, 219, 3, 0),
(4, 137, 1, 0),
(4, 297, 1, 0),
(4, 115, 1, 0),
(4, 77, 1, 1),
(4, 112, 3, 0),
(4, 83, 2, 0),
(4, 312, 1, 0),
(4, 328, 2, 0),
(4, 352, 1, 1),
(4, 357, 1, 0),
(4, 354, 1, 0),
(4, 251, 1, 0),
(5, 54, 2, 0),
(5, 167, 1, 0),
(5, 289, 2, 0),
(5, 219, 3, 0),
(5, 137, 1, 0),
(5, 297, 1, 0),
(5, 115, 1, 0),
(5, 112, 3, 0),
(5, 77, 1, 1),
(5, 328, 2, 0),
(5, 84, 2, 0),
(5, 317, 1, 0),
(5, 352, 1, 1),
(5, 357, 1, 0),
(5, 354, 1, 0),
(5, 249, 1, 0),
(6, 55, 2, 0),
(6, 62, 1, 0),
(6, 168, 1, 0),
(6, 287, 2, 0),
(6, 219, 3, 0),
(6, 252, 1, 0),
(6, 138, 1, 0),
(6, 297, 1, 0),
(6, 115, 1, 0),
(6, 112, 3, 0),
(6, 77, 1, 1),
(6, 85, 2, 0),
(6, 328, 2, 0),
(6, 352, 1, 1),
(6, 357, 1, 0),
(6, 354, 1, 0),
(7, 56, 2, 0),
(7, 267, 1, 0),
(7, 290, 2, 0),
(7, 226, 12, 0),
(7, 253, 1, 0),
(7, 144, 1, 0),
(7, 112, 3, 0),
(7, 115, 1, 0),
(7, 77, 1, 1),
(7, 339, 4, 0),
(7, 347, 1, 0),
(7, 335, 1, 0),
(7, 86, 2, 0),
(7, 328, 2, 0),
(7, 275, 2, 0),
(7, 298, 1, 0),
(7, 354, 1, 0),
(7, 352, 1, 1),
(7, 357, 1, 0),
(7, 180, 2, 0),
(7, 194, 2, 0),
(8, 57, 2, 0),
(8, 323, 1, 0),
(8, 268, 1, 0),
(8, 291, 1, 0),
(8, 225, 12, 0),
(8, 255, 1, 0),
(8, 141, 1, 0),
(8, 298, 1, 0),
(8, 277, 2, 0),
(8, 181, 2, 0),
(8, 115, 1, 0),
(8, 112, 3, 0),
(8, 77, 1, 1),
(8, 340, 4, 0),
(8, 336, 1, 0),
(8, 87, 2, 0),
(8, 194, 2, 0),
(8, 328, 2, 0),
(8, 352, 1, 1),
(8, 354, 1, 0),
(8, 357, 1, 0),
(9, 58, 2, 0),
(9, 269, 1, 0),
(9, 292, 1, 0),
(9, 225, 16, 0),
(9, 255, 1, 0),
(9, 141, 1, 0),
(9, 298, 1, 0),
(9, 115, 1, 0),
(9, 112, 3, 0),
(9, 77, 1, 1),
(9, 341, 4, 0),
(9, 348, 1, 0),
(9, 337, 1, 0),
(9, 88, 2, 0),
(9, 328, 2, 0),
(9, 354, 1, 0),
(9, 352, 1, 1),
(9, 357, 1, 0),
(9, 279, 2, 0),
(9, 182, 2, 0),
(9, 194, 2, 0),
(10, 59, 2, 0),
(10, 255, 1, 0),
(10, 144, 1, 0),
(10, 299, 1, 0),
(10, 112, 3, 0),
(10, 115, 1, 0),
(10, 77, 1, 1),
(10, 270, 1, 0),
(10, 293, 1, 0),
(10, 235, 20, 0),
(10, 342, 4, 0),
(10, 349, 1, 0),
(10, 338, 1, 0),
(10, 89, 2, 0),
(10, 328, 2, 0),
(10, 352, 1, 1),
(10, 357, 1, 0),
(10, 354, 1, 0),
(10, 280, 2, 0),
(10, 183, 2, 0),
(10, 194, 2, 0),
(11, 60, 2, 0),
(11, 271, 1, 0),
(11, 294, 1, 0),
(11, 238, 20, 0),
(11, 146, 1, 0),
(11, 299, 1, 0),
(11, 112, 3, 0),
(11, 115, 1, 0),
(11, 90, 2, 0),
(11, 258, 1, 0),
(11, 328, 2, 0),
(11, 77, 1, 1),
(11, 352, 1, 1),
(11, 357, 1, 0),
(11, 354, 1, 0),
(11, 154, 1, 0),
(11, 281, 2, 0),
(11, 184, 2, 0),
(11, 194, 2, 0),
(11, 343, 4, 0),
(11, 350, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_option_codes`
--

CREATE TABLE `oc_trade_import_option_codes` (
  `option_id` int(11) NOT NULL,
  `nomenclature_uuid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_option_value_codes`
--

CREATE TABLE `oc_trade_import_option_value_codes` (
  `option_value_id` int(11) NOT NULL,
  `characteristic_uuid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomenclature_uuid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_product_codes`
--

CREATE TABLE `oc_trade_import_product_codes` (
  `product_id` int(11) NOT NULL,
  `nomenclature_uuid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_uuid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oc_trade_import_product_codes`
--

INSERT INTO `oc_trade_import_product_codes` (`product_id`, `nomenclature_uuid`, `group_uuid`) VALUES
(302, '01f04731-3a90-491b-9879-2708b1828e42', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(127, '0454d646-c69a-4a8b-9ef1-ef0a2cfcce79', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(286, '047a4001-2337-4b50-b2bf-b70196fea32c', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(145, '049006d6-f909-42c1-b748-a8608006858e', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(355, '065fc6ce-031e-45ac-a93c-82afa4e1bd49', 'b0f82632-76f9-4527-9317-0c53de2ad253'),
(74, '08267649-8faa-4eaa-9900-bf0f6146876f', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(185, '086441a2-ed13-4e53-9cdc-d55c14a880c6', '011ccdb6-d7e5-49d8-b801-abcab152c5d2'),
(317, '09138d55-859d-4c2d-9f52-d645be3ffe11', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(354, '09fbb05c-60d5-4fd3-92d6-e1ccf60ca73d', 'b0f82632-76f9-4527-9317-0c53de2ad253'),
(62, '0d89728e-d803-4eba-9f27-14978cc81a45', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(140, '0df55deb-fb7e-4fb2-afc4-2c061d298bc1', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(353, '0ffbbbb7-7769-4395-a5cd-60f153255bc5', '2d5942ee-8287-4026-9b32-34f0ac87c49c'),
(131, '105e3d3c-786b-4892-86ba-d66e726ed350', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(295, '10db20a2-2d33-40f4-a8de-a2458a5ff33c', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(54, '11baa68c-1c52-486e-a137-424c81ea2665', '32bdaf53-578d-46b6-ab7d-94940a279515'),
(167, '121dfb6c-4600-404b-880f-76f369539cc3', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(262, '1300f78e-32c9-42d8-b6df-ef80fb85c3bc', 'f5192646-626a-4161-8eee-12be2756611a'),
(153, '14be6807-de4c-442f-81bc-4cc4d0bea60a', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(178, '16111021-6571-4133-9da4-408ecbe6d1eb', '43537669-9bf1-4bc4-841b-7c79c69e6be2'),
(183, '165faa1e-2de1-4bc8-9027-c51dbe75f39a', '011ccdb6-d7e5-49d8-b801-abcab152c5d2'),
(328, '166120c9-86e1-4447-b9b1-3cccc40dd4fa', '4f4b76d3-1cc6-4c3b-a9df-565352c23792'),
(333, '16c92c29-3b16-4bf2-ae8d-2caadb9b2d6c', '185546a2-6405-4824-9395-4f4dd841cbe7'),
(228, '17dc15f2-732b-4e5d-9cca-4a331af18213', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(310, '181da19e-1211-4ce6-8b0d-94b7df079e0b', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(151, '1abe13be-2c71-48e8-9fde-72d567cc3e0c', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(52, '1b7be893-d0bd-400a-9542-c4d20ff1b2e2', '32bdaf53-578d-46b6-ab7d-94940a279515'),
(270, '1d5c4ecb-c3cb-49cb-bd25-c2a3e16e1de3', '0a73c5b2-5a21-4fbd-b63d-239041670c5f'),
(255, '1d738431-57f5-4f2d-bda3-2d1841366cc1', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(309, '1f18c969-1cb6-4c07-a1a6-9215274efc6b', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(171, '1f423215-6573-4d22-9026-ed746892552d', '43537669-9bf1-4bc4-841b-7c79c69e6be2'),
(192, '1f75b30f-8cbc-4534-9099-99014b3f2f30', '25aa6aba-b75a-410d-ae65-935dc34f57a6'),
(208, '2162e05d-a009-48fb-8576-ac28b6493bb5', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(249, '23944055-a6a0-4121-b863-ebfe8294000b', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(146, '245729a3-10b2-4f3a-ac0e-cbf7cb7e1120', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(340, '25bddba1-954c-4d06-b944-ee5d9f353185', '1883bcc5-6b6b-4dc2-b05b-5d139b915037'),
(261, '25e77435-8568-4ab0-9b89-692a9f380a94', 'f5192646-626a-4161-8eee-12be2756611a'),
(307, '27732305-6454-46e7-98e5-0f987e4d20e7', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(341, '27952243-330e-4319-932c-4c55dacb6026', '1883bcc5-6b6b-4dc2-b05b-5d139b915037'),
(248, '2c005ea6-ba2f-4667-9149-a46f032119ad', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(272, '2c0110de-87b7-48b0-ab78-901545dd8bf7', '0a73c5b2-5a21-4fbd-b63d-239041670c5f'),
(130, '2c9b655c-8cac-42e3-bc12-ec1fb8416909', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(102, '2cf65fbc-d5a3-4edd-b916-3a1f557e6bf4', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(128, '2f1bf55d-0d2f-4f2b-9083-200de2ef6464', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(96, '2f364465-a225-40e3-a8ea-e364826e5ee4', '56ca7bd0-fb0e-4db4-9145-859814194ceb'),
(279, '2fc57c24-a7a5-40f9-8c01-d81f03233644', '24146130-dbeb-4bc3-9639-3e87766d1c5c'),
(275, '30584f44-9177-4c01-a45b-d8ef125edf55', '24146130-dbeb-4bc3-9639-3e87766d1c5c'),
(202, '32e9d395-ae05-400d-9363-c96583e250b1', '797f9048-363d-4eb7-9523-e972d37a2fdc'),
(55, '337e8695-5fc1-455c-a733-16781a9329ef', '32bdaf53-578d-46b6-ab7d-94940a279515'),
(186, '33a9a3b8-32a9-4f32-99ee-43b362ce037d', '011ccdb6-d7e5-49d8-b801-abcab152c5d2'),
(259, '34f1f80c-cb84-4ea5-aadd-116abb9a4080', 'f5192646-626a-4161-8eee-12be2756611a'),
(64, '355cc644-4fea-4ef3-af3e-86284a0bc99c', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(322, '36673548-e5f2-4bc7-9679-200c4c2d8c4a', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(278, '36704578-2143-4241-a126-c1f2393aa2ca', '24146130-dbeb-4bc3-9639-3e87766d1c5c'),
(95, '386168de-8a79-4673-a80e-a48b92d4cbd2', '56ca7bd0-fb0e-4db4-9145-859814194ceb'),
(101, '3a9e73d0-0e90-4228-92b9-4e937d5aea66', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(73, '3b014dfc-e1e4-4457-834b-47c9a508a9b3', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(321, '3bcd2b12-10d8-46b2-9c31-3449f25fb5b9', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(69, '3be03526-d3d4-49b0-89d9-6110565d2ced', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(325, '3c01acc7-c756-4abf-b919-535197b62bdf', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(88, '3c592e91-a2ee-4d3c-a86a-d611f8abbc36', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(260, '3cc4a764-677e-4670-b965-6d03f0dc9c3b', 'f5192646-626a-4161-8eee-12be2756611a'),
(350, '3e6e7ad3-a123-45fd-a8dd-7192c6ff183a', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(306, '3e8aa9b1-8d73-48f7-bac3-f9a6c7d0736a', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(149, '3f26556f-b5d5-48f4-9838-5aa90d63686f', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(77, '4312ff34-ea08-4a13-890d-52b3160238f8', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(217, '434dd2aa-8acc-4907-8c4f-32048f1020ee', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(158, '44747bda-0bd5-4bc0-bdbe-2b188b34c269', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(187, '44c8cbd4-f9d8-4d9e-8b8a-592eecd4960d', '011ccdb6-d7e5-49d8-b801-abcab152c5d2'),
(349, '45b034b5-21a3-4cde-97c8-d1475b279ae7', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(285, '45d8234f-ea51-4a71-94bc-d0f6c4ca1af3', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(168, '463f28a0-6d19-4f2b-83c1-03e1b30a12d3', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(243, '46a421ac-b76b-43b2-a04a-62fc7462c2bd', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(91, '4834a053-8f81-4a70-ace4-ac202159b2f0', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(111, '487dcde2-c750-4331-94d9-8cc8b7550a66', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(280, '48c7451b-28c8-4958-b80c-b4d689a5caeb', '24146130-dbeb-4bc3-9639-3e87766d1c5c'),
(67, '48e7e426-7841-4a33-8e71-b13f2a4b40ab', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(229, '4921bab8-99bf-462f-ac00-43c69ebfd191', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(222, '497853b3-fe5c-4e3f-99f5-b14a047d07b1', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(289, '4b487af9-190c-474c-b771-a3540c608cc7', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(70, '4b584730-c58c-44db-9e46-f8856f644de0', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(57, '4c1f3d69-4bfb-4c8e-a705-3fd981f2a14d', '32bdaf53-578d-46b6-ab7d-94940a279515'),
(346, '4cb814a3-5258-49e7-85a8-524ce5e1a3b7', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(257, '4e63f814-97e1-49f6-8100-a20a10bb8e98', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(218, '4fe0577d-2377-45a5-89bb-c999be50d187', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(97, '501accb9-b691-44a0-8d88-5613401ec80f', '56ca7bd0-fb0e-4db4-9145-859814194ceb'),
(138, '511f1623-4c35-4100-b0c0-051aa11f7ade', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(238, '512b502b-7411-48fc-868f-a55069a78b0e', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(342, '5185ef88-3bc9-440a-86c0-6efb93a297af', '1883bcc5-6b6b-4dc2-b05b-5d139b915037'),
(244, '529a3f1a-1093-4e32-a482-47cd4a9dc20d', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(281, '52b9fd7d-c770-4b4f-b8c5-26a35c0b4fee', '24146130-dbeb-4bc3-9639-3e87766d1c5c'),
(235, '53f89d53-ffb1-46d5-be6a-04d84299e38e', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(188, '54abb600-0244-4fc8-8f97-ea876aacba54', '25aa6aba-b75a-410d-ae65-935dc34f57a6'),
(126, '555147b0-9dad-4c75-b066-8274c0617b09', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(172, '558c1be3-1c9f-4ec5-afcc-ee76dc7663d4', '43537669-9bf1-4bc4-841b-7c79c69e6be2'),
(66, '55d0b79a-ca07-43f0-a7cb-47c2bd54e940', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(104, '560cad6c-9da5-419e-abd7-0972eb261e30', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(76, '56406cbb-983d-4562-b0ef-c4a0dbe273a4', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(290, '56ad9ff4-e008-4116-a8e4-7d0b7531620f', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(274, '56fe3d5e-f196-4340-b2e3-6511eae59b72', '24146130-dbeb-4bc3-9639-3e87766d1c5c'),
(108, '588a1e18-2c45-42a5-b0c0-5177ff32caa2', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(294, '5919addb-b281-4eec-ac03-3c36bd64e246', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(114, '5a559609-d028-4662-8ca3-aa2ab87dd71e', '461b7616-9840-4e2f-bd53-7b679e72ed56'),
(304, '5a81ac4e-2743-4f3f-aff8-e8e9aeadff81', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(266, '5abce3e8-5414-4e63-b933-a632eb715935', 'a34d61db-3733-4444-bb20-c67a79824874'),
(225, '5ae251c0-81ba-4cee-a1d5-6dd110737bec', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(105, '5be30fdd-7dbf-42fd-8dc8-36acf7b63a87', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(308, '5d5fbf9c-9687-4e88-80dc-4bb61ec1e638', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(245, '5d9d065d-40d3-4373-9a22-368a29e80410', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(135, '5e05d8f2-ebc6-45d3-8d7b-adb27445152d', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(268, '5f648d52-f589-4f79-93d2-51528dadd4ae', '0a73c5b2-5a21-4fbd-b63d-239041670c5f'),
(300, '60a5f918-6321-4a46-8c53-8e259c97ab48', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(152, '613b49d1-31e5-4869-9519-de81384391ab', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(287, '626a26b7-827f-4875-9a07-1c92a6454ff6', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(142, '62e83de0-1552-493f-b321-ee086b517fd8', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(124, '6301b9ac-e0fc-4303-a8d5-0e26e1741be9', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(240, '638dba72-ed20-4361-8991-6efa1d337847', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(121, '6463d1db-9245-46c1-b5ea-9bd57e83cc62', 'd62a47df-7d41-496c-9f91-3ccfa012f4e7'),
(99, '64a3bbde-d767-415f-85e3-1981e4a21df3', '56ca7bd0-fb0e-4db4-9145-859814194ceb'),
(122, '64d529d7-dc59-4305-b82d-e5fe4db122f6', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(363, '64fbd1dc-47d2-417e-bee1-ab12547bd066', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(198, '657000ae-8c06-401c-afbe-85fe47402c31', '6f47bb42-e7fd-448f-a9ae-4e9e0cd15f43'),
(53, '675041ff-364e-4157-893a-4dd0d8038d6c', '32bdaf53-578d-46b6-ab7d-94940a279515'),
(137, '697cff54-4f3f-41b1-a66b-ef3b11f37969', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(90, '6b85b55a-fa2a-4e2b-91bd-97a2c1f05ee3', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(283, '700ab3f7-b97a-44fd-8fd6-efe9a40b6edc', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(311, '701df026-4710-4a57-915f-e8b23f998e96', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(129, '7026aca5-a0da-4af0-812c-35f10df81236', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(94, '70a81364-f1e4-4964-b78a-2944574140e5', '56ca7bd0-fb0e-4db4-9145-859814194ceb'),
(348, '70ad9c5c-466d-4d96-875b-2d3f7fb93e49', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(361, '70b3e16e-1574-4b92-9636-19f707108ad1', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(117, '70b767b4-4742-4d84-8a2e-ca3ca9fad880', 'd62a47df-7d41-496c-9f91-3ccfa012f4e7'),
(246, '722c8401-5c72-4014-a1a7-7313cd86b24b', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(75, '72c107df-c2e5-4f25-8da0-c6d79aec7835', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(81, '74891705-d509-4048-aae0-26a19750adb7', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(226, '756a6472-cf18-4fa8-8ff6-616e01997039', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(87, '757d37f5-d8bf-4864-a674-3c5dbf31bf11', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(319, '759bdaaa-9fa4-4229-8adc-bde308b93a6d', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(203, '78b05b93-0b87-4e74-94e9-90211f82b871', '797f9048-363d-4eb7-9523-e972d37a2fdc'),
(316, '794743ec-ebde-4959-ae5c-910f78bdb9d5', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(282, '79518315-2fde-40c4-9d20-4e33686980a7', '24146130-dbeb-4bc3-9639-3e87766d1c5c'),
(107, '79819d46-79dc-4a7a-9306-e5dcdcb5daf6', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(241, '7b5a84c6-c350-4eba-b9cf-71bd01a2fe33', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(163, '7bb7c518-b9ee-4475-85e0-3e7fdb93a95d', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(324, '7cdc28b8-cfd8-4bea-ba8d-b331087ea5a8', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(291, '7d477883-04e2-42d4-b5dc-6cd43ff181c9', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(134, '7e390a08-583d-4dcd-b74f-8ec7da3b7a0a', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(109, '7e7efcb1-aecc-41e5-bacc-e92b99946ecf', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(175, '8022d525-8fa4-4988-b138-b10478cf4b68', '43537669-9bf1-4bc4-841b-7c79c69e6be2'),
(356, '806b92fb-661f-4897-9e1f-f8775b17aa58', 'b0f82632-76f9-4527-9317-0c53de2ad253'),
(334, '807725ca-2af6-4bb6-9081-4461ad710266', '185546a2-6405-4824-9395-4f4dd841cbe7'),
(314, '80f105dd-6265-41a5-bcf4-c097622434e1', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(273, '82d75081-3716-4333-8794-da186ffa3277', '0a73c5b2-5a21-4fbd-b63d-239041670c5f'),
(61, '8576795c-7ce9-42d8-9386-9b6560ed41f1', '32bdaf53-578d-46b6-ab7d-94940a279515'),
(210, '85dfd078-8ffd-4336-8533-8ba03fcb4736', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(298, '86a5c2ee-e928-487e-9175-bfce28d14846', 'ac2ef08d-32f9-4c8d-98d9-8f46728f3478'),
(205, '872b6699-d68e-496d-adfc-c65bf2f9aad4', '797f9048-363d-4eb7-9523-e972d37a2fdc'),
(195, '87b33a57-ecff-429a-ac1c-ffefb5bf321f', '6da3740d-295f-4313-9e33-e6a7ca3c0aca'),
(92, '87e96a63-aa72-482d-ab50-f16c534617a0', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(164, '87ea704c-f2a1-46e5-83c6-5ecf8acce161', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(161, '88d534a8-ead0-4613-b5e4-8154bf1bffc6', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(150, '897dcfed-70a8-4a6c-84b6-8331356e01e1', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(365, '8a1bf756-eab0-4284-9158-ebdde958c456', '185546a2-6405-4824-9395-4f4dd841cbe7'),
(254, '8adc1737-5810-4ecc-9b07-49666b20f848', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(230, '8b090d88-78b1-4a66-bd38-b0ede150d274', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(263, '8c1159f6-93b2-4ca6-a85f-028e8e7cc2dd', 'f5192646-626a-4161-8eee-12be2756611a'),
(180, '90352979-47c1-406e-b9a7-5f68a7b9b510', '011ccdb6-d7e5-49d8-b801-abcab152c5d2'),
(100, '90c655df-aa49-4c17-8f23-2ae2fc99d83a', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(339, '90f4d752-f88f-468a-80cf-d08443356cc3', '1883bcc5-6b6b-4dc2-b05b-5d139b915037'),
(227, '9133ad60-45a4-48dc-b997-8e1a275610f3', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(212, '9161f0f3-8756-41f6-ac44-0863a173af8a', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(303, '94882228-5242-444d-a487-623cc0bae959', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(207, '94dbf0bb-76a4-434a-9426-347eed111bd1', '797f9048-363d-4eb7-9523-e972d37a2fdc'),
(206, '95ea5486-3c31-48f8-b046-7c716e945157', '797f9048-363d-4eb7-9523-e972d37a2fdc'),
(336, '95fa682e-079b-4f14-91ba-24afb8074a06', '185546a2-6405-4824-9395-4f4dd841cbe7'),
(170, '9624a7b1-3a3b-410f-9918-30115c9eacf2', '43537669-9bf1-4bc4-841b-7c79c69e6be2'),
(196, '971bdcde-069d-47f3-9255-f054ef5bda42', '6da3740d-295f-4313-9e33-e6a7ca3c0aca'),
(189, '97b6d1a7-8049-409b-9c7e-5803f5bd860a', '25aa6aba-b75a-410d-ae65-935dc34f57a6'),
(330, '97ea565c-86a9-4faf-9adb-9549636e53f8', '185546a2-6405-4824-9395-4f4dd841cbe7'),
(359, '98221445-554e-4b9a-877e-5cb5e886f96f', 'ee32f7aa-a9fb-474c-95ff-a3bee2ab58f1'),
(78, '9a04e7d1-5eb3-4c92-8d22-e538be5eba3d', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(358, '9b2f6293-03d7-45fe-a3e1-d55a1c057628', 'ee32f7aa-a9fb-474c-95ff-a3bee2ab58f1'),
(264, '9b3283cf-5d3b-43a4-a8f2-32bb58f94db3', 'a34d61db-3733-4444-bb20-c67a79824874'),
(65, '9bfc9041-2dc9-46c8-bde9-da146ccb7b3e', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(296, '9d1243e4-757d-41dc-b39d-9ea9226e17cc', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(220, '9e45b131-cd91-487b-b375-f2f835165267', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(360, '9f27fa80-e985-4131-a379-e2c38caaaee1', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(258, '9f606195-69fa-46d3-8691-abbcd2aa434e', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(253, 'a04bedf3-d518-4773-b19f-3684057bd0a5', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(299, 'a0cf46ad-3811-4853-9747-c659c31ea745', 'ac2ef08d-32f9-4c8d-98d9-8f46728f3478'),
(200, 'a0df6dce-8d31-4218-8e9b-e4a047fa45b4', 'b2e78c7f-1e51-4823-9f8b-391ac895a43f'),
(265, 'a2011265-5c27-442f-bf66-29bf09386d0c', 'a34d61db-3733-4444-bb20-c67a79824874'),
(56, 'a38de7b0-ac23-45ec-97c6-8c5261ee6208', '32bdaf53-578d-46b6-ab7d-94940a279515'),
(231, 'a518e31b-422f-4279-bbca-83e1c9836c48', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(154, 'a53ba8df-8779-4235-abcf-f582c652e84b', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(318, 'a5e89856-ff94-41e7-8a18-9aa3384e70b0', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(338, 'a6d15885-168c-4697-96b4-1fbe0fdc0289', '185546a2-6405-4824-9395-4f4dd841cbe7'),
(313, 'a70ff451-b98f-412c-8d1c-b33e5b3e556a', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(345, 'a8ad5faa-3295-484b-a321-6f41530c5ad7', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(190, 'a96c78c0-04ba-45e4-a959-b61b44545d34', '25aa6aba-b75a-410d-ae65-935dc34f57a6'),
(72, 'a9c90165-7efa-482c-9931-5644349230aa', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(271, 'aa027f11-b0d3-4ee6-99c7-5ea0e9093941', '0a73c5b2-5a21-4fbd-b63d-239041670c5f'),
(156, 'ab72e1dc-171a-48a0-8614-fa238f6d1c92', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(118, 'abfae52f-8edf-4c10-8e8e-45ed7e3f22f9', 'd62a47df-7d41-496c-9f91-3ccfa012f4e7'),
(51, 'acbb6f4a-af8e-48b0-9011-d29e78ce2076', '32bdaf53-578d-46b6-ab7d-94940a279515'),
(221, 'acd14179-e3c8-4813-a2a9-4a50f87e37fe', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(236, 'ad925899-8177-4fae-8776-0ebb349e3f05', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(68, 'ae2137e5-f0cd-4bb3-b1be-9eb6fd4b4a2b', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(166, 'b1895f43-a0f2-4961-bcf3-171f9a77b18c', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(181, 'b1b95f31-dab6-4137-8896-225e9fe94c8f', '011ccdb6-d7e5-49d8-b801-abcab152c5d2'),
(213, 'b2559834-594a-4126-a49d-014040472e06', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(132, 'b26aa493-75d6-41b3-91b8-f510faf53b46', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(215, 'b2bbbd01-8fd1-4712-a2f5-1cf7bbac610c', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(177, 'b3fad541-a47c-4170-9ec0-fc5f0dbac947', '43537669-9bf1-4bc4-841b-7c79c69e6be2'),
(93, 'b415999e-1b4d-42ee-b84b-8ec83ba431f7', '56ca7bd0-fb0e-4db4-9145-859814194ceb'),
(115, 'b563a11f-d8e8-4ae5-b1de-1d5577615fb8', '461b7616-9840-4e2f-bd53-7b679e72ed56'),
(160, 'b61d2656-be8b-4576-ae79-10defac15fca', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(233, 'b6907975-549e-41e6-8c74-eb8b83bfbe24', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(148, 'b73149af-f574-4ba8-aefe-2d46c3fa9883', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(60, 'b7e1a435-9504-4981-ac6f-b17dc06f642e', '32bdaf53-578d-46b6-ab7d-94940a279515'),
(211, 'b964e198-6d54-447c-ac15-a8e4b3cd69f7', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(323, 'ba7f3f67-4001-4656-a7ea-428ba5f793c5', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(269, 'bc647983-941c-4dff-b294-0cc051c5ece9', '0a73c5b2-5a21-4fbd-b63d-239041670c5f'),
(327, 'bcf0bc50-4c0c-4914-bb4d-6b4c11e3ef0f', '4f4b76d3-1cc6-4c3b-a9df-565352c23792'),
(83, 'bd813fd2-a84c-417e-910b-e2853815c425', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(315, 'bda1c365-82d5-4eb2-b40e-8d845437fea1', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(239, 'bdee2aad-778e-4337-9d41-777cecd28b4a', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(329, 'be703ddb-c135-4944-84f1-271d2077c3c9', '4f4b76d3-1cc6-4c3b-a9df-565352c23792'),
(82, 'c04db91f-4ba5-48e3-97ef-be8825b0ebd1', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(86, 'c0b6ff31-fdcd-4c6c-a6bd-2f4130e3abfb', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(232, 'c10b0c8a-f0e9-4a62-b0c6-d6ad3ca59854', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(209, 'c15733e2-559d-437f-b0ca-ee548c751fc1', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(123, 'c2b007ab-3452-4165-9342-92f70cca3d65', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(326, 'c33c1cb5-6b5b-479c-8ea1-34ba96c1fdf2', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(292, 'c3649491-33dc-4930-b9a3-ee69d4b9388e', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(147, 'c3a64ab3-0362-4240-9b0b-cbc3df3ec469', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(58, 'c3df47c6-f677-4a17-a4c4-2e92cf550cb7', '32bdaf53-578d-46b6-ab7d-94940a279515'),
(267, 'c43eefc3-52f5-4039-9705-65d14ebc2e43', '0a73c5b2-5a21-4fbd-b63d-239041670c5f'),
(204, 'c53ccae8-7655-40ef-a548-1eac51a1c459', '797f9048-363d-4eb7-9523-e972d37a2fdc'),
(98, 'c6386153-24b1-442f-9112-0f35036d6f6a', '56ca7bd0-fb0e-4db4-9145-859814194ceb'),
(182, 'c67358db-20cc-413f-a7d8-091d7cc2a5ef', '011ccdb6-d7e5-49d8-b801-abcab152c5d2'),
(80, 'c713b3fa-c5e1-4b19-a053-eff1c8f1fbbe', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(223, 'c716a4b2-7d2d-4baa-a209-8373d337cff4', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(305, 'c8643b61-3eee-4763-822a-ad90627655da', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(293, 'c90b68ef-3fb4-47ba-9d4e-b7101a42b100', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(320, 'cb185a19-4652-4211-a28a-74c0193ce842', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(364, 'cbfb6dd4-c75e-4fd9-bb84-b194b2554d2a', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(332, 'cc4f4e85-4194-4996-8e35-4f1e561fdd78', '185546a2-6405-4824-9395-4f4dd841cbe7'),
(84, 'cc75c324-1e1a-4d70-aed6-79c678dd4f32', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(139, 'cda8f876-f9d6-4a06-afc5-7c36a325374c', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(284, 'cddb0b0c-048c-40cf-a633-d314cd339bbf', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(174, 'd07822fc-91cb-46db-a256-aaba086259b1', '43537669-9bf1-4bc4-841b-7c79c69e6be2'),
(224, 'd11e0621-869c-4a36-a66c-6d1f7211f29b', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(216, 'd14dee56-28ef-4e96-a019-b6852f8eae75', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(343, 'd1f4019b-6124-4c62-8046-a705d408a444', '1883bcc5-6b6b-4dc2-b05b-5d139b915037'),
(234, 'd3b197c0-a440-4348-90cc-476def3f2a21', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(214, 'd4154868-ee1b-4b7c-b524-f64b25056de0', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(162, 'd4723faa-62a2-4aa7-8327-2340f196012a', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(331, 'd4b051c9-047d-4eb9-a431-44862f6f082c', '185546a2-6405-4824-9395-4f4dd841cbe7'),
(247, 'd4f2aabb-c088-4e4b-9127-c26c3ae87242', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(85, 'd67d3ed7-a9d9-49b0-b5a5-219104c3081b', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(176, 'd7b100a0-9a74-47ee-bcc0-e99a92aa7294', '43537669-9bf1-4bc4-841b-7c79c69e6be2'),
(301, 'd7b98b13-f5d0-49f6-ac6c-80695b774350', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(335, 'd8267cb3-670c-4358-937c-38e28695fd5a', '185546a2-6405-4824-9395-4f4dd841cbe7'),
(297, 'd86d346b-7e6f-44b9-a593-2ca31baf2519', 'ac2ef08d-32f9-4c8d-98d9-8f46728f3478'),
(125, 'd87b97d3-a239-40b3-bbbf-cef42b4afecd', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(120, 'd8dccf92-f1d0-435d-879d-6071f576967d', 'd62a47df-7d41-496c-9f91-3ccfa012f4e7'),
(201, 'd91740c7-8a72-4eab-834e-3c9304b03391', '797f9048-363d-4eb7-9523-e972d37a2fdc'),
(184, 'd935b961-2dcf-47b0-b780-5b2c7e462c0a', '011ccdb6-d7e5-49d8-b801-abcab152c5d2'),
(197, 'd99c9730-02d6-4b1c-9da3-cc66a22b0494', '6da3740d-295f-4313-9e33-e6a7ca3c0aca'),
(344, 'da425adb-3133-40a2-9c47-605d54760ede', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(173, 'da58ac02-6b98-435f-9f37-90e23f248831', '43537669-9bf1-4bc4-841b-7c79c69e6be2'),
(347, 'da6d495a-1d40-4ea6-9579-07b5908b0f9e', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(159, 'da8f7ebd-0c55-40d5-a151-2fef41f538cb', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(193, 'daa29e34-aec5-49a7-bbf9-de006989530f', '25aa6aba-b75a-410d-ae65-935dc34f57a6'),
(119, 'dabef0a1-c9a2-4120-bc87-ce780e007c87', 'd62a47df-7d41-496c-9f91-3ccfa012f4e7'),
(237, 'dbb21d0c-7882-4320-85a6-1fa977a0da37', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(112, 'dc526f9a-a12e-4b27-9fd4-6958071eb431', '461b7616-9840-4e2f-bd53-7b679e72ed56'),
(169, 'dcb0a823-18f2-4b7c-8047-d1716c5d005c', '43537669-9bf1-4bc4-841b-7c79c69e6be2'),
(362, 'de365fe8-ea3c-4e46-8966-5b8a1ef3e825', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(59, 'de891883-b711-4ef1-a696-dd266b27451d', '32bdaf53-578d-46b6-ab7d-94940a279515'),
(143, 'dee95d97-dd66-4ce3-bb80-4b212cc0d28e', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(312, 'e046f5b4-71f8-467f-9f82-72052af4d688', '795d9f3b-3a65-4b93-b45e-4ce2d08522bd'),
(133, 'e1cede31-fb3c-4bf5-be7e-12c2fad3117a', 'f6a97ff8-3993-4009-ad26-f6111cdaafb9'),
(136, 'e22b35f1-922c-4486-adc3-b01608ce358f', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(191, 'e2ae39b0-463e-4c27-a1b1-3df8fcae2e01', '25aa6aba-b75a-410d-ae65-935dc34f57a6'),
(252, 'e583f856-e90b-48c9-8be4-168226a5431b', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(277, 'e667791d-e423-4bc9-b45a-25ed99e1e73d', '24146130-dbeb-4bc3-9639-3e87766d1c5c'),
(63, 'e84b2d7f-6be6-437b-bcf9-821067d92992', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(251, 'e8959617-d049-42b4-91b4-4798204bfc92', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(89, 'e99d9ec1-9785-47a7-900b-c214bd5d8322', '696b6314-6a6a-4b12-b484-d175d69e7dcc'),
(103, 'eddac860-6c0d-4299-b801-226218964324', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(116, 'ee53ab77-4fbc-455e-8aa2-0c90fe3a1105', '461b7616-9840-4e2f-bd53-7b679e72ed56'),
(219, 'ef901f53-9246-4c8d-9d4f-0955028e19e7', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(351, 'f1d60a21-30a8-4ce9-bdf7-b8f0f41b503a', '4f4b76d3-1cc6-4c3b-a9df-565352c23792'),
(141, 'f2f12946-551e-497a-9567-5eef60b6fadc', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(155, 'f4e95cc5-001c-4ef9-b8fc-30109498e4b9', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(71, 'f5a29a40-9607-41eb-92fc-022279c542c4', '849d6200-46b7-46b0-93bc-52d78c2d20ae'),
(250, 'f5b761ff-e4da-4bf5-8c97-9a863e0f8b02', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(199, 'f5c3c489-4b4d-48ab-8c55-2f79cd81a4d7', 'b2e78c7f-1e51-4823-9f8b-391ac895a43f'),
(106, 'f74fcbdd-9ad6-4696-aebe-931b18938ef2', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(194, 'f79e4b81-981b-480b-b079-655319894d8d', '1d30352d-b0ea-4cbd-b26b-7e773ef64dec'),
(357, 'f8c53b44-20d1-43c9-bd7a-3aff7fc98af1', 'ee32f7aa-a9fb-474c-95ff-a3bee2ab58f1'),
(288, 'f9c1800f-90d4-4b0c-81cb-8142095918fa', '3efdc9ef-aee0-413d-9c5f-3775ebe3f8e4'),
(79, 'f9d06603-9199-41c7-8e4d-dfe7ef084a75', '13683c84-de8b-4527-bf66-8fff6d292e6c'),
(50, 'fac921fc-a496-4744-aebb-b1b469573c3f', '32bdaf53-578d-46b6-ab7d-94940a279515'),
(179, 'face3435-ff8e-4fe4-b913-9a3766ccfa3a', '011ccdb6-d7e5-49d8-b801-abcab152c5d2'),
(144, 'fb843e13-07af-4b90-a037-a8173aff154e', 'b1cfe27a-6806-4ae6-be93-d452b65fb503'),
(352, 'fbfc8b3c-94df-4569-aea5-028abb224052', '2d5942ee-8287-4026-9b32-34f0ac87c49c'),
(242, 'fc23c292-ba42-4709-9f18-141f988ef93a', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(256, 'fc4643cc-8440-498f-9d52-085148a60679', 'c3625231-7f5c-4991-960e-7bcc68da0724'),
(165, 'fcfe1f4d-af25-4fcf-842d-3133766eb261', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(276, 'fd84f326-88b5-4820-a565-582153a252d5', '24146130-dbeb-4bc3-9639-3e87766d1c5c'),
(157, 'fe89d47a-edcf-4b31-932d-4c72691356f4', 'ae8e077a-d865-44a8-9673-41082ee250ca'),
(113, 'fe9931ad-4c89-4c3a-96e0-10213a5f283b', '461b7616-9840-4e2f-bd53-7b679e72ed56'),
(337, 'ff420c59-94a3-4c11-aaad-9584dfd98231', '185546a2-6405-4824-9395-4f4dd841cbe7'),
(110, 'ffec6d9c-3b7b-4fe1-a77f-2248eb5c42d9', '13683c84-de8b-4527-bf66-8fff6d292e6c');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_orders`
--

CREATE TABLE `oc_trade_orders` (
  `operation_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `order_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oc_trade_orders`
--

INSERT INTO `oc_trade_orders` (`operation_id`, `order_id`, `order_data`, `response`) VALUES
(1, 44, '{\"orders\":[{\"contractor\":{\"phone\":\"null\",\"email\":\"demonized@4ait.ru\",\"name\":\"\\u0442\\u0435\\u0441\\u0442\",\"address\":\"null\"},\"date\":\"2021-01-20T21:05:37+00:00\",\"description\":\"\\u0410\\u043f\\u0442\\u0435\\u043a\\u0430 - \\u041f\\u0430\\u043a\\u0435\\u0442 \\u21161\",\"goods\":[{\"nomenclature_uuid\":\"fbfc8b3c-94df-4569-aea5-028abb224052\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":175.75,\"discount\":0,\"total\":175.75},{\"nomenclature_uuid\":\"4312ff34-ea08-4a13-890d-52b3160238f8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1150,\"discount\":0,\"total\":1150},{\"nomenclature_uuid\":\"fac921fc-a496-4744-aebb-b1b469573c3f\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":3410.5,\"discount\":0,\"total\":6821},{\"nomenclature_uuid\":\"09fbb05c-60d5-4fd3-92d6-e1ccf60ca73d\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":8000,\"discount\":0,\"total\":8000},{\"nomenclature_uuid\":\"166120c9-86e1-4447-b9b1-3cccc40dd4fa\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":6754.5,\"discount\":0,\"total\":13509},{\"nomenclature_uuid\":\"d86d346b-7e6f-44b9-a593-2ca31baf2519\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":26500,\"discount\":0,\"total\":26500},{\"nomenclature_uuid\":\"f8c53b44-20d1-43c9-bd7a-3aff7fc98af1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":7500,\"discount\":0,\"total\":7500},{\"nomenclature_uuid\":\"700ab3f7-b97a-44fd-8fd6-efe9a40b6edc\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":1966.5,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"c713b3fa-c5e1-4b19-a053-eff1c8f1fbbe\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":788.5,\"discount\":0,\"total\":1577},{\"nomenclature_uuid\":\"4cb814a3-5258-49e7-85a8-524ce5e1a3b7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":3135,\"discount\":0,\"total\":3135},{\"nomenclature_uuid\":\"b563a11f-d8e8-4ae5-b1de-1d5577615fb8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1605.5,\"discount\":0,\"total\":1605.5},{\"nomenclature_uuid\":\"dc526f9a-a12e-4b27-9fd4-6958071eb431\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":3,\"price\":1311,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"7e390a08-583d-4dcd-b74f-8ec7da3b7a0a\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1100,\"discount\":0,\"total\":1100},{\"nomenclature_uuid\":\"e8959617-d049-42b4-91b4-4798204bfc92\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":598.5,\"discount\":0,\"total\":598.5},{\"nomenclature_uuid\":\"ef901f53-9246-4c8d-9d4f-0955028e19e7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":4,\"price\":418,\"discount\":0,\"total\":1672},{\"nomenclature_uuid\":\"9f27fa80-e985-4131-a379-e2c38caaaee1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1226,\"discount\":0,\"total\":1226}]}]}', '{\"uuids\":[42]}'),
(2, 45, '{\"orders\":[{\"contractor\":{\"phone\":\"null\",\"email\":\"demonized@4ait.ru\",\"name\":\"\\u0442\\u0435\\u0441\\u0442\",\"address\":\"null\"},\"date\":\"2021-01-20T21:06:10+00:00\",\"description\":\"\\u041e\\u0444\\u0438\\u0441 - \\u041f\\u0430\\u043a\\u0435\\u0442 \\u21161\",\"goods\":[{\"nomenclature_uuid\":\"fbfc8b3c-94df-4569-aea5-028abb224052\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":175.75,\"discount\":0,\"total\":175.75},{\"nomenclature_uuid\":\"4312ff34-ea08-4a13-890d-52b3160238f8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1150,\"discount\":0,\"total\":1150},{\"nomenclature_uuid\":\"fac921fc-a496-4744-aebb-b1b469573c3f\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":3410.5,\"discount\":0,\"total\":6821},{\"nomenclature_uuid\":\"09fbb05c-60d5-4fd3-92d6-e1ccf60ca73d\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":8000,\"discount\":0,\"total\":8000},{\"nomenclature_uuid\":\"166120c9-86e1-4447-b9b1-3cccc40dd4fa\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":6754.5,\"discount\":0,\"total\":13509},{\"nomenclature_uuid\":\"d86d346b-7e6f-44b9-a593-2ca31baf2519\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":26500,\"discount\":0,\"total\":26500},{\"nomenclature_uuid\":\"f8c53b44-20d1-43c9-bd7a-3aff7fc98af1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":7500,\"discount\":0,\"total\":7500},{\"nomenclature_uuid\":\"700ab3f7-b97a-44fd-8fd6-efe9a40b6edc\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":1966.5,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"c713b3fa-c5e1-4b19-a053-eff1c8f1fbbe\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":788.5,\"discount\":0,\"total\":1577},{\"nomenclature_uuid\":\"4cb814a3-5258-49e7-85a8-524ce5e1a3b7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":3135,\"discount\":0,\"total\":3135},{\"nomenclature_uuid\":\"b563a11f-d8e8-4ae5-b1de-1d5577615fb8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1605.5,\"discount\":0,\"total\":1605.5},{\"nomenclature_uuid\":\"dc526f9a-a12e-4b27-9fd4-6958071eb431\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":3,\"price\":1311,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"7e390a08-583d-4dcd-b74f-8ec7da3b7a0a\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1100,\"discount\":0,\"total\":1100},{\"nomenclature_uuid\":\"e8959617-d049-42b4-91b4-4798204bfc92\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":598.5,\"discount\":0,\"total\":598.5},{\"nomenclature_uuid\":\"ef901f53-9246-4c8d-9d4f-0955028e19e7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":4,\"price\":418,\"discount\":0,\"total\":1672},{\"nomenclature_uuid\":\"9f27fa80-e985-4131-a379-e2c38caaaee1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1226,\"discount\":0,\"total\":1226}]}]}', '{\"uuids\":[43]}'),
(3, 46, '{\"orders\":[{\"contractor\":{\"phone\":\"null\",\"email\":\"khui@ss.com\",\"name\":\"\\u0445\\u0435\\u0440\",\"address\":\"null\"},\"date\":\"2021-01-20T21:08:17+00:00\",\"description\":\"\\u041a\\u0430\\u0444\\u0435, \\u0440\\u0435\\u0441\\u0442\\u043e\\u0440\\u0430\\u043d - \\u041f\\u0430\\u043a\\u0435\\u0442 \\u21163\",\"goods\":[{\"nomenclature_uuid\":\"fbfc8b3c-94df-4569-aea5-028abb224052\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":175.75,\"discount\":0,\"total\":175.75},{\"nomenclature_uuid\":\"4312ff34-ea08-4a13-890d-52b3160238f8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":1150,\"discount\":0,\"total\":2300},{\"nomenclature_uuid\":\"09fbb05c-60d5-4fd3-92d6-e1ccf60ca73d\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":8000,\"discount\":0,\"total\":8000},{\"nomenclature_uuid\":\"f8c53b44-20d1-43c9-bd7a-3aff7fc98af1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":7500,\"discount\":0,\"total\":7500},{\"nomenclature_uuid\":\"166120c9-86e1-4447-b9b1-3cccc40dd4fa\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":6754.5,\"discount\":0,\"total\":13509},{\"nomenclature_uuid\":\"d86d346b-7e6f-44b9-a593-2ca31baf2519\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":26500,\"discount\":0,\"total\":26500},{\"nomenclature_uuid\":\"27732305-6454-46e7-98e5-0f987e4d20e7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":6083.8,\"discount\":0,\"total\":6083.8},{\"nomenclature_uuid\":\"c04db91f-4ba5-48e3-97ef-be8825b0ebd1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":950,\"discount\":0,\"total\":1900},{\"nomenclature_uuid\":\"dc526f9a-a12e-4b27-9fd4-6958071eb431\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":3,\"price\":1311,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"b563a11f-d8e8-4ae5-b1de-1d5577615fb8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1605.5,\"discount\":0,\"total\":1605.5},{\"nomenclature_uuid\":\"697cff54-4f3f-41b1-a66b-ef3b11f37969\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1400,\"discount\":0,\"total\":1400},{\"nomenclature_uuid\":\"ef901f53-9246-4c8d-9d4f-0955028e19e7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":3,\"price\":418,\"discount\":0,\"total\":1254},{\"nomenclature_uuid\":\"e8959617-d049-42b4-91b4-4798204bfc92\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":598.5,\"discount\":0,\"total\":598.5},{\"nomenclature_uuid\":\"45d8234f-ea51-4a71-94bc-d0f6c4ca1af3\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":2593.5,\"discount\":0,\"total\":5187},{\"nomenclature_uuid\":\"fcfe1f4d-af25-4fcf-842d-3133766eb261\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1605.5,\"discount\":0,\"total\":1605.5},{\"nomenclature_uuid\":\"1b7be893-d0bd-400a-9542-c4d20ff1b2e2\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":4427,\"discount\":0,\"total\":8854}]}]}', '{\"uuids\":[44]}'),
(4, 47, '{\"orders\":[{\"contractor\":{\"phone\":\"null\",\"email\":\"khui@ss.com\",\"name\":\"\\u0445\\u0435\\u0440\",\"address\":\"null\"},\"date\":\"2021-01-20T21:10:45+00:00\",\"description\":\"\\u0410\\u043f\\u0442\\u0435\\u043a\\u0430 - \\u041f\\u0430\\u043a\\u0435\\u0442 \\u21161\",\"goods\":[{\"nomenclature_uuid\":\"fbfc8b3c-94df-4569-aea5-028abb224052\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":175.75,\"discount\":0,\"total\":175.75},{\"nomenclature_uuid\":\"4312ff34-ea08-4a13-890d-52b3160238f8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1150,\"discount\":0,\"total\":1150},{\"nomenclature_uuid\":\"fac921fc-a496-4744-aebb-b1b469573c3f\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":3410.5,\"discount\":0,\"total\":6821},{\"nomenclature_uuid\":\"09fbb05c-60d5-4fd3-92d6-e1ccf60ca73d\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":8000,\"discount\":0,\"total\":8000},{\"nomenclature_uuid\":\"166120c9-86e1-4447-b9b1-3cccc40dd4fa\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":6754.5,\"discount\":0,\"total\":13509},{\"nomenclature_uuid\":\"d86d346b-7e6f-44b9-a593-2ca31baf2519\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":26500,\"discount\":0,\"total\":26500},{\"nomenclature_uuid\":\"f8c53b44-20d1-43c9-bd7a-3aff7fc98af1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":7500,\"discount\":0,\"total\":7500},{\"nomenclature_uuid\":\"700ab3f7-b97a-44fd-8fd6-efe9a40b6edc\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":1966.5,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"c713b3fa-c5e1-4b19-a053-eff1c8f1fbbe\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":788.5,\"discount\":0,\"total\":1577},{\"nomenclature_uuid\":\"4cb814a3-5258-49e7-85a8-524ce5e1a3b7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":3135,\"discount\":0,\"total\":3135},{\"nomenclature_uuid\":\"b563a11f-d8e8-4ae5-b1de-1d5577615fb8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1605.5,\"discount\":0,\"total\":1605.5},{\"nomenclature_uuid\":\"dc526f9a-a12e-4b27-9fd4-6958071eb431\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":3,\"price\":1311,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"7e390a08-583d-4dcd-b74f-8ec7da3b7a0a\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1100,\"discount\":0,\"total\":1100},{\"nomenclature_uuid\":\"e8959617-d049-42b4-91b4-4798204bfc92\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":598.5,\"discount\":0,\"total\":598.5},{\"nomenclature_uuid\":\"ef901f53-9246-4c8d-9d4f-0955028e19e7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":4,\"price\":418,\"discount\":0,\"total\":1672},{\"nomenclature_uuid\":\"9f27fa80-e985-4131-a379-e2c38caaaee1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1226,\"discount\":0,\"total\":1226}]}]}', '{\"uuids\":[45]}'),
(5, 48, '{\"orders\":[{\"contractor\":{\"phone\":\"null\",\"email\":\"khui@ss.com\",\"name\":\"\\u0442\\u0435\\u0441\\u0442\",\"address\":\"null\"},\"date\":\"2021-01-20T21:12:40+00:00\",\"description\":\"\\u0410\\u043f\\u0442\\u0435\\u043a\\u0430 - \\u041f\\u0430\\u043a\\u0435\\u0442 \\u21161\",\"goods\":[{\"nomenclature_uuid\":\"fbfc8b3c-94df-4569-aea5-028abb224052\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":175.75,\"discount\":0,\"total\":175.75},{\"nomenclature_uuid\":\"4312ff34-ea08-4a13-890d-52b3160238f8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1150,\"discount\":0,\"total\":1150},{\"nomenclature_uuid\":\"fac921fc-a496-4744-aebb-b1b469573c3f\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":3410.5,\"discount\":0,\"total\":6821},{\"nomenclature_uuid\":\"09fbb05c-60d5-4fd3-92d6-e1ccf60ca73d\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":8000,\"discount\":0,\"total\":8000},{\"nomenclature_uuid\":\"166120c9-86e1-4447-b9b1-3cccc40dd4fa\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":6754.5,\"discount\":0,\"total\":13509},{\"nomenclature_uuid\":\"d86d346b-7e6f-44b9-a593-2ca31baf2519\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":26500,\"discount\":0,\"total\":26500},{\"nomenclature_uuid\":\"f8c53b44-20d1-43c9-bd7a-3aff7fc98af1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":7500,\"discount\":0,\"total\":7500},{\"nomenclature_uuid\":\"700ab3f7-b97a-44fd-8fd6-efe9a40b6edc\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":1966.5,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"c713b3fa-c5e1-4b19-a053-eff1c8f1fbbe\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":788.5,\"discount\":0,\"total\":1577},{\"nomenclature_uuid\":\"4cb814a3-5258-49e7-85a8-524ce5e1a3b7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":3135,\"discount\":0,\"total\":3135},{\"nomenclature_uuid\":\"b563a11f-d8e8-4ae5-b1de-1d5577615fb8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1605.5,\"discount\":0,\"total\":1605.5},{\"nomenclature_uuid\":\"dc526f9a-a12e-4b27-9fd4-6958071eb431\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":3,\"price\":1311,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"7e390a08-583d-4dcd-b74f-8ec7da3b7a0a\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1100,\"discount\":0,\"total\":1100},{\"nomenclature_uuid\":\"e8959617-d049-42b4-91b4-4798204bfc92\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":598.5,\"discount\":0,\"total\":598.5},{\"nomenclature_uuid\":\"ef901f53-9246-4c8d-9d4f-0955028e19e7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":4,\"price\":418,\"discount\":0,\"total\":1672},{\"nomenclature_uuid\":\"9f27fa80-e985-4131-a379-e2c38caaaee1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1226,\"discount\":0,\"total\":1226}]}]}', '{\"uuids\":[46]}'),
(6, 49, '{\"orders\":[{\"contractor\":{\"phone\":\"null\",\"email\":\"khui@ss.com\",\"name\":\"\\u0442\\u0435\\u0441\\u0442\",\"address\":\"null\"},\"date\":\"2021-01-20T21:14:11+00:00\",\"description\":\"\\u0410\\u043f\\u0442\\u0435\\u043a\\u0430 - \\u041f\\u0430\\u043a\\u0435\\u0442 \\u21161\",\"goods\":[{\"nomenclature_uuid\":\"fbfc8b3c-94df-4569-aea5-028abb224052\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":175.75,\"discount\":0,\"total\":175.75},{\"nomenclature_uuid\":\"4312ff34-ea08-4a13-890d-52b3160238f8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1150,\"discount\":0,\"total\":1150},{\"nomenclature_uuid\":\"fac921fc-a496-4744-aebb-b1b469573c3f\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":3410.5,\"discount\":0,\"total\":6821},{\"nomenclature_uuid\":\"09fbb05c-60d5-4fd3-92d6-e1ccf60ca73d\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":8000,\"discount\":0,\"total\":8000},{\"nomenclature_uuid\":\"166120c9-86e1-4447-b9b1-3cccc40dd4fa\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":6754.5,\"discount\":0,\"total\":13509},{\"nomenclature_uuid\":\"d86d346b-7e6f-44b9-a593-2ca31baf2519\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":26500,\"discount\":0,\"total\":26500},{\"nomenclature_uuid\":\"f8c53b44-20d1-43c9-bd7a-3aff7fc98af1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":7500,\"discount\":0,\"total\":7500},{\"nomenclature_uuid\":\"700ab3f7-b97a-44fd-8fd6-efe9a40b6edc\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":1966.5,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"c713b3fa-c5e1-4b19-a053-eff1c8f1fbbe\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":788.5,\"discount\":0,\"total\":1577},{\"nomenclature_uuid\":\"4cb814a3-5258-49e7-85a8-524ce5e1a3b7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":3135,\"discount\":0,\"total\":3135},{\"nomenclature_uuid\":\"b563a11f-d8e8-4ae5-b1de-1d5577615fb8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1605.5,\"discount\":0,\"total\":1605.5},{\"nomenclature_uuid\":\"dc526f9a-a12e-4b27-9fd4-6958071eb431\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":3,\"price\":1311,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"7e390a08-583d-4dcd-b74f-8ec7da3b7a0a\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1100,\"discount\":0,\"total\":1100},{\"nomenclature_uuid\":\"e8959617-d049-42b4-91b4-4798204bfc92\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":598.5,\"discount\":0,\"total\":598.5},{\"nomenclature_uuid\":\"ef901f53-9246-4c8d-9d4f-0955028e19e7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":4,\"price\":418,\"discount\":0,\"total\":1672},{\"nomenclature_uuid\":\"9f27fa80-e985-4131-a379-e2c38caaaee1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1226,\"discount\":0,\"total\":1226}]}]}', '{\"uuids\":[47]}'),
(7, 50, '{\"orders\":[{\"contractor\":{\"phone\":\"null\",\"email\":\"khui@ss.com\",\"name\":\"432535235235b32\",\"address\":\"null\"},\"date\":\"2021-01-20T21:16:47+00:00\",\"description\":\"\\u0410\\u043f\\u0442\\u0435\\u043a\\u0430 - \\u041f\\u0430\\u043a\\u0435\\u0442 \\u21161\",\"goods\":[{\"nomenclature_uuid\":\"fbfc8b3c-94df-4569-aea5-028abb224052\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":175.75,\"discount\":0,\"total\":175.75},{\"nomenclature_uuid\":\"4312ff34-ea08-4a13-890d-52b3160238f8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1150,\"discount\":0,\"total\":1150},{\"nomenclature_uuid\":\"fac921fc-a496-4744-aebb-b1b469573c3f\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":3410.5,\"discount\":0,\"total\":6821},{\"nomenclature_uuid\":\"09fbb05c-60d5-4fd3-92d6-e1ccf60ca73d\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":8000,\"discount\":0,\"total\":8000},{\"nomenclature_uuid\":\"166120c9-86e1-4447-b9b1-3cccc40dd4fa\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":6754.5,\"discount\":0,\"total\":13509},{\"nomenclature_uuid\":\"d86d346b-7e6f-44b9-a593-2ca31baf2519\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":26500,\"discount\":0,\"total\":26500},{\"nomenclature_uuid\":\"f8c53b44-20d1-43c9-bd7a-3aff7fc98af1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":7500,\"discount\":0,\"total\":7500},{\"nomenclature_uuid\":\"700ab3f7-b97a-44fd-8fd6-efe9a40b6edc\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":1966.5,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"c713b3fa-c5e1-4b19-a053-eff1c8f1fbbe\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":788.5,\"discount\":0,\"total\":1577},{\"nomenclature_uuid\":\"4cb814a3-5258-49e7-85a8-524ce5e1a3b7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":3135,\"discount\":0,\"total\":3135},{\"nomenclature_uuid\":\"b563a11f-d8e8-4ae5-b1de-1d5577615fb8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1605.5,\"discount\":0,\"total\":1605.5},{\"nomenclature_uuid\":\"dc526f9a-a12e-4b27-9fd4-6958071eb431\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":3,\"price\":1311,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"7e390a08-583d-4dcd-b74f-8ec7da3b7a0a\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1100,\"discount\":0,\"total\":1100},{\"nomenclature_uuid\":\"e8959617-d049-42b4-91b4-4798204bfc92\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":598.5,\"discount\":0,\"total\":598.5},{\"nomenclature_uuid\":\"ef901f53-9246-4c8d-9d4f-0955028e19e7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":4,\"price\":418,\"discount\":0,\"total\":1672},{\"nomenclature_uuid\":\"9f27fa80-e985-4131-a379-e2c38caaaee1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1226,\"discount\":0,\"total\":1226}]}]}', '{\"uuids\":[48]}'),
(8, 51, '{\"orders\":[{\"contractor\":{\"phone\":\"null\",\"email\":\"fdsf\",\"name\":\"\\u0442\\u0435\\u0441\\u0442232\",\"address\":\"null\"},\"date\":\"2021-01-20T21:17:35+00:00\",\"description\":\"\\u0410\\u043f\\u0442\\u0435\\u043a\\u0430 - \\u041f\\u0430\\u043a\\u0435\\u0442 \\u21161\",\"goods\":[{\"nomenclature_uuid\":\"fbfc8b3c-94df-4569-aea5-028abb224052\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":175.75,\"discount\":0,\"total\":175.75},{\"nomenclature_uuid\":\"4312ff34-ea08-4a13-890d-52b3160238f8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1150,\"discount\":0,\"total\":1150},{\"nomenclature_uuid\":\"fac921fc-a496-4744-aebb-b1b469573c3f\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":3410.5,\"discount\":0,\"total\":6821},{\"nomenclature_uuid\":\"09fbb05c-60d5-4fd3-92d6-e1ccf60ca73d\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":8000,\"discount\":0,\"total\":8000},{\"nomenclature_uuid\":\"166120c9-86e1-4447-b9b1-3cccc40dd4fa\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":6754.5,\"discount\":0,\"total\":13509},{\"nomenclature_uuid\":\"d86d346b-7e6f-44b9-a593-2ca31baf2519\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":26500,\"discount\":0,\"total\":26500},{\"nomenclature_uuid\":\"f8c53b44-20d1-43c9-bd7a-3aff7fc98af1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":7500,\"discount\":0,\"total\":7500},{\"nomenclature_uuid\":\"700ab3f7-b97a-44fd-8fd6-efe9a40b6edc\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":1966.5,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"c713b3fa-c5e1-4b19-a053-eff1c8f1fbbe\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":788.5,\"discount\":0,\"total\":1577},{\"nomenclature_uuid\":\"4cb814a3-5258-49e7-85a8-524ce5e1a3b7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":3135,\"discount\":0,\"total\":3135},{\"nomenclature_uuid\":\"b563a11f-d8e8-4ae5-b1de-1d5577615fb8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1605.5,\"discount\":0,\"total\":1605.5},{\"nomenclature_uuid\":\"dc526f9a-a12e-4b27-9fd4-6958071eb431\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":3,\"price\":1311,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"7e390a08-583d-4dcd-b74f-8ec7da3b7a0a\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1100,\"discount\":0,\"total\":1100},{\"nomenclature_uuid\":\"e8959617-d049-42b4-91b4-4798204bfc92\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":598.5,\"discount\":0,\"total\":598.5},{\"nomenclature_uuid\":\"ef901f53-9246-4c8d-9d4f-0955028e19e7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":4,\"price\":418,\"discount\":0,\"total\":1672},{\"nomenclature_uuid\":\"9f27fa80-e985-4131-a379-e2c38caaaee1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1226,\"discount\":0,\"total\":1226}]}]}', '{\"uuids\":[49]}'),
(9, 52, '{\"orders\":[{\"contractor\":{\"phone\":\"null\",\"email\":\"khui@ss.com\",\"name\":\"\\u0445\\u0435\\u0440\",\"address\":\"null\"},\"date\":\"2021-01-20T21:19:53+00:00\",\"description\":\"\\u0427\\u0430\\u0441\\u0442\\u043d\\u044b\\u0439 \\u0434\\u043e\\u043c - \\u041f\\u0430\\u043a\\u0435\\u0442 \\u21161\",\"goods\":[{\"nomenclature_uuid\":\"fbfc8b3c-94df-4569-aea5-028abb224052\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":175.75,\"discount\":0,\"total\":175.75},{\"nomenclature_uuid\":\"4312ff34-ea08-4a13-890d-52b3160238f8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1150,\"discount\":0,\"total\":1150},{\"nomenclature_uuid\":\"fac921fc-a496-4744-aebb-b1b469573c3f\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":3410.5,\"discount\":0,\"total\":6821},{\"nomenclature_uuid\":\"09fbb05c-60d5-4fd3-92d6-e1ccf60ca73d\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":8000,\"discount\":0,\"total\":8000},{\"nomenclature_uuid\":\"166120c9-86e1-4447-b9b1-3cccc40dd4fa\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":6754.5,\"discount\":0,\"total\":13509},{\"nomenclature_uuid\":\"d86d346b-7e6f-44b9-a593-2ca31baf2519\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":26500,\"discount\":0,\"total\":26500},{\"nomenclature_uuid\":\"f8c53b44-20d1-43c9-bd7a-3aff7fc98af1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":7500,\"discount\":0,\"total\":7500},{\"nomenclature_uuid\":\"700ab3f7-b97a-44fd-8fd6-efe9a40b6edc\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":1966.5,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"c713b3fa-c5e1-4b19-a053-eff1c8f1fbbe\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":788.5,\"discount\":0,\"total\":1577},{\"nomenclature_uuid\":\"4cb814a3-5258-49e7-85a8-524ce5e1a3b7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":3135,\"discount\":0,\"total\":3135},{\"nomenclature_uuid\":\"b563a11f-d8e8-4ae5-b1de-1d5577615fb8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1605.5,\"discount\":0,\"total\":1605.5},{\"nomenclature_uuid\":\"dc526f9a-a12e-4b27-9fd4-6958071eb431\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":3,\"price\":1311,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"7e390a08-583d-4dcd-b74f-8ec7da3b7a0a\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1100,\"discount\":0,\"total\":1100},{\"nomenclature_uuid\":\"e8959617-d049-42b4-91b4-4798204bfc92\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":598.5,\"discount\":0,\"total\":598.5},{\"nomenclature_uuid\":\"ef901f53-9246-4c8d-9d4f-0955028e19e7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":4,\"price\":418,\"discount\":0,\"total\":1672},{\"nomenclature_uuid\":\"9f27fa80-e985-4131-a379-e2c38caaaee1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1226,\"discount\":0,\"total\":1226}]}]}', '{\"uuids\":[50]}'),
(10, 53, '{\"orders\":[{\"contractor\":{\"phone\":\"null\",\"email\":\"demonized@4ait.ru\",\"name\":\"\\u0442\\u0435\\u0441\\u0442\",\"address\":\"null\"},\"date\":\"2021-01-20T21:21:34+00:00\",\"description\":\"\\u0410\\u043f\\u0442\\u0435\\u043a\\u0430 - \\u041f\\u0430\\u043a\\u0435\\u0442 \\u21161\",\"goods\":[{\"nomenclature_uuid\":\"fbfc8b3c-94df-4569-aea5-028abb224052\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":175.75,\"discount\":0,\"total\":175.75},{\"nomenclature_uuid\":\"4312ff34-ea08-4a13-890d-52b3160238f8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1150,\"discount\":0,\"total\":1150},{\"nomenclature_uuid\":\"fac921fc-a496-4744-aebb-b1b469573c3f\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":3410.5,\"discount\":0,\"total\":6821},{\"nomenclature_uuid\":\"09fbb05c-60d5-4fd3-92d6-e1ccf60ca73d\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":8000,\"discount\":0,\"total\":8000},{\"nomenclature_uuid\":\"166120c9-86e1-4447-b9b1-3cccc40dd4fa\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":6754.5,\"discount\":0,\"total\":13509},{\"nomenclature_uuid\":\"d86d346b-7e6f-44b9-a593-2ca31baf2519\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":26500,\"discount\":0,\"total\":26500},{\"nomenclature_uuid\":\"f8c53b44-20d1-43c9-bd7a-3aff7fc98af1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":7500,\"discount\":0,\"total\":7500},{\"nomenclature_uuid\":\"700ab3f7-b97a-44fd-8fd6-efe9a40b6edc\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":1966.5,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"c713b3fa-c5e1-4b19-a053-eff1c8f1fbbe\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":788.5,\"discount\":0,\"total\":1577},{\"nomenclature_uuid\":\"4cb814a3-5258-49e7-85a8-524ce5e1a3b7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":3135,\"discount\":0,\"total\":3135},{\"nomenclature_uuid\":\"b563a11f-d8e8-4ae5-b1de-1d5577615fb8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1605.5,\"discount\":0,\"total\":1605.5},{\"nomenclature_uuid\":\"dc526f9a-a12e-4b27-9fd4-6958071eb431\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":3,\"price\":1311,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"7e390a08-583d-4dcd-b74f-8ec7da3b7a0a\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1100,\"discount\":0,\"total\":1100},{\"nomenclature_uuid\":\"e8959617-d049-42b4-91b4-4798204bfc92\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":598.5,\"discount\":0,\"total\":598.5},{\"nomenclature_uuid\":\"ef901f53-9246-4c8d-9d4f-0955028e19e7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":4,\"price\":418,\"discount\":0,\"total\":1672},{\"nomenclature_uuid\":\"9f27fa80-e985-4131-a379-e2c38caaaee1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1226,\"discount\":0,\"total\":1226}]}]}', '{\"uuids\":[51]}'),
(11, 54, '{\"orders\":[{\"contractor\":{\"phone\":\"null\",\"email\":\"demonized@4ait.ru\",\"name\":\"\\u0442\\u0435\\u0441\\u0442\",\"address\":\"null\"},\"date\":\"2021-01-20T21:23:08+00:00\",\"description\":\"\\u0410\\u043f\\u0442\\u0435\\u043a\\u0430 - \\u041f\\u0430\\u043a\\u0435\\u0442 \\u21161\",\"goods\":[{\"nomenclature_uuid\":\"fbfc8b3c-94df-4569-aea5-028abb224052\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":175.75,\"discount\":0,\"total\":175.75},{\"nomenclature_uuid\":\"4312ff34-ea08-4a13-890d-52b3160238f8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1150,\"discount\":0,\"total\":1150},{\"nomenclature_uuid\":\"fac921fc-a496-4744-aebb-b1b469573c3f\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":3410.5,\"discount\":0,\"total\":6821},{\"nomenclature_uuid\":\"09fbb05c-60d5-4fd3-92d6-e1ccf60ca73d\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":8000,\"discount\":0,\"total\":8000},{\"nomenclature_uuid\":\"166120c9-86e1-4447-b9b1-3cccc40dd4fa\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":6754.5,\"discount\":0,\"total\":13509},{\"nomenclature_uuid\":\"d86d346b-7e6f-44b9-a593-2ca31baf2519\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":26500,\"discount\":0,\"total\":26500},{\"nomenclature_uuid\":\"f8c53b44-20d1-43c9-bd7a-3aff7fc98af1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":7500,\"discount\":0,\"total\":7500},{\"nomenclature_uuid\":\"700ab3f7-b97a-44fd-8fd6-efe9a40b6edc\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":1966.5,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"c713b3fa-c5e1-4b19-a053-eff1c8f1fbbe\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":2,\"price\":788.5,\"discount\":0,\"total\":1577},{\"nomenclature_uuid\":\"4cb814a3-5258-49e7-85a8-524ce5e1a3b7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":3135,\"discount\":0,\"total\":3135},{\"nomenclature_uuid\":\"b563a11f-d8e8-4ae5-b1de-1d5577615fb8\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1605.5,\"discount\":0,\"total\":1605.5},{\"nomenclature_uuid\":\"dc526f9a-a12e-4b27-9fd4-6958071eb431\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":3,\"price\":1311,\"discount\":0,\"total\":3933},{\"nomenclature_uuid\":\"7e390a08-583d-4dcd-b74f-8ec7da3b7a0a\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1100,\"discount\":0,\"total\":1100},{\"nomenclature_uuid\":\"e8959617-d049-42b4-91b4-4798204bfc92\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":598.5,\"discount\":0,\"total\":598.5},{\"nomenclature_uuid\":\"ef901f53-9246-4c8d-9d4f-0955028e19e7\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":4,\"price\":418,\"discount\":0,\"total\":1672},{\"nomenclature_uuid\":\"9f27fa80-e985-4131-a379-e2c38caaaee1\",\"characteristic_uuid\":null,\"shipment_uuid\":null,\"quantity\":1,\"price\":1226,\"discount\":0,\"total\":1226}]}]}', '{\"uuids\":[52]}');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_translation`
--

CREATE TABLE `oc_translation` (
  `translation_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_upload`
--

CREATE TABLE `oc_upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_user`
--

CREATE TABLE `oc_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_user`
--

INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'admin', 'e4100f855779bbffe220c436baa64a7c854c7e66', 'ci41DFV87', 'John', 'Doe', 'demonized@4ait.ru', '', '', '127.0.0.1', 1, '2020-03-10 16:35:43');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_user_group`
--

CREATE TABLE `oc_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Администратор', '{\"access\":[\"blog\\/article\",\"blog\\/category\",\"blog\\/review\",\"blog\\/setting\",\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/security\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"dashboard\\/manager\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/feed\\/unisender\",\"extension\\/feed\\/yandex_market\",\"extension\\/feed\\/yandex_turbo\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/blog_category\",\"extension\\/module\\/blog_featured\",\"extension\\/module\\/blog_latest\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/featured_article\",\"extension\\/module\\/featured_product\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/manager\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/trade_import\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/ocstore_w1\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/webmoney_wmb\",\"extension\\/payment\\/webmoney_wme\",\"extension\\/payment\\/webmoney_wmk\",\"extension\\/payment\\/webmoney_wmr\",\"extension\\/payment\\/webmoney_wmu\",\"extension\\/payment\\/webmoney_wmv\",\"extension\\/payment\\/webmoney_wmz\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"marketplace\\/opencartforum\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"search\\/search\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"],\"modify\":[\"blog\\/article\",\"blog\\/category\",\"blog\\/review\",\"blog\\/setting\",\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/security\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"dashboard\\/manager\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/feed\\/unisender\",\"extension\\/feed\\/yandex_market\",\"extension\\/feed\\/yandex_turbo\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/blog_category\",\"extension\\/module\\/blog_featured\",\"extension\\/module\\/blog_latest\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/featured_article\",\"extension\\/module\\/featured_product\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/manager\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/trade_import\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/ocstore_w1\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/webmoney_wmb\",\"extension\\/payment\\/webmoney_wme\",\"extension\\/payment\\/webmoney_wmk\",\"extension\\/payment\\/webmoney_wmr\",\"extension\\/payment\\/webmoney_wmu\",\"extension\\/payment\\/webmoney_wmv\",\"extension\\/payment\\/webmoney_wmz\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"marketplace\\/opencartforum\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"search\\/search\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"]}');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher`
--

CREATE TABLE `oc_voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher_history`
--

CREATE TABLE `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher_theme`
--

CREATE TABLE `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher_theme_description`
--

CREATE TABLE `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT 0.00000000
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Килограммы', 'кг'),
(1, 2, 'Kilogram', 'kg'),
(2, 1, 'Граммы', 'г'),
(2, 2, 'Gram', 'g'),
(5, 1, 'Фунты', 'lb'),
(5, 2, 'Pound', 'lb'),
(6, 1, 'Унции', 'oz'),
(6, 2, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_zone`
--

CREATE TABLE `oc_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M\'Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu\'a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Арагацотн', 'AGT', 1),
(181, 11, 'Арарат', 'ARR', 1),
(182, 11, 'Армавир', 'ARM', 1),
(183, 11, 'Гегаркуник', 'GEG', 1),
(184, 11, 'Котайк', 'KOT', 1),
(185, 11, 'Лори', 'LOR', 1),
(186, 11, 'Ширак', 'SHI', 1),
(187, 11, 'Сюник', 'SYU', 1),
(188, 11, 'Тавуш', 'TAV', 1),
(189, 11, 'Вайоц Дзор', 'VAY', 1),
(190, 11, 'Ереван', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'Kärnten', 'KAR', 1),
(201, 14, 'Nieder&ouml;sterreich', 'NOS', 1),
(202, 14, 'Ober&ouml;sterreich', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore\'s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Брест', 'BR', 1),
(338, 20, 'Гомель', 'HO', 1),
(339, 20, 'Минск', 'HM', 1),
(340, 20, 'Гродно', 'HR', 1),
(341, 20, 'Могилев', 'MA', 1),
(342, 20, 'Минская область', 'MI', 1),
(343, 20, 'Витебск', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liège', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George\'s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith\'s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapá', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceará', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espírito Santo', 'ES', 1),
(448, 30, 'Goiás', 'GO', 1),
(449, 30, 'Maranhão', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Pará', 'PA', 1),
(454, 30, 'Paraíba', 'PB', 1),
(455, 30, 'Paraná', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piauí', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondônia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'São Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M\'Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O\'Higgins', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chilena', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovarsko-bilogorska', 'BB', 1),
(848, 53, 'Grad Zagreb', 'GZ', 1),
(849, 53, 'Dubrovačko-neretvanska', 'DN', 1),
(850, 53, 'Istarska', 'IS', 1),
(851, 53, 'Karlovačka', 'KA', 1),
(852, 53, 'Koprivničko-križevačka', 'KK', 1),
(853, 53, 'Krapinsko-zagorska', 'KZ', 1),
(854, 53, 'Ličko-senjska', 'LS', 1),
(855, 53, 'Međimurska', 'ME', 1),
(856, 53, 'Osječko-baranjska', 'OB', 1),
(857, 53, 'Požeško-slavonska', 'PS', 1),
(858, 53, 'Primorsko-goranska', 'PG', 1),
(859, 53, 'Šibensko-kninska', 'SK', 1),
(860, 53, 'Sisačko-moslavačka', 'SM', 1),
(861, 53, 'Brodsko-posavska', 'BP', 1),
(862, 53, 'Splitsko-dalmatinska', 'SD', 1),
(863, 53, 'Varaždinska', 'VA', 1),
(864, 53, 'Virovitičko-podravska', 'VP', 1),
(865, 53, 'Vukovarsko-srijemska', 'VS', 1),
(866, 53, 'Zadarska', 'ZA', 1),
(867, 53, 'Zagrebačka', 'ZG', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '\'Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma\'iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa\'id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina\'', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina\'', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan lääni', 'AL', 1),
(1086, 72, 'Etelä-Suomen lääni', 'ES', 1),
(1087, 72, 'Itä-Suomen lääni', 'IS', 1),
(1088, 72, 'Länsi-Suomen lääni', 'LS', 1),
(1089, 72, 'Lapin lääni', 'LA', 1),
(1090, 72, 'Oulun lääni', 'OU', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d\'Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-W&uuml;rttemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Th&uuml;ringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand\'Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Puducherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'BoDeTaBek', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta Raya', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1),
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta\'mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1),
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be\'er Sheva', 'BS', 1),
(1613, 104, 'Bika\'at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '\'Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al \'Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa\'', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa\'', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma\'an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Алматинская область', 'AL', 1),
(1717, 109, 'Алматы - город республ-го значения', 'AC', 1),
(1718, 109, 'Акмолинская область', 'AM', 1),
(1719, 109, 'Актюбинская область', 'AQ', 1),
(1720, 109, 'Астана - город республ-го значения', 'AS', 1),
(1721, 109, 'Атырауская область', 'AT', 1),
(1722, 109, 'Западно-Казахстанская область', 'BA', 1),
(1723, 109, 'Байконур - город республ-го значения', 'BY', 1),
(1724, 109, 'Мангистауская область', 'MA', 1),
(1725, 109, 'Южно-Казахстанская область', 'ON', 1),
(1726, 109, 'Павлодарская область', 'PA', 1),
(1727, 109, 'Карагандинская область', 'QA', 1),
(1728, 109, 'Костанайская область', 'QO', 1),
(1729, 109, 'Кызылординская область', 'QY', 1),
(1730, 109, 'Восточно-Казахстанская область', 'SH', 1),
(1731, 109, 'Северо-Казахстанская область', 'SO', 1),
(1732, 109, 'Жамбылская область', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P\'yongan-bukto', 'PYB', 1),
(1769, 112, 'P\'yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P\'yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch\'ungch\'ong-bukto', 'CO', 1),
(1774, 113, 'Ch\'ungch\'ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch\'on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t\'ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al \'Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra\'', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale\'s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha\'s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al \'Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati\'', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'MY-01', 1),
(1972, 129, 'Kedah', 'MY-02', 1),
(1973, 129, 'Kelantan', 'MY-03', 1),
(1974, 129, 'Labuan', 'MY-15', 1),
(1975, 129, 'Melaka', 'MY-04', 1),
(1976, 129, 'Negeri Sembilan', 'MY-05', 1),
(1977, 129, 'Pahang', 'MY-06', 1),
(1978, 129, 'Perak', 'MY-08', 1),
(1979, 129, 'Perlis', 'MY-09', 1),
(1980, 129, 'Pulau Pinang', 'MY-07', 1),
(1981, 129, 'Sabah', 'MY-12', 1),
(1982, 129, 'Sarawak', 'MY-13', 1),
(1983, 129, 'Selangor', 'MY-10', 1),
(1984, 129, 'Terengganu', 'MY-11', 1),
(1985, 129, 'Kuala Lumpur', 'MY-14', 1),
(4035, 129, 'Putrajaya', 'MY-16', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord Brabant', 'NB', 1),
(2336, 150, 'Noord Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke\'s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa\'id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Республика Хакасия', 'KK', 1),
(2722, 176, 'Забайкальский край', 'ZAB', 1),
(2723, 176, 'Чукотский АО', 'CHU', 1),
(2724, 176, 'Архангельская область', 'ARK', 1),
(2725, 176, 'Астраханская область', 'AST', 1),
(2726, 176, 'Алтайский край', 'ALT', 1),
(2727, 176, 'Белгородская область', 'BEL', 1),
(2728, 176, 'Еврейская АО', 'YEV', 1),
(2729, 176, 'Амурская область', 'AMU', 1),
(2730, 176, 'Брянская область', 'BRY', 1),
(2731, 176, 'Чувашская Республика', 'CU', 1),
(2732, 176, 'Челябинская область', 'CHE', 1),
(2733, 176, 'Карачаево-Черкесия', 'KC', 1),
(2735, 176, 'Таймырский АО', 'TDN', 1),
(2736, 176, 'Республика Калмыкия', 'KL', 1),
(2738, 176, 'Республика Алтай', 'AL', 1),
(2739, 176, 'Чеченская Республика', 'CE', 1),
(2740, 176, 'Иркутская область', 'IRK', 1),
(2741, 176, 'Ивановская область', 'IVA', 1),
(2742, 176, 'Удмуртская Республика', 'UD', 1),
(2743, 176, 'Калининградская область', 'KGD', 1),
(2744, 176, 'Калужская область', 'KLU', 1),
(2745, 176, 'Краснодарский край', 'KDA', 1),
(2746, 176, 'Республика Татарстан', 'TA', 1),
(2747, 176, 'Кемеровская область', 'KEM', 1),
(2748, 176, 'Хабаровский край', 'KHA', 1),
(2749, 176, 'Ханты-Мансийский АО - Югра', 'KHM', 1),
(2750, 176, 'Костромская область', 'KOS', 1),
(2751, 176, 'Московская область', 'MOS', 1),
(2752, 176, 'Красноярский край', 'KYA', 1),
(2753, 176, 'Коми-Пермяцкий АО', 'KOP', 1),
(2754, 176, 'Курганская область', 'KGN', 1),
(2755, 176, 'Курская область', 'KRS', 1),
(2756, 176, 'Республика Тыва', 'TY', 1),
(2757, 176, 'Липецкая область', 'LIP', 1),
(2758, 176, 'Магаданская область', 'MAG', 1),
(2759, 176, 'Республика Дагестан', 'DA', 1),
(2760, 176, 'Республика Адыгея', 'AD', 1),
(2761, 176, 'Москва', 'MOW', 1),
(2762, 176, 'Мурманская область', 'MUR', 1),
(2763, 176, 'Республика Кабардино-Балкария', 'KB', 1),
(2764, 176, 'Ненецкий АО', 'NEN', 1),
(2765, 176, 'Республика Ингушетия', 'IN', 1),
(2766, 176, 'Нижегородская область', 'NIZ', 1),
(2767, 176, 'Новгородская область', 'NGR', 1),
(2768, 176, 'Новосибирская область', 'NVS', 1),
(2769, 176, 'Омская область', 'OMS', 1),
(2770, 176, 'Орловская область', 'ORL', 1),
(2771, 176, 'Оренбургская область', 'ORE', 1),
(2772, 176, 'Корякский АО', 'KOR', 1),
(2773, 176, 'Пензенская область', 'PNZ', 1),
(2774, 176, 'Пермский край', 'PER', 1),
(2775, 176, 'Камчатский край', 'KAM', 1),
(2776, 176, 'Республика Карелия', 'KR', 1),
(2777, 176, 'Псковская область', 'PSK', 1),
(2778, 176, 'Ростовская область', 'ROS', 1),
(2779, 176, 'Рязанская область', 'RYA', 1),
(2780, 176, 'Ямало-Ненецкий АО', 'YAN', 1),
(2781, 176, 'Самарская область', 'SAM', 1),
(2782, 176, 'Республика Мордовия', 'MO', 1),
(2783, 176, 'Саратовская область', 'SAR', 1),
(2784, 176, 'Смоленская область', 'SMO', 1),
(2785, 176, 'Санкт-Петербург', 'SPE', 1),
(2786, 176, 'Ставропольский край', 'STA', 1),
(2787, 176, 'Республика Коми', 'KO', 1),
(2788, 176, 'Тамбовская область', 'TAM', 1),
(2789, 176, 'Томская область', 'TOM', 1),
(2790, 176, 'Тульская область', 'TUL', 1),
(2791, 176, 'Ленинградская область', 'LEN', 1),
(2792, 176, 'Тверская область', 'TVE', 1),
(2793, 176, 'Тюменская область', 'TYU', 1),
(2794, 176, 'Республика Башкортостан', 'BA', 1),
(2795, 176, 'Ульяновская область', 'ULY', 1),
(2796, 176, 'Республика Бурятия', 'BU', 1),
(2798, 176, 'Республика Северная Осетия', 'SE', 1),
(2799, 176, 'Владимирская область', 'VLA', 1),
(2800, 176, 'Приморский край', 'PRI', 1),
(2801, 176, 'Волгоградская область', 'VGG', 1),
(2802, 176, 'Вологодская область', 'VLG', 1),
(2803, 176, 'Воронежская область', 'VOR', 1),
(2804, 176, 'Кировская область', 'KIR', 1),
(2805, 176, 'Республика  Саха / Якутия', 'SA', 1),
(2806, 176, 'Ярославская область', 'YAR', 1),
(2807, 176, 'Свердловская область', 'SVE', 1),
(2808, 176, 'Республика Марий Эл', 'ME', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A\'ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa\'asaleleaga', 'FA', 1),
(2856, 181, 'Gaga\'emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa\'itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va\'a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '\'Asir', 'AS', 1),
(2882, 184, 'Ha\'il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand\' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand\' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3034, 199, 'A\'ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa\'iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa\'iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1),
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'G&auml;vleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'J&auml;mtland', 'Z', 1),
(3080, 203, 'J&ouml;nk&ouml;ping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, '&Ouml;rebro', 'T', 1),
(3085, 203, '&Ouml;sterg&ouml;tland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'S&ouml;dermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'V&auml;rmland', 'S', 1),
(3091, 203, 'V&auml;sterbotten', 'AC', 1),
(3092, 203, 'V&auml;sternorrland', 'Y', 1),
(3093, 203, 'V&auml;stmanland', 'U', 1),
(3094, 203, 'V&auml;stra G&ouml;taland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3104, 204, 'Graub&uuml;nden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Z&uuml;rich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1),
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t\'ou', 'NT', 1),
(3145, 206, 'P\'eng-hu', 'PH', 1),
(3146, 206, 'P\'ing-tung', 'PT', 1),
(3147, 206, 'T\'ai-chung', 'TG', 1),
(3148, 206, 'T\'ai-nan', 'TA', 1),
(3149, 206, 'T\'ai-pei county', 'TP', 1),
(3150, 206, 'T\'ai-tung', 'TT', 1),
(3151, 206, 'T\'ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T\'ai-chung', 'TH', 1),
(3157, 206, 'T\'ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T\'ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha\'apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava\'u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakır', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazığ', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kırıkkale', 'KRK', 1),
(3364, 215, 'Kırklareli', 'KLR', 1),
(3365, 215, 'Kırşehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Черкасская область', '71', 1),
(3481, 220, 'Черниговская область', '74', 1),
(3482, 220, 'Черновицкая область', '77', 1),
(3483, 220, 'Крым', '43', 1),
(3484, 220, 'Днепропетровская область', '12', 1),
(3485, 220, 'Донецкая область', '14', 1),
(3486, 220, 'Ивано-Франковская область', '26', 1),
(3487, 220, 'Херсонская область', '65', 1),
(3488, 220, 'Хмельницкая область', '68', 1),
(3489, 220, 'Кировоградская область', '35', 1),
(3490, 220, 'Киев', '30', 1),
(3491, 220, 'Киевская область', '32', 1),
(3492, 220, 'Луганская область', '09', 1),
(3493, 220, 'Львовская область', '46', 1),
(3494, 220, 'Николаевская область', '48', 1),
(3495, 220, 'Одесская область', '51', 1),
(3496, 220, 'Полтавская область', '53', 1),
(3497, 220, 'Ровненская область', '56', 1),
(3498, 220, 'Севастополь', '40', 1),
(3499, 220, 'Сумская область', '59', 1),
(3500, 220, 'Тернопольская область', '61', 1),
(3501, 220, 'Винницкая область', '05', 1),
(3502, 220, 'Волынская область', '07', 1),
(3503, 220, 'Закарпатская область', '21', 1),
(3504, 220, 'Запорожская область', '23', 1),
(3505, 220, 'Житомирская область', '18', 1),
(3506, 221, 'Abu Zaby', 'AZ', 1),
(3507, 221, '\'Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubai', 'DU', 1),
(3511, 221, 'R\'as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg\'ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog\'iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma\'rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa\'dah', 'SD', 1),
(3805, 235, 'San\'a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta\'izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3862, 105, 'Carbonia-Iglesias', 'CI', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3895, 105, 'Medio Campidano', 'VS', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3902, 105, 'Ogliastra', 'OG', 1),
(3903, 105, 'Olbia-Tempio', 'OT', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3973, 242, 'Andrijevica', '01', 1),
(3974, 242, 'Bar', '02', 1),
(3975, 242, 'Berane', '03', 1),
(3976, 242, 'Bijelo Polje', '04', 1),
(3977, 242, 'Budva', '05', 1),
(3978, 242, 'Cetinje', '06', 1),
(3979, 242, 'Danilovgrad', '07', 1),
(3980, 242, 'Herceg-Novi', '08', 1),
(3981, 242, 'Kolašin', '09', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nikšić', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plužine', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Rožaje', '17', 1),
(3990, 242, 'Šavnik', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, 'Žabljak', '21', 1),
(3994, 243, 'Belgrade', '00', 1),
(3995, 243, 'North Bačka', '01', 1),
(3996, 243, 'Central Banat', '02', 1),
(3997, 243, 'North Banat', '03', 1),
(3998, 243, 'South Banat', '04', 1),
(3999, 243, 'West Bačka', '05', 1),
(4000, 243, 'South Bačka', '06', 1),
(4001, 243, 'Srem', '07', 1),
(4002, 243, 'Mačva', '08', 1),
(4003, 243, 'Kolubara', '09', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Braničevo', '11', 1),
(4006, 243, 'Šumadija', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaječar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Raška', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Nišava', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'Pčinja', '24', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1),
(4036, 117, 'Ainaži, Salacgrīvas novads', '0661405', 1),
(4037, 117, 'Aizkraukle, Aizkraukles novads', '0320201', 1),
(4038, 117, 'Aizkraukles novads', '0320200', 1),
(4039, 117, 'Aizpute, Aizputes novads', '0640605', 1),
(4040, 117, 'Aizputes novads', '0640600', 1),
(4041, 117, 'Aknīste, Aknīstes novads', '0560805', 1),
(4042, 117, 'Aknīstes novads', '0560800', 1),
(4043, 117, 'Aloja, Alojas novads', '0661007', 1),
(4044, 117, 'Alojas novads', '0661000', 1),
(4045, 117, 'Alsungas novads', '0624200', 1),
(4046, 117, 'Alūksne, Alūksnes novads', '0360201', 1),
(4047, 117, 'Alūksnes novads', '0360200', 1),
(4048, 117, 'Amatas novads', '0424701', 1),
(4049, 117, 'Ape, Apes novads', '0360805', 1),
(4050, 117, 'Apes novads', '0360800', 1),
(4051, 117, 'Auce, Auces novads', '0460805', 1),
(4052, 117, 'Auces novads', '0460800', 1),
(4053, 117, 'Ādažu novads', '0804400', 1),
(4054, 117, 'Babītes novads', '0804900', 1),
(4055, 117, 'Baldone, Baldones novads', '0800605', 1),
(4056, 117, 'Baldones novads', '0800600', 1),
(4057, 117, 'Baloži, Ķekavas novads', '0800807', 1),
(4058, 117, 'Baltinavas novads', '0384400', 1),
(4059, 117, 'Balvi, Balvu novads', '0380201', 1),
(4060, 117, 'Balvu novads', '0380200', 1),
(4061, 117, 'Bauska, Bauskas novads', '0400201', 1),
(4062, 117, 'Bauskas novads', '0400200', 1),
(4063, 117, 'Beverīnas novads', '0964700', 1),
(4064, 117, 'Brocēni, Brocēnu novads', '0840605', 1),
(4065, 117, 'Brocēnu novads', '0840601', 1),
(4066, 117, 'Burtnieku novads', '0967101', 1),
(4067, 117, 'Carnikavas novads', '0805200', 1),
(4068, 117, 'Cesvaine, Cesvaines novads', '0700807', 1),
(4069, 117, 'Cesvaines novads', '0700800', 1),
(4070, 117, 'Cēsis, Cēsu novads', '0420201', 1),
(4071, 117, 'Cēsu novads', '0420200', 1),
(4072, 117, 'Ciblas novads', '0684901', 1),
(4073, 117, 'Dagda, Dagdas novads', '0601009', 1),
(4074, 117, 'Dagdas novads', '0601000', 1),
(4075, 117, 'Daugavpils', '0050000', 1),
(4076, 117, 'Daugavpils novads', '0440200', 1),
(4077, 117, 'Dobele, Dobeles novads', '0460201', 1),
(4078, 117, 'Dobeles novads', '0460200', 1),
(4079, 117, 'Dundagas novads', '0885100', 1),
(4080, 117, 'Durbe, Durbes novads', '0640807', 1),
(4081, 117, 'Durbes novads', '0640801', 1),
(4082, 117, 'Engures novads', '0905100', 1),
(4083, 117, 'Ērgļu novads', '0705500', 1),
(4084, 117, 'Garkalnes novads', '0806000', 1),
(4085, 117, 'Grobiņa, Grobiņas novads', '0641009', 1),
(4086, 117, 'Grobiņas novads', '0641000', 1),
(4087, 117, 'Gulbene, Gulbenes novads', '0500201', 1),
(4088, 117, 'Gulbenes novads', '0500200', 1),
(4089, 117, 'Iecavas novads', '0406400', 1),
(4090, 117, 'Ikšķile, Ikšķiles novads', '0740605', 1),
(4091, 117, 'Ikšķiles novads', '0740600', 1),
(4092, 117, 'Ilūkste, Ilūkstes novads', '0440807', 1),
(4093, 117, 'Ilūkstes novads', '0440801', 1),
(4094, 117, 'Inčukalna novads', '0801800', 1),
(4095, 117, 'Jaunjelgava, Jaunjelgavas novads', '0321007', 1),
(4096, 117, 'Jaunjelgavas novads', '0321000', 1),
(4097, 117, 'Jaunpiebalgas novads', '0425700', 1),
(4098, 117, 'Jaunpils novads', '0905700', 1),
(4099, 117, 'Jelgava', '0090000', 1),
(4100, 117, 'Jelgavas novads', '0540200', 1),
(4101, 117, 'Jēkabpils', '0110000', 1),
(4102, 117, 'Jēkabpils novads', '0560200', 1),
(4103, 117, 'Jūrmala', '0130000', 1),
(4104, 117, 'Kalnciems, Jelgavas novads', '0540211', 1),
(4105, 117, 'Kandava, Kandavas novads', '0901211', 1),
(4106, 117, 'Kandavas novads', '0901201', 1),
(4107, 117, 'Kārsava, Kārsavas novads', '0681009', 1),
(4108, 117, 'Kārsavas novads', '0681000', 1),
(4109, 117, 'Kocēnu novads ,bij. Valmieras)', '0960200', 1),
(4110, 117, 'Kokneses novads', '0326100', 1),
(4111, 117, 'Krāslava, Krāslavas novads', '0600201', 1),
(4112, 117, 'Krāslavas novads', '0600202', 1),
(4113, 117, 'Krimuldas novads', '0806900', 1),
(4114, 117, 'Krustpils novads', '0566900', 1),
(4115, 117, 'Kuldīga, Kuldīgas novads', '0620201', 1),
(4116, 117, 'Kuldīgas novads', '0620200', 1),
(4117, 117, 'Ķeguma novads', '0741001', 1),
(4118, 117, 'Ķegums, Ķeguma novads', '0741009', 1),
(4119, 117, 'Ķekavas novads', '0800800', 1),
(4120, 117, 'Lielvārde, Lielvārdes novads', '0741413', 1),
(4121, 117, 'Lielvārdes novads', '0741401', 1),
(4122, 117, 'Liepāja', '0170000', 1),
(4123, 117, 'Limbaži, Limbažu novads', '0660201', 1),
(4124, 117, 'Limbažu novads', '0660200', 1),
(4125, 117, 'Līgatne, Līgatnes novads', '0421211', 1),
(4126, 117, 'Līgatnes novads', '0421200', 1),
(4127, 117, 'Līvāni, Līvānu novads', '0761211', 1),
(4128, 117, 'Līvānu novads', '0761201', 1),
(4129, 117, 'Lubāna, Lubānas novads', '0701413', 1),
(4130, 117, 'Lubānas novads', '0701400', 1),
(4131, 117, 'Ludza, Ludzas novads', '0680201', 1),
(4132, 117, 'Ludzas novads', '0680200', 1),
(4133, 117, 'Madona, Madonas novads', '0700201', 1),
(4134, 117, 'Madonas novads', '0700200', 1),
(4135, 117, 'Mazsalaca, Mazsalacas novads', '0961011', 1),
(4136, 117, 'Mazsalacas novads', '0961000', 1),
(4137, 117, 'Mālpils novads', '0807400', 1),
(4138, 117, 'Mārupes novads', '0807600', 1),
(4139, 117, 'Mērsraga novads', '0887600', 1),
(4140, 117, 'Naukšēnu novads', '0967300', 1),
(4141, 117, 'Neretas novads', '0327100', 1),
(4142, 117, 'Nīcas novads', '0647900', 1),
(4143, 117, 'Ogre, Ogres novads', '0740201', 1),
(4144, 117, 'Ogres novads', '0740202', 1),
(4145, 117, 'Olaine, Olaines novads', '0801009', 1),
(4146, 117, 'Olaines novads', '0801000', 1),
(4147, 117, 'Ozolnieku novads', '0546701', 1),
(4148, 117, 'Pārgaujas novads', '0427500', 1),
(4149, 117, 'Pāvilosta, Pāvilostas novads', '0641413', 1),
(4150, 117, 'Pāvilostas novads', '0641401', 1),
(4151, 117, 'Piltene, Ventspils novads', '0980213', 1),
(4152, 117, 'Pļaviņas, Pļaviņu novads', '0321413', 1),
(4153, 117, 'Pļaviņu novads', '0321400', 1),
(4154, 117, 'Preiļi, Preiļu novads', '0760201', 1),
(4155, 117, 'Preiļu novads', '0760202', 1),
(4156, 117, 'Priekule, Priekules novads', '0641615', 1),
(4157, 117, 'Priekules novads', '0641600', 1),
(4158, 117, 'Priekuļu novads', '0427300', 1),
(4159, 117, 'Raunas novads', '0427700', 1),
(4160, 117, 'Rēzekne', '0210000', 1),
(4161, 117, 'Rēzeknes novads', '0780200', 1),
(4162, 117, 'Riebiņu novads', '0766300', 1),
(4163, 117, 'Rīga', '0010000', 1),
(4164, 117, 'Rojas novads', '0888300', 1),
(4165, 117, 'Ropažu novads', '0808400', 1),
(4166, 117, 'Rucavas novads', '0648500', 1),
(4167, 117, 'Rugāju novads', '0387500', 1),
(4168, 117, 'Rundāles novads', '0407700', 1),
(4169, 117, 'Rūjiena, Rūjienas novads', '0961615', 1),
(4170, 117, 'Rūjienas novads', '0961600', 1),
(4171, 117, 'Sabile, Talsu novads', '0880213', 1),
(4172, 117, 'Salacgrīva, Salacgrīvas novads', '0661415', 1),
(4173, 117, 'Salacgrīvas novads', '0661400', 1),
(4174, 117, 'Salas novads', '0568700', 1),
(4175, 117, 'Salaspils novads', '0801200', 1),
(4176, 117, 'Salaspils, Salaspils novads', '0801211', 1),
(4177, 117, 'Saldus novads', '0840200', 1),
(4178, 117, 'Saldus, Saldus novads', '0840201', 1),
(4179, 117, 'Saulkrasti, Saulkrastu novads', '0801413', 1),
(4180, 117, 'Saulkrastu novads', '0801400', 1),
(4181, 117, 'Seda, Strenču novads', '0941813', 1),
(4182, 117, 'Sējas novads', '0809200', 1),
(4183, 117, 'Sigulda, Siguldas novads', '0801615', 1),
(4184, 117, 'Siguldas novads', '0801601', 1),
(4185, 117, 'Skrīveru novads', '0328200', 1),
(4186, 117, 'Skrunda, Skrundas novads', '0621209', 1),
(4187, 117, 'Skrundas novads', '0621200', 1),
(4188, 117, 'Smiltene, Smiltenes novads', '0941615', 1),
(4189, 117, 'Smiltenes novads', '0941600', 1),
(4190, 117, 'Staicele, Alojas novads', '0661017', 1),
(4191, 117, 'Stende, Talsu novads', '0880215', 1),
(4192, 117, 'Stopiņu novads', '0809600', 1),
(4193, 117, 'Strenči, Strenču novads', '0941817', 1),
(4194, 117, 'Strenču novads', '0941800', 1),
(4195, 117, 'Subate, Ilūkstes novads', '0440815', 1),
(4196, 117, 'Talsi, Talsu novads', '0880201', 1),
(4197, 117, 'Talsu novads', '0880200', 1),
(4198, 117, 'Tērvetes novads', '0468900', 1),
(4199, 117, 'Tukuma novads', '0900200', 1),
(4200, 117, 'Tukums, Tukuma novads', '0900201', 1),
(4201, 117, 'Vaiņodes novads', '0649300', 1),
(4202, 117, 'Valdemārpils, Talsu novads', '0880217', 1),
(4203, 117, 'Valka, Valkas novads', '0940201', 1),
(4204, 117, 'Valkas novads', '0940200', 1),
(4205, 117, 'Valmiera', '0250000', 1),
(4206, 117, 'Vangaži, Inčukalna novads', '0801817', 1),
(4207, 117, 'Varakļāni, Varakļānu novads', '0701817', 1),
(4208, 117, 'Varakļānu novads', '0701800', 1),
(4209, 117, 'Vārkavas novads', '0769101', 1),
(4210, 117, 'Vecpiebalgas novads', '0429300', 1),
(4211, 117, 'Vecumnieku novads', '0409500', 1),
(4212, 117, 'Ventspils', '0270000', 1),
(4213, 117, 'Ventspils novads', '0980200', 1),
(4214, 117, 'Viesīte, Viesītes novads', '0561815', 1),
(4215, 117, 'Viesītes novads', '0561800', 1),
(4216, 117, 'Viļaka, Viļakas novads', '0381615', 1),
(4217, 117, 'Viļakas novads', '0381600', 1),
(4218, 117, 'Viļāni, Viļānu novads', '0781817', 1),
(4219, 117, 'Viļānu novads', '0781800', 1),
(4220, 117, 'Zilupe, Zilupes novads', '0681817', 1),
(4221, 117, 'Zilupes novads', '0681801', 1),
(4222, 43, 'Arica y Parinacota', 'AP', 1),
(4223, 43, 'Los Rios', 'LR', 1),
(4224, 220, 'Харьковская область', '63', 1),
(4225, 118, 'Beirut', 'LB-BR', 1),
(4226, 118, 'Bekaa', 'LB-BE', 1),
(4227, 118, 'Mount Lebanon', 'LB-ML', 1),
(4228, 118, 'Nabatieh', 'LB-NB', 1),
(4229, 118, 'North', 'LB-NR', 1),
(4230, 118, 'South', 'LB-ST', 1),
(4231, 99, 'Telangana', 'TS', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_zone_to_geo_zone`
--

CREATE TABLE `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT 0,
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(1, 222, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `oc_address`
--
ALTER TABLE `oc_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Индексы таблицы `oc_api`
--
ALTER TABLE `oc_api`
  ADD PRIMARY KEY (`api_id`);

--
-- Индексы таблицы `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Индексы таблицы `oc_api_session`
--
ALTER TABLE `oc_api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Индексы таблицы `oc_article`
--
ALTER TABLE `oc_article`
  ADD PRIMARY KEY (`article_id`);

--
-- Индексы таблицы `oc_article_description`
--
ALTER TABLE `oc_article_description`
  ADD PRIMARY KEY (`article_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_article_image`
--
ALTER TABLE `oc_article_image`
  ADD PRIMARY KEY (`article_image_id`);

--
-- Индексы таблицы `oc_article_related`
--
ALTER TABLE `oc_article_related`
  ADD PRIMARY KEY (`article_id`,`related_id`);

--
-- Индексы таблицы `oc_article_related_mn`
--
ALTER TABLE `oc_article_related_mn`
  ADD PRIMARY KEY (`article_id`,`manufacturer_id`);

--
-- Индексы таблицы `oc_article_related_product`
--
ALTER TABLE `oc_article_related_product`
  ADD PRIMARY KEY (`article_id`,`product_id`);

--
-- Индексы таблицы `oc_article_related_wb`
--
ALTER TABLE `oc_article_related_wb`
  ADD PRIMARY KEY (`article_id`,`category_id`);

--
-- Индексы таблицы `oc_article_to_blog_category`
--
ALTER TABLE `oc_article_to_blog_category`
  ADD PRIMARY KEY (`article_id`,`blog_category_id`);

--
-- Индексы таблицы `oc_article_to_download`
--
ALTER TABLE `oc_article_to_download`
  ADD PRIMARY KEY (`article_id`,`download_id`);

--
-- Индексы таблицы `oc_article_to_layout`
--
ALTER TABLE `oc_article_to_layout`
  ADD PRIMARY KEY (`article_id`,`store_id`);

--
-- Индексы таблицы `oc_article_to_store`
--
ALTER TABLE `oc_article_to_store`
  ADD PRIMARY KEY (`article_id`,`store_id`);

--
-- Индексы таблицы `oc_attribute`
--
ALTER TABLE `oc_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Индексы таблицы `oc_attribute_description`
--
ALTER TABLE `oc_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Индексы таблицы `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Индексы таблицы `oc_attribute_group_description`
--
ALTER TABLE `oc_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Индексы таблицы `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Индексы таблицы `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Индексы таблицы `oc_blog_category`
--
ALTER TABLE `oc_blog_category`
  ADD PRIMARY KEY (`blog_category_id`);

--
-- Индексы таблицы `oc_blog_category_description`
--
ALTER TABLE `oc_blog_category_description`
  ADD PRIMARY KEY (`blog_category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_blog_category_path`
--
ALTER TABLE `oc_blog_category_path`
  ADD PRIMARY KEY (`blog_category_id`,`path_id`);

--
-- Индексы таблицы `oc_blog_category_to_layout`
--
ALTER TABLE `oc_blog_category_to_layout`
  ADD PRIMARY KEY (`blog_category_id`,`store_id`);

--
-- Индексы таблицы `oc_blog_category_to_store`
--
ALTER TABLE `oc_blog_category_to_store`
  ADD PRIMARY KEY (`blog_category_id`,`store_id`);

--
-- Индексы таблицы `oc_cart`
--
ALTER TABLE `oc_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`);

--
-- Индексы таблицы `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_category_filter`
--
ALTER TABLE `oc_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Индексы таблицы `oc_category_path`
--
ALTER TABLE `oc_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Индексы таблицы `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Индексы таблицы `oc_category_to_store`
--
ALTER TABLE `oc_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Индексы таблицы `oc_country`
--
ALTER TABLE `oc_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Индексы таблицы `oc_coupon`
--
ALTER TABLE `oc_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Индексы таблицы `oc_coupon_category`
--
ALTER TABLE `oc_coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Индексы таблицы `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Индексы таблицы `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Индексы таблицы `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Индексы таблицы `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Индексы таблицы `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  ADD PRIMARY KEY (`customer_activity_id`);

--
-- Индексы таблицы `oc_customer_affiliate`
--
ALTER TABLE `oc_customer_affiliate`
  ADD PRIMARY KEY (`customer_id`);

--
-- Индексы таблицы `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  ADD PRIMARY KEY (`customer_approval_id`);

--
-- Индексы таблицы `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Индексы таблицы `oc_customer_group_description`
--
ALTER TABLE `oc_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Индексы таблицы `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Индексы таблицы `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Индексы таблицы `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Индексы таблицы `oc_customer_online`
--
ALTER TABLE `oc_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Индексы таблицы `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Индексы таблицы `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  ADD PRIMARY KEY (`customer_search_id`);

--
-- Индексы таблицы `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Индексы таблицы `oc_customer_wishlist`
--
ALTER TABLE `oc_customer_wishlist`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Индексы таблицы `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Индексы таблицы `oc_custom_field_customer_group`
--
ALTER TABLE `oc_custom_field_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Индексы таблицы `oc_custom_field_description`
--
ALTER TABLE `oc_custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Индексы таблицы `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Индексы таблицы `oc_custom_field_value_description`
--
ALTER TABLE `oc_custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Индексы таблицы `oc_download`
--
ALTER TABLE `oc_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Индексы таблицы `oc_download_description`
--
ALTER TABLE `oc_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Индексы таблицы `oc_event`
--
ALTER TABLE `oc_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Индексы таблицы `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Индексы таблицы `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  ADD PRIMARY KEY (`extension_install_id`);

--
-- Индексы таблицы `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  ADD PRIMARY KEY (`extension_path_id`);

--
-- Индексы таблицы `oc_filter`
--
ALTER TABLE `oc_filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Индексы таблицы `oc_filter_description`
--
ALTER TABLE `oc_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Индексы таблицы `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Индексы таблицы `oc_filter_group_description`
--
ALTER TABLE `oc_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Индексы таблицы `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Индексы таблицы `oc_information`
--
ALTER TABLE `oc_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Индексы таблицы `oc_information_description`
--
ALTER TABLE `oc_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Индексы таблицы `oc_information_to_layout`
--
ALTER TABLE `oc_information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Индексы таблицы `oc_information_to_store`
--
ALTER TABLE `oc_information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Индексы таблицы `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_layout`
--
ALTER TABLE `oc_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Индексы таблицы `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Индексы таблицы `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Индексы таблицы `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Индексы таблицы `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Индексы таблицы `oc_location`
--
ALTER TABLE `oc_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Индексы таблицы `oc_manufacturer_to_layout`
--
ALTER TABLE `oc_manufacturer_to_layout`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Индексы таблицы `oc_manufacturer_to_store`
--
ALTER TABLE `oc_manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Индексы таблицы `oc_marketing`
--
ALTER TABLE `oc_marketing`
  ADD PRIMARY KEY (`marketing_id`);

--
-- Индексы таблицы `oc_modification`
--
ALTER TABLE `oc_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Индексы таблицы `oc_module`
--
ALTER TABLE `oc_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Индексы таблицы `oc_option`
--
ALTER TABLE `oc_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Индексы таблицы `oc_option_description`
--
ALTER TABLE `oc_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Индексы таблицы `oc_option_value`
--
ALTER TABLE `oc_option_value`
  ADD PRIMARY KEY (`option_value_id`);

--
-- Индексы таблицы `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`);

--
-- Индексы таблицы `oc_order`
--
ALTER TABLE `oc_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Индексы таблицы `oc_order_history`
--
ALTER TABLE `oc_order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Индексы таблицы `oc_order_option`
--
ALTER TABLE `oc_order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Индексы таблицы `oc_order_product`
--
ALTER TABLE `oc_order_product`
  ADD PRIMARY KEY (`order_product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Индексы таблицы `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Индексы таблицы `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  ADD PRIMARY KEY (`order_shipment_id`);

--
-- Индексы таблицы `oc_order_status`
--
ALTER TABLE `oc_order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Индексы таблицы `oc_order_total`
--
ALTER TABLE `oc_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Индексы таблицы `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Индексы таблицы `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Индексы таблицы `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_product_filter`
--
ALTER TABLE `oc_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Индексы таблицы `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_product_option`
--
ALTER TABLE `oc_product_option`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Индексы таблицы `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);

--
-- Индексы таблицы `oc_product_recurring`
--
ALTER TABLE `oc_product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Индексы таблицы `oc_product_related`
--
ALTER TABLE `oc_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Индексы таблицы `oc_product_related_article`
--
ALTER TABLE `oc_product_related_article`
  ADD PRIMARY KEY (`article_id`,`product_id`);

--
-- Индексы таблицы `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Индексы таблицы `oc_product_special`
--
ALTER TABLE `oc_product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `oc_product_to_download`
--
ALTER TABLE `oc_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Индексы таблицы `oc_product_to_layout`
--
ALTER TABLE `oc_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Индексы таблицы `oc_product_to_store`
--
ALTER TABLE `oc_product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Индексы таблицы `oc_recurring`
--
ALTER TABLE `oc_recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Индексы таблицы `oc_recurring_description`
--
ALTER TABLE `oc_recurring_description`
  ADD PRIMARY KEY (`recurring_id`,`language_id`);

--
-- Индексы таблицы `oc_return`
--
ALTER TABLE `oc_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Индексы таблицы `oc_return_action`
--
ALTER TABLE `oc_return_action`
  ADD PRIMARY KEY (`return_action_id`,`language_id`);

--
-- Индексы таблицы `oc_return_history`
--
ALTER TABLE `oc_return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Индексы таблицы `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  ADD PRIMARY KEY (`return_reason_id`,`language_id`);

--
-- Индексы таблицы `oc_return_status`
--
ALTER TABLE `oc_return_status`
  ADD PRIMARY KEY (`return_status_id`,`language_id`);

--
-- Индексы таблицы `oc_review`
--
ALTER TABLE `oc_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_review_article`
--
ALTER TABLE `oc_review_article`
  ADD PRIMARY KEY (`review_article_id`),
  ADD KEY `article_id` (`article_id`);

--
-- Индексы таблицы `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  ADD PRIMARY KEY (`seo_url_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Индексы таблицы `oc_session`
--
ALTER TABLE `oc_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Индексы таблицы `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Индексы таблицы `oc_shipping_courier`
--
ALTER TABLE `oc_shipping_courier`
  ADD PRIMARY KEY (`shipping_courier_id`);

--
-- Индексы таблицы `oc_statistics`
--
ALTER TABLE `oc_statistics`
  ADD PRIMARY KEY (`statistics_id`);

--
-- Индексы таблицы `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  ADD PRIMARY KEY (`stock_status_id`,`language_id`);

--
-- Индексы таблицы `oc_store`
--
ALTER TABLE `oc_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Индексы таблицы `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Индексы таблицы `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Индексы таблицы `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Индексы таблицы `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Индексы таблицы `oc_theme`
--
ALTER TABLE `oc_theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Индексы таблицы `oc_trade_import`
--
ALTER TABLE `oc_trade_import`
  ADD PRIMARY KEY (`operation_id`);

--
-- Индексы таблицы `oc_trade_import_category_codes`
--
ALTER TABLE `oc_trade_import_category_codes`
  ADD PRIMARY KEY (`group_uuid`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `oc_trade_import_complect`
--
ALTER TABLE `oc_trade_import_complect`
  ADD PRIMARY KEY (`complect_id`);

--
-- Индексы таблицы `oc_trade_import_complect_products`
--
ALTER TABLE `oc_trade_import_complect_products`
  ADD KEY `complect_id` (`complect_id`);

--
-- Индексы таблицы `oc_trade_import_option_codes`
--
ALTER TABLE `oc_trade_import_option_codes`
  ADD PRIMARY KEY (`nomenclature_uuid`),
  ADD KEY `option_id` (`option_id`);

--
-- Индексы таблицы `oc_trade_import_option_value_codes`
--
ALTER TABLE `oc_trade_import_option_value_codes`
  ADD PRIMARY KEY (`characteristic_uuid`),
  ADD KEY `option_value_id` (`option_value_id`);

--
-- Индексы таблицы `oc_trade_import_product_codes`
--
ALTER TABLE `oc_trade_import_product_codes`
  ADD PRIMARY KEY (`nomenclature_uuid`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_trade_orders`
--
ALTER TABLE `oc_trade_orders`
  ADD PRIMARY KEY (`operation_id`);

--
-- Индексы таблицы `oc_translation`
--
ALTER TABLE `oc_translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Индексы таблицы `oc_upload`
--
ALTER TABLE `oc_upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Индексы таблицы `oc_user`
--
ALTER TABLE `oc_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Индексы таблицы `oc_voucher`
--
ALTER TABLE `oc_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Индексы таблицы `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Индексы таблицы `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Индексы таблицы `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Индексы таблицы `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Индексы таблицы `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Индексы таблицы `oc_zone`
--
ALTER TABLE `oc_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Индексы таблицы `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_api`
--
ALTER TABLE `oc_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `oc_api_session`
--
ALTER TABLE `oc_api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `oc_article`
--
ALTER TABLE `oc_article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT для таблицы `oc_article_image`
--
ALTER TABLE `oc_article_image`
  MODIFY `article_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3981;

--
-- AUTO_INCREMENT для таблицы `oc_attribute`
--
ALTER TABLE `oc_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT для таблицы `oc_blog_category`
--
ALTER TABLE `oc_blog_category`
  MODIFY `blog_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT для таблицы `oc_cart`
--
ALTER TABLE `oc_cart`
  MODIFY `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=294;

--
-- AUTO_INCREMENT для таблицы `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT для таблицы `oc_country`
--
ALTER TABLE `oc_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT для таблицы `oc_coupon`
--
ALTER TABLE `oc_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  MODIFY `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  MODIFY `customer_approval_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  MODIFY `customer_search_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_download`
--
ALTER TABLE `oc_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_event`
--
ALTER TABLE `oc_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT для таблицы `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT для таблицы `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  MODIFY `extension_install_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  MODIFY `extension_path_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT для таблицы `oc_filter`
--
ALTER TABLE `oc_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `oc_information`
--
ALTER TABLE `oc_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `oc_layout`
--
ALTER TABLE `oc_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT для таблицы `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT для таблицы `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `oc_location`
--
ALTER TABLE `oc_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `oc_marketing`
--
ALTER TABLE `oc_marketing`
  MODIFY `marketing_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_modification`
--
ALTER TABLE `oc_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `oc_module`
--
ALTER TABLE `oc_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `oc_option`
--
ALTER TABLE `oc_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_option_value`
--
ALTER TABLE `oc_option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order`
--
ALTER TABLE `oc_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT для таблицы `oc_order_history`
--
ALTER TABLE `oc_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT для таблицы `oc_order_option`
--
ALTER TABLE `oc_order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order_product`
--
ALTER TABLE `oc_order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=956;

--
-- AUTO_INCREMENT для таблицы `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  MODIFY `order_shipment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order_status`
--
ALTER TABLE `oc_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `oc_order_total`
--
ALTER TABLE `oc_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT для таблицы `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=366;

--
-- AUTO_INCREMENT для таблицы `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_special`
--
ALTER TABLE `oc_product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_recurring`
--
ALTER TABLE `oc_recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_return`
--
ALTER TABLE `oc_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_return_action`
--
ALTER TABLE `oc_return_action`
  MODIFY `return_action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `oc_return_history`
--
ALTER TABLE `oc_return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  MODIFY `return_reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `oc_return_status`
--
ALTER TABLE `oc_return_status`
  MODIFY `return_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `oc_review`
--
ALTER TABLE `oc_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_review_article`
--
ALTER TABLE `oc_review_article`
  MODIFY `review_article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  MODIFY `seo_url_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2803;

--
-- AUTO_INCREMENT для таблицы `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1164;

--
-- AUTO_INCREMENT для таблицы `oc_statistics`
--
ALTER TABLE `oc_statistics`
  MODIFY `statistics_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  MODIFY `stock_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `oc_store`
--
ALTER TABLE `oc_store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT для таблицы `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT для таблицы `oc_theme`
--
ALTER TABLE `oc_theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_trade_import`
--
ALTER TABLE `oc_trade_import`
  MODIFY `operation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `oc_trade_import_complect`
--
ALTER TABLE `oc_trade_import_complect`
  MODIFY `complect_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `oc_trade_orders`
--
ALTER TABLE `oc_trade_orders`
  MODIFY `operation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `oc_translation`
--
ALTER TABLE `oc_translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_upload`
--
ALTER TABLE `oc_upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_user`
--
ALTER TABLE `oc_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_voucher`
--
ALTER TABLE `oc_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `oc_zone`
--
ALTER TABLE `oc_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4232;

--
-- AUTO_INCREMENT для таблицы `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_category_codes`
--
ALTER TABLE `oc_trade_import_category_codes`
  ADD CONSTRAINT `oc_trade_import_category_codes_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `oc_category` (`category_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_complect_products`
--
ALTER TABLE `oc_trade_import_complect_products`
  ADD CONSTRAINT `oc_trade_import_complect_products_ibfk_1` FOREIGN KEY (`complect_id`) REFERENCES `oc_trade_import_complect` (`complect_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_option_codes`
--
ALTER TABLE `oc_trade_import_option_codes`
  ADD CONSTRAINT `oc_trade_import_option_codes_ibfk_1` FOREIGN KEY (`option_id`) REFERENCES `oc_option` (`option_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_option_value_codes`
--
ALTER TABLE `oc_trade_import_option_value_codes`
  ADD CONSTRAINT `oc_trade_import_option_value_codes_ibfk_1` FOREIGN KEY (`option_value_id`) REFERENCES `oc_option_value` (`option_value_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_product_codes`
--
ALTER TABLE `oc_trade_import_product_codes`
  ADD CONSTRAINT `oc_trade_import_product_codes_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `oc_product` (`product_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
